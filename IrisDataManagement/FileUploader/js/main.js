$(function () {
    'use strict';

    // Initialize the jQuery File Upload widget:
    $('#fileupload').fileupload({
        url: 'upload/upload',
        maxFileSize: 20000000,
    });

    $('#fileupload').bind('fileuploadadded', function (e, data) {
        data.formData = { folder: GetFolderName(), keywords: $('#txtKeywords').val(), date: $('#txtDocDate').val(), level: $('#cmbLevel').val(), type: GetType(), allowdepts: $('#cmbDocDept').val(), allowusers: $('#cmbStaffAllowed').val(), doclocation: $('#txtPhysLocation').val() };

        var fileType = data.files[0].name.split('.').pop(), allowdtypes = 'doc,jpg,png,jpeg,gif,docx,pdf,zip,xls,xlsx,csv,ppt,pptx';
        $.each(data.files, function (index, file) {
            if (allowdtypes.indexOf(fileType.toLowerCase()) < 0) {
                ShowPopup('<b>Invalid File type:</b> Only Word, PDF, Pictures, Excel & Powerpoint allowed');
                $($('#fileupload .files .cancel')[data.context[index].rowIndex]).click();
            }
        });
    })

    $('#fileupload').bind('fileuploadsubmit', function (e, data) {
        data.formData.level = $('#cmbLevel').val();
        data.formData.date = $('#txtDocDate').val();
        if (!data.formData.level) {
            ShowPopup('Please select a privilege level before uploading a file.');
            data.context.find('button').prop('disabled', false);
            input.focus();
            return false;
        }

        if (!data.formData.date) {
            ShowPopup('Please enter when the document date (give an estimate if you cannot confirm this)');
            data.context.find('button').prop('disabled', false);
            input.focus();
            return false;
        }
    });

    $('#fileupload').bind('fileuploaddone', function (e, data) {

    });

    // Enable iframe cross-domain access via redirect option:
    $('#fileupload').fileupload(
        'option',
        'redirect',
        window.location.href.replace(
            /\/[^\/]*$/,
            '/cors/result.html?%s'
        )
    );
});

function GetFolderName() {
    if ($('#IsPublicUpload').is(':checked'))
        return $('#SelectedFolder').val().replace(/\\/g, "++");
    else
        return $('#IrisSelectedFolder').val().replace(/\\/g, "++");
}

function GetType() {
    if ($('#IsPublicUpload').is(':checked'))
        return "NotIris";
    else
        return "Iris";
}