﻿$(function () {
    $('input:radio[name="IsIrisUpload"]').change(
    function () {
        if ($(this).is(':checked')) {
            $('#IsPublicUpload').prop('checked', false);
            $('.publicUpload').hide();
            $('.uploadwell').hide();
            $('#docInfoDiv').hide();
            $('.irisUpload').fadeIn();
            $('#irisUploadDiv').show();

            if ($('#IrisSelectedFolder').val() != '') {
                $('#irisUploadDiv').fadeIn();
                $('.irisUpload').fadeIn();
                $('#docInfoDiv').fadeIn();
                $('.uploadwell').fadeIn();
            }
        }
    });

    $('input:radio[name="IsPublicUpload"]').change(
    function () {
        if ($(this).is(':checked')) {
            $('#IsIrisUpload').prop('checked', false);
            $('.irisUpload').hide();
            $('.uploadwell').hide();
            $('#docInfoDiv').hide();
            $('#irisUploadDiv').hide();
            $('.publicUpload').fadeIn();

            if ($('#SelectedFolder').val() != '') {
                $('.uploadwell').fadeIn();
                $('.irisUpload').fadeIn();
                $('#docInfoDiv').fadeIn();
            }
        }
    });

    $('.newFolderbtn').click(function (e) {
        $('#txtNewFolderName').val('');
        $('#FolderNameDiv').lightbox_me({
            centered: true,
            onLoad: function () {
                $('#foldererr').hide();
                $('#txtNewFolderName').focus();
            }
        });
        $('#FolderNameDiv').appendTo("#MyDocsDiv");
        e.preventDefault();
    });

    $('#createBtn').click(function (e) {
        if ($('#txtNewFolderName').val() != '') {
            $('#foldererr').hide();
            if ($('#CreateDocsDiv').css('display') == 'none') {
                UpdateFolderTree($('#txtNewFolderName').val(), $('#SelectedFolder').val());
            }
            else {
                UpdateCreateFolderTree($('#txtNewFolderName').val(), $('#CreateSelectedFolder').val());
            }
            $('.closebtn').click();
        }
        else {
            $('#foldererr').slideDown();
        }
    });
});


$(document).ready(function () {
    $('#IsIrisUpload').prop('checked', false);
    $('#IsPublicUpload').prop('checked', false);

    $("#cmbLevel, #cmbcreateLevel").select2({
        placeholder: "--",
        minimumResultsForSearch: 8,
        data: $.parseJSON($('#LevelArray').val())
    });

    $("#cmbStaffAllowed, #cmbcreateStaffAllowed").select2({
        placeholder: "No staff selected yet...",
        minimumResultsForSearch: 8,
        multiple: true,
        data: $.parseJSON($('#StaffArray').val())
    });

    $("#cmbDocDept, #cmbCreateDocDept").select2({
        placeholder: "No dept selected yet...",
        minimumResultsForSearch: 8,
        multiple: true,
        data: $.parseJSON($('#DeptArray').val())
    });
});

function UpdateIrisFolderTree(folder, location) {
    $('#irisoutercontainer').fadeOut(function () {
        $('#IrisFolderTreeDiv').addClass("loading");
        $.ajax({
            type: "GET",
            url: "/UpdateIrisFolderTree",
            data: { folder: folder, location: location },
            dataType: "json",
            error: function (result) {
                $('#IrisFolderTreeDiv').removeClass("loading");
                $('#irisoutercontainer').fadeIn();
                ShowPopup("Unable to update folder tree. Please check your internet connection or call an administrator.");
            },
            success: function (result) {
                if (result.success == "1") {
                    CreateTree(1, result.tree);
                    ShowPopup("Folder successfully created.");
                }

                if (result.success == "2")
                    ShowPopup("Could not create folder - Please seek an administrator.");

                if (result.success == "3")
                    ShowPopup(folder + " already exists in the selected folder.");

                if (result.success == "0")
                    ShowPopup("Unable to update folder tree. Please check your internet connection or call an administrator");

                $('#IrisFolderTreeDiv').removeClass("loading");
                $('#irisoutercontainer').fadeIn();
            }
        });
    });
}

function UpdateFolderTree(folder, location) {
    $('#outercontainer').fadeOut(function () {
        $('#folderTreeDiv').addClass("loading");
        $.ajax({
            type: "GET",
            url: "/UpdateFolderTree",
            data: { folder: folder, location: location },
            dataType: "json",
            error: function (result) {
                $('#folderTreeDiv').removeClass("loading");
                $('#outercontainer').fadeIn();
                ShowPopup("Unable to update folder tree. Please check your internet connection or call an administrator.");
            },
            success: function (result) {
                if (result.success == "1") {
                    CreateTree(3, result.tree2);
                    CreateTree(2, result.tree1);
                    ShowPopup("Folder successfully created.");
                }

                if (result.success == "2")
                    ShowPopup("Could not create folder - Please seek an administrator.");

                if (result.success == "3")
                    ShowPopup(folder + " already exists in the selected folder.");

                if (result.success == "0")
                    ShowPopup("Unable to update folder tree. Please check your internet connection or call an administrator");

                $('#folderTreeDiv').removeClass("loading");
                $('#outercontainer').fadeIn();
            }
        });
    });
}

function UpdateCreateFolderTree(folder, location) {
    $('#createoutercontainer').fadeOut(function () {
        $('#createfolderTreeDiv').addClass("loading");
        $.ajax({
            type: "GET",
            url: "/UpdateCreateFolderTree",
            data: { folder: folder, location: location },
            dataType: "json",
            error: function (result) {
                $('#createfolderTreeDiv').removeClass("loading");
                $('#createoutercontainer').fadeIn();
                ShowPopup("Unable to update folder tree. Please check your internet connection or call an administrator.");
            },
            success: function (result) {
                if (result.success == "1") {
                    CreateTree(3, result.tree2);
                    CreateTree(2, result.tree1);
                    ShowPopup("Folder successfully created.");
                }

                if (result.success == "2")
                    ShowPopup("Could not create folder - Please seek an administrator.");

                if (result.success == "3")
                    ShowPopup(folder + " already exists in the selected folder.");

                if (result.success == "0")
                    ShowPopup("Unable to update folder tree. Please check your internet connection or call an administrator");

                $('#createfolderTreeDiv').removeClass("loading");
                $('#createoutercontainer').fadeIn();
            }
        });
    });
}

function ShowPopup(message) {
    $('#headertxt').html(message);
    $('#PopupInfo').lightbox_me({
        centered: true
    });
}

function CreateTree(type, tree) {
    if (type == '1') {
        $('#irisoutercontainer').html('<div id="container">' + tree + '</div');
        $('#iriscontainer').jstree();
        $("#iriscontainer").jstree("open_node", $("#Iris"));
        $('#iriscontainer').on('changed.jstree', function (e, data) {
            $('#IrisSelectedFolder').val(data.selected);
            $('.uploadwell').fadeIn();
        });
    }

    if (type == '2') {
        $('#outercontainer').html('<div id="container">' + tree + '</div');
        $('#container').jstree();
        $("#container").jstree("open_node", $(".jstree-last"));
        $('#container').on('changed.jstree', function (e, data) {
            $('#SelectedFolder').val(data.selected);
            $('#FolderBtn').fadeIn();
            $('.uploadwell').fadeIn();
        });

        $('#FolderBtn').fadeOut();
    }

    if (type == '3') {
        $('#createoutercontainer').html('<div id="createcontainer">' + tree + '</div');
        $('#createcontainer').jstree();
        $('#createcontainer').jstree("refresh");
        $("#createcontainer").jstree("open_node", $(".jstree-last"));
        $('#createcontainer').on('changed.jstree', function (e, data) {
            $('#CreateSelectedFolder').val(data.selected);
            $('#createFolderBtn').fadeIn();
        });

        $('#createFolderBtn').fadeOut();
    }
}