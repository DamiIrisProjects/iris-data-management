﻿var showing = 0;

$(document).ready(function () {
    $('.nibss').click(function (e) {
        if (!$(this).hasClass('active')) {
            $('.active').toggleClass('active inActive');
            $(this).toggleClass('active inActive');
            HideAllDivs();
            $('#nibssdiv').fadeIn();
        }
    });

    $('.passport').click(function (e) {
        if (!$(this).hasClass('active')) {
            $('.active').toggleClass('active inActive');
            $(this).toggleClass('active inActive');
            HideAllDivs();
            $('#passportdiv').fadeIn();
        }
    });

    $('.admin').click(function (e) {
        if (!$(this).hasClass('active')) {
            $('.active').toggleClass('active inActive');
            $(this).toggleClass('active inActive');
            HideAllDivs();
            $('#admindiv').fadeIn();
        }
    });

    $('.general').click(function (e) {
        if (!$(this).hasClass('active')) {
            $('.active').toggleClass('active inActive');
            $(this).toggleClass('active inActive');
            HideAllDivs();
            $('#generaldiv').fadeIn();
        }
    });

    $('.uploadFile').click(function (e) {
        if (!$(this).hasClass('active')) {
            $('.active').toggleClass('active inActive');
            $(this).toggleClass('active inActive');
            $('#NibssReportsDiv').fadeIn();
            $('#NibssMaintenanceDiv').hide();
        }
    });

    $('input:radio[name="IsMaintenance"]').change(
    function () {
        ClearSort();

        if ($(this).is(':checked')) {
            $('#IsReport').prop('checked', false);
            $('#NibssReportsDiv').hide();
            $('#NibssMaintenanceDiv').fadeIn();
        }
    });

    $('input:radio[name="IsReport"]').change(
    function () {
        ClearSort();
        if ($(this).is(':checked')) {
            $('#IsMaintenance').prop('checked', false);
            $('#NibssReportsDiv').hide();
            $('#NibssMaintenanceDiv').hide();
            $('#NibssReportsDiv').fadeIn();
        }
    });
    
    $(document).on('click', '#updatelevelbtn', function () {
        EditClearance($('#selectedid').val());
    });

    $('.maximise').click(function (e) {
        $('.cke_button__maximize').click();
    });

    var popup = $('#popup').val();

    if (popup != "") {
        $('#PopupInfo').lightbox_me({
            centered: true
        });
        $('#popup').val("");
    }

    $('#CloseTagPop').click(function (e) {
        $(this).fadeOut(function () { $(this).trigger('close'); });
    });

    $(".jdpicker").datepicker({
        yearRange: "-20:+0",
        dateFormat: "dd/mm/yy",
        changeMonth: true,
        changeYear: true
    });     

    $("#AdmincmbReportType").select2({
        placeholder: "Select report...",
        minimumResultsForSearch: 8,
        data: $.parseJSON($('#AdminReportsArray').val())
    });

    $("#PassportcmbReportType").select2({
        placeholder: "Select report to print...",
        minimumResultsForSearch: 8,
        data: $.parseJSON($('#PassportReportsArray').val())
    });

    $("#AdmincmbReportType").change(function () {
        showing = 1;
        ClearSearchDivs();
        $("#AdmindocSearchDiv").fadeIn();
        $("#cmbReportType").select2("val", "");
        AdminReportSearch('', '', '');
    });

    $("#GeneralcmbReportType").select2({
        placeholder: "Select report...",
        minimumResultsForSearch: 8,
        data: $.parseJSON($('#GeneralReportsArray').val())
    });

    $("#GeneralcmbReportType").change(function () {
        showing = 2;
        ClearSearchDivs();
        $("#GeneraldocSearchDiv").fadeIn();
        $("#cmbReportType").select2("val", "");
        GeneralReportSearch('', '', '');
    });

    $("#cmbReportType").select2({
        placeholder: "Select report...",
        minimumResultsForSearch: 8,
        data: $.parseJSON($('#ReportsArray').val())
    });

    $("#cmbReportType").change(function () {
        showing = 0;
        ClearSearchDivs();
        $("#docSearchDiv").fadeIn();
        $("#AdmincmbReportType").select2("val", "");
        if ($("#cmbReportType").val() != '6' && $('#specificdiv').css('display') == 'none')
            $('#specificdiv').fadeIn();

        if ($("#cmbReportType").val() == '1') { $('#currentsort').val("Date") }
        if ($("#cmbReportType").val() == '2') { $('#currentsort').val("LoginDate") }
        if ($("#cmbReportType").val() == '3') { $('#currentsort').val("GSM") }
        if ($("#cmbReportType").val() == '4') { $('#currentsort').val("Email") }
        if ($("#cmbReportType").val() == '5') { $('#currentsort').val("ExDate") }
        if ($("#cmbReportType").val() == '6') { $('#currentsort').val("CursorVal"); $('#specificdiv').fadeOut(); ReportSearch('', '', ''); }
        if ($("#cmbReportType").val() == '0') {
            $('#specificdiv').fadeOut(function () {
                ReportSearch('', '', '');
                $(this).fadeIn();
            });
        }

        ClearSort();
    });

    window.setInterval(function () {
        if ($('#AdminreportType').val() == '1') {
            AdminSearch('', '', '', true);
        }

        if ($('#reportType').val() == '0') {
            ReportSearch('', '', '', true);
        }

    }, 60000);

    $('.accMainDiv').css('visibility', 'visible').hide().fadeIn();
});

function HideAllDivs() {
    $('#passportdiv').hide();
    $('#nibssdiv').hide();
    $('#generaldiv').hide();
    $('#admindiv').hide();
}

function ShowSort(count) {
    if (count != 0) {
        if ($(".SearchResults").css('display') == 'none')
            $(".SearchResults").fadeIn();

        if ($("#cmbReportType").val() == '1') {
            $("#PassportSortHeaders").fadeIn();
            $("#PassportSortOptions").fadeIn();
        }

        if ($("#cmbReportType").val() == '2') {
            $("#OperatorLoginHeaders").fadeIn();
            $("#LoginSortOptions").fadeIn();
        }

        if ($("#cmbReportType").val() == '3') {
            $("#MessageHeadersGSM").fadeIn();
            $("#MessageOptionsGSM").fadeIn();
        }

        if ($("#cmbReportType").val() == '4') {
            $("#MessageHeadersEmail").fadeIn();
            $("#MessageOptionsEmail").fadeIn();
        }

        if ($("#cmbReportType").val() == '5') {
            $("#ErrorHeaders").fadeIn();
            $("#ErrorsOptions").fadeIn();
        }

        if ($("#cmbReportType").val() == '6') {
            $("#CursorHeaders").fadeIn();
            $("#CursorsOptions").fadeIn();
        }

        if ($("#AdmincmbReportType").val() == '10') {
            $("#AuditTrailHeaders").fadeIn();
            $("#AuditTrailOptions").fadeIn();            
        }

        if ($("#GeneralcmbReportType").val() == '20') {
            $("#UsersHeaders").fadeIn();
            $("#UsersOptions").fadeIn();
        }
    }    
}

function ClearSearchDivs() {
    $("#AdmindocSearchDiv").hide();
    $("#GeneraldocSearchDiv").hide();
    $("#docSearchDiv").hide(); 
}

function ClearSort() {
    $(".SearchResults").slideUp();

    // normal
    $("#viewReportInner").fadeOut(function () {
        $("#PassportSortHeaders").hide(); 
        $("#OperatorLoginHeaders").hide();
        $("#MessageHeadersGSM").hide();
        $("#MessageHeadersEmail").hide();
        $("#ErrorHeaders").hide();
        $("#CursorHeaders").hide();

        $("#PassportSortOptions").hide();
        $("#LoginSortOptions").hide();
        $("#MessageOptionsGSM").hide();
        $("#MessageOptionsEmail").hide();
        $("#ErrorsOptions").hide();
        $("#CursorsOptions").hide();

        $("#txtOperator").show();
        $("#txtOperatorHyphen").show();

        $("#hasChanged").val("1");
    });

    // Admin
    $("#AdminviewReportInner").fadeOut(function () {
        $("#AuditTrailHeaders").hide();
        $("#AuditTrailOptions").hide();

        $("#AdminhasChanged").val("1");
    });

    // General
    $("#GeneralviewReportInner").fadeOut(function () {
        $("#UsersHeaders").hide();
        $("#UsersOptions").hide();
        $("#GeneralcmbReportType").select2("val", "");

        $("#GeneralhasChanged").val("1");

    });
}

function ShowClearance(level, id) {
    $('#selectedid').val(id);
    $("#cmbLevel").select2({
        placeholder: "--",
        minimumResultsForSearch: 8
    });
    $('#cmbLevel').select2("val", level);
    $('#UpdateStaffLevel').lightbox_me({
        centered: true
    });
}

function EditClearance(id) {
    $('#regloadingdiv').fadeIn(function () {
        $.ajax({
            type: "GET",
            url: "/ChangeStaffAccessLevel",
            data: { pid: id, level: $('#cmbLevel').val() },
            dataType: "json",
            error: function (result) {
                $('#regloadingdiv').fadeOut();
                $('.PopupWindow').fadeOut(function () {
                    $(this).trigger('close');
                    ShowPopup("Could not connect to server. Please ensure you are still connected to the network.");
                });
            
            },
            success: function (result) {
                if (result.status == 0) {
                    ShowPopup("You do not have the rights to change this user's access level.");
                }

                if (result.status == 1) {
                    $('#regloadingdiv').fadeOut();
                    $(this).trigger('close');
                    $('.PopupWindow').fadeOut(function () {                        
                        $('#txtlvl' + id).text($('#cmbLevel').val());
                        ShowPopup("User's access level has been updated.");
                    });
                }

                if (result.status == 2) {
                    window.location.href = '/';
                }
            }
        });
    });
}

function ShowGeneralReport(type) {
    $("#cmbReportType").select2("val", type);

    if (type != 6) {
        $('#specificdiv').fadeIn();
    }

    ReportSearch('', '', '', false);
}

function ShowPassportPreview(id) {
    $('.popupinnerdiv').fadeOut(function () {
        $('#ppinfodiv').lightbox_me({
            centered: true,
            onLoad: function () {
                $('#ppinfodiv').addClass("loading");
                $.ajax({
                    type: "GET",
                    url: "/GetPassportImage",
                    data: { ppid: id },
                    dataType: "json",
                    error: function (result) {
                        $('#ppinfodiv').removeClass("loading");
                        $('#popupinnerdiv').fadeIn();
                        ShowPopup("Could not get document data. Please ensure you are still connected to the network.");
                    },
                    success: function (result) {
                        if (result.status == 0) {
                            $('#ppinfodiv').removeClass("loading");
                            $('#popupinnerdiv').fadeIn();
                            ShowPopup("Passport details for this person is incomplete.");
                        }

                        if (result.status == 1) {
                            $('#ppinfodiv').removeClass("loading");
                            $('.popupinnerdiv').html(result.page);
                            $('.popupinnerdiv').fadeIn();
                        }

                        if (result.status == 2) {
                            window.location.href = '/';
                        }
                    }
                });
            }
        });
    });
}

function ShowStaffPreview(id) {
    $('.Generalpopupinnerdiv').hide();
    $('#Generalppinfodiv').lightbox_me({
        centered: true,
        onLoad: function () {
            $('#Generalppinfodiv').addClass("loading");
            $.ajax({
                type: "GET",
                url: "/GetStaffImage",
                data: { ppid: id },
                dataType: "json",
                error: function (result) {
                    $('#Generalppinfodiv').removeClass("loading");
                    $('#Generalpopupinnerdiv').fadeIn();
                    ShowPopup("Could not get document data. Please ensure you are still connected to the network.");
                },
                success: function (result) {
                    $('#Generalppinfodiv').removeClass("loading");
                    if (result.status == 0) {
                        ShowPopup("An error occurred acquiring this file. Please try again later.");
                    }

                    if (result.status == 1) {
                        $('.Generalpopupinnerdiv').html(result.page);
                        $('.Generalpopupinnerdiv').fadeIn();
                    }

                    if (result.status == 2) {
                        window.location.href = '/';
                    }
                }
            });
        }
    });
}

function AdminReportSearch(orderby, page, cs, isRefresh) {
    var searchtype = $("#AdmincmbReportType").val();

    if (searchtype == '') {
        if (isRefresh === undefined || isRefresh == false)
            ShowPopup("Please select what report you would like to view");
        return false;
    }

    var skip = 0;
    var haschanged = $("#AdminhasChanged").val();
    $("#AdminhasChanged").val("0");
    var from = $('#AdmintxtFromDatex').val();
    var to = $('#AdmintxtToDatex').val();

    var prevp = $('#Adminviewing').val();
    var curr = $('#Admincurrentsort').val();
    var isdesc = $('#Adminisdesc').val().charAt(4);
    if (page == '') {
        page = prevp;
    }

    if (cs === undefined)
        cs = 0;
    if (orderby == '') {
        orderby = curr;
        skip = 1;
    }

    if (isRefresh == true) {
        if ($("#AdmincmbReportType").val() != '' && $('#AdminviewReportInner').css('display') == 'block') {
            $.ajax({
                type: "GET",
                url: "/GetViewReport",
                data: { searchtype: searchtype, from: from, to: to, page: page, pgsize: $('#ddlPageSizeReport').val(), isdesc: isdesc, prevp: prevp, numj: cs, orderby: orderby, sortby: $('#Admincurrentsort').val() },
                dataType: "json",
                error: function (result) {
                    // do nothing
                },
                success: function (result) {
                    if (result.status == 1) {
                        $('#AdminviewReportInner').html(result.page);
                        $('.results').text(result.count + ' entries returned');
                        $('#AdminviewReportInner').fadeIn();
                    }
                }
            });
        }
    }
    else {
        $('#AdminviewReportInner').fadeOut(200, function () {
            $('#AdminReportsDiv').addClass("loading");
            $.ajax({
                type: "GET",
                url: "/GetViewReport",
                data: { searchtype: searchtype, from: from, to: to, page: page, pgsize: $('#ddlPageSizeReport').val(), isdesc: isdesc, prevp: prevp, numj: cs, orderby: orderby, sortby: $('#Admincurrentsort').val() },
                dataType: "json",
                error: function (result) {
                    $('#AdminReportsDiv').removeClass("loading");
                    $('#AdminviewReportInner').fadeIn();
                    ShowPopup("Could not get report data. Please ensure you are still connected to the network.");
                },
                success: function (result) {
                    if (result.status == 1) {

                        if (skip == 0 || haschanged == "1") {
                            $('.selectedSort').toggleClass("selectedSort UnselectedSort");
                            $('#txtAdmin' + orderby).toggleClass("selectedSort UnselectedSort");
                            if (curr == orderby) {
                                if ($('#txtAdmin' + orderby).html() == (AddSpace(curr) + $('<div/>').html(' &#9650;').text())) {
                                    $('#txtAdmin' + orderby).html(AddSpace(curr) + ' &#9660;');
                                    $('#Adminisdesc').val('&#9660;');
                                }
                                else {
                                    $('#txtAdmin' + orderby).html(AddSpace(curr) + ' &#9650;');
                                    $('#Adminisdesc').val('&#9650;');
                                }
                            }
                            else {
                                $('#txtAdmin' + curr).html(AddSpace(curr));
                                $('#Adminisdesc').val('&#9660;');
                                $('#txtAdmin' + orderby).html(AddSpace(orderby) + ' &#9660;');
                                $('#Admincurrentsort').val(orderby);
                            }
                        }

                        $('#AdminReportsDiv').removeClass("loading");
                        $('#AdminviewReportInner').html(result.page);
                        $('.results').text(result.count + ' entries returned');
                        $('#AdminviewReportInner').fadeIn();
                        ShowSort(result.count);
                    }

                    else if (result.status == 3) {
                        window.location.href = '/';
                    }

                    else {
                        $('#AdminReportsDiv').removeClass("loading");
                        $('#AdminviewReportInner').fadeIn();
                        ShowPopup("An error occurred getting the data.");
                    }
                }
            });
        });
    }
}

function PassportReportPrint() {
    if ($('.printbutton').css('opacity') == '0.2')
        return;

    var searchtype = $("#PassportcmbReportType").val();

    if (searchtype == '') {
        ShowPopup("Please select what report you would like to print");
        return false;
    }

    var from = $('#PassporttxtFromDatex').val();
    var to = $('#PassporttxtToDatex').val();

    $('#inforesult').text('');
    $('.printbutton').css('opacity', '0.2');
    $('.printinfo').fadeIn(function () {
        var intval = LoadingText('infomsg', 'Generating Report', 15, 700);
        $.ajax({
            type: "GET",
            url: "/GenerateReport",
            data: { searchtype: searchtype, from: from, to: to },
            dataType: "json",
            error: function (result) {
                clearInterval(intval);
                $('#infomsg').text('Generating Report . . . . .');
                $('#inforesult').css('color', 'red');
                $('#inforesult').text('Failed: ' + result.errormsg);
                $('.printbutton').css('opacity', '1');
            },
            success: function (result) {
                $('.printbutton').css('opacity', '1');
                clearInterval(intval);
                $('#infomsg').text('Generating Report . . . . .');

                if (result.status == 1) {
                    $('#inforesult').css('color', 'green');
                    $('#inforesult').text('Success. Report sent to printer.');
                    $('#branchReportDiv').html(result.page);
                    window.print();

                    if (window.stop) {
                        location.reload(); //triggering unload (e.g. reloading the page) makes the print dialog appear
                        window.stop(); //immediately stop reloading
                    }
                }

                else if (result.status == 0) {
                    $('#inforesult').css('color', 'red');
                    $('#inforesult').text('Denied: You do not have access rights to print this report.');
                }

                else if (result.status == 3) {
                    window.location.href = '/';
                }

                else {
                    $('#inforesult').css('color', 'red');
                    $('#inforesult').text('Failed: ' + result.errormsg);
                }
            }
        });
    });
}

function LoadingText(span, text, maxdots, intvl)
{
    var span = document.getElementById(span);


    var int = setInterval(function () {
        if ((span.innerHTML += ' . ').length > (text.length + maxdots)) {
            span.innerHTML = text;
        }
        //clearInterval( int ); // at some point, clear the setInterval
    }, intvl);

    return int;
}

function GeneralReportSearch(orderby, page, cs, isRefresh) {
    var searchtype = $("#GeneralcmbReportType").val();

    if (searchtype == '') {
        if (isRefresh === undefined || isRefresh == false)
            ShowPopup("Please select what report you would like to view");
        return false;
    }

    var skip = 0;
    var haschanged = $("#GeneralhasChanged").val();
    $("#GeneralhasChanged").val("0");
    var from = $('#GeneraltxtFromDatex').val();
    var to = $('#GeneraltxtToDatex').val();

    var prevp = $('#Generalviewing').val();
    var curr = $('#Generalcurrentsort').val();
    var isdesc = $('#Generalisdesc').val().charAt(4);
    if (page == '') {
        page = prevp;
    }

    if (cs === undefined)
        cs = 0;
    if (orderby == '') {
        orderby = curr;
        skip = 1;
    }

    if (isRefresh == true) {
        if ($("#GeneralcmbReportType").val() != '' && $('#GeneralviewReportInner').css('display') == 'block') {
            $.ajax({
                type: "GET",
                url: "/GetViewReport",
                data: { searchtype: searchtype, from: from, to: to, page: page, pgsize: $('#ddlPageSizeReport').val(), isdesc: isdesc, prevp: prevp, numj: cs, orderby: orderby, sortby: $('#Generalcurrentsort').val() },
                dataType: "json",
                error: function (result) {
                    // do nothing
                },
                success: function (result) {
                    if (result.status == 1) {
                        $('#GeneralviewReportInner').html(result.page);
                        $('.results').text(result.count + ' entries returned');
                        $('#GeneralviewReportInner').fadeIn();
                    }
                }
            });
        }
    }
    else {
        $('#GeneralviewReportInner').fadeOut(200, function () {
            $('#GeneralReportsDiv').addClass("loading");
            $.ajax({
                type: "GET",
                url: "/GetViewReport",
                data: { searchtype: searchtype, from: from, to: to, page: page, pgsize: $('#ddlPageSizeReport').val(), isdesc: isdesc, prevp: prevp, numj: cs, orderby: orderby, sortby: $('#Generalcurrentsort').val() },
                dataType: "json",
                error: function (result) {
                    $('#GeneralReportsDiv').removeClass("loading");
                    $('#GeneralviewReportInner').fadeIn();
                    ShowPopup("Could not get report data. Please ensure you are still connected to the network.");
                },
                success: function (result) {
                    if (result.status == 1) {

                        if (skip == 0 || haschanged == "1") {
                            $('.selectedSort').toggleClass("selectedSort UnselectedSort");
                            $('#txtGeneral' + orderby).toggleClass("selectedSort UnselectedSort");
                            if (curr == orderby) {
                                if ($('#txtGeneral' + orderby).html() == (AddSpace(curr) + $('<div/>').html(' &#9650;').text())) {
                                    $('#txtGeneral' + orderby).html(AddSpace(curr) + ' &#9660;');
                                    $('#Generalisdesc').val('&#9660;');
                                }
                                else {
                                    $('#txtGeneral' + orderby).html(AddSpace(curr) + ' &#9650;');
                                    $('#Generalisdesc').val('&#9650;');
                                }
                            }
                            else {
                                $('#txtGeneral' + curr).html(AddSpace(curr));
                                $('#Generalisdesc').val('&#9660;');
                                $('#txtGeneral' + orderby).html(AddSpace(orderby) + ' &#9660;');
                                $('#Generalcurrentsort').val(orderby);
                            }
                        }

                        $('#GeneralReportsDiv').removeClass("loading");
                        $('#GeneralviewReportInner').html(result.page);
                        $('.results').text(result.count + ' entries returned');
                        $('#GeneralviewReportInner').fadeIn();
                        ShowSort(result.count);
                    }

                    else if (result.status == 3) {
                        window.location.href = '/';
                    }

                    else {
                        $('#GeneralReportsDiv').removeClass("loading");
                        $('#GeneralviewReportInner').fadeIn();
                        ShowPopup("An error occurred getting the data.");
                    }
                }
            });
        });
    }
}

function ReportSearch(orderby, page, cs, isRefresh) {
    if (showing == 1) {
        AdminReportSearch(orderby, page, cs, isRefresh);
        return false;
    }

    var searchtype = $("#cmbReportType").val();

    if (searchtype == '') {
        if (isRefresh === undefined || isRefresh == false)
            ShowPopup("Please select what report you would like to view");
        return false;
    }

    var skip = 0;
    var haschanged = $("#hasChanged").val();
    $("#hasChanged").val("0");
    var from = $('#txtFromDatex').val();
    var to = $('#txtToDatex').val();

    var prevp = $('#viewing').val();
    var curr = $('#currentsort').val();
    var isdesc = $('#isdesc').val().charAt(4);
    if (page == '') {
        page = prevp;
    }

    if (cs === undefined)
        cs = 0;
    if (orderby == '') {
        orderby = curr;
        skip = 1;
    }

    if (isRefresh == true) {
        if ($("#cmbReportType").val() != '' && $('#viewReportInner').css('display') == 'block') {
            $.ajax({
                type: "GET",
                url: "/GetViewReport",
                data: { searchtype: searchtype, from: from, to: to, page: page, pgsize: $('#ddlPageSizeReport').val(), isdesc: isdesc, prevp: prevp, numj: cs, orderby: orderby, sortby: $('#currentsort').val() },
                dataType: "json",
                error: function (result) {
                    // do nothing
                },
                success: function (result) {
                    if (result.status == 1) {
                        $('#viewReportInner').html(result.page);
                        $('.results').text(result.count + ' entries returned');
                        $('#viewReportInner').fadeIn();
                    }
                }
            });
        }
    }
    else {
        $('#viewReportInner').fadeOut(200, function () {
            $('#ReportsDiv').addClass("loading");
            $.ajax({
                type: "GET",
                url: "/GetViewReport",
                data: { searchtype: searchtype, from: from, to: to, page: page, pgsize: $('#ddlPageSizeReport').val(), isdesc: isdesc, prevp: prevp, numj: cs, orderby: orderby, sortby: $('#currentsort').val() },
                dataType: "json",
                error: function (result) {
                    $('#ReportsDiv').removeClass("loading");
                    $('#viewReportInner').fadeIn();
                    ShowPopup("Could not get report data. Please ensure you are still connected to the network.");
                },
                success: function (result) {
                    if (result.status == 1) {

                        if (skip == 0 || haschanged == "1") {
                            $('.selectedSort').toggleClass("selectedSort UnselectedSort");
                            $('#txt' + orderby).toggleClass("selectedSort UnselectedSort");
                            if (curr == orderby) {
                                if ($('#txt' + orderby).html() == (AddSpace(curr) + $('<div/>').html(' &#9650;').text())) {
                                    $('#txt' + orderby).html(AddSpace(curr) + ' &#9660;');
                                    $('#isdesc').val('&#9660;');
                                }
                                else {
                                    $('#txt' + orderby).html(AddSpace(curr) + ' &#9650;');
                                    $('#isdesc').val('&#9650;');
                                }
                            }
                            else {
                                $('#txt' + curr).html(AddSpace(curr));
                                $('#isdesc').val('&#9660;');
                                $('#txt' + orderby).html(AddSpace(orderby) + ' &#9660;');
                                $('#currentsort').val(orderby);
                            }
                        }

                        $('#ReportsDiv').removeClass("loading");
                        $('#viewReportInner').html(result.page);
                        $('.results').text(result.count + ' entries returned');
                        $('#viewReportInner').fadeIn();
                        ShowSort(result.count);
                    }

                    else if (result.status == 3) {
                        window.location.href = '/';
                    }

                    else {
                        $('#ReportsDiv').removeClass("loading");
                        $('#viewReportInner').fadeIn();
                        ShowPopup(result.message);
                    }
                }
            });
        });
    }
}


function ShowPopup(message) {
    $('#headertxt').text(message);
    $('#PopupInfo').lightbox_me({
        centered: true
    });
}

function htmlEscape(str) {
    return String(str)
            .replace(/&/g, '&amp;')
            .replace(/"/g, '&quot;')
            .replace(/'/g, '&#39;')
            .replace(/</g, '&lt;')
            .replace(/>/g, '&gt;');
}

function htmlUnescape(value) {
    return String(value)
        .replace(/&quot;/g, '"')
        .replace(/&#39;/g, "'")
        .replace(/&lt;/g, '<')
        .replace(/&gt;/g, '>')
        .replace(/&amp;/g, '&');
}

function capitaliseFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}

function AddSpace(s) {
    return s.replace(/([a-z])([A-Z])/g, '$1 $2');
}