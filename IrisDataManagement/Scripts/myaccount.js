﻿$(document).ready(function () {
    $('.myAcc').click(function (e) {
        if (!$(this).hasClass('active')) {
            $('.active').toggleClass('active inActive');
            $(this).toggleClass('active inActive');
        }
    });

    $('#txtPassword').keypress(function (e) {
        if (e.which == 13) {
            UpdateUser();
        }
    });

    $('.userinfotitle2').click(function (e) {
        if ($('#changepwdiv').css('display') == 'none')
            $('#changepwdiv').slideDown();
        else {
            VerifyNewPassword(true);  
        }      
    });

    $(document).on('click', '#imgPicture', function () {
        $("#photosubmit").click();
    });

    $("#cmbGender").select2({
        placeholder: "---",
        minimumResultsForSearch: 8,
        data: $.parseJSON($('#GenderArray').val())
    });

    $("#cmbPosition").select2({
        placeholder: "Select position",
        minimumResultsForSearch: 8,
        data: $.parseJSON($('#PositionArray').val())
    });

    $("#cmbDepartment").select2({
        placeholder: "Select department",
        minimumResultsForSearch: 8,
        data: $.parseJSON($('#DepartmentArray').val())
    });

    $('.accMainDiv').css('visibility', 'visible').hide().fadeIn();
});

function VerifyNewPassword(pop) {
    $('#txtPassword').css('background-color', 'white');

    if ($('#txtNewPassword').val() == '') {
        $('#passwordvalidation').text('Please enter a new password');
        return false;
    }
    if ($('#txtNewPasswordConfirm').val() == '') {
        $('#passwordvalidation').text('Please confirm your new password');
        return false;
    }
    if ($('#txtNewPassword').val().length < 5) {
        $('#passwordvalidation').text('Password must be at least 4 characters');
        return false;
    }
    if ($('#txtNewPassword').val() != $('#txtNewPasswordConfirm').val()) {
        $('#passwordvalidation').text('Password and confirm password do not match');
        return false;
    }

    if (pop) {
        $('#passwordvalidation').text('');
        $('#txtPassword').css('background-color', 'pink');
        ShowPopup("Please enter your current password and click 'Save Changes'");
    }
}

function UpdateUser() {
    $('#txtPassword').css('background-color', 'white');
    if ($('#txtPassword').val() == '') {
        $('#pwvalidation').text('Please enter your password');
        return false;
    }

    var newpw = null;
    if ($('#txtNewPassword').val() != '' || $('#txtNewPasswordConfirm').val() != '') {
        if (VerifyNewPassword(false) == false)
            return false;
        
        newpw = $('#txtNewPassword').val();
    }

    $('#cploadingdiv').fadeIn();
    $.ajax({
        type: "GET",
        url: "/UpdateUserProfile",
        data: { firstname: $('#txtFirstname').val(), surname: $('#txtSurname').val(), phone: $('#txtPhoneNumber').val(), dep: $('#cmbDepartment').val(), pos: $('#cmbPosition').val(), gender: $('#cmbGender').val(), oldpw: $('#txtPassword').val(), newpw: newpw },
        dataType: "json",
        error: function (result) {
            $('#cploadingdiv').fadeOut();
            ShowPopup("Could not connect to database. Please ensure you are still connected to the network.");
        },
        success: function (result) {
            $('#cploadingdiv').fadeOut();
            if (result.success == 1) {
                ShowPopup("Could not connect to database. Please ensure you are still connected to the network.");
            }

            if (result.success == 1) {
                $('#txtPassword').val('');
                $('#txtNewPassword').val('');
                $('#txtNewPasswordConfirm').val('');
                if ($('#changepwdiv').css('display') == 'block')
                    $('#changepwdiv').slideUp();
                $('#txtDep').val($('#cmbPosition').select2('data').text);
                $('#txtPos').val($('#cmbPosition').select2('data').text);

                ShowPopup("You profile has been updated.");
            }

            if (result.success == 2) {
                $('#txtPassword').css('background-color', 'pink');
                ShowPopup("Incorrect password entered");
            }

            if (result.success == 3) {
                revertFunc();
                window.location.href = '/';
            }
        }
    });
}

function imageUploaded() {
    $(".staffpic").addClass("loading");
    var file = document.getElementById("photosubmit").files[0];

    $.ajax({
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Cache-Control", "no-cache");
            xhr.setRequestHeader("X-File-Name", file.fileName);
            xhr.setRequestHeader("X-File-Size", file.fileSize);
            xhr.setRequestHeader("X-File-Type", file.type);
            xhr.setRequestHeader("Content-Type", "multipart/form-data");
        },
        type: 'POST',
        url: "/UploadProfilePicture",
        processData: false,
        data: file,
        success: function (data, textStatus, xhr) {
            if (data.success == 1) {
                var image = document.createElement('img');
                image.setAttribute('src', data.img);
                image.setAttribute('id', 'imgPicture');
                $("#staffpic").animate({ opacity: 0 }, function () {
                    $("#staffpic").children("img").remove();
                    document.getElementById("staffpic").appendChild(image);
                    $(".staffpic").removeClass("loading");
                    $("#staffpic").animate({ opacity: 1 });
                });
            }
            else {
                ShowPopup("Error uploading image.");
                $(".staffpic").removeClass("loading");
            }
        },
        error: function (xhr, textStatus, errorThrown) {
            ShowPopup("Could not upload image. Please check your internet connection.");
        }
    });
}

function ShowPopup(message) {
    $('#headertxt').text(message);
    $('#PopupInfo').lightbox_me({
        centered: true
    });
}

function htmlEscape(str) {
    return String(str)
            .replace(/&/g, '&amp;')
            .replace(/"/g, '&quot;')
            .replace(/'/g, '&#39;')
            .replace(/</g, '&lt;')
            .replace(/>/g, '&gt;');
}

function htmlUnescape(value) {
    return String(value)
        .replace(/&quot;/g, '"')
        .replace(/&#39;/g, "'")
        .replace(/&lt;/g, '<')
        .replace(/&gt;/g, '>')
        .replace(/&amp;/g, '&');
}