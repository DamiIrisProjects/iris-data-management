﻿$(document).ready(function () {
    $('.myDocs').click(function (e) {
        if (!$(this).hasClass('active')) {
            $('.active').toggleClass('active inActive');
            $(this).toggleClass('active inActive');
            $('#UploadDocsDiv').hide();
            $('#CreateDocsDiv').hide();
            $('#ViewDocsDiv').fadeIn();
        }
    });

    $('.clearme').click(function (e) {
        $('#txtDocDate').val('');
        $('#txtKeywords').val('');
        $('#txtPhysLocation').val('');
        $('#cmbLevel').select2("val", "");
        $('#cmbStaffAllowed').select2("val", "");
        $('#cmbDocDept').select2("val", "");
        $('#cmbCreateDocDept').select2("val", "");
    });

    $('#txtNewFolderName').keypress(function (e) {
        if (e.which == 13) {
            $('#createBtn').click();
        }
    });

    $('.uploadFile').click(function (e) {
        if (!$(this).hasClass('active')) {
            $('.active').toggleClass('active inActive');
            $(this).toggleClass('active inActive');
            $('#CreateDocsDiv').hide();
            $('#ViewDocsDiv').hide();
            $('#UploadDocsDiv').fadeIn();
        }
    });

    $('#createcontainer').jstree();
    $("#createcontainer").jstree("open_node", $(".jstree-last"));
    $('#createcontainer').on('changed.jstree', function (e, data) {
        $('#CreateSelectedFolder').val(data.selected);
        $('#createFolderBtn').fadeIn();
    });

    $('#container').jstree();
    $("#container").jstree("open_node", $(".jstree-last"));
    $('#container').on('changed.jstree', function (e, data) {
        $('#SelectedFolder').val(data.selected);
        $('#FolderBtn').fadeIn();
        $('.uploadwell').fadeIn();
        $('.irisUpload').fadeIn();
        $('#docInfoDiv').fadeIn(); 
        $('.clearme').fadeIn();
    });

    $('#iriscontainer').jstree();
    $("#iriscontainer").jstree("open_node", $("#Iris"));
    $('#iriscontainer').on('changed.jstree', function (e, data) {
        $('#IrisSelectedFolder').val(data.selected);
        $('.uploadwell').fadeIn();
        $('.irisUpload').fadeIn();
        $('#irisUploadDiv').fadeIn();
        $('#docInfoDiv').fadeIn(); 
        $('.clearme').fadeIn();
    });

    $('.createFile').click(function (e) {
        if (!$(this).hasClass('active')) {
            $('.active').toggleClass('active inActive');
            $(this).toggleClass('active inActive');
            $('#UploadDocsDiv').hide();
            $('#ViewDocsDiv').hide();
            $('#CreateDocsDiv').fadeIn();
        }
    });

    $('.maximise').click(function (e) {
        $('.cke_button__maximize').click();
    });

    var popup = $('#popup').val();

    if (popup != "") {
        $('#PopupInfo').lightbox_me({
            centered: true
        });
        $('#popup').val("");
    }

    $('#CloseTagPop').click(function (e) {
        $(this).trigger('close');
    });

    $('#btnNo').click(function (e) {
        $(this).trigger('close');
    });

    $('.deletebtn').click(function (e) {
        $('#txtAreyousure').text('Are you sure you want to delete this file?');
        $('#DeleteFile').lightbox_me({
            centered: true
        });
        $('#DeleteFile').appendTo("#MyDocsDiv");
    });

    $('.accMainDiv').css('visibility', 'visible').hide().fadeIn();

    $(".jdpicker").datepicker({
        yearRange: "-20:+0",
        dateFormat: "dd/mm/yy",
        changeMonth: true,
        changeYear: true
    });

    $('.maximise').click();
    $('.maximise').click();
});

function verifyDoc() {
    var escapedVar = CKEDITOR.instances.editor1.getData();
    if (escapedVar != '') {
        SaveDocs(escapedVar);
    }
    else {
        ShowPopup("You cannot create an empty document");
    }
}

function ShowPopup(message) {
    $('#headertxt').text(message);
    $('#PopupInfo').lightbox_me({
        centered: true
    });
}

function SaveDocs(doc) {
    if ($('#CreateSelectedFolder').val() == '') {
        ShowPopup("Please select the folder you want to save this document to.");
    }
    else if ($('#txtDocumentName').val() == '') {
        $('#txtDocumentName').css("background-color", "Pink");
        ShowPopup("Please give a title for the document");
    }
    else if ($('#cmbcreateLevel').val() == '') {
        ShowPopup("Please select a privilege level");
    }
    else 
    {
        $('#txtDocumentName').css("background-color", "White");
        $('#CreateDocsDiv').addClass("loading");
        var zfolder = $('#CreateSelectedFolder').val().replace(/\\/g, "|");
        $.ajax({
            type: "GET",
            url: "/CreateFile",
            data: { doc: doc, title: $('#txtDocumentName').val(), folder: zfolder, keywords: $('#txtcreateKeywords').val(), level: $('#cmbcreateLevel').val(), allowusers: $('#cmbcreateStaffAllowed').val() },
            dataType: "json",
            error: function(xhr, status, error) {
                var err = JSON.parse(xhr.responseText);
                alert(err.Message);
            },
            success: function (result) {
                if (result.status == 3)
                    window.location.replace('/Home?ReturnUrl=%2fMyDocuments%2fCreateDocument');

                if (result.status == 1) {
                    $('#CreateDocsDiv').removeClass("loading");
                    CKEDITOR.instances.editor1.setData('');
                    $('#txtDocumentName').val('');
                    ShowPopup('Your Document has been created.');
                }

                if (result.status == 0) {
                    $('#createDocInner').fadeIn();
                    ShowPopup('There was an error while saving your details.');
                }
            }
        });
    }
}
