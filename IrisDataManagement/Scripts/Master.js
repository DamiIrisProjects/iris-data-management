﻿$(function () {
    var defaultvals = { key1: "test" };
    var zx = 0;

    // Turn off all cacheing for ajax
    $.ajaxSetup({ cache: false });

    $('.outersub').css("min-width", $('#divLoggedIn').width() + "px").css("left", "-" + ($('#divLoggedIn').width() - 107 + 77) + "px");

    $('.validationMessage').bind("DOMSubtreeModified", function () {
        var span = $('#' + this.id + ' span span');
        if ($(this).css('display') == 'none' && (span.text() != '' || $(this).hasClass("isDiv")))
            $(this).slideDown();
    });

    $('.validationMessage2').bind("DOMSubtreeModified", function () {
        if ($(this).css('display') == 'none' && $(this).text() != '')
            $(this).slideDown();
    });

    $(document).on('click', '.closeme', function () {
        $('.PopupWindow').fadeOut(function () {
            $(this).trigger('close');
            $('.js_lb_overlay').remove();
        });
    });

    $('.accountoptions').click(function () {
        $("#menu2").fadeIn();
    });

    // login stuff
    $('.outersub').css("min-width", $('#divLoggedIn').width() + "px").css("left", "-" + ($('#divLoggedIn').width() - 107 + 77) + "px");

    $(document).on('click', '.menuitem', function () {
        $(this).find(".outersub").hide().fadeIn();
    });

    $(document).mouseup(function (e) {
        var container = $(".menuitem");
        container.find(".outersub").fadeOut(100);
    });

     $(document).ajaxSuccess(function (event, jqxhr, settings, exception) {
         if (jqxhr.getResponseHeader("Content-Type").toLowerCase().indexOf("text/html") >= 0) {
             window.location = '/';
         }
     });
});


