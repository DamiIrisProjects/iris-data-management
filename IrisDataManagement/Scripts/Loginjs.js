﻿$(document).ready(function () {
    var popup = $('#popup').val();
    if (popup == "1") {
        $('#PopupInfo').lightbox_me({
            centered: true
        });
        $('#popup').val("");
    }

    $('#bluff3').focus(function () {
        $(this).hide();
        $('#txtRegPasswordx').show().focus();
    });

    $('#txtRegPasswordx').blur(function () {
        if ($(this).val() == '') {
            $(this).hide();
            $('#bluff3').show();
        }
    });

    $('#bluff4').focus(function () {
        $(this).hide();
        $('#txtConfirmPasswordx').show().focus();
    });

    $('#txtConfirmPasswordx').blur(function () {
        if ($(this).val() == '') {
            $(this).hide();
            $('#bluff4').show();
        }
    });

    $('#CloseTag').click(function (e) {
        $('#PopupInfo').fadeOut(function () { $(this).trigger('close'); });
    });

    $('.newregistration').click(function () {
        $('#RegistrationPopup').lightbox_me({
            centered: true
        });
    });

    $('.forgottenpw').click(function () {
        $('#ForgotPassword').lightbox_me({
            centered: true
        });
    }); 
});

function SendReset() {
    var email = $('#txtResetEmail').val();
    if (email == '') {
        $('#forgotpwval').text('Please enter your email address');
    }
    else {
        $('#floadingdiv').slideDown(function () {
            $.ajax({
                type: "GET",
                url: "/ResetPassword",
                data: { email: email },
                dataType: "json",
                error: function (result) {
                   $('#floadingdiv').slideUp(function(){
                        $('#ForgotPassword').trigger('close');
                    });
                },
                success: function (result) {
                    $('#floadingdiv').slideUp(function(){
                        $('#ForgotPassword').trigger('close');
                        setTimeout( function () {
                            if (result.status == "0")
                                ShowPopup("An email has been sent to you Iris mailbox giving details on how to reset your password.");

                            if (result.status == "1")
                                ShowPopup("<div>An email has already been sent to your mailbox giving details on how to reset your password.</div>");

                            if (result.status == "2")
                                ShowPopup("<div>This email did not match any of our registered users.</div>");

                            if (result.status == "5")
                                ShowPopup("<div>" + result.message + "</div>");
                        }, 500);
                    });
                }
            });
        });
    }
}

function validateReg() {
    var result = 1;

    if ($('#txtFirstname').val() == '' || $('#txtFirstname').val() == 'First name') {
        $('#txtFirstname').css('background-color', 'pink');
        result = 0;
    }

    if ($('#txtLastname').val() == '' || $('#txtLastname').val() == 'Last name') {
        $('#txtLastname').css('background-color', 'pink');
        result = 0;
    }

    if (result == 1) {
        $('#regloadingdiv').slideDown(function () {
            $.ajax({
                type: "GET",
                url: "/Register",
                data: { firstname: $('#txtFirstname').val(), surname: $('#txtLastname').val(), phone: $('#txtPhoneNumber').val(), email: $('#txtRegEmailx').val(), pw: $('#txtRegPasswordx').val() },
                dataType: "json",
                error: function (result) {
                    $('#regloadingdiv').slideUp(function(){
                        $('#RegistrationPopup').trigger('close');
                    });
                    ShowPopup("Unable to register. Please check your internet connection");
                },
                success: function (result) {
                    $('#regloadingdiv').slideUp(function () {
                        $('#regclosediv').trigger('close');
                        setTimeout( function () {
                            if (result.status == "0")
                                ShowPopup("<div style='margin: 10px'>An email has been sent to you Iris mailbox.</div><div>Please click on the confirmation link in the mail to complete your registration.</div>");

                            if (result.status == "1")
                                ShowPopup("<div>A verification email has already been sent to your mailbox.</div>");

                            if (result.status == "2")
                                ShowPopup("<div>This email address is already registered. Please recover your password if it is forgotten.</div>");

                            if (result.status == "3")
                                ShowPopup("<div>Please fill out the required information before registering.</div>");
                        }, 500);
                    });
                }
            });
        });
    }
}


function validateEmail(email) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}

function ShowPopup(message) {
    $('#headertxt').html(message);
    $('#PopupInfo').lightbox_me({
        centered: true
    });
}
