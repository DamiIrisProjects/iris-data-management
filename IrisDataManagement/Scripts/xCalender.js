﻿var isDel = false;
var calender;

$(document).ready(function () {
    $("#cmbStaff").select2({
        placeholder: "Select staff invited",
        multiple: true,
        minimumResultsForSearch: 8,
        data: $.parseJSON($('#StaffArray').val())
    });

    $("#cmbHourStart").select2({
        placeholder: "--",
        minimumResultsForSearch: 8,
        data: $.parseJSON($('#HourArray').val())
    });
    $("#cmbHourStart").select2("val", "6");
    $("#cmbHourStart").change(function () {
        var hr = parseInt($("#cmbHourStart").val()) + 1;
        var min = parseInt($("#cmbMinuteStart").val());
        $("#cmbHourEnd").select2("val", hr);
        $("#cmbMinuteEnd").select2("val", min);

        if ($('#timeEndDiv').css('display') == 'none' && !$("#chkIsAllDay").is(':checked'))
            $('#timeEndDiv').slideDown();
    });

    $("#cmbMinuteStart").select2({
        placeholder: "--",
        minimumResultsForSearch: 8,
        data: $.parseJSON($('#MinuteArray').val())
    });
    $("#cmbMinuteStart").select2("val", "0");
    $("#cmbMinuteStart").change(function () {
        var hr = parseInt($("#cmbHourStart").val()) + 1;
        var min = parseInt($("#cmbMinuteStart").val());
        $("#cmbHourEnd").select2("val", hr);
        $("#cmbMinuteEnd").select2("val", min);

        if ($('#timeEndDiv').css('display') == 'none')
            $('#timeEndDiv').slideDown();
    });

    $("#cmbHourEnd").select2({
        placeholder: "--",
        minimumResultsForSearch: 8,
        data: $.parseJSON($('#HourArray').val())
    });
    $("#cmbHourEnd").select2("val", "7");

    $("#cmbMinuteEnd").select2({
        placeholder: "--",
        minimumResultsForSearch: 8,
        data: $.parseJSON($('#MinuteArray').val())
    });

    $("#cmbGroup").select2({
        placeholder: "Select group",
        minimumResultsForSearch: 8,
        data: $.parseJSON($('#GroupsArray').val())
    });
    $("#cmbGroup").select2("val", "1");

    $("#cmbColor").select2({
        placeholder: "--",
        minimumResultsForSearch: 8,
        data: $.parseJSON($('#ColorsArray').val())
    });
    $("#cmbColor").select2("val", "#FFFFFF");

    $("#cmbBgColor").select2({
        placeholder: "--",
        minimumResultsForSearch: 8,
        data: $.parseJSON($('#ColorsArray').val())
    });
    $("#cmbBgColor").select2("val", "#336699");

    $('input[type="checkbox"][name="chkIsAllDay"]').change(function () {
        if (this.checked) {
            $('#timeEndDiv').slideUp();
            $("#cmbColor").select2("val", "#FFFFFF");
            $("#cmbBgColor").select2("val", "#0094FF");
            $("#cmbHourEnd").select2("val", 21);
        }
        else {
            $('#timeEndDiv').slideDown();
            $("#cmbColor").select2("val", "#FFFFFF");
            $("#cmbBgColor").select2("val", "#336699");
            var hr = parseInt($("#cmbHourStart").val()) + 1;
            $("#cmbHourEnd").select2("val", hr);
        }
    });

    calender = $('#calendar').fullCalendar({
        header: {
            left: 'prev,next today',
            center: 'title',
            right: 'month,agendaWeek,agendaDay'
        },
        defaultDate: $('#defaultDate').val(),
        selectable: true,
        timeFormat: 'h(:mm)a',
        selectHelper: true,
        select: function (start, end) {
            $('.eventpopupheader').text("Create New Event");
            $(".eventline :input").attr("disabled", false);

            // Reset defaults
            $('#btnCreateEvent').show();
            $('#visDiv').show();
            $('#textColorDiv').show();
            $('#bgColorDiv').show();
            $('#staffemailDiv').show();

            $('#txtEventTitle').val('');
            $('#txtStart').val(start);
            $('#txtStop').val(start);
            $('#txtEventTitle').text('');
            $('#txtDescription').val('');
            $("#cmbHourStart").select2("val", "6");
            $("#cmbMinuteStart").select2("val", "0");
            $("#cmbColor").select2("val", "#FFFFFF");
            $("#cmbBgColor").select2("val", "#336699");
            $("#cmbGroup").select2("val", "1");
            $('#chkIsAllDay').prop('checked', false);
            $('#timeEndDiv').hide();
            $("#cmbStaff").select2("val", "");
            $('#chkEmailStaff').prop('checked', false);

            $('#btnCreateEvent').off('click');
            $('#btnCreateEvent').click(function () {
                var calEvent = GetCurrentEvent();
                CreateEvent(calEvent);
            });

            $('#CalenderPopup').lightbox_me({
                centered: true,
                onLoad: function () {
                    $('#txtEventTitle').focus();
                }
            });
        },
        editable: true,
        eventDrop: function (event, revertFunc) {
            ResizeEvent(event, revertFunc);
        },
        eventResize: function (event, revertFunc) {
            ResizeEvent(event, revertFunc);
        },
        eventMouseover: function (calEvent, domEvent) {
            if ($('#MyEvents').val().indexOf(calEvent.id + ',') != -1 || $('#MyEvents').val().indexOf('boss') != -1) {
                var layer = "<div id='events-layer' class='fc-transparent' style='position:absolute; width:100%; height:100%; top:0px; right:1px; text-align:right; z-index:100'> <a> <div class='closevent' onClick='deleteEvent(" + calEvent.id + ", false);'>X</div></a></div>";
                $(this).append(layer);
            }
        },
        eventMouseout: function (calEvent, domEvent) {
            $("#events-layer").remove();
        },
        eventClick: function (calEvent, jsEvent, view) {
            $(".eventline :input").attr("disabled", false);
            if (!isDel) {
                currevent = calEvent;

                var s, e;

                $('#txtDescription').val(calEvent.desc);
                $('#txtPostedby').val(calEvent.postedby);
                $('#txtStart').val(calEvent.start);
                $('#txtStop').val(calEvent.end);
                $('#txtEventTitle').val(calEvent.title);
                $('#txtEventId').text(calEvent.id);
                $("#cmbStaff").select2("val", [calEvent.staffinv]);
                if (calEvent.email)
                    $('#chkEmailStaff').prop('checked', true);
                else
                    $('#chkEmailStaff').prop('checked', false);

                if (calEvent.allDay) {
                    $('#chkIsAllDay').prop('checked', true);
                    s = new Date(calEvent.realstime);
                    $('#timeEndDiv').hide();
                }
                else {
                    $('#chkIsAllDay').prop('checked', false);
                    s = new Date(calEvent.start);
                    $('#timeEndDiv').show();
                }

                $("#cmbHourStart").select2("val", s.getHours() - 1);
                $("#cmbMinuteStart").select2("val", s.getMinutes());

                e = new Date(calEvent.end);
                $("#cmbHourEnd").select2("val", e.getHours() - 1);
                $("#cmbMinuteEnd").select2("val", e.getMinutes());

                $('#txtPostedby').text("Posted by " + calEvent.postedby);

                if ($('#MyEvents').val().indexOf(calEvent.id + ',') != -1 || $('#MyEvents').val().indexOf('boss') != -1) {
                    $('.eventpopupheader').text("Edit Event");
                    $('#btnCreateEvent').show();
                    $('#visDiv').show();
                    $('#textColorDiv').show();
                    $('#bgColorDiv').show();
                    $('#staffemailDiv').show();
                    $('#CalenderPopup').lightbox_me({
                        centered: true,
                        onLoad: function () {
                            $('#txtEventTitle').focus();
                        }
                    });

                    $('#btnCreateEvent').off('click');
                    $('#btnCreateEvent').click(function () {
                        UpdateEvent(calEvent);
                    });
                }
                else {
                    $('.eventpopupheader').text("View Event");
                    $('#btnCreateEvent').hide();
                    $('#visDiv').hide();
                    $('#staffemailDiv').hide();
                    $('#textColorDiv').hide();
                    $('#bgColorDiv').hide();
                    $(".eventline :input").attr("disabled", true);
                    $('#CalenderPopup').lightbox_me({
                        centered: true
                    });
                }
            }

            isDel = false;
        },
        events: $.parseJSON($('#EventsArray').val())
    });
});

function CreateEvent()
{
    if ($('#txtEventTitle').val() == '') {
        $('#erreventtitle').text("Please enter the event's title");
    }
    else {

        var eventData = GetCurrentEvent();
        $('#calloadingdiv').slideDown(function () {
            $.ajax({
                type: "GET",
                url: "/CreateCalenderEvent",
                data: { bgcolor: eventData.backgroundColor, desc: eventData.desc, end: eventData.end, groupid: eventData.groupid, start: eventData.start, textColor: eventData.textColor, title: eventData.title, allDay: eventData.allDay, staffinv: eventData.staffinv, email: eventData.email, isUpdate: 0 },
                dataType: "json",
                error: function (result) {
                    $('#CalenderPopup').fadeOut(function () {
                        $(this).trigger('close');
                        ShowPopup("Could not connect to database. Please ensure you are still connected to the network.");
                    });
                },
                success: function (result) {
                    if (result.success == 0) {
                        ShowPopup("An error occurred creating this event. Please try again later.");
                    }

                    if (result.success == 1) {
                        $('#calloadingdiv').slideUp(function(){
                            eventData.id = result.eventid;
                            $('#MyEvents').val($('#MyEvents').val() + result.eventid + ",");
                            eventData.postedby = result.postedby;
                            eventData.realstime = eventData.start;
                            $('#calendar').fullCalendar('renderEvent', eventData, true);
                            $('#calendar').fullCalendar('unselect');

                            $('#CalenderPopup').fadeOut(function () {
                                $('#erreventtitle').text("");
                                $(this).trigger('close');
                            });
                        });
                    }

                    if (result.success == 2) {
                        ShowPopup("Stop that.");
                    }

                    if (result.success == 3) {
                        window.location.href = '/';
                    }
                }
            });
        });
    }
}

function UpdateEvent(currevent) {
    if ($('#txtEventTitle').val() == '') {
        $('#erreventtitle').text("Please enter the event's title");
    }
    else {
        var eventData = GetCurrentEvent();

        $.ajax({
            type: "GET",
            url: "/CreateCalenderEvent",
            data: { eventid: currevent.id, bgcolor: eventData.backgroundColor, desc: eventData.desc, end: eventData.end, groupid: eventData.groupid, start: eventData.start, textColor: eventData.textColor, title: eventData.title, allDay: eventData.allDay, staffinv: eventData.staffinv, email: eventData.email, isUpdate: 1 },
            dataType: "json",
            error: function (result) {
                $('#CalenderPopup').fadeOut(function () {
                    $(this).trigger('close');
                    ShowPopup("Could not connect to database. Please ensure you are still connected to the network.");
                });
            },
            success: function (result) {
                if (result.success == 0) {
                    ShowPopup("An error occurred creating this event. Please try again later.");
                }

                if (result.success == 1) {
                    eventData.id = currevent.id;
                    eventData.postedby = currevent.postedby;
                    deleteEvent(currevent.id, true);
                    eventData.realstime = eventData.start;
                    $('#calendar').fullCalendar('renderEvent', eventData, true);
                    $('#calendar').fullCalendar('unselect');

                    $('#CalenderPopup').fadeOut(function () {
                        $('#erreventtitle').text("");
                        $(this).trigger('close');
                    });

                    $('#btnYes').off('click');
                    $('#btnYes').click(function () {
                        eventData = GetCurrentEvent();
                        $.ajax({
                            type: "GET",
                            url: "/EmailChanges",
                            data: { eventid: currevent.id, desc: eventData.desc, end: eventData.end, start: eventData.start, title: eventData.title, staffinv: eventData.staffinv, isDel: 0 },
                            dataType: "json",
                            error: function (result) {
                                $('#CalenderPopup').fadeOut(function () {
                                    $(this).trigger('close');
                                    ShowPopup("Could not connect to database. Please ensure you are still connected to the network.");
                                });
                            },
                            success: function (result) {
                                // Well...do nothing really.
                            }
                        });
                    });

                    $('#CalenderPopup').fadeOut(function () {
                        ShowPopupQ("Send an email to update staff involved?");
                    });
                }

                if (result.success == 2) {
                    ShowPopup("Stop that.");
                }

                if (result.success == 3) {
                    window.location.href = '/';
                }
            }
        });
    }
}

function ResizeEvent(event, revertFunc) {
    if (event.end == null)
        event.end = event.start;

    if ($('#MyEvents').val().indexOf(event.id + ',') != -1 || $('#MyEvents').val().indexOf('boss') != -1) {
        $.ajax({
            type: "GET",
            url: "/CreateCalenderEvent",
            data: { eventid: event.id, start: GetCalDate(event.start), end: GetCalDate(event.end), bgcolor: event.backgroundColor, desc: event.desc, groupid: event.groupid, textColor: event.textColor, title: event.title, allday: event.allDay, staffinv: event.staffinv, email: event.email, isUpdate: 1 },
            dataType: "json",
            error: function (result) {
                $('#CalenderPopup').fadeOut(function () {
                    $(this).trigger('close');
                    revertFunc();
                    ShowPopup("Could not connect to database. Please ensure you are still connected to the network.");
                });
            },
            success: function (result) {
                if (result.success == 0) {
                    revertFunc();
                    ShowPopup("An error occurred creating this event. Please try again later.");
                }

                if (result.success == 1) {
                    // allow it

                    $('#btnYes').off('click');
                    $('#btnYes').click(function () {
                        $.ajax({
                            type: "GET",
                            url: "/EmailChanges",
                            data: { eventid: event.id, start: GetCalDate(event.start), desc: event.desc, end: GetCalDate(event.end), title: event.title, staffinv: event.staffinv, isDel: 0 },
                            dataType: "json",
                            error: function (result) {
                                $('#CalenderPopup').fadeOut(function () {
                                    $(this).trigger('close');
                                    ShowPopup("Could not connect to database. Please ensure you are still connected to the network.");
                                });
                            },
                            success: function (result) {
                                // Well...do nothing really.
                            }
                        });
                    });

                    ShowPopupQ("Send an email to update staff involved?");
                }

                if (result.success == 2) {
                    revertFunc();
                    ShowPopup("Stop that.");
                }

                if (result.success == 3) {
                    revertFunc();
                    window.location.href = '/';
                }
            }
        });
    }
    else {
        ShowPopup("Only the one who posted this event can edit it");
        revertFunc();
    }
}

function deleteEvent(eventId, isUpdate) {
    isDel = true;

    if (isUpdate) {
        calender.fullCalendar('removeEvents', eventId);
        calender.fullCalendar("rerenderEvents");
    }
    else {
        $.ajax({
            type: "GET",
            url: "/DeleteCalenderEvent",
            data: { eventid: eventId },
            dataType: "json",
            error: function (result) {
                ShowPopup("Could not connect to database. Please ensure you are still connected to the network.");
            },
            success: function (result) {
                if (result.success == 0) {
                    ShowPopup("An error occurred creating this event. Please try again later.");
                }

                if (result.success == 1) {
                    $('#btnYes').off('click');
                    $('#btnYes').click(function () {
                        $.ajax({
                            type: "GET",
                            url: "/EmailChanges",
                            data: { eventid: eventId, isDel: 1 },
                            dataType: "json",
                            error: function (result) {
                                $('#CalenderPopup').fadeOut(function () {
                                    $(this).trigger('close');
                                    ShowPopup("Could not connect to database. Please ensure you are still connected to the network.");
                                });
                            },
                            success: function (result) {
                                // Well...do nothing really.
                            }
                        });
                    });

                    ShowPopupQ("Send an email to update staff involved?");
                    calender.fullCalendar('removeEvents', eventId);
                    calender.fullCalendar("rerenderEvents");
                }

                if (result.success == 2) {
                    ShowPopup("Stop that.");
                }

                if (result.success == 3) {
                    window.location.href = '/';
                }
            }
        });
    }
}

function GetCurrentEvent() {
    var eventData = {
        title: $('#txtEventTitle').val(),
        start: GetCalDateFromString($('#txtStart').val(), $('#cmbHourStart').val(), $('#cmbMinuteStart').val()),
        desc: $('#txtDescription').val(),
        groupid: $('#cmbGroup').val(),
        backgroundColor: $('#cmbBgColor').val(),
        textColor: $('#cmbColor').val(),
        realstime: GetCalDateFromString($('#txtStart').val(), $('#cmbHourStart').val(), $('#cmbMinuteStart').val()),
        email: $('#chkEmailStaff').is(':checked'),
        staffinv: $('#cmbStaff').val()
    };

    if ($("#chkIsAllDay").is(':checked')) {
        eventData.allDay = true;
    }
    
    eventData.end = GetCalDateFromString($('#txtStop').val(), $('#cmbHourEnd').val(), $('#cmbMinuteEnd').val());
    return eventData;
}


function GetCalDateFromString(date, hr, min) {
    var sdate = new Date(date);
    var curr_date = sdate.getDate();
    var curr_month = sdate.getMonth() + 1; //Months are zero based
    var curr_year = sdate.getFullYear();

    var time;
    if (hr == null) {
        return curr_year + "-" + pad(curr_month, 2, 0) + "-" + pad(curr_date, 2, 0);
    }
    else {
        time = "T" + pad(hr, 2, 0) + ":" + pad(min, 2, 0) + ":00";
    }

    return curr_year + "-" + pad(curr_month, 2, 0) + "-" + pad(curr_date, 2, 0) + time;
}

function GetCalDate(date) {
    var sdate = new Date(date);
    var curr_date = sdate.getDate();
    var curr_month = sdate.getMonth() + 1; //Months are zero based
    var curr_year = sdate.getFullYear();
    var time = "T" + pad(sdate.getHours() - 1, 2, 0) + ":" + pad(sdate.getMinutes(), 2, 0) + ":00";

    return curr_year + "-" + pad(curr_month, 2, 0) + "-" + pad(curr_date, 2, 0) + time;
}

function GetCalEndFromStart(date) {
    var sdate = new Date(date);
    var curr_date = sdate.getDate();
    var curr_month = sdate.getMonth() + 1; //Months are zero based
    var curr_year = sdate.getFullYear();
    var time = "T00:00:00"; //  + pad(23, 2, 0) + ":" + pad(sdate.getMinutes(), 2, 0) + ":00";

    return curr_year + "-" + pad(curr_month, 2, 0) + "-" + pad(curr_date, 2, 0) + time;
}

function pad(n, width, z) {
    z = z || '0';
    n = n + '';
    return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
}

function ShowPopup(message) {
    $('#headertxt').text(message);
    $('#PopupInfo').lightbox_me({
        centered: true
    });
}

function ShowPopupQ(message) {
    $('#headertxt2').text(message);
    $('#PopupQuestion').lightbox_me({
        centered: true
    });
}