﻿$(function () {
    $('.advancedSearchbtn').click(function () {
        if ($('#txtSearchBtn').text() == 'Narrow Your Search') {
            ToggleSearchbox(2);
        }
        else if ($('#txtSearchBtn').text() == 'Close') {
            ToggleSearchbox(1);
        }
        else {
            ToggleSearchbox(3);
        }
    });

    $('#txtKeyword, #txtDateUploadedFrom, txtDateUploadedTo, #txtDocDateFrom, #txtDocDateTo').keypress(function (e) {
        if (e.which == 13) {
            setvals();
        }
    });

    $("#cmbTierOne").select2({
        placeholder: "None selected yet",
        minimumResultsForSearch: 8,
        multiple: true,
        data: $.parseJSON($('#TierOneArray').val())
    });

    $("#cmbTierOne").on("change", function (e) {
        if ($(this).val().indexOf("Iris") != -1) {
            $('.option7').slideDown();
        }
        else {
            $('#IrisSubArray').select2("val", "");
            $('.option7').slideUp();
        }

        if ($(this).val().indexOf("Staff") != -1) {
            $('.option6').slideDown();
        }
        else {
            $('#cmbStaff').select2("val", "");
            $('.option6').slideUp();
        }
    });

    $("#cmbIrisSub").on("change", function (e) {
        if ($(this).val().indexOf("Clients") != -1) {
            $('.option8').slideDown();
        }
        else {
            $('#cmbIrisClient').select2("val", "");
            $('.option8').slideUp();
        }
    });

    $("#cmbIrisClient").on("change", function (e) {
        if ($(this).val() != '') {
            $('.option9').slideDown();
        }
        else {
            $('#cmbIrisClientSub').select2("val", "");
            $('.option9').slideUp();
        }
    });

    $("#cmbIrisSub").select2({
        placeholder: "Select section",
        minimumResultsForSearch: 8,
        multiple: true,
        data: $.parseJSON($('#IrisSubArray').val())
    });

    $("#cmbIrisClient").select2({
        placeholder: "No client selected yet",
        minimumResultsForSearch: 8,
        multiple: true,
        data: $.parseJSON($('#IrisClientArray').val())
    });

    $("#cmbIrisClientSub").select2({
        placeholder: "Select type",
        minimumResultsForSearch: 8,
        multiple: true,
        data: $.parseJSON($('#IrisClientSubArray').val())
    });

    $("#cmbStaff").select2({
        placeholder: "No staff selected yet",
        minimumResultsForSearch: 8,
        multiple: true,
        data: $.parseJSON($('#StaffArray').val())
    });

    $("#cmbStaffLevel").select2({
        placeholder: "No staff selected yet",
        minimumResultsForSearch: 8,
        multiple: true,
        data: $.parseJSON($('#StaffIdArray').val())
    });

    $("#cmbDepts").select2({
        placeholder: "No dept selected yet",
        minimumResultsForSearch: 8,
        multiple: true,
        data: $.parseJSON($('#DeptIdArray').val())
    });

    $("#cmbFileType").select2({
        placeholder: "No filetype selected yet",
        minimumResultsForSearch: 8,
        multiple: true,
        data: $.parseJSON($('#FileTypeArray').val())
    });

    $(document).on('click', '.Unlock', function () {
        var docid = $(this).attr('id').replace('unlockdoc', '');
        $("#selectedDoc").val(docid);
        $("#cmbStaffLevel").select2("val", [$("#allowedusers" + docid).val()]);
        $("#cmbDepts").select2("val", [$("#alloweddepts" + docid).val()]);
        $('#EditLevel').lightbox_me({
            centered: true
        });
    });

    $('.closeAdv').click(function () {
        $('.advSearch').slideToggle("slow", function () {
            $('.advancedSearchbtn').toggle();
        });
    });

    $(document).on('click', '#prevClose', function () {
        $('.PopupWindow').fadeOut(function () {
            $(this).trigger('close');
            $('#PreviewDocument').css('height', 'auto').css('width', 'auto');
            $('#ViewPDFDiv').css('min-height', '200px');
        });
    });


    $(".jdpicker").datepicker({
        yearRange: "-20:+0",
        dateFormat: "dd/mm/yy",
        changeMonth: true,
        changeYear: true
    });
});

function AssignAccess() {
    var peeps = $('#cmbStaffLevel').val();
    var depts = $('#cmbDepts').val();

    $('#assignloadingdiv').slideDown(function () {
            $.ajax({
                type: "GET",
                url: "/AssignAccess",
                data: { peeps: peeps, docid: $('#selectedDoc').val(), depts: depts },
                dataType: "json",
                error: function (result) {
                    $('.closeme').click();
                    $("#cmbStaffLevel").select2("val", "");
                    $("#cmbDepts").select2("val", "");
                    ShowPopup("Unable to assign privilege. Please check your internet connection");
                },
                success: function (result) {
                    $('#assignloadingdiv').slideUp(function () {
                        $('#EditLevel').trigger('close');
                        setTimeout(function () {
                            $("#cmbStaffLevel").select2("val", "");
                            $("#cmbDepts").select2("val", "");

                            if (result.success == 1) {
                                $("#allowedusers" + $('#selectedDoc').val()).val(peeps);
                                $("#alloweddepts" + $('#selectedDoc').val()).val(depts);
                                $('#headertxt2').text("Privileges have been successfully assigned");
                                $('#PopupInfo2').lightbox_me({
                                    centered: true
                                });
                            }

                            if (result.success == 2) {
                                $('#headertxt2').text("You don't have access to do this...");
                                $('#PopupInfo2').lightbox_me({
                                    centered: true
                                });
                            }

                            if (result.success == 3) {
                                window.location.href = '/';
                            }
                        }, 400);
                    });
                }
            });
        });
}

function ToggleSearchbox(val) {
    if (val == 1) {
        $('#select1').removeClass('selected');
        $('#select1').addClass('unselected');
        $('.option').slideUp();
        $('.searchboxx').animate({ 'bottom': '34px' }, 450);

        $('#txtSearchBtn').text('Narrow Your Search');
        $('.advSearchInner').css('width', 'auto');
    }

    if (val == 2) 
    {
        $('#select1').removeClass('unselected');
        $('#select1').addClass('selected');
        $('.option').slideDown();
        $('.searchboxx').animate({ 'bottom': '26px' }, 450);

        $('#txtSearchBtn').text('Close');
        $('.advSearchInner').width(70);
    }

    if (val == 3) {
        $('.option').slideDown();
        $('#txtSearchBtn').text('Narrow Your Search');
        $('.searchboxx').animate({ 'bottom': '26px' }, 450);
        $('#txtSearchBtn').text('Close');
        $('.advSearchInner').width(70);
    }
}

function setvals() {
    DocSearch('', '', '');   
    $('.SearchResults').fadeIn();
}

function HideAllOfEm() {
    $('.AdvDateInfo').hide();
}

function ShowPopup(message) {
    $('#headertxt').text(message);
    $('#PopupInfo').lightbox_me({
        centered: true
    });
}

function capitaliseFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}

function AddSpace(s) {
    return s.replace(/([a-z])([A-Z])/g, '$1 $2');
}

function ShowPreview(doctype, docid) {
    $('#viewDocInner').fadeOut(function () {
        $('#ViewPDFDiv').addClass("loading");
        $('#PreviewDocument').lightbox_me({
            centered: true,
            onLoad: function () {
                $.ajax({
                    type: "GET",
                    url: "/GetViewDoc",
                    data: { docid: docid },
                    dataType: "json",
                    error: function (result) {
                        $('#ViewPDFDiv').removeClass("loading");
                        $('#viewDocInner').fadeIn();
                        ShowPopup("Could not get document data. Please ensure you are still connected to the network.");
                    },
                    success: function (result) {
                        var height, width;

                        // Pictures
                        if (result.status == 1) {
                            $('#viewDocInner').html(result.page);
                            if ($('#viewimagediv').length) {
                                var imgLoad = imagesLoaded('#viewimagediv');
                                imgLoad.on('always', function () {
                                    height = imgLoad.images[0].img.height + 10;
                                    width = imgLoad.images[0].img.width;
                                    ShowDocument(height, width);
                                });
                            }
                        }

                        // Word docs
                        if (result.status == 4) {
                            $('#viewDocInner').html(result.page);
                            width = $('#viewDocInner').show().width() + 20;
                            if (width < 800)
                                width = 800;
                            $('#viewDocInner').hide();
                            ShowDocument(600, width);
                        }

                        // pdfs
                        if (result.status == 5) {
                            $('#viewDocInner').html(result.page);
                            height = $('#viewDocInner').show().height() + 20;
                            width = $('#viewDocInner').width();
                            $('#viewDocInner').hide();
                            if (height > 600) height = 600;
                            ShowDocument(height, width);
                        }

                        // Excel
                        if (result.status == 6) {
                            $('#viewDocInner').html(result.page);
                            height = 105;
                            width = 740;
                            ShowDocument(height, width, true);
                        }

                        if (result.status == 2) {
                            ShowPopup("You do not have permission to view this document. (You think ya smart ba..?)");
                        }

                        if (result.status == 0) {
                            ShowPopup("An error occurred acquiring this file. Please try again later.");
                        }

                        if (result.status == 100) {
                            $('#ViewPDFDiv').removeClass("loading");
                            $('#viewDocInner').fadeIn();
                            ShowPopup(result.message);
                        }

                        if (result.status == 3) {
                            window.location.href = '/';
                        }
                    }
                });
            }
        });
        $('#PreviewDocument').appendTo("#SearchDiv");
    });
}

function ShowDocument(height, width, isExcel) {
    $('#ViewPDFDiv').removeClass("loading");
    $('#PreviewDocument').animate({ "height": height, "top": parseInt($('#PreviewDocument').css('top'), 10) - ((height - $('#PreviewDocument').height()) / 2),
        "width": width, "left": parseInt($('#PreviewDocument').css('left'), 10) - ((width - $('#PreviewDocument').width()) / 2)
    }, function () {
        if (isExcel)
            $('#ViewPDFDiv').css('min-height', '100px');
        $('#viewDocInner').fadeIn();
    });
}

function DocSearch(orderby, page, cs) {
    $('.advSearchInner').css('width', 'auto');

    // close adv search bar if open
    ToggleSearchbox(1);

    var skip = 0;
    var prevp = $('#viewing').val();
    var key = $('#txtKeyword').val();
    var curr = $('#currentsort').val();
    var isdesc = $('#isdesc').val().charAt(4);
    var updatefrom = $('#txtDateUploadedFrom').val();
    var updateto = $('#txtDateUploadedTo').val();
    var docdatefrom = $('#txtDocDateFrom').val();
    var docdateto = $('#txtDocDateTo').val();
    var filetype = $('#cmbFileType').val();
    var doctype = $('#cmbIrisSub').val();
    var clienttype = $('#cmbIrisClient').val();
    var docinnertype = $('#cmbIrisClientSub').val();
    var staffdoc = ''; 
    var cat = $("#cmbTierOne").val().replace(",", "++");
    var proj = '';
    if ($("#cmbTierOne").val().indexOf("Clients") != -1)
        proj = $("#cmbTierTwo").val().replace(",", "++");
    if ($("#cmbTierOne").val().indexOf("Staff") != -1)
        staffdoc = $('#cmbStaff').val(); 
    if (page == '') {
        page = prevp;
    }

    if (cs === undefined)
        cs = 0;
    if (orderby == '') {
        orderby = curr;
        skip = 1;
    }

    if (key == 'Enter Keyword(s) like "immigration", "contract" or "december" ')
        key = '';

    if (updatefrom != '' || updateto != '' || docdatefrom != '' || docdateto != '' || filetype != '' || cat != '' || proj != '')
        $('#txtSearchBtn').text('Edit Narrow Search');

    $('html,body').animate({ scrollTop: ($('#top').offset().top - 120) }, 800);
    $('#DocsInnerDiv').fadeOut(function () {
        $('#DocsDiv').addClass("loading");
        $.ajax({
            type: "GET",
            url: "/SearchForDocument",
            data: { k: key, orderby: orderby, sortby: $('#currentsort').val(), cat: cat, proj: proj, staffdoc: staffdoc, doctype: doctype, docinnertype: docinnertype, client: clienttype, updatefrom: updatefrom, updateto: updateto, docdatefrom: docdatefrom, docdateto: docdateto, ext: filetype, page: page, pgsize: $('#ddlPageSize').val(), isdesc: isdesc, prevp: prevp, numj: cs },
            dataType: "json",
            error: function (result) {
                $('#DocsDiv').removeClass("loading");
                $('#DocsInnerDiv').fadeIn();
                ShowPopup("Unable to update jobs. Please check your internet connection");
            },
            success: function (result) {
                if (result.status == 1) {
                    $('#JSinnerDiv').hide();

                    if (skip == 0) {
                        $('.sortOptions').hide();
                        $('.selectedSort').toggleClass("selectedSort UnselectedSort");
                        $('#txt' + orderby).toggleClass("selectedSort UnselectedSort");
                        if (curr == orderby) {
                            if ($('#txt' + orderby).html() == (AddSpace(curr) + $('<div/>').html(' &#9650;').text())) {
                                $('#txt' + orderby).html(AddSpace(curr) + ' &#9660;');
                                $('#isdesc').val('&#9660;');
                            }
                            else {
                                $('#txt' + orderby).html(AddSpace(curr) + ' &#9650;');
                                $('#isdesc').val('&#9650;');
                            }
                        }
                        else {
                            $('#txt' + curr).html(AddSpace(curr));
                            $('#isdesc').val('&#9660;');
                            $('#txt' + orderby).html(AddSpace(orderby) + ' &#9660;');
                            $('#currentsort').val(orderby);
                        }

                        $('.sortOptions').fadeIn();
                    }

                    $('#DocsInnerDiv').html(result.page);
                    $('#DocsDiv').removeClass("loading");
                    $('#DocsInnerDiv').fadeIn();
                }

                if (result.status == 2) {
                    $('#DocsDiv').removeClass("loading");
                    $('#DocsInnerDiv').fadeIn();
                    ShowPopup("Error doing search. Please try again later");
                }

                if (result.status == 3) {
                    window.location.href = '/';
                }
            }
        });
    });
}

$(document).ready(function () {
    if (!String.prototype.trim) {
        String.prototype.trim = function () { return this.replace(/^\s+|\s+$/g, ''); };
    }
    
    if ($('#stat').val() == "1") {
        $('.jobSearchHideme').hide();
        $('#advancedInfo').text('[Collapse]');
        $('#arrowImg').removeClass('advArrowUp advArrowDown');
        $('#arrowImg').addClass('advArrowDown');
    }

    if ($('#okToClearAll').val() == 1)
        $('.ui-multiselect-none').click();

    $('.DoneButton2').click();
    $('.advancedSearchbtn').click();
});