﻿var refreshIntervalId;
var refreshcounter = 0;
var waitsmallid = 0;
var stopspinning = 0;
var isspinning = 0;
var waitsmall = false;
var retrievingdata = false;

$(function () {
    var s = eval($('#TableData').val());
    var tableWidgetData = {
        "aaData": s,
        "aoColumns": JSON.parse($('#TableColumns').val()),
        "iDisplayLength": 25,
        "aLengthMenu": [[1, 25, 50, -1], [1, 25, 50, "All"]],
        "bPaginate": true,
        "bAutoWidth": false
    };

    var dashboardJSON = [
    {
        widgetTitle: "Bank Passport Verifications (Weekly)",
        widgetId: "weeklydash",
        widgetType: "table",
        enableRefresh: true,
        refreshCallBack: function (widgetData) {
            return {
                // Do refresh
                "aaData": eval($('#ChartTempData').val())
            };
        },
        widgetContent: tableWidgetData
    },
    {
        widgetTitle: "Bank Passport Verification Chart (Top 4 Banks)",
        widgetId: "bankVerificationsDash",
        widgetContent: $("#bankslinechart")
    },
    {
        widgetTitle: "Passports Viewed Today",
        widgetId: "passportsViewedDash",
        widgetContent:$("#minipassportviewer")
    },
    {
        widgetTitle: "Total Passport Verifications",
        widgetId: "totalpassports",
        widgetContent:$("#totalverifications")
    },
    {
        widgetTitle: "Registered Bank Users (Top 4 Banks)",
        widgetId: "bankusers",
        widgetContent: $("#bankuserschart")
    },
    {
        widgetTitle: "NHIS Online Pingdom Reports (Last 30 days)",
        widgetId: "pingdomStats",
        widgetContent: $("#pingdomchart")
    },
    ];

    //basic initialization example
    $("#myDashboard").sDashboard({
        dashboardData: dashboardJSON
    });

    $(".refreshpage").click(function () {
        DoRefresh(1);
    });

    $(".startSlide").click(function () {
        if ($(this).hasClass('stopSlide'))
            $('.startSlide').removeClass('stopSlide');
        else
            DoMainSlide(true);
    });

    var beenclicked = false;
    $(".sDashboardExpand").click(function () {
        var div = $(this).parent().parent().parent();
        if (!$('#slider').length)
        {
            $('#multiviewdiv').addClass("loading");
            retrievingdata = true;
            $.ajax({
                type: "GET",
                url: "/Dashboards/GetSliderPanel",
                data: { dobigupdate: 1 },
                dataType: "json",
                error: function (result) {
                    ShowPopup("Unable to connect to database. Please check your internet connection");
                    retrievingdata = false;
                },
                success: function (result) {
                    retrievingdata = false;
                    $("#multiviewdiv").fadeOut(function() {
                        $('#multiviewdiv').removeClass("loading");
                        $("#sliderdiv").show().css('visibility', 'hidden');

                        $("#sliderdiv").html(result.page);
                        $('#BigPieChart').html(result.BigPieChart);
                        $('#PassportVerificationAll').html(result.PassportVerificationAll);
                        $('#PassportVerificationBar').html(result.PassportVerificationBar);
                        $('#BankUsersChart').html(result.BankUsersChart);

                        $("#sliderdiv").css('visibility', 'visible').fadeIn(); //in case it still invisible
                    });
                }
            });
        }
        else
        {
            if ($("#multiviewdiv").css('display') == 'block')
            {
                $("#multiviewdiv").fadeOut(function() {
                    $("#sliderdiv").fadeIn();

                    // select correct slide
                    var slider = $('#slider').data('movingBoxes');

                    if (div.attr('id') == 'totalpassports')
                        slider.change(1);

                    if (div.attr('id') == 'bankVerificationsDash')
                        slider.change(2);

                    if (div.attr('id') == 'bankusers')
                        slider.change(3);

                    if (div.attr('id') == 'oraclecursor')
                        slider.change(5);
                });
            }
        }
    });

    // hide it again
    $("#sliderdiv").css('visibility', 'visible').hide();
});

function DoRefresh(dobigupdate)
{
    if (dobigupdate == 1)
        HideButtons(true);

    // Rotate image
    var $elie = $(".refreshpage"), degree = 0, timer;

    if (isspinning == 0)
    {
        isspinning = 1;
        rotate();
        function rotate() {
            $elie.css({ WebkitTransform: 'rotate(' + degree + 'deg)'});
            $elie.css({ '-moz-transform': 'rotate(' + degree + 'deg)'});
            timer = setTimeout(function() {
                ++degree; rotate();

                if (stopspinning == 1 && (degree % 360 === 0))
                {
                    stopspinning = 0;
                    isspinning = 0;
                    clearTimeout(timer);
                }
            },5);
        }
    }

    retrievingdata = true;
    $.ajax({
        type: "GET",
        url: "/Dashboards/GetSliderPanel",
        data: { dobigupdate: dobigupdate },
        dataType: "json",
        error: function (result) {
            // Stop rotation
            stopspinning = 1;
            retrievingdata = false;
            HideButtons(false);
        },
        success: function (result) {
            retrievingdata = false;
            $("#sliderdiv").show().css('visibility', 'hidden');

            // Stop rotation
            stopspinning = 1;

            if (dobigupdate == "1")
            {
                // In case the slider isnt done yet
                if (!$('#slider').length)
                {
                    $("#sliderdiv").empty();
                }

                $("#sliderdiv").html(result.page);

                // Refresh big charts
                $('#BigPieChart').html(result.BigPieChart);
                $('#PassportVerificationAll').html(result.PassportVerificationAll);
                $('#PassportVerificationBar').html(result.PassportVerificationBar);
                $('#OracleCounterChart').html(result.OracleCounterChart);
                $('#BankUsersChart').html(result.BankUsersChart);
            }

            if (dobigupdate == "3")
            {
                if (result.PingdomChart != null && result.PingdomChart != '')
                    $('#pingdomchart').html(result.PingdomChart);
            }

            $('#minipassportviewer').html(result.PassportsViewedToday);
            $('#ChartTempData').val(result.Banks5dayData);
            $('#weeklydash').find('.sDashboard-refresh-icon').click();

            // hide it again
            $("#sliderdiv").css('visibility', 'visible').hide();
            HideButtons(false);
        }
    });
}

function DoMainSlide(val)
{
    if (val)
    {
        if (!$('.startSlide').hasClass('stopSlide'))
            $('.startSlide').addClass('stopSlide');

        if ($("#sliderdiv").css('display') != 'none')
        {
            $("#sliderdiv").fadeOut(function() {
                $("#multiviewdiv").fadeIn();
            });
        }
    }
}

function ShowPassportPreview(id) {
    $('.startSlide').removeClass('stopSlide');

    if (waitsmall)
    {
        waitsmallid = id;
    }
    else
    {
        retrievingdata = true;
        $('.popupinnerdiv').fadeOut(function () {
            $('#ppinfodiv').lightbox_me({
                centered: true,
                onLoad: function () {
                    $('#ppinfodiv').addClass("loading");
                    retrievingdata = true;
                    $.ajax({
                        type: "GET",
                        url: "/GetPassportImage",
                        data: { ppid: id },
                        dataType: "json",
                        error: function (result) {
                            $('#ppinfodiv').removeClass("loading");
                            $('#popupinnerdiv').fadeIn();
                            retrievingdata = false;
                            ShowPopup("Could not get document data. Please ensure you are still connected to the network.");
                        },
                        success: function (result) {
                            retrievingdata = false;
                            $('#ppinfodiv').removeClass("loading");
                            if (result.status == 0) {
                                ShowPopup("Incomplete passport data.");
                            }

                            if (result.status == 1) {
                                $('.popupinnerdiv').html(result.page);
                                $('.popupinnerdiv').fadeIn();
                            }

                            if (result.status == 2) {
                                //window.location.href = '/';
                            }
                        }
                    });
                }
            });
        });
    }
}

function ShowPopup(message) {
    $('#headertxt').text(message);
    $('#PopupInfo').lightbox_me({
        centered: true
    });
}

function HideButtons(val)
{
    if (val)
    {
        waitsmall = true;
        $(".sDashboardExpand").fadeOut();
        $("#weeklydash div div .sDashboard-circle-plus-icon").fadeOut();
        $("#pingdomStats div div .sDashboard-circle-plus-icon").fadeOut();
        $("#passportsViewedDash div div .sDashboard-circle-plus-icon").fadeOut();
    }
    else{
        waitsmall = false;

        if (waitsmallid != 0)
        {
            ShowPassportPreview(waitsmallid);
            waitsmallid = 0;
        }

        $(".sDashboardExpand").fadeIn();
        $("#weeklydash div div .sDashboard-circle-plus-icon").fadeIn();
        $("#pingdomStats div div .sDashboard-circle-plus-icon").fadeIn();
        $("#passportsViewedDash div div .sDashboard-circle-plus-icon").fadeIn();
    }
}

$(document).ready(function () {
    refreshIntervalId = window.setInterval(function () {
        // Refresh every day at 12 am
        var now = new Date();

        if (now.getHours() % 6 == 0)
        {
            //location.reload();
        }

        if (!retrievingdata)
        {
            if ($("#sliderdiv").css('display') == 'none')
            {
                if (refreshcounter == 0)
                {
                    DoRefresh(1);
                }

                else if (refreshcounter < 5)
                {
                    DoRefresh(0);
                }

                else if (refreshcounter == 5)
                {
                    // refresh pingdom
                    DoRefresh(3);
                }

                else
                {
                    refreshcounter = 1;
                    var slider = $('#slider').data('movingBoxes');

                    if ($('#slider').length  && $('.startSlide').hasClass('stopSlide'))
                    {
                        $("#multiviewdiv").fadeOut(function() {
                            $("#sliderdiv").fadeIn();
                            slider.change(1);
                            if (!$('#btnplay').hasClass('pause')) {
                                $('#btnplay').click();
                            }
                        });
                    }
                }

                refreshcounter++;
            }
        }
    }, 15000);

    $('.startSlide').addClass('stopSlide');
});
