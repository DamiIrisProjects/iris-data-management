﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using IrisDataManagement.Models;
using Irisproxy;
using IrisCommon.Entities;
using DotNet.Highcharts.Enums;
using DotNet.Highcharts.Helpers;
using DotNet.Highcharts;
using System.Drawing;
using DotNet.Highcharts.Options;
using System.Web.Script.Serialization;
using System.Globalization;
using System.IO;
using System.Threading.Tasks;
using System.Net;
using Newtonsoft.Json.Linq;
using IrisCommon.Helpers;

namespace IrisDataManagement.Controllers
{
    public class TestingScript : Controller
    {
        #region Variables

        private Series[] _currentseries = null;
        private bool _hideLegend = false;
        private List<Report> _piereports = null;
        private List<Report> _verificationreports = null;
        private List<Report> _oracleCursor = null;
        private Dictionary<string, int> _enrolledLast7Days = new Dictionary<string, int>();
        private Dictionary<string, int> _issuedLast7Days = new Dictionary<string, int>();
        private Dictionary<string, DataTemplateItem> _results = new Dictionary<string, DataTemplateItem>();
        private Dictionary<string, int[]> _bankusersreports = null;

        private Dictionary<string, Color?> _bankColors = new Dictionary<string, Color?>() {  
            { "FBN", ColorTranslator.FromHtml("#2f7ed8") },
            { "WEMA", ColorTranslator.FromHtml("#1E55CC") },
            { "Skye", ColorTranslator.FromHtml("#8bbc21") },
            { "FCMB", ColorTranslator.FromHtml("#910000") },
            { "Diamond", ColorTranslator.FromHtml("#1aadce") },
            { "Zenith", ColorTranslator.FromHtml("#492970") },
            { "Unity", ColorTranslator.FromHtml("#f28f43") },
            { "Union", ColorTranslator.FromHtml("#77a1e5") },
            { "Eco", ColorTranslator.FromHtml("#c42525") },
            { "GTB", ColorTranslator.FromHtml("#FF7C30") },
            { "Others", ColorTranslator.FromHtml("#666666") }
        };

        private Dictionary<string, string> _bankColorsHtml = new Dictionary<string, string>() {  
            { "FBN", "#2f7ed8" },
            { "WEMA", "#1E55CC" },
            { "Skye", "#8bbc21" },
            { "FCMB", "#910000" },
            { "Diamond", "#1aadce" },
            { "Zenith", "#492970" },
            { "Unity", "#f28f43" },
            { "Union", "#77a1e5" },
            { "Eco", "#c42525" },
            { "GTB", "#FF7C30" },
            { "Others", "#666666" }
        };

        private Dictionary<string, Color?> _afisColour = new Dictionary<string, Color?>() {  
            { "Approved", ColorTranslator.FromHtml("#1E55CC") },
            { "Rejected", ColorTranslator.FromHtml("#F33B2F") },
            { "Pending", ColorTranslator.FromHtml("#8bbc21") },
            { "Dropped", ColorTranslator.FromHtml("#333333") },
            { "Enrolled", ColorTranslator.FromHtml("#1aadce") },
            { "Issued", ColorTranslator.FromHtml("#c42525") },
            { "Enrolled (Avg)", ColorTranslator.FromHtml("#8EC0CC") },
            { "Issued (Avg)", ColorTranslator.FromHtml("#FF7777") },
            { "Others", ColorTranslator.FromHtml("#666666") },

            { "KADUNA", ColorTranslator.FromHtml("#f28f43") },
            { "OWERRI", ColorTranslator.FromHtml("#B19E84") },
            { "ASABA", ColorTranslator.FromHtml("#CFA9FA") },
            { "FCT-ABUJA", ColorTranslator.FromHtml("#747FB7") },
            { "P.H.", ColorTranslator.FromHtml("#77a1e5") },
            { "FESTAC", ColorTranslator.FromHtml("#c42525") },
            { "KANO", ColorTranslator.FromHtml("#FF7C30") },
            { "IBADAN", ColorTranslator.FromHtml("#472E19") },
            { "ALAUSA", ColorTranslator.FromHtml("#EDAA69") },
            { "ABUJA HQRS", ColorTranslator.FromHtml("#1D577E") },
            { "IKOYI", ColorTranslator.FromHtml("#377524") },
            { "BENIN", ColorTranslator.FromHtml("#C8267A") },
            { "ABEOKUTA", ColorTranslator.FromHtml("#4F9B52") },
            { "LONDON UK", ColorTranslator.FromHtml("#E7EA94") },
            { "BRUSSELS", ColorTranslator.FromHtml("#B59D8E") },
            { "CALABAR", ColorTranslator.FromHtml("#F6ED51") },
            { "ENUGU", ColorTranslator.FromHtml("#4E26F0") },
            { "STOCKHOLM", ColorTranslator.FromHtml("#0BE70C") }
        };

        private Dictionary<string, string> _afisColorsHtml = new Dictionary<string, string>() {  
            { "Approved", "#1E55CC" },
            { "Rejected", "#F33B2F" },
            { "Pending", "#8bbc21" },
            { "Dropped", "#333333" },
            { "Enrolled", "#1aadce" },
            { "Issued", "#492970" },
            { "Others", "#666666" },
            { "Enrolled (Avg)", "#8EC0CC" },
            { "Issued (Avg)", "#FF7777" },

            { "KADUNA", "#f28f43" },
            { "OWERRI", "#B19E84" },
            { "ASABA", "#CFA9FA" },
            { "P.H.", "#77a1e5" },
            { "FESTAC", "#c42525" },
            { "KANO", "#FF7C30" },
            { "IBADAN", "#472E19" },
            { "ALAUSA", "#EDAA69" },
            { "ABUJA HQRS", "#1D577E" },
            { "IKOYI", "#377524" },
            { "BENIN", "#C8267A" },
            { "ABEOKUTA", "#4F9B52" },
            { "LONDON UK", "#E7EA94" },
            { "BRUSSELS", "#B59D8E" },
            { "CALABAR", "#F6ED51" },
            { "ENUGU", "#4E26F0" },
            { "STOCKHOLM", "#0BE70C" }
        };

        private List<String> _selectedbanks = new List<string>();
        private SliderData _sliderData = new SliderData();
        private SliderData _passportData = new SliderData();
        private DateTime _currDate;

        #endregion

        #region Actions

        public async Task<ActionResult> Home(string view)
        {
            // TODO: Data can be streamlined to no repeat calls to database. 
            DashboardModel model = new DashboardModel();

            if (view == "Passport")
            {
                model.Dropdown = 1;
                _currDate = DateTime.Now;

                model.MultiCharts = new List<ChartItem>();

                // Get default values
                model.MultiCharts.AddRange(GetDefaultAfisChartData(false));
                GetDashboardBaseData(model, true);

                AddEnrolVsIssueData(DateTime.Now.AddDays(-7), DateTime.Now, true, false, model);

                AddAfisActivityData(model);

                AddSummaryData(model);

                return View(model);
            }
            else
            {
                try
                {
                    // Pingdom NHIS
                    await GetPingdomChecks(model);

                    // Get default values
                    GetSliderData();

                    model.MultiCharts = new List<ChartItem>();

                    // Table data                
                    model.MultiCharts.Add(GetPassports5DaysData());

                    // Line chart
                    model.MultiCharts.Add(new ChartItem() { Chart = GetTop4BanksPassportVerification(2, 12, 1), ChartType = "ChartTemplate" });

                    // Pie chart
                    model.MultiCharts.Add(new ChartItem() { Chart = GetBanksVerificationPieChart(2), ChartType = "PieChartTemplate" });

                    // Bank users chart
                    model.MultiCharts.Add(new ChartItem() { Chart = GetTop4BanksUserRegistration(2, 12, 1), ChartType = "ChartTemplate" });

                    // Mini passport viewer
                    AddPassportViewer(model);

                    // Speed chart
                    model.MultiCharts.Add(new ChartItem() { Chart = GetOracleCounterGuage(2), ChartType = "ChartTemplate" });

                    GetDashboardBaseData(model, false);

                    return View(model);
                }
                catch (Exception ex)
                {
                    throw;
                }
            }
        }

        public ActionResult ViewDashes()
        {
            return View();
        }

        #endregion

        #region Charts

        #region Passport

        private List<ChartItem> GetDefaultAfisChartData(bool isBig)
        {
            List<ChartItem> result = new List<ChartItem>();

            ChartItem item = new ChartItem();

            try
            {
                int start = 2;
                int end = 12;

                List<Report> reports = new List<Report>();
                DateTime startDate = new DateTime(DateTime.Now.AddMonths(-start).Year, DateTime.Now.AddMonths(-start).Month, 1);
                DateTime endDate = new DateTime(DateTime.Now.AddMonths(-end).Year, DateTime.Now.AddMonths(-end).Month, DateTime.DaysInMonth(DateTime.Now.AddMonths(-end).Year, DateTime.Now.AddMonths(-end).Month));

                int daysInMonth = DateTime.DaysInMonth(startDate.Year, startDate.Month);

                // All data for the last year
                _passportData = new ServiceProxy().Channel.GetPassportDefaultData();

                string[] months = new string[12];

                // Get months in correct order
                for (int i = 0; i < 12; i++)
                {
                    months[i] = DateTime.Now.AddMonths(-12 + i).ToString("MMM");
                }

                // Also get days for a month
                string[] days = null;
                if (end == start - 1)
                {
                    days = new string[DateTime.DaysInMonth(startDate.Year, startDate.Month)];

                    for (int i = 0; i < DateTime.DaysInMonth(startDate.Year, startDate.Month); i++)
                    {
                        days[i] = (i + 1).ToString();
                    }
                }

                // Afis Activity Pie Chart
                item.Chart = AddAfisPieChart(isBig);
                item.ChartType = "PieChartTemplate";
                result.Add(item);

                // Add branch Pie Chart
                item = new ChartItem();
                item.Chart = AddActivityAfisPieChart(isBig);
                item.ChartType = "PieChartTemplate";
                result.Add(item);

                // Add Enrol vs Issued
                item = new ChartItem();
                item.Chart = AddEnrollVsIssueChart(isBig, 2, 12, true, false);
                if (isBig) item.ChartName = "BigAfisActivity_container";
                item.ChartType = "ChartTemplate";
                result.Add(item);
            }
            catch (Exception ex)
            {
                throw;
            }

            return result;
        }

        private void AddSummaryData(DashboardModel model)
        {
            model.Summary = new SummaryInfo();
            model.Summary.PendingItems = _passportData.PendingReports;
            model.Summary.AfisGrandTotal = model.AfisMonitor.DataItems.Sum(x => Convert.ToDecimal(x.Item5)).ToString();
            model.Summary.ApprovedTotal = model.AfisMonitor.DataItems.Sum(x => Convert.ToDecimal(x.Item2)).ToString();
            model.Summary.DroppedTotal = model.AfisMonitor.DataItems.Sum(x => Convert.ToDecimal(x.Item4)).ToString();
            model.Summary.PendingTotal = model.Summary.PendingItems.Count.ToString();
            model.Summary.RejectedTotal = model.AfisMonitor.DataItems.Sum(x => Convert.ToDecimal(x.Item3)).ToString();
            model.Summary.NumberOfBranches = model.AfisMonitor.DataItems.Count;

            model.Summary.IssuedTotal = _passportData.TotalPassportData.Where(x => x.IssueDay == DateTime.Today).Sum(x => Convert.ToDecimal(x.NumberIssued)).ToString();
            model.Summary.EnrolledTotal = _passportData.TotalAfisData.Where(x => x.EntryDay == DateTime.Today).Sum(x => Convert.ToDecimal(x.Enrols)).ToString();
            //model.Summary.RejectedTotal = model.AfisMonitor.DataItems.Sum(x => Convert.ToDecimal(x.Item5)).ToString();


        }

        private void AddAfisActivityData(DashboardModel model)
        {
            model.AfisMonitor = new DataTemplateInfo();
            model.AfisMonitor.DataItems = new List<DataTemplateItem>();
            model.AfisMonitor.Header1 = "Branch Name";
            model.AfisMonitor.Header2 = "Appr";
            model.AfisMonitor.Header3 = "Rej";
            model.AfisMonitor.Header4 = "Fail";
            model.AfisMonitor.Header5 = "Pen";
            model.AfisMonitor.Header6 = "Enrol";
            model.AfisMonitor.Header7 = "Issue";
            model.AfisMonitor.NumberOfCols = 7;

            if (_passportData == null || _passportData.TotalAfisData == null || _passportData.TotalPassportData == null)
                _passportData = new ServiceProxy().Channel.GetPassportData(DateTime.Now, DateTime.Now.AddDays(-1));

            foreach (AfisReport rep in _passportData.TotalAfisData)
            {
                if (rep.EntryDay.Date == DateTime.Today)
                {
                    if (!_results.ContainsKey(rep.BranchName))
                    {
                        _results.Add(rep.BranchName, new DataTemplateItem());
                        _results[rep.BranchName].Item1 = rep.BranchName;
                        _results[rep.BranchName].Item2 = "0";
                        _results[rep.BranchName].Item3 = "0";
                        _results[rep.BranchName].Item4 = "0";
                        _results[rep.BranchName].Item5 = "0";
                        _results[rep.BranchName].Item6 = "0";
                        _results[rep.BranchName].Item7 = "0";
                    }

                    _results[rep.BranchName].Item2 = (int.Parse(_results[rep.BranchName].Item2) + rep.Approved).ToString();
                    _results[rep.BranchName].Item3 = (int.Parse(_results[rep.BranchName].Item3) + rep.Rejected).ToString();
                    _results[rep.BranchName].Item4 = (int.Parse(_results[rep.BranchName].Item4) + rep.Dropped).ToString();
                    _results[rep.BranchName].Item5 = (int.Parse(_results[rep.BranchName].Item5) + rep.Pending).ToString();
                    _results[rep.BranchName].Item6 = (int.Parse(_results[rep.BranchName].Item5) + rep.Enrols).ToString();
                }
            }

            foreach (PassportReport rep in _passportData.TotalPassportData)
            {
                if (rep.IssueDay.Date == DateTime.Today)
                {
                    if (_results.ContainsKey(rep.BranchName))
                    {
                        _results[rep.BranchName].Item7 = (int.Parse(_results[rep.BranchName].Item6) + rep.NumberIssued).ToString();
                    }
                }
            }

            // order by name
            _results = _results.OrderBy(x => x.Key).ToDictionary(d => d.Key, d => d.Value);

            foreach (DataTemplateItem item in _results.Values)
                model.AfisMonitor.DataItems.Add(item);
        }

        private void AddEnrolVsIssueData(DateTime startDate, DateTime endDate, bool useStart, bool isMonth, DashboardModel model)
        {
            int daysInMonth = DateTime.DaysInMonth(startDate.Year, startDate.Month);

            if (!useStart)
            {
                _passportData = new ServiceProxy().Channel.GetPassportData(startDate, endDate);

                foreach (PassportReport rep in _passportData.TotalPassportData)
                {
                    if (rep.IssueDay.Date > startDate.Date && rep.IssueDay.Date < endDate.Date)
                    {
                        if (!_issuedLast7Days.ContainsKey(rep.IssueDay.ToString("dd-MMM-yyyy (ddd)")))
                            _issuedLast7Days.Add(rep.IssueDay.ToString("dd-MMM-yyyy (ddd)"), 0);

                        _issuedLast7Days[rep.IssueDay.ToString("dd-MMM-yyyy (ddd)")] = _issuedLast7Days[rep.IssueDay.ToString("dd-MMM-yyyy (ddd)")] + rep.NumberIssued;
                    }
                }

                foreach (AfisReport rep in _passportData.TotalAfisData)
                {
                    if (rep.EntryDay.Date > startDate.Date && rep.EntryDay.Date < endDate.Date)
                    {
                        if (!_enrolledLast7Days.ContainsKey(rep.EntryDay.ToString("dd-MMM-yyyy (ddd)")))
                            _enrolledLast7Days.Add(rep.EntryDay.ToString("dd-MMM-yyyy (ddd)"), 0);

                        _enrolledLast7Days[rep.EntryDay.ToString("dd-MMM-yyyy (ddd)")] = _enrolledLast7Days[rep.EntryDay.ToString("dd-MMM-yyyy (ddd)")] + rep.Enrols;
                    }
                }
            }

            string result = string.Empty;

            _enrolledLast7Days.OrderBy(x => x.Key);

            model.Last7DaysActivity = new DataTemplateInfo();
            model.Last7DaysActivity.DataItems = new List<DataTemplateItem>();
            model.Last7DaysActivity.Header1 = "Date";
            model.Last7DaysActivity.Header2 = "Enrolled";
            model.Last7DaysActivity.Header3 = "Issued";
            model.Last7DaysActivity.NumberOfCols = 3;

            _enrolledLast7Days = _enrolledLast7Days.OrderBy(x => DateTime.Parse(x.Key.Substring(0, x.Key.Length - 6))).ToDictionary(pair => pair.Key, pair => pair.Value);
            _issuedLast7Days = _issuedLast7Days.OrderBy(x => DateTime.Parse(x.Key.Substring(0, x.Key.Length - 6))).ToDictionary(pair => pair.Key, pair => pair.Value);

            // Cater for sundays, add padding
            if (_issuedLast7Days.Count < 7)
            {
                while (_issuedLast7Days.Count < 7)
                    _issuedLast7Days.Add(string.Empty, 0);
            }

            foreach (KeyValuePair<string, int> val in _enrolledLast7Days)
            {
                DataTemplateItem item = new DataTemplateItem();
                item.Item1 = val.Key;
                item.Item2 = val.Value.ToString();
                item.Item3 = (_issuedLast7Days.ContainsKey(val.Key) ? _issuedLast7Days[val.Key] : 0).ToString();
                model.Last7DaysActivity.DataItems.Add(item);
            }

            // Leave a space
            model.Last7DaysActivity.DataItems.Add(new DataTemplateItem() { Item1 = " ", Item2 = " ", Item3 = " " });

            // Calculate averages
            model.Last7DaysActivity.DataItems.Add(new DataTemplateItem()
            {
                Item1 = "Average (Full Week)",
                Item2 = Convert.ToInt32(_enrolledLast7Days.Values.Average()).ToString(),
                Item3 = Convert.ToInt32(_issuedLast7Days.Values.Average()).ToString(),
                IsBold = true
            });

            model.Last7DaysActivity.DataItems.Add(new DataTemplateItem()
            {
                Item1 = "Average (Weekdays)",
                Item2 = Convert.ToInt32(_enrolledLast7Days.Where(x => !x.Key.ToUpper().Contains("SAT") && !x.Key.ToUpper().Contains("SUN")).ToDictionary(t => t.Key, t => t.Value).Values.Average()).ToString(),
                Item3 = Convert.ToInt32(_issuedLast7Days.Where(x => !x.Key.ToUpper().Contains("SAT") && !x.Key.ToUpper().Contains("SUN") && !string.IsNullOrEmpty(x.Key)).ToDictionary(t => t.Key, t => t.Value).Values.Average()).ToString(),
                IsBold = true
            });

            //string[] months = new string[12];

            //// Get months in correct order
            //int monthcount = ((startDate.Year - endDate.Year) * 12) + startDate.Month - endDate.Month;

            //for (int i = 0; i < monthcount; i++)
            //{
            //    months[i] = endDate.AddMonths(-monthcount + i).ToString("MMM");
            //}

            //string[] days = null;

            //if (isMonth)
            //{
            //    // And days as well
            //    days = new string[DateTime.DaysInMonth(startDate.Year, startDate.Month)];

            //    for (int i = 0; i < DateTime.DaysInMonth(startDate.Year, startDate.Month); i++)
            //    {
            //        days[i] = (i + 1).ToString();
            //    }
            //}
        }

        public Highcharts AddEnrollVsIssueChart(bool isBig, int start, int end, bool shiftMonths, bool useBarChart)
        {
            List<Report> reports = new List<Report>();
            DateTime startDate = new DateTime(DateTime.Now.AddMonths(-start).Year, DateTime.Now.AddMonths(-start).Month, 1);

            DateTime endDate = new DateTime(DateTime.Now.AddMonths(-end).Year, DateTime.Now.AddMonths(-end).Month, DateTime.DaysInMonth(DateTime.Now.AddMonths(-end).Year, DateTime.Now.AddMonths(-end).Month));

            if (useBarChart)
                endDate = DateTime.Now;

            int daysInMonth = DateTime.DaysInMonth(startDate.Year, startDate.Month);

            // All data for the last year
            if (_passportData.TotalAfisData == null || _passportData.TotalPassportData == null)
                _passportData = new ServiceProxy().Channel.GetPassportData(endDate, startDate);


            // Get years in correct order
            int yeararray = 0;
            string[] years = new string[yeararray];

            if (useBarChart)
            {
                yeararray = endDate.Year - startDate.Year;
                years = new string[yeararray];
                int counter = 0;
                for (int i = startDate.Year + 1; i < endDate.Year + 1; i++)
                {
                    years[counter] = i.ToString();
                    counter++;
                }
            }

            // Get months in correct order
            string[] months = new string[12];
            for (int i = 0; i < 12; i++)
            {
                months[i] = shiftMonths ? DateTime.Now.AddMonths(-12 + i).ToString("MMM") : new DateTime(2000, 1, 1).AddMonths(i).ToString("MMM");
            }

            // Also get days for a month
            string[] days = null;
            if (end == start - 1)
            {
                days = new string[DateTime.DaysInMonth(startDate.Year, startDate.Month)];

                for (int i = 0; i < DateTime.DaysInMonth(startDate.Year, startDate.Month); i++)
                {
                    days[i] = (i + 1).ToString();
                }
            }

            Dictionary<string, int[]> infovscount = new Dictionary<string, int[]>();
            Dictionary<string, int> yearDicIss = new Dictionary<string, int>();
            Dictionary<string, int> yearDicEnr = new Dictionary<string, int>();
            Dictionary<string, int> monthDicIss = new Dictionary<string, int>();
            Dictionary<string, int> monthDicEnr = new Dictionary<string, int>();
            Dictionary<string, int> daysDicIss = new Dictionary<string, int>();
            Dictionary<string, int> daysDicEnr = new Dictionary<string, int>();


            // Enrolled
            Dictionary<string, int[]> series = new Dictionary<string, int[]>();

            if (useBarChart)
            {
                series.Add("Enrolled", new int[yeararray]);
                series.Add("Issued", new int[yeararray]);

                // set defaults first
                foreach (string yr in years)
                {
                    yearDicIss.Add(yr, 0);
                    yearDicEnr.Add(yr, 0);
                }
            }
            else
            {
                int arraylength = days == null ? months.Length : days.Length;
                series.Add("Enrolled", new int[arraylength]);
                series.Add("Issued", new int[arraylength]);
                series.Add("Enrolled (Avg)", new int[arraylength]);
                series.Add("Issued (Avg)", new int[arraylength]);

                if (days == null)
                {
                    foreach (string mn in months)
                    {
                        monthDicEnr.Add(mn, 0);
                        monthDicIss.Add(mn, 0);
                    }
                }
                else
                {
                    foreach (string dys in days)
                    {
                        daysDicIss.Add(dys, 0);
                        daysDicEnr.Add(dys, 0);
                    }
                }
            }

            // Enrolled
            foreach (AfisReport rep in _passportData.TotalAfisData)
            {
                if (useBarChart)
                {
                    if (years.Contains(rep.EntryDay.Year.ToString()))
                    {
                        yearDicEnr[rep.EntryDay.Year.ToString()] = yearDicEnr[rep.EntryDay.Year.ToString()] + rep.Enrols;
                    }
                }
                else
                {
                    if (days == null)
                    {
                        if (months.Contains(rep.EntryDay.ToString("MMM")))
                            monthDicEnr[rep.EntryDay.ToString("MMM")] = monthDicEnr[rep.EntryDay.ToString("MMM")] + rep.Enrols;

                        // Use opportunity to populate last 7 days data
                        if (rep.EntryDay.Date >= DateTime.Now.AddDays(-8) && rep.EntryDay.Date < DateTime.Now.AddDays(-1))
                        {
                            if (!_enrolledLast7Days.ContainsKey(rep.EntryDay.ToString("dd-MMM-yyyy (ddd)")))
                                _enrolledLast7Days.Add(rep.EntryDay.ToString("dd-MMM-yyyy (ddd)"), 0);

                            _enrolledLast7Days[rep.EntryDay.ToString("dd-MMM-yyyy (ddd)")] = _enrolledLast7Days[rep.EntryDay.ToString("dd-MMM-yyyy (ddd)")] + rep.Enrols;
                        }
                    }
                    else
                    {
                        if (days.Contains(rep.EntryDay.Day.ToString()))
                            daysDicEnr[rep.EntryDay.Day.ToString()] = daysDicEnr[rep.EntryDay.Day.ToString()] + rep.Enrols;
                    }
                }
            }

            // Issued
            foreach (PassportReport rep in _passportData.TotalPassportData)
            {
                if (useBarChart)
                {
                    if (years.Contains(rep.IssueDay.Year.ToString()))
                        yearDicIss[rep.IssueDay.Year.ToString()] = yearDicIss[rep.IssueDay.Year.ToString()] + rep.NumberIssued;
                }
                else
                {
                    if (days == null)
                    {
                        if (months.Contains(rep.IssueDay.ToString("MMM")))
                            monthDicIss[rep.IssueDay.ToString("MMM")] = monthDicIss[rep.IssueDay.ToString("MMM")] + rep.NumberIssued;

                        // Use opportunity to populate last 7 days data
                        if (rep.IssueDay.Date >= DateTime.Now.AddDays(-8) && rep.IssueDay.Date < DateTime.Now.AddDays(-1))
                        {
                            if (!_issuedLast7Days.ContainsKey(rep.IssueDay.ToString("dd-MMM-yyyy (ddd)")))
                                _issuedLast7Days.Add(rep.IssueDay.ToString("dd-MMM-yyyy (ddd)"), 0);

                            _issuedLast7Days[rep.IssueDay.ToString("dd-MMM-yyyy (ddd)")] = _issuedLast7Days[rep.IssueDay.ToString("dd-MMM-yyyy (ddd)")] + rep.NumberIssued;
                        }
                    }
                    else
                    {
                        if (days.Contains(rep.IssueDay.Day.ToString()))
                            daysDicIss[rep.IssueDay.Day.ToString()] = daysDicIss[rep.IssueDay.Day.ToString()] + rep.NumberIssued;
                    }
                }
            }

            if (!useBarChart)
            {
                if (days == null)
                {
                    series["Issued"] = monthDicIss.Values.ToArray();
                    series["Enrolled"] = monthDicEnr.Values.ToArray();
                }
                else
                {
                    series["Issued"] = daysDicIss.Values.ToArray();
                    series["Enrolled"] = daysDicEnr.Values.ToArray();
                }


                // Averages
                for (int i = 0; i < (days == null ? months.Length : days.Length); i++)
                {
                    (series["Enrolled (Avg)"] as int[])[i] = Convert.ToInt32(series["Enrolled"].Average());
                    (series["Issued (Avg)"] as int[])[i] = Convert.ToInt32(series["Issued"].Average());
                }
            }
            else
            {
                series["Issued"] = yearDicIss.Values.ToArray();
                series["Enrolled"] = yearDicEnr.Values.ToArray();
            }

            // Get series
            Series[] seriez = new Series[series.Count];
            int z = 0;

            _hideLegend = false;
            foreach (KeyValuePair<string, int[]> val in series)
            {
                seriez[z] = new Series { PlotOptionsSeries = new PlotOptionsSeries() { LineWidth = 3, DashStyle = val.Key.Contains("(Avg)") ? DashStyles.ShortDashDot : DashStyles.Solid, Color = _afisColour.ContainsKey(val.Key) ? _afisColour[val.Key] : null, Marker = new PlotOptionsSeriesMarker() { Enabled = isBig && !val.Key.Contains("(Avg)") ? true : false } }, Name = val.Key, Data = new Data(val.Value.Cast<object>().ToArray()) };
                z++;
            }

            _currentseries = seriez;

            if (useBarChart)
                return CreatePassportChart(2, years, seriez, isBig, useBarChart);

            return CreatePassportChart(2, days == null ? months : days, seriez, isBig, useBarChart);
        }

        private Highcharts AddAfisPieChart(bool isBig)
        {
            int total = _passportData.TotalAfisData.Count;

            Dictionary<string, int> afisPart = new Dictionary<string, int>();

            afisPart.Add("Approved", 0);
            afisPart.Add("Dropped", 0);
            afisPart.Add("Rejected", 0);

            foreach (AfisReport rep in _passportData.TotalAfisData)
            {
                afisPart["Dropped"] = afisPart["Dropped"] + rep.Dropped;
                afisPart["Approved"] = afisPart["Approved"] + rep.Approved;
                afisPart["Rejected"] = afisPart["Rejected"] + rep.Rejected;
            }

            // Get series
            List<object[]> datalist = new List<object[]>();
            double totalperc = 0;
            var random = new Random();
            List<string> colors = new List<string>();

            foreach (KeyValuePair<string, int> val in afisPart)
            {
                if ((val.Value * 100 / total) > 2)
                {
                    totalperc += val.Value;
                    datalist.Add(new object[] { val.Key, val.Value });
                }
            }

            Highcharts chart = new Highcharts(isBig ? "PassportReportsBig" : "AfisActivityPieChart")
                .SetCredits(new Credits { Enabled = false })
                .InitChart(new Chart { PlotBackgroundColor = null, PlotBorderWidth = null, PlotShadow = false })
                .SetTitle(new Title { Style = "display: 'none'", Text = "AFIS Activity (Over Last Year)" })
                .SetTooltip(new Tooltip { PointFormat = "{series.name}: <b>{point.y}</b>" /*, percentageDecimals: 1*/ })
                .SetPlotOptions(new PlotOptions
                {
                    Pie = new PlotOptionsPie
                    {
                        AllowPointSelect = true,
                        Cursor = Cursors.Pointer,
                        DataLabels = new PlotOptionsPieDataLabels
                        {
                            Enabled = true,
                            Color = ColorTranslator.FromHtml("#000000"),
                            ConnectorColor = ColorTranslator.FromHtml("#000000"),
                            Formatter = "function() { return '<b>'+ this.point.name +'</b>: '+ this.point.percentage.toFixed(1) +' %'; }"
                        },
                        Size = new PercentageOrPixel(80, true),
                        Colors = afisPart.Keys.Select(x => _afisColour.ContainsKey(x) ? _afisColour[x].GetValueOrDefault() : ColorTranslator.FromHtml(String.Format("#{0:X6}", random.Next(0x1000000)))).ToArray()
                    }
                })
                .SetSeries(new Series
                {
                    Type = ChartTypes.Pie,
                    Name = "Activities",
                    Data = new Data(datalist.ToArray())
                });

            return chart;
        }

        private Highcharts AddActivityAfisPieChart(bool isBig)
        {
            int total = 0;

            Dictionary<string, int> afisPart = new Dictionary<string, int>();
            Dictionary<string, int> afisPartx = new Dictionary<string, int>();

            foreach (AfisReport rep in _passportData.TotalAfisData)
            {
                if (rep.BranchName == "PORTHARCOURT") rep.BranchName = "P.H.";
                if (rep.BranchName == "FESTAC, LAGOS") rep.BranchName = "FESTAC";
                if (rep.BranchName == "ALAUSA, LAGOS") rep.BranchName = "ALAUSA";
                if (rep.BranchName == "IKOYI, LAGOS") rep.BranchName = "IKOYI";

                if (afisPart.ContainsKey(rep.BranchName))
                    afisPart[rep.BranchName] = afisPart[rep.BranchName] + rep.Enrols;
                else
                    afisPart.Add(rep.BranchName, rep.Enrols);

                total = total + rep.Enrols;
            }

            // Get series
            List<object[]> datalist = new List<object[]>();
            double totalperc = 0;
            double perc = isBig ? 0.8 : 1;
            var random = new Random();
            List<string> colors = new List<string>();

            foreach (KeyValuePair<string, int> val in afisPart)
            {
                if ((val.Value * 100 / total) > perc)
                {
                    totalperc += val.Value;
                    datalist.Add(new object[] { val.Key, val.Value });
                    afisPartx.Add(val.Key, val.Value);
                }
            }

            // Add others
            datalist.Add(new object[] { "Others", total - totalperc });
            afisPartx.Add("Others", 2);

            Highcharts chart = new Highcharts(isBig ? "BigAfisBranchChart" : "AfisBranchActivityPieChart")
                .SetCredits(new Credits { Enabled = false })
                .InitChart(new Chart { PlotBackgroundColor = null, PlotBorderWidth = null, PlotShadow = false })
                .SetTitle(new Title { Style = "display: 'none'", Text = "AFIS Branch Activity (Over Last Year)" })
                .SetTooltip(new Tooltip { PointFormat = "{series.name}: <b>{point.y}</b>" /*, percentageDecimals: 1*/ })
                .SetPlotOptions(new PlotOptions
                {
                    Pie = new PlotOptionsPie
                    {
                        AllowPointSelect = true,
                        Cursor = Cursors.Pointer,
                        DataLabels = new PlotOptionsPieDataLabels
                        {
                            Enabled = true,
                            Color = ColorTranslator.FromHtml("#000000"),
                            ConnectorColor = ColorTranslator.FromHtml("#000000"),
                            Formatter = "function() { return '<b>'+ this.point.name +'</b>: '+ this.point.percentage.toFixed(1) +' %'; }"
                        },
                        Size = new PercentageOrPixel(80, true),
                        Colors = afisPartx.Keys.Select(x => _afisColour.ContainsKey(x) ? _afisColour[x].GetValueOrDefault() : ColorTranslator.FromHtml(String.Format("#{0:X6}", random.Next(0x1000000)))).ToArray()
                    }
                })
                .SetSeries(new Series
                {
                    Type = ChartTypes.Pie,
                    Name = "Branch Activity",
                    Data = new Data(datalist.ToArray())
                });

            return chart;
        }

        #endregion

        #region NIBSS

        public void GetSliderData()
        {
            try
            {
                _sliderData = new ServiceProxy().Channel.GetSliderData();

                _piereports = _sliderData.Piereports;
                _verificationreports = _sliderData.Verificationreports;
                _bankusersreports = _sliderData.Bankusersreports;
                _oracleCursor = _sliderData.OracleCursor;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public Highcharts GetBanksVerificationPieChart(int type)
        {
            // Note: i'm using for the last 10 years as I assume this won't still be in use then. If it is, then damn...
            if (_piereports == null)
                _piereports = new ServiceProxy().Channel.GetPassportVerificationHistory(DateTime.Now.AddYears(-10), DateTime.Now, false);

            int total = _piereports.Count;

            Dictionary<string, int[]> bankcount = new Dictionary<string, int[]>();

            foreach (var line in _piereports.GroupBy(info => info.Bank.BankName)
                        .Select(group => new
                        {
                            BankName = group.Key,
                            Count = group.Count()
                        })
                        .OrderByDescending(x => x.Count))
            {
                bankcount.Add(line.BankName, new int[] { line.Count });
            }

            // Get series
            List<object[]> datalist = new List<object[]>();
            double totalperc = 0;
            List<string> colors = new List<string>();

            foreach (KeyValuePair<string, int[]> val in bankcount)
            {
                if ((val.Value[0] * 100 / total) > 2)
                {
                    totalperc += val.Value[0];
                    colors.Add(_bankColorsHtml[val.Key]);
                    datalist.Add(new object[] { val.Key, val.Value[0] });
                }
            }

            // Add others
            datalist.Add(new object[] { "Others", total - totalperc });
            colors.Add(_bankColorsHtml["Others"]);

            Highcharts chart = new Highcharts(type == 2 ? "BigBanksVerificationPieChart" : "BanksVerificationPieChart")
                .SetCredits(new Credits { Enabled = false })
                .InitChart(new Chart { PlotBackgroundColor = null, PlotBorderWidth = null, PlotShadow = false })
                .SetTitle(new Title { Style = type == 2 ? "display: 'none'" : "", Text = "Total Passport Verifications" })
                .SetTooltip(new Tooltip { PointFormat = "{series.name}: <b>{point.y}</b>" /*, percentageDecimals: 1*/ })
                .SetPlotOptions(new PlotOptions
                {
                    Pie = new PlotOptionsPie
                    {
                        AllowPointSelect = true,
                        Cursor = Cursors.Pointer,
                        DataLabels = new PlotOptionsPieDataLabels
                        {
                            Enabled = true,
                            Color = ColorTranslator.FromHtml("#000000"),
                            ConnectorColor = ColorTranslator.FromHtml("#000000"),
                            Formatter = "function() { return '<b>'+ this.point.name +'</b>: '+ this.point.percentage.toFixed(1) +' %'; }"
                        },
                        Size = new PercentageOrPixel(80, true)
                    }
                })
                .SetSeries(new Series
                {
                    Type = ChartTypes.Pie,
                    Name = "Verifications",
                    Data = new Data(datalist.ToArray())
                });

            ViewBag.Colors = "'" + string.Join(",", colors.ToArray()).Replace(",", "','") + "'";
            return chart;
        }

        public Highcharts GetTop4BanksPassportVerification(int chartType, int start, int end)
        {
            List<Report> reports = new List<Report>();
            DateTime startDate = new DateTime(DateTime.Now.AddMonths(-start).Year, DateTime.Now.AddMonths(-start).Month, 1);
            DateTime endDate = new DateTime(DateTime.Now.AddMonths(-end).Year, DateTime.Now.AddMonths(-end).Month, DateTime.DaysInMonth(DateTime.Now.AddMonths(-end).Year, DateTime.Now.AddMonths(-end).Month));

            int daysInMonth = DateTime.DaysInMonth(startDate.Year, startDate.Month);

            // All data from the beginning
            if (_verificationreports == null)
                _verificationreports = new ServiceProxy().Channel.GetPassportVerificationHistory(new DateTime(2013, 1, 1), DateTime.Now, false);

            if (end == start - 1)
                reports = _verificationreports.Where(x => x.ViewDate >= new DateTime(startDate.Year, startDate.Month, 1) && x.ViewDate <= new DateTime(startDate.Year, startDate.Month, daysInMonth)).ToList();
            else
                reports = _verificationreports.Where(x => x.ViewDate >= startDate && x.ViewDate <= endDate).ToList();

            string[] months = new string[12];

            // Get months in correct order
            for (int i = 0; i < 12; i++)
            {
                months[i] = DateTime.Now.AddMonths(-12 + i).ToString("MMM");
            }

            // Also get days for a month
            string[] days = null;
            if (end == start - 1)
            {
                days = new string[DateTime.DaysInMonth(startDate.Year, startDate.Month)];

                for (int i = 0; i < DateTime.DaysInMonth(startDate.Year, startDate.Month); i++)
                {
                    days[i] = (i + 1).ToString();
                }
            }

            Dictionary<string, int[]> bankvscount = new Dictionary<string, int[]>();
            List<string> bankcount = new List<string>();
            int count = 0;

            foreach (var line in reports.GroupBy(info => info.Bank.BankName)
                        .Select(group => new
                        {
                            BankName = group.Key,
                            Count = group.Count()
                        })
                        .OrderByDescending(x => x.Count))
            {
                bankcount.Add(line.BankName);

                if (count > 3 && chartType == 2)
                    bankcount.Remove(line.BankName);

                if (count < 4)
                {
                    if (!_selectedbanks.Contains(line.BankName))
                    {
                        _selectedbanks.Add(line.BankName);
                    }
                }

                count++;
            }

            if (days == null)
            {
                foreach (Report report in reports)
                {
                    if (bankcount.Contains(report.Bank.BankName))
                    {
                        if (!bankvscount.ContainsKey(report.Bank.BankName))
                            bankvscount.Add(report.Bank.BankName, new int[months.Length]);

                        (bankvscount[report.Bank.BankName] as int[])[MathMod(report.ViewDate.Month - startDate.Month, 12)]++;
                    }
                }
            }
            else
            {
                foreach (Report report in reports)
                {
                    if (bankcount.Contains(report.Bank.BankName))
                    {
                        if (!bankvscount.ContainsKey(report.Bank.BankName))
                            bankvscount.Add(report.Bank.BankName, new int[days.Length]);

                        (bankvscount[report.Bank.BankName] as int[])[MathMod(report.ViewDate.Day - 1, days.Length)]++;
                    }
                }
            }

            // Get series
            Series[] series = new Series[bankvscount.Count()];
            int z = 0;

            if (bankvscount.Count == 0)
            {
                series = new Series[1];
                series[0] = new Series { Data = null };
                _hideLegend = true;
            }
            else
            {
                _hideLegend = false;
                foreach (KeyValuePair<string, int[]> val in bankvscount)
                {
                    series[z] = new Series { PlotOptionsSeries = new PlotOptionsSeries() { LineWidth = 3, Color = _bankColors.ContainsKey(val.Key) ? _bankColors[val.Key] : null, Visible = _selectedbanks.Contains(val.Key), Marker = new PlotOptionsSeriesMarker() { Enabled = chartType == 2 ? false : true } }, Name = val.Key, Data = new Data(val.Value.Cast<object>().ToArray()) };
                    z++;
                }
            }

            _currentseries = series;

            if (days == null)
                return CreateChart(chartType, months, series);
            else
                return CreateChart(chartType, days, series);
        }

        public Highcharts GetTop4BanksUserRegistration(int chartType, int start, int end)
        {
            if (_bankusersreports == null)
                _bankusersreports = new ServiceProxy().Channel.GetBankUsersRegistered(DateTime.Now.AddMonths(-start), DateTime.Now.AddMonths(-end));


            Series[] series = new Series[_selectedbanks.Count];
            string[] months = new string[12];

            int z = 0;

            foreach (KeyValuePair<string, int[]> val in _bankusersreports.OrderBy(key => key.Key))
            {
                // Only show for top 4 banks
                if (_selectedbanks.Contains(val.Key))
                {
                    series[z] = new Series { PlotOptionsSeries = new PlotOptionsSeries() { LineWidth = 3, Color = _bankColors.ContainsKey(val.Key) ? _bankColors[val.Key] : null, Visible = _selectedbanks.Contains(val.Key) ? true : false, Marker = new PlotOptionsSeriesMarker() { Enabled = chartType == 2 ? false : true } }, Name = val.Key, Data = new Data(val.Value.Cast<object>().ToArray()) };
                    z++;
                }
            }

            // Get months in correct order
            for (int i = 0; i < 12; i++)
            {
                months[i] = DateTime.Now.AddMonths(-12 + i).ToString("MMM");
            }

            Highcharts chart = new Highcharts(chartType == 2 ? "Top4BanksUserRegistration" : "Top4BanksUserRegistrationBig")
                .SetCredits(new Credits { Enabled = false })
                .InitChart(new Chart
                {
                    DefaultSeriesType = ChartTypes.Line
                    ,
                    MarginRight = chartType == 2 ? 0 : 130,
                    ClassName = chartType == 2 ? "chart" : ""
                })
                .SetTitle(new Title { Style = chartType == 2 ? "display: 'none'" : "", Text = chartType == 1 ? "Registered Users (Top 4 Banks)" : "Registered Users Over The Last Year" })
                .SetSubtitle(new Subtitle { Style = "display:'none'", Text = "Top 4 Banks" })
                .SetXAxis(new XAxis { Categories = months, Min = 0 })
                .SetYAxis(new YAxis
                {
                    Min = 0,
                    Title = new YAxisTitle { Text = "Registered Users" }
                })
                .SetTooltip(new Tooltip
                {
                    Formatter = @"function() {
                                        return '<b>'+ this.series.name +'</b><br/>Users: '+ this.y;
                                }"
                })
                .SetLegend(new Legend
                {
                    Layout = chartType == 2 ? Layouts.Horizontal : Layouts.Vertical,
                    Align = chartType == 2 ? HorizontalAligns.Center : HorizontalAligns.Right,
                    VerticalAlign = VerticalAligns.Top,
                    X = chartType == 2 ? 0 : -10,
                    Y = chartType == 2 ? -5 : 100,
                    Floating = true,
                    BackgroundColor = new BackColorOrGradient(ColorTranslator.FromHtml("#FFFFFF")),
                    Shadow = true,
                    MaxHeight = 154,
                    //Enabled = !hideLegend,
                    Padding = chartType == 2 ? 10 : 20,
                    ItemStyle = "fontSize: '14px'"
                })
                .SetPlotOptions(new PlotOptions
                {
                    Column = new PlotOptionsColumn
                    {
                        BorderWidth = 0
                    },
                    Line = new PlotOptionsLine { Marker = new PlotOptionsLineMarker() { Enabled = true, Symbol = "circle" } },
                })
                .SetSeries(series);

            return chart;
        }

        public Highcharts GetOracleCounterGuage(int chartType)
        {
            if (_oracleCursor == null)
                _oracleCursor = new ServiceProxy().Channel.GetBankCursorStatus();

            int val = _oracleCursor.OrderByDescending(x => x.Cursor.Value).FirstOrDefault().Cursor.Value;

            Highcharts chart = new Highcharts(chartType == 2 ? "OracleCounterGuage" : "OracleCounterGuageBig")
                .SetCredits(new Credits { Enabled = false })
                .InitChart(new Chart
                {
                    Type = ChartTypes.Gauge,
                    PlotBackgroundColor = null,
                    PlotBackgroundImage = null,
                    PlotBorderWidth = 0,
                    PlotShadow = false
                })
                .SetTitle(new Title { Style = chartType == 2 ? "display: 'none'" : "", Text = "Oracle Cursor Value" })
                .SetPane(new Pane
                {
                    StartAngle = -150,
                    EndAngle = 150,
                    Background = new[]
                            {
                                new BackgroundObject
                                    {
                                        BackgroundColor = new BackColorOrGradient(new Gradient
                                            {
                                                LinearGradient = new[] { 0, 0, 0, 1 },
                                                Stops = new object[,] { { 0, "#FFF" }, { 1, "#333" } }
                                            }),
                                        BorderWidth = new PercentageOrPixel(0),
                                        OuterRadius = new PercentageOrPixel(109, true)
                                    },
                                new BackgroundObject
                                    {
                                        BackgroundColor = new BackColorOrGradient(new Gradient
                                            {
                                                LinearGradient = new[] { 0, 0, 0, 1 },
                                                Stops = new object[,] { { 0, "#333" }, { 1, "#FFF" } }
                                            }),
                                        BorderWidth = new PercentageOrPixel(1),
                                        OuterRadius = new PercentageOrPixel(107, true)
                                    },
                                new BackgroundObject(),
                                new BackgroundObject
                                    {
                                        BackgroundColor = new BackColorOrGradient(ColorTranslator.FromHtml("#DDD")),
                                        BorderWidth = new PercentageOrPixel(0),
                                        OuterRadius = new PercentageOrPixel(105, true),
                                        InnerRadius = new PercentageOrPixel(103, true)
                                    }
                            }
                })
                .SetYAxis(new YAxis
                {
                    Min = 0,
                    Max = 400,

                    //MinorTickInterval = "auto",
                    MinorTickWidth = 1,
                    MinorTickLength = 10,
                    MinorTickPosition = TickPositions.Inside,
                    MinorTickColor = ColorTranslator.FromHtml("#666"),
                    TickPixelInterval = 30,
                    TickWidth = 2,
                    TickPosition = TickPositions.Inside,
                    TickLength = 10,
                    TickColor = ColorTranslator.FromHtml("#666"),
                    Labels = new YAxisLabels
                    {
                        Step = 2,
                        //Rotation = "auto"
                    },
                    Title = new YAxisTitle { Text = "Today" },
                    PlotBands = new[]
                            {
                                new YAxisPlotBands { From = 0, To = 300, Color = ColorTranslator.FromHtml("#55BF3B") },
                                new YAxisPlotBands { From = 300, To = 350, Color = ColorTranslator.FromHtml("#DDDF0D") },
                                new YAxisPlotBands { From = 350, To = 400, Color = ColorTranslator.FromHtml("#DF5353") }
                            }
                })
                .SetSeries(new Series
                {
                    Name = "Speed",
                    Data = new Data(new object[] { val })
                });

            return chart;
        }

        public Highcharts CreatePassportChart(int chartType, string[] timespan, Series[] series, bool isBig, bool isBarChart)
        {
            Highcharts chart = new Highcharts(chartType == 2 && !isBig ? "PassportReport" : chartType == 2 && isBig ? "BigAfisActivity" : chartType == 1 ? "Top4BanksPassportVerificationLine" : "BankPassportVerification")
                .SetCredits(new Credits { Enabled = false })
                .InitChart(new Chart
                {
                    DefaultSeriesType = isBarChart ? ChartTypes.Column : ChartTypes.Line
                    ,
                    MarginRight = chartType == 1 ? 130 : chartType == 3 || isBarChart || isBig ? 170 : 0,
                    ClassName = chartType == 1 ? "chart" : ""
                })
                .SetTitle(new Title { Style = chartType == 2 ? "display: 'none'" : "", Text = chartType == 1 ? "Passports Report" : "Passports Report Over The Last Year" })
                .SetSubtitle(new Subtitle { Style = chartType == 2 ? "" : "display:'none'", Text = "Top 4 Banks" })
                .SetXAxis(new XAxis { Categories = timespan, Min = 0, Title = new XAxisTitle { Text = chartType == 2 && !isBarChart ? null : isBarChart ? "Year" : timespan.Length == 12 ? "Month" : "Date" } })
                .SetYAxis(new YAxis
                {
                    Min = 0,
                    Title = new YAxisTitle { Text = "Number of Passports" }
                })
                .SetLegend(new Legend
                {
                    Layout = chartType == 2 ? Layouts.Horizontal : Layouts.Vertical,
                    Align = chartType == 2 ? HorizontalAligns.Center : chartType == 3 ? HorizontalAligns.Right : HorizontalAligns.Right,
                    VerticalAlign = chartType == 2 ? VerticalAligns.Top : VerticalAligns.Top,
                    X = chartType == 2 ? 0 : chartType == 3 ? -10 : -10,
                    Y = chartType == 2 ? -5 : chartType == 3 ? 100 : 100,
                    Floating = true,
                    BackgroundColor = new BackColorOrGradient(ColorTranslator.FromHtml("#FFFFFF")),
                    Shadow = true,
                    MaxHeight = 154,
                    Enabled = !_hideLegend,
                    Padding = chartType == 2 ? 10 : 20,
                    ItemStyle = "fontSize: '14px'"
                })
                .SetPlotOptions(new PlotOptions
                {
                    Column = new PlotOptionsColumn
                    {
                        BorderWidth = 0
                    },
                    Line = new PlotOptionsLine { Marker = new PlotOptionsLineMarker() { Enabled = true, Symbol = "circle" } },
                })
                .SetSeries(series);

            return chart;
        }

        public Highcharts CreateChart(int chartType, string[] timespan, Series[] series)
        {
            Highcharts chart = new Highcharts(chartType == 1 ? "Top4BanksPassportVerification" : chartType == 2 ? "Top4BanksPassportVerificationLine" : "BankPassportVerification")
                .SetCredits(new Credits { Enabled = false })
                .InitChart(new Chart
                {
                    DefaultSeriesType = chartType == 1 ? ChartTypes.Column : ChartTypes.Line
                    ,
                    MarginRight = chartType == 1 ? 130 : chartType == 3 ? 170 : 0,
                    ClassName = chartType == 1 ? "chart" : ""
                })
                .SetTitle(new Title { Style = chartType == 2 ? "display: 'none'" : "", Text = chartType == 3 ? "Passport Verifications" : "Passport Verifications Over The Last Year" })
                .SetSubtitle(new Subtitle { Style = chartType == 2 ? "" : "display:'none'", Text = "Top 4 Banks" })
                .SetXAxis(new XAxis { Categories = timespan, Min = 0, Title = new XAxisTitle { Text = chartType == 2 ? null : timespan.Length == 12 ? "Month" : "Date" } })
                .SetYAxis(new YAxis
                {
                    Min = 0,
                    Title = new YAxisTitle { Text = "Passport Verifications" }
                })
                .SetLegend(new Legend
                {
                    Layout = chartType == 2 ? Layouts.Horizontal : Layouts.Vertical,
                    Align = chartType == 2 ? HorizontalAligns.Center : chartType == 3 ? HorizontalAligns.Right : HorizontalAligns.Right,
                    VerticalAlign = chartType == 2 ? VerticalAligns.Top : VerticalAligns.Top,
                    X = chartType == 2 ? 0 : chartType == 3 ? -10 : -10,
                    Y = chartType == 2 ? -5 : chartType == 3 ? 100 : 100,
                    Floating = true,
                    BackgroundColor = new BackColorOrGradient(ColorTranslator.FromHtml("#FFFFFF")),
                    Shadow = true,
                    MaxHeight = 154,
                    Enabled = !_hideLegend,
                    Padding = chartType == 2 ? 10 : 20,
                    ItemStyle = "fontSize: '14px'"
                })
                .SetPlotOptions(new PlotOptions
                {
                    Column = new PlotOptionsColumn
                    {
                        BorderWidth = 0
                    },
                    Line = new PlotOptionsLine { Marker = new PlotOptionsLineMarker() { Enabled = true, Symbol = "circle" } },
                })
                .SetSeries(series);

            return chart;
        }

        public string GetBanksPassportVerificationData()
        {
            string result = string.Empty;

            // All data from the beginning
            if (_verificationreports == null)
                _verificationreports = new ServiceProxy().Channel.GetPassportVerificationHistory(new DateTime(2013, 1, 1), DateTime.Now, false);

            List<Report> reports = _verificationreports.Where(x => x.ViewDate >= DateTime.Now.AddMonths(-1) && x.ViewDate <= DateTime.Now).ToList();

            Dictionary<string, int> total = new Dictionary<string, int>();

            Dictionary<string, int> today = GetBanksPassportVerificationData(0, 0, reports);
            total = today;

            Dictionary<string, int> last5Days = GetBanksPassportVerificationData(0, 7, reports);
            if (last5Days.Count > total.Count) total = last5Days;

            Dictionary<string, int> prev5Days = GetBanksPassportVerificationData(8, 14, reports);
            if (prev5Days.Count > total.Count) total = prev5Days;

            foreach (KeyValuePair<string, int> val in total)
            {
                if (result == string.Empty)
                {
                    result = "[['" + val.Key.ToUpper() + "'," + (today.ContainsKey(val.Key) ? today[val.Key] : 0) + "," + (last5Days.ContainsKey(val.Key) ? last5Days[val.Key] : 0) + "," + (prev5Days.ContainsKey(val.Key) ? prev5Days[val.Key] : 0) + "]";
                }
                else
                {
                    result += ",['" + val.Key.ToUpper() + "'," + (today.ContainsKey(val.Key) ? today[val.Key] : 0) + "," + (last5Days.ContainsKey(val.Key) ? last5Days[val.Key] : 0) + "," + (prev5Days.ContainsKey(val.Key) ? prev5Days[val.Key] : 0) + "]";
                }
            }

            return result + "]";
        }

        private ChartItem GetPassports5DaysData()
        {
            ChartItem item = new ChartItem();
            var jsonList = new List<object>();

            jsonList.AddMany(new { sTitle = "Bank" }, new { sTitle = "Today" }, new { sTitle = "Last 5 days" }, new { sTitle = "prior 5 days" });
            item.TableColumns = new JavaScriptSerializer().Serialize(jsonList);
            item.TableData = GetBanksPassportVerificationData();
            return item;
        }

        public async Task<DashboardModel> GetPingdomChecks(DashboardModel model)
        {
            PingdomItem item = new PingdomItem();
            var client = new WebClient();
            client.Credentials = new System.Net.NetworkCredential("gielvn@exarotech.com", "exaro123");
            client.Headers.Add("App-Key", "11p9cs7a9gl57d8zn9w0vngzgzsp6gii");
            string checkid = "537635";
            string url = "https://api.pingdom.com/api/2.0/";

            // Get uptime data
            string parameters = "?includeuptime=true&from=" + GetUnixTimestamp(DateTime.Now.AddMonths(-1));
            string action = url + "summary.average/" + checkid + parameters;
            string data = await client.DownloadStringTaskAsync(action);
            dynamic stuff = JObject.Parse(data);

            item.UptimePercent = double.Parse((string)stuff["summary"]["status"]["totalup"]) / (double.Parse((string)stuff["summary"]["status"]["totalup"]) + double.Parse((string)stuff["summary"]["status"]["totaldown"])) * 100;
            TimeSpan span = TimeSpan.FromSeconds(int.Parse((string)stuff["summary"]["status"]["totaldown"]));

            // Round up minutes
            int myminutes = span.Minutes;
            if (span.Seconds > 30)
                myminutes++;
            if (span.Days > 0)
            {
                if (span.Hours > 0)
                    item.DownTime = string.Format("{0}d {1}h {2:00}m", span.Days, span.Hours, myminutes);
                else
                    item.DownTime = string.Format("{0}d {1:00}m", span.Days, myminutes);
            }
            else
                item.DownTime = string.Format("{0}h {1:00}m", span.Hours, myminutes);

            // Get downtime details
            parameters = "?from=" + GetUnixTimestamp(DateTime.Now.AddMonths(-1));
            action = url + "summary.outage/" + checkid + parameters;
            data = await client.DownloadStringTaskAsync(action);
            stuff = JObject.Parse(data);

            item.ListItems = new List<PingdomListItem>();

            foreach (var x in stuff["summary"]["states"])
            {
                PingdomListItem litem = new PingdomListItem();
                DateTime from = GetDateFromUnix(int.Parse(x["timefrom"].ToString()));
                DateTime to = GetDateFromUnix(int.Parse(x["timeto"].ToString()));

                litem.Status = Helper.ToFirstLetterUpper(x["status"].ToString());
                litem.From = from.ToString("dd/MM/yyyy hh:mm:tt").ToUpper();
                litem.To = to.ToString("dd/MM/yyyy hh:mm:tt").ToUpper();
                TimeSpan diff = to - from;

                if (diff.Days > 0)
                {
                    if (diff.Hours == 0)
                    {
                        litem.Duration = string.Format(
                               CultureInfo.CurrentCulture,
                               "{0}d {1}m",
                               diff.Days,
                               diff.Minutes);
                    }
                    else
                    {
                        litem.Duration = string.Format(
                               CultureInfo.CurrentCulture,
                               "{0}d {1}h {2}m",
                               diff.Days,
                               diff.Hours,
                               diff.Minutes);
                    }
                }

                else if (diff.Hours > 0)
                {
                    litem.Duration = string.Format(
                           CultureInfo.CurrentCulture,
                           "{0}h {1}m",
                           diff.Hours,
                           diff.Minutes);
                }

                else if (diff.Minutes > 0)
                {
                    litem.Duration = string.Format(
                           CultureInfo.CurrentCulture,
                           "{0}m",
                           diff.Minutes);
                }

                else
                {
                    litem.Duration = string.Format(
                           CultureInfo.CurrentCulture,
                           "{0}s",
                           diff.Seconds);
                }

                item.ListItems.Add(litem);
            }

            item.NumberOfDownTimes = item.ListItems.Where(x => x.Status == "Down").Count();
            model.PingdomItem = item;

            return model;
        }

        private void AddPassportViewer(DashboardModel model)
        {
            model.Reports = new Reports();
            model.Reports.ReportsList = new List<Report>();
            model.Reports.NoReportsMessage = string.Empty;

            if (_verificationreports == null)
            {
                try
                {
                    DateTime @from = DateTime.Now.Date;
                    DateTime to = DateTime.Now.Date.AddDays(1);

                    model.Reports.ReportsList = new ServiceProxy().Channel.GetPassportVerificationHistory(@from, to, true);
                    Session["CurrentReport"] = model.Reports.ReportsList;
                }
                catch (Exception ex)
                {
                    throw;
                }
            }
            else
            {
                model.Reports.ReportsList = _verificationreports.Where(x => x.ViewDate.Date == DateTime.Now.Date).ToList();
                Session["CurrentReport"] = model.Reports.ReportsList;
            }
        }

        private Dictionary<string, int> GetBanksPassportVerificationData(int start, int days, List<Report> reports)
        {
            Dictionary<string, int> bankcount = new Dictionary<string, int>();

            if (start == 0 && days == 0)
            {
                foreach (var line in reports.Where(info => info.ViewDate.Date == DateTime.Now.Date)
                            .GroupBy(info => info.Bank.BankName)
                            .Select(group => new
                            {
                                BankName = group.Key,
                                Count = group.Count()
                            })
                            .OrderByDescending(x => x.Count))
                {
                    bankcount.Add(line.BankName, line.Count);
                }
            }
            else
            {
                foreach (var line in reports.Where(info => info.ViewDate <= DateTime.Now.AddDays(-start + 1) && info.ViewDate >= DateTime.Now.AddDays(-days))
                            .GroupBy(info => info.Bank.BankName)
                            .Select(group => new
                            {
                                BankName = group.Key,
                                Count = group.Count()
                            })
                            .OrderByDescending(x => x.Count))
                {
                    bankcount.Add(line.BankName, line.Count);
                }
            }

            return bankcount;
        }

        #endregion

        #endregion

        #region Json events

        public ActionResult UpdatePassportAll(string type, string year, string month)
        {
            DashboardModel model = new DashboardModel();

            ChartItem item = new ChartItem();

            if (type == "3")
            {
                if (!string.IsNullOrEmpty(month))
                {
                    if (int.Parse(month) == 0)
                    {
                        item.Chart = GetTop4BanksPassportVerification(int.Parse(type), ((DateTime.Now.Year + 1 - int.Parse(year)) * 12), ((DateTime.Now.Year + 1 - int.Parse(year)) * 12) - 12);
                    }
                    else
                    {
                        item.Chart = GetTop4BanksPassportVerification(int.Parse(type), ((DateTime.Now.Year - int.Parse(year)) * 12) - int.Parse(month) + DateTime.Now.Month, ((DateTime.Now.Year - int.Parse(year)) * 12) - int.Parse(month) + DateTime.Now.Month - 1);
                    }
                }

                return Json(new { success = 1, page = JsonPartialView(this, "ChartTemplate", item.Chart) }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { success = 0 }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult UpdateEnrollVsIssue(string type, string year, string month)
        {
            DashboardModel model = new DashboardModel();

            ChartItem item = new ChartItem();

            if (type == "3")
            {
                if (!string.IsNullOrEmpty(month))
                {
                    if (int.Parse(month) == 0)
                    {
                        // Show for whole year. Means a bit of annoying mathematics to fit our structure but meh
                        if (int.Parse(year) != DateTime.Now.Year)
                            item.Chart = AddEnrollVsIssueChart(true, (((DateTime.Now.Year + 1 - int.Parse(year)) * 12) - (13 - DateTime.Now.Month)), (((DateTime.Now.Year + 1 - int.Parse(year)) * 12) - (24 - DateTime.Now.Month)), false, false);
                        else
                            item.Chart = AddEnrollVsIssueChart(true, ((DateTime.Now.Year + 1 - int.Parse(year)) * 12), ((DateTime.Now.Year + 1 - int.Parse(year)) * 12) - 12, true, false);
                    }
                    else
                    {
                        if (int.Parse(month) == 100)
                        {
                            // Till date
                            item.Chart = AddEnrollVsIssueChart(true, (((DateTime.Now.Year + 2 - int.Parse(year)) * 12) - (24 - DateTime.Now.Month)), ((DateTime.Now.Month)), false, true);
                        }
                        else
                            item.Chart = AddEnrollVsIssueChart(true, ((DateTime.Now.Year - int.Parse(year)) * 12) - int.Parse(month) + DateTime.Now.Month, ((DateTime.Now.Year - int.Parse(year)) * 12) - int.Parse(month) + DateTime.Now.Month - 1, true, false);
                    }
                }

                return Json(new { success = 1, page = JsonPartialView(this, "ChartTemplate", item.Chart) }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { success = 0 }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult GetSliderPanel()
        {
            DashboardModel model = new DashboardModel();
            model.Charts = new List<ChartItem>();

            // Get default data in one query
            GetSliderData();

            model.Charts.Add(new ChartItem() { Chart = GetBanksVerificationPieChart(1), ChartName = "BigPieChart", ChartType = "PieChartTemplate" });
            model.Charts.Add(new ChartItem() { Chart = GetTop4BanksPassportVerification(3, 12, 1), ChartName = "PassportVerificationAll", ChartType = "ChartTemplate" });
            model.Charts.Add(new ChartItem() { Chart = GetTop4BanksUserRegistration(1, 12, 0), ChartName = "BankUsersChart", ChartType = "ChartTemplate" });
            model.Charts.Add(new ChartItem() { Chart = GetTop4BanksPassportVerification(1, 12, 1), ChartName = "PassportVerificationBar", ChartType = "ChartTemplate" });
            model.Charts.Add(new ChartItem() { Chart = GetOracleCounterGuage(1), ChartName = "OracleCounterChart", ChartType = "ChartTemplate" });

            GetDashboardBaseData(model, false);
            AddPassportViewer(model);
            ChartItem item = GetPassports5DaysData();

            return Json(new
            {
                success = 1,
                BigPieChart = JsonPartialView(this, "ChartTemplate", model.Charts[0].Chart),
                SmallPieChart = JsonPartialView(this, "ChartTemplate", GetBanksVerificationPieChart(2)),
                PassportVerificationAll = JsonPartialView(this, "ChartTemplate", model.Charts[1].Chart),
                SmallPassportVerificationAll = JsonPartialView(this, "ChartTemplate", GetTop4BanksPassportVerification(2, 12, 1)),
                BankUsersChart = JsonPartialView(this, "ChartTemplate", model.Charts[2].Chart),
                SmallBankUsersChart = JsonPartialView(this, "ChartTemplate", GetTop4BanksUserRegistration(2, 12, 1)),
                PassportVerificationBar = JsonPartialView(this, "ChartTemplate", model.Charts[3].Chart),
                OracleCounterChart = JsonPartialView(this, "ChartTemplate", model.Charts[4].Chart),
                SmallOracleCounterChart = JsonPartialView(this, "ChartTemplate", GetOracleCounterGuage(2)),
                PassportsViewedToday = JsonPartialView(this, "_MiniPassportReport", model.Reports),
                Banks5dayData = item.TableData,
                page = JsonPartialView(this, "_SliderPanel", model)
            }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult UpdatePassportData(string dobigupdate)
        {
            try
            {
                DashboardModel model = new DashboardModel();

                if (dobigupdate == "0")
                {
                    AddAfisActivityData(model);
                    AddSummaryData(model);

                    return Json(new
                    {
                        success = 1,
                        AfisActivityData = JsonPartialView(this, "_DataTemplateAfisActivity", model.AfisMonitor),
                        SummaryData = JsonPartialView(this, "SummaryTemplate", model.Summary)
                    }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    model.MultiCharts = new List<ChartItem>();
                    model.MultiCharts.AddRange(GetDefaultAfisChartData(true));

                    AddAfisActivityData(model);
                    AddSummaryData(model);

                    return Json(new
                    {
                        success = 1,
                        PassportReportsBig = JsonPartialView(this, "ChartTemplate", model.MultiCharts[0].Chart),
                        BigAfisBranchChart = JsonPartialView(this, "ChartTemplate", model.MultiCharts[1].Chart),
                        AfisActivity = JsonPartialView(this, "ChartTemplate", model.MultiCharts[2].Chart),
                        AfisActivityData = JsonPartialView(this, "_DataTemplateAfisActivity", model.AfisMonitor),
                        SummaryData = JsonPartialView(this, "SummaryTemplate", model.Summary),
                        page = JsonPartialView(this, "_PassportSliderPanel", model)
                    }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    success = 0,
                    message = ex.Message
                }, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        #region Operations

        public int MathMod(int a, int b)
        {
            return ((a % b) + b) % b;
        }

        public int GetUnixTimestamp(DateTime datetime)
        {
            return (int)(datetime.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;
        }

        private static DateTime GetDateFromUnix(int unixTimeStamp)
        {
            DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
            return dtDateTime.AddSeconds(unixTimeStamp).ToLocalTime();
        }

        private void GetDashboardBaseData(DashboardModel model, bool isPassport)
        {
            // List arrays
            var jsonList = new List<object>();
            string[] names = DateTimeFormatInfo.CurrentInfo.MonthNames;

            jsonList.Add(new
            {
                text = "Full Year",
                id = "0"
            });

            if (isPassport)
            {
                jsonList.Add(new
                {
                    text = "Till Date",
                    id = "100"
                });
            }

            for (int i = 0; i < names.Length; i++)
            {
                if (!string.IsNullOrEmpty(names[i]))
                {
                    jsonList.Add(new
                    {
                        text = names[i],
                        id = i + 1,
                    });
                }
            }

            model.MonthArray = new JavaScriptSerializer().Serialize(jsonList);

            jsonList = new List<object>();

            for (int i = isPassport ? 2007 : 2013; i < DateTime.Now.Year + 1; i++)
            {
                jsonList.Add(new
                {
                    text = i.ToString(),
                    id = i,
                });
            }

            model.YearArray = new JavaScriptSerializer().Serialize(jsonList);
        }

        public string JsonPartialView(Controller controller, string viewName, object model)
        {
            controller.ViewData.Model = model;

            using (var sw = new StringWriter())
            {
                var viewResult = ViewEngines.Engines.FindPartialView(controller.ControllerContext, viewName);
                var viewContext = new ViewContext(controller.ControllerContext, viewResult.View, controller.ViewData, controller.TempData, sw);

                viewResult.View.Render(viewContext, sw);
                viewResult.ViewEngine.ReleaseView(controller.ControllerContext, viewResult.View);

                return sw.ToString();
            }
        }

        #endregion

    }
}
