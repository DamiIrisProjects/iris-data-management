﻿using System.Collections.Generic;

namespace IrisDataManagement.Models
{
    public class DashboardModel
    {
        public List<ChartItem> Charts { get; set; }
        public List<ChartItem> MultiCharts { get; set; }

        public PingdomItem PingdomItem { get; set; }

        public DataTemplateInfo Last7DaysActivity { get; set; }

        public DataTemplateInfo AfisMonitor { get; set; }

        public DataTemplateInfo SocketworksMonitor { get; set; }

        public List<IrisCommon.Entities.SocketWorksEntity> SocketworksToday { get; set; }

        public IrisCommon.Entities.SocketWorksSummary SocketworksSummary { get; set; }

        public DataTemplateInfo BranchViewer{ get; set; }

        public DataTemplateInfo BranchViewerForeign { get; set; }

        public SummaryInfo Summary { get; set; }

        public List<string> SelectedBanks { get; set; }
        public Reports Reports { get; set; }
        public List<string> Banks { get; set; }

        public string ITotal { get; set; }

        public string ETotal { get; set; }

        public string RTotal { get; set; }

        public string MonthArray { get; set; }
        public string YearArray { get; set; }
        public string PopupMsg { get; set; }

        public int Dropdown { get; set; }
    }
}