﻿using System.ComponentModel.DataAnnotations;
using IrisCommon.Entities;

namespace IrisDataManagement.Models
{
    public class RegisterExternalLoginModel
    {
        [Required]
        [Display(Name = "User name")]
        public string UserName { get; set; }

        public string ExternalLoginData { get; set; }
    }

    public class LocalPasswordModel
    {
        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Current password")]
        public string OldPassword { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "New password")]
        public string NewPassword { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm new password")]
        [System.Web.Mvc.Compare("NewPassword", ErrorMessage = "The new password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }
    }

    public class RegisterModel
    {
        [IsRequired(ErrorMessage = "Please enter your first name.", Default = "First name")]
        [StringLength(70, ErrorMessage = "Your {0} must be at least {2} characters long (max 50).", MinimumLength = 1)]
        public string FirstName { get; set; }

        [IsRequired(ErrorMessage = "Please enter your surname.", Default = "Last name")]
        [StringLength(70, ErrorMessage = "Your {0} must be at least {2} characters long (max 50).", MinimumLength = 1)]
        public string Surname { get; set; }

        [IsRequired(ErrorMessage = "Please enter an email address.")]
        [StringLength(70, ErrorMessage = "Your {0} must be at least {2} characters long (max 70).", MinimumLength = 5)]
        public string EmailAddress { get; set; }

        [Required(ErrorMessage = "Please confirm your email address.")]
        [System.Web.Mvc.Compare("EmailAddress", ErrorMessage = "The email entered and confirmation email do not match.")]
        public string EmailAddressConfirm { get; set; }

        [Required(ErrorMessage = "Please enter your password.")]
        [StringLength(15, ErrorMessage = "Your {0} must be at least {2} characters long.", MinimumLength = 4)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [Required(ErrorMessage = "Please confirm your password.")]
        [System.Web.Mvc.Compare("Password", ErrorMessage = "The password entered and confirmation password do not match.")]
        public string PasswordConfirm { get; set; }

        public bool IsTrue
        { get { return true; } }

        //[Required]
        //[Compare("isTrue", ErrorMessage = "You must read the Terms & Conditions and accept them to proceed")]
        public bool AgreeToTerms { get; set; }

        [Required(ErrorMessage = "Please enter your phone number.")]
        public string PhoneNumber { get; set; }

        public bool LoggedIn { get; set; }
        public string ReturnUrl { get; set; }
        public string ErrorMsg { get; set; }
        public string CurrentJobRef { get; set; }
        public string Captcha { get; set; }
        public string CaptchaVal { get; set; }
        public string CaptchaErrorMsg { get; set; }
        public string Error { get; set; }

        // Registration
        public Person Registration { get; set; }
    }

    public class LoginModel
    {
        [RegularExpression(@"^(([^<>()[\]\\.,;:\s@\""]+(\.[^<>()[\]\\.,;:\s@\""]+)*)|(\"".+\""))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$",
            ErrorMessage = "The email you have entered is invalid")]
        [IsRequired(ErrorMessage = "Please enter an email address.", Default = "Your Email address")]
        [StringLength(70, ErrorMessage = "Your {0} must be at least {2} characters long (max 70).", MinimumLength = 5)]
        public string EmailAddress { get; set; }

        [IsRequired(ErrorMessage = "Please enter your password.", Default = "Enter Password")]
        [StringLength(15, ErrorMessage = "Your {0} must be at least {2} characters long.", MinimumLength = 4)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        public string FirstName { get; set; }
        public string Surname { get; set; }
        public string ReturnUrl { get; set; }
        public string CurrentJobRef { get; set; }
        public string ErrorMsg { get; set; }
        public bool IsPopup { get; set; }
    }

    public class IsRequired : ValidationAttribute
    {
        public string Value { get; set; }
        public string Default { get; set; }
        public override bool IsValid(object value)
        {
            if (value == null || value.ToString() == string.Empty || Value == Default)
                return false;

            return true;
        }
    }

    public class ExternalLogin
    {
        public string Provider { get; set; }
        public string ProviderDisplayName { get; set; }
        public string ProviderUserId { get; set; }
    }
}
