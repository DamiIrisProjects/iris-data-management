﻿using IrisCommon.Entities;
using System.Collections.Generic;

namespace IrisDataManagement.Models
{
    public class SummaryInfo
    {
        public string PendingTotal { get; set; }

        public string ApprovedTotal { get; set; }

        public string DroppedTotal { get; set; }

        public string RejectedTotal { get; set; }

        public string AfisGrandTotal { get; set; }

        public string IssuedTotal { get; set; }

        public string EnrolledTotal { get; set; }

        public int NumberOfBranches { get; set; }

        public List<Pending> PendingItems { get; set; }
    }
}