﻿using System.Collections.Generic;
using System.Web.Mvc;
using IrisCommon.Entities;

namespace IrisDataManagement.Models
{
    public class Search
    {
        public SelectList TierOneList { get; set; }
        public SelectList TierTwoList { get; set; }
        public SelectList TierThreeList { get; set; }
        public SelectList PageSizeList { get; set; }   

        public string IsDesc { get; set; }
        public string CurrentSort { get; set; }
        public string NoJobsMessage { get; set; }

        public string TierOne { get; set; }
        public string TierOneArray { get; set; }
        public string TierOneQuestion { get; set; }
        public string TierTwo { get; set; }
        public string TierTwoArray { get; set; }
        public string TierTwoQuestion { get; set; }
        public string TierThree { get; set; }
        public string TierThreeArray { get; set; }
        public string TierThreeQuestion { get; set; }
        public string TierFour { get; set; }
        public string TierFourArray { get; set; }
        public string TierFourQuestion { get; set; }
        public string TierFive { get; set; }
        public string TierFiveArray { get; set; }
        public string TierFiveQuestion { get; set; }

        public string IrisSubcats { get; set; }
        public string SelectedIrisSub { get; set; }
        public string IrisClientArray { get; set; }

        public string IrisClientSubArray { get; set; }
        public string SelectedClient { get; set; }
        public string SelectedIrisClientSub { get; set; }
        public string LevelsArray { get; set; }
        public string FileTypes { get; set; }
        public string FileTypeArray { get; set; }
        public string StaffArray { get; set; }
        public string StaffIdArray { get; set; }
        public string StaffIdAllArray { get; set; }
        public string SelectedStaff { get; set; }
        public string DepartmentArray { get; set; }
        public string PositionArray { get; set; }
        public string GenderArray { get; set; }

        public List<Document> DocList { get; set; }

        public string Searchstring { get; set; }
        public string ViewDoc { get; set; }
        public string ViewType { get; set; }
        public string Message { get; set; }
        public string UploadDateFrom { get; set; }
        public string UploadDateTo { get; set; }
        public string DocDateFrom { get; set; }
        public string DocDateTo { get; set; }
        public string Error { get; set; }
        public bool IsDocsPage { get; set; }
        public bool ShowSearchMessage { get; set; }
        public bool ShowJobSearchDiv { get; set; }
        public int ClearAll { get; set; }
        public int MyLevel { get; set; }
        public int PageCount { get; set; }
        public string PageSize { get; set; }
        public int NowViewing { get; set; }
        public int JobListCount { get; set; }
        public int CurrentUserId { get; set; }

        public int CurrentDeptId { get; set; }
        
        public byte[] DocTempImage { get; set; }
    }
}