﻿using System.Collections.Generic;

namespace IrisDataManagement.Models
{
    public class DataTemplateInfo
    {
        public string Header1 { get; set; }
        public string Header2 { get; set; }
        public string Header3 { get; set; }
        public string Header4 { get; set; }

        public string Header5 { get; set; }

        public string Header6 { get; set; }

        public string Header7 { get; set; }

        public string Header8 { get; set; }

        public string Header9 { get; set; }

        public int NumberOfCols { get; set; }

        public List<DataTemplateItem> DataItems { get; set; }

        public bool IsLocal { get; set; }
    }

    public class DataTemplateItem
    {
        public string Item1 { get; set; }

        public string Item2 { get; set; }

        public string Item3 { get; set; }

        public string Item4 { get; set; }

        public string Item5 { get; set; }

        public string Item6 { get; set; }

        public string Item7 { get; set; }

        public string Item8 { get; set; }

        public string Item9 { get; set; }

        public string Item10 { get; set; }

        public string ErrorMessage { get; set; }

        public string Pending { get; set; }

        public bool IsBold { get; set; }

        public string Item5Color { get; set; }

        public string Item6Color { get; set; }

        public int FontSize { get; set; }
    }
}