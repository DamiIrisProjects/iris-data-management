﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
using IrisCommon.Entities;

namespace IrisDataManagement.Models
{
    public class BigModel
    {
        // General
        public string CurrentView { get; set; }
        public string CurrentEmail { get; set; }
        public string ReturnUrl { get; set; }
        public string Message { get; set; }
        public string ErrorMsg { get; set; }
        public string IsAuth { get; set; }
        public string PopupMsg { get; set; }
        public string TabIndex { get; set; }
        public string Password { get; set; }
        public string Guid { get; set; }
        public bool HideMenuOptions { get; set; }
        public bool Flipit { get; set; }
        public bool ShowPasswordChange { get; set; }
        public string RejectionReason { get; set; }
        public int PageCount { get; set; }
        public MyDocuments MyDocs { get; set; }
        public Reports IrisReports { get; set; }
        public Reports AdminReports { get; set; }
        public SelectList PageSizeList { get; set; }
        public string PageSize { get; set; }
        public int NowViewing { get; set; }
        public bool IsAdmin { get; set; }

        [AvalidFile(Allowed = new string[] { ".doc", ".docx", ".pdf", ".zip", ".xls", ".xlsx", ".csv", ".ppt", ".pptx" },
            MaxLength = 1024 * 10000,
            ErrorMessage = "Only Microsoft Word Documents (.doc, .docx) or pdf allowed - Max Size 10MB.")]
        public HttpPostedFileBase File { get; set; }

        [AvalidFile(Allowed = new string[] { ".mp4", ".avi", ".wmv", ".flv", ".mov", ".mpg" },
            MaxLength = 1024 * 1024 * 10,
            ErrorMessage = "Please ensure you have a valid video format - Max Size 10MB.")]
        public HttpPostedFileBase Video { get; set; }

        // Login
        public Person User { get; set; }
        public bool RedirectToAccount { get; set; }
        public bool HideLogin { get; set; }
        public LoginModel LoginModel { get; set; }
        public RegisterModel RegisterModel { get; set; }

        // Search
        public Search Search { get; set; }

        // Calender
        public Calender Calender { get; set; }               
    }

    public class AvalidFileAttribute : ValidationAttribute
    {
        public int MaxLength { get; set; }
        public string[] Allowed { get; set; }
        public override bool IsValid(object value)
        {
            var file = value as HttpPostedFileBase;
            if (file == null)
                return false;//it's always exit here even if the file is valid

            if (file.ContentLength > MaxLength)
                return false;

            if (string.IsNullOrEmpty(file.FileName) && string.IsNullOrWhiteSpace(file.FileName))
                return false;

            if (!Allowed.Contains(file.FileName.Substring(file.FileName.LastIndexOf('.'))))
                return false;

            return true;
        }
    }
}
