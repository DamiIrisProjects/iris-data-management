﻿namespace IrisDataManagement.Models
{
    public class MyDocuments
    {
        public string FolderStructure { get; set; }
        public string CreateFolderStructure { get; set; }
        public string IrisFolderStructure { get; set; }
        public string PrivateRootDirector { get; set; }
        public string PublicRootDirector { get; set; }

        public string SelectedDept { get; set; }
        public string IrisRootDirector { get; set; }
        public string SelectedFolder { get; set; }
        public string CreateSelectedFolder { get; set; }
        public string IrisSelectedFolder { get; set; }
        public string UploadedFile { get; set; }
        public string CreateDocument { get; set; }
        public string DocName { get; set; }
        public string PhysicalLocation { get; set; }
        public string Keywords { get; set; }
        public string DocDate { get; set; }
        public string SelectedStaff { get; set; }
        public string NewFolderName { get; set; }
        public string Level { get; set; }

        public string ToolBars { get; set; }

        public string Options { get; set; }
        
        public Search Search { get; set; }

        public string TierOneOther { get; set; }
        public string TierTwoOther { get; set; }
        public string TierThreeOther { get; set; }

        public bool IsIrisUpload { get; set; }
        public bool IsPublicUpload { get; set; }
    }
}