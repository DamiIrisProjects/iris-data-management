﻿using DotNet.Highcharts;

namespace IrisDataManagement.Models
{
    public class ChartItem
    {
        public string ChartType { get; set; }
        public string ChartName { get; set; }
        public Highcharts Chart { get; set; }

        // Tables
        public string TableColumns { get; set; }
        public string TableData { get; set; }
    }
}