﻿using System.Collections.Generic;
using IrisCommon.Entities;
using System.Web.Mvc;

namespace IrisDataManagement.Models
{
    public class Reports
    {
        public string IsDesc { get; set; }
        public string CurrentSort { get; set; }
        public int PageCount { get; set; }
        public string PageSize { get; set; }
        
        public int NowViewing { get; set; }
        public int NowViewingBankLogin { get; set; }
        public int ReportsListCount { get; set; }

        public List<Report> ReportsList { get; set; }
        public SelectList PageSizeList { get; set; }   
        public string ReportsArray { get; set; }
        public string PassportReportsArray { get; set; }
        public string IrisUsers { get; set; }
        public string ReportType { get; set; }
        public string NoReportsMessage { get; set; }
        public string IntervalStart { get; set; }
        public string IntervalEnd { get; set; }

        public bool IsAdmin { get; set; }
    }
}