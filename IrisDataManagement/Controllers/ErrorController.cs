﻿using IrisDataManagement.Models;
using System.Web.Mvc;

namespace IrisDataManagement.Controllers
{
    public class ErrorController : Controller
    {
        //
        // GET: /Errors/

        public ActionResult Index()
        {
            BigModel model = new BigModel();
            model.HideLogin = true;

            model.ErrorMsg = "An error has occurred!<br/>Sorry for the inconvenience but please try again later!";
            return View(model);
        }

        public ViewResult NotFound()
        {
            BigModel model = new BigModel();
            model.HideLogin = true;

            model.ErrorMsg = "The page you have entered does not exist on iDoc.";
            return View("Index", model);
        }

        public ViewResult NoAccess()
        {
            BigModel model = new BigModel();
            model.HideLogin = true;

            model.ErrorMsg = "You do not have access to view this file.";
            return View("Index", model);
        }
    }
}
