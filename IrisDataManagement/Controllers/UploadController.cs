﻿using System;
using System.Collections.Generic;
using System.Web;
using System.IO;
using IrisCommon.Entities;
using System.Web.Mvc;
using System.Data;
using Irisproxy;
using System.Configuration;

namespace IrisDataManagement.Controllers
{
    [System.Web.Mvc.Authorize]
    public class UploadController : Controller
    {
        private static string _appDir = ConfigurationManager.AppSettings["Server"];

        public JsonResult Upload(string folder, string keywords, string date, string level, string type, string allowusers, string allowdepts, string doclocation)
        {
            // Get a reference to the file that our jQuery sent.  Even with multiple files, they will all be their own request and be the 0 index
            if (Request.Files.Count != 0)
            {
                Person person = Session["CurrentUser"] as Person;
                HttpPostedFileBase file = Request.Files[0];
                List<string> filelist = new List<string>() { ".doc", ".docx", ".pdf", ".jpg", ".jpeg", ".png", ".zip", ".xls", ".xlsx", ".csv", ".ppt", ".pptx" };
                var serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
                Response.ContentType = "text/plain";

                if (file != null && file.ContentLength > 0 && file.ContentLength < 30720000 && filelist.Contains(Path.GetExtension(file.FileName).ToLower()))
                {
                    if (file != null && file.ContentLength > 0)
                    {
                        var fileName = Path.GetFileNameWithoutExtension(file.FileName);
                        var fileNameExt = Path.GetFileName(file.FileName);
                        var path = string.Empty;
                        string levelval = string.Empty;

                        DataSet dset = new ServiceProxy().Channel.GetLists();

                        if (type == "Iris")
                        {
                            path = _appDir + folder.Replace("++", "/") + "/" + fileNameExt;
                            folder = folder.Replace("++", "/");
                        }
                        else
                        {
                            path = _appDir + folder.Replace("++", "/") + "/" + fileNameExt;
                            folder = folder.Replace("++", "/");
                        }

                        for (int i = 0; i < dset.Tables["Levels"].Rows.Count; i++)
                        {
                            DataRow row = dset.Tables["Levels"].Rows[i];

                            if (row["level_id"].ToString() == level)
                            {
                                levelval = row["level_name"].ToString();
                                break;
                            }
                        }

                        DateTime? docdate = null;
                        if (!string.IsNullOrEmpty(date))
                            docdate = DateTime.Parse(date);

                        Document doc = new Document()
                        {
                            DatePosted = DateTime.Now,
                            PhysicalLocation = doclocation,
                            AllowIdString = allowusers,
                            AllowDeptIdString = allowdepts,
                            DocDate = docdate,
                            DocName = fileName,
                            DocType = Path.GetExtension(file.FileName).Replace(".", ""),
                            Level = levelval,
                            Location = folder,
                            Keywords = keywords,
                            User = person
                        };

                        // Save document to database
                        try
                        {
                            int result = new ServiceProxy().Channel.SaveDocument(doc);

                            if (result == 2)
                            {
                                var errorjason = new
                                {
                                    files = new[]
                                    {
                                    new{
                                        error = "File with identical name already exists"
                                        }
                                    },
                                };

                                return Json(errorjason, JsonRequestBehavior.AllowGet);
                            }
                        }
                        catch (Exception ex)
                        {
                            var errorjason = new
                            {
                                files = new[]
                                {
                                new{
                                    error = "Error saving to database: " + ex.Message
                                    }
                                },
                            };

                            return Json(errorjason, JsonRequestBehavior.AllowGet);
                        }

                        new FileInfo(path).Directory.Create();
                        file.SaveAs(path);
                    }

                    var json = new
                    {
                        files = new[]
                        {
                        new{
                            name = file.FileName,
                            size = file.ContentLength
                            }
                        },
                    };

                    return Json(json, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    var json = new
                    {
                        files = new[]
                        {
                        new{
                            error = "Invalid File Type"
                            }
                        },
                    };

                    return Json(json, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                var json = new
                {
                    files = new[]
                        {
                        new{
                            error = "No File Found"
                            }
                        },
                };

                return Json(json, JsonRequestBehavior.AllowGet);
            }
        }       
    }
}
