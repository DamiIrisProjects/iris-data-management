﻿using System.IO;
using System.Web.Mvc;
using ElFinder;
using IrisCommon.Entities;

namespace IrisDataManagement.Controllers
{
    public class FilesController : Controller
    {
        private Connector _connector;

        public Connector Connector
        {
            get
            {
                if (!User.Identity.IsAuthenticated || Session["CurrentUser"] == null)
                {
                    return null;
                }

                Person user = Session["CurrentUser"] as Person;

                if (_connector == null)
                {
                    FileSystemDriver driver = new FileSystemDriver();
                    DirectoryInfo thumbsStorage = new DirectoryInfo(Server.MapPath(Path.Combine("~/Repository/Thumbnails")));
                    DirectoryInfo iris = new DirectoryInfo(Server.MapPath(Path.Combine("~/Repository/Iris")));
                    DirectoryInfo minePublic = new DirectoryInfo(Server.MapPath(Path.Combine("~/Repository/Public")));

                    // Restrict Visibility of some files

                    if (user != null && user.IsAdmin)
                    {
                        driver.AddRoot(new Root(iris)
                        {
                            IsLocked = false, 
                            IsReadOnly = false,
                            IsShowOnly = false,
                            ThumbnailsStorage = thumbsStorage,
                            MaxUploadSizeInMb = 10,
                            ThumbnailsUrl = "Thumbnails/"
                        });
                    }
                    else
                    {
                        driver.AddRoot(new Root(iris)
                        {
                            IsLocked = true,
                            IsReadOnly = true,
                            IsShowOnly = true,
                            Url = "javaScript:void(0);",
                            ThumbnailsStorage = thumbsStorage,
                            MaxUploadSizeInMb = 10,
                            ThumbnailsUrl = "Thumbnails/"
                        });
                    }

                    driver.AddRoot(new Root(minePublic)
                    {
                        Alias = "My Public Folder",
                        IsLocked = false,
                        IsReadOnly = false,
                        IsShowOnly = false,
                        StartPath = minePublic,
                        ThumbnailsStorage = thumbsStorage,
                        MaxUploadSizeInMb = 10,
                        ThumbnailsUrl = "Thumbnails/"
                    });

                    //**** Private Folder Removed for now ****//

                    //driver.AddRoot(new Root(MinePrivate)
                    //{
                    //    Alias = "My Private Folder",
                    //    StartPath = MinePublic,
                    //    ThumbnailsStorage = thumbsStorage,
                    //    MaxUploadSizeInMb = 4,
                    //    ThumbnailsUrl = "Thumbnails/"
                    //});

                    _connector = new Connector(driver); 
                }
                return _connector;
            }
        }
        public ActionResult Index()
        {
            return Connector.Process(HttpContext.Request);
        }

        public ActionResult SelectFile(string target)
        {
            return Json(Connector.GetFileByHash(target).FullName);
        }

        public ActionResult Thumbs(string tmb)
        {
            return Connector.GetThumbnail(Request, Response, tmb);
        }
    }
}
