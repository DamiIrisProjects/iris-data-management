﻿using System;
using System.Web.Mvc;
using System.Web.Security;
using Microsoft.Web.WebPages.OAuth;
using IrisDataManagement.Models;
using System.Web.Routing;
using Irisproxy;
using IrisCommon.Entities;
using IrisCommon.Helpers;

namespace IrisDataManagement.Controllers
{    
    public class AccountController : Controller
    {
        public ActionResult Login(LoginModel model, string returnUrl)
        {
            if (ModelState.IsValid)
            {
                Person user;

                try
                {
                    ServiceProxy proxy = new ServiceProxy();
                    user = proxy.Channel.LoginUser(model.EmailAddress, model.Password);
                }
                catch (Exception ex)
                {
                    new ServiceProxy().Channel.SaveError(Helper.CreateError(ex));

                    return View("Error");
                }

                if (user != null)
                {
                    if (!user.IsActive)
                    {
                        return RedirectToAction("Home", "Home");
                    }
                    else
                    {
                        Session["CurrentUser"] = user;
                        FormsAuthentication.SetAuthCookie(user.FullName, false);

                        if (!string.IsNullOrEmpty(model.ReturnUrl))
                            returnUrl = model.ReturnUrl;

                        if (returnUrl.Contains("~"))
                            return Redirect("~" + returnUrl.Replace("%2f", "/"));

                        if (string.IsNullOrEmpty(returnUrl) || returnUrl == "/UserLogin")
                            returnUrl = "~";
                        returnUrl = returnUrl.Replace("Retry/", "");
                        return Redirect(returnUrl.Replace("%2f", "/"));
                    }
                }

                return RedirectToAction("Home", "Home", new { id = "Retry" });
            }

            return RedirectToAction("Home", "Home", new { id = "Retry" });
        }

        [Authorize]
        [HttpPost]
        [HandleAntiForgeryError]
        [ValidateAntiForgeryToken]
        public ActionResult LogOut(string returnUrl)
        {
            FormsAuthentication.SignOut();
            Session["CurrentUser"] = null;
            return RedirectToAction("Home", "Home");
        }

        #region Helpers

        public class HandleAntiForgeryError : ActionFilterAttribute, IExceptionFilter
        {
            public void OnException(ExceptionContext filterContext)
            {
                var exception = filterContext.Exception as HttpAntiForgeryException;
                if (exception != null)
                {
                    var routeValues = new RouteValueDictionary
                    {
                        ["controller"] = "Home",
                        ["action"] = "Home"
                    };
                    filterContext.Result = new RedirectToRouteResult(routeValues);
                    filterContext.ExceptionHandled = true;
                }
            }
        }
        
        public enum ManageMessageId
        {
            ChangePasswordSuccess,
            SetPasswordSuccess,
            RemoveLoginSuccess,
        }

        internal class ExternalLoginResult : ActionResult
        {
            public ExternalLoginResult(string provider, string returnUrl)
            {
                Provider = provider;
                ReturnUrl = returnUrl;
            }

            public string Provider { get; }
            public string ReturnUrl { get; }

            public override void ExecuteResult(ControllerContext context)
            {
                OAuthWebSecurity.RequestAuthentication(Provider, ReturnUrl);
            }
        }

        #endregion
    }
}
