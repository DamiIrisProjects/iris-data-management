﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using IrisDataManagement.Models;
using System.Web.Security;
using IrisCommon.Entities;
using IrisCommon.Helpers;
using Irisproxy;
using System.Data;
using System.Web.Script.Serialization;
using System.IO;
using System.Drawing;
using IrisDataManagement.Helpers;

namespace IrisDataManagement.Controllers
{
    public class HomeController : Controller
    {
        #region Actions

        public ActionResult Home(string id, string returnUrl, string guid, string usr)
        {
            BigModel model = GetModelDefaultInfo();
            model.RegisterModel = new RegisterModel();
            model.ReturnUrl = returnUrl;

            if (id == "Welcome")
                model.PopupMsg = "<div style='margin-bottom: 10px'>Your registration is now complete.</div><div>Please log in using your Iris email and then enter your password.</div>";
            if (id == "Retry")
                model.PopupMsg = "<div style='margin-bottom: 10px'>Invalid username or password.</div>";
            if (id == "Inactive")
                model.PopupMsg = "<div style='margin-bottom: 10px'>Your account has not been activated.</div><div>Please check your email for the activation message.</div>";
            if (!string.IsNullOrEmpty(guid) && !string.IsNullOrEmpty(usr))
            {
                try
                {
                    int result = new ServiceProxy().Channel.VerifyPasswordChange(usr, guid);

                    if (result == 0)
                    {
                        model.ShowPasswordChange = true;
                        model.CurrentEmail = usr;
                    }

                    if (result == 2)
                    {
                        model.PopupMsg = "<div style='margin-bottom: 10px'>Your request to change password has expired. Please do another request.</div>";
                    }
                }
                catch (Exception ex)
                {
                    throw;
                }
            }

            if (!string.IsNullOrEmpty(returnUrl))
                model.PopupMsg = "<div style='margin-bottom: 10px'>Please login to have access to the repository.</div>";

            return View(model);
        }

        public ActionResult Search()
        {
            if (!User.Identity.IsAuthenticated || Session["CurrentUser"] == null)
            {
                return RedirectToAction("Home", "Home");
            }

            BigModel model = GetModelDefaultInfo();
            model.Search = new Search();

            if (model.User == null)
                return RedirectToAction("Home");

            model.Search.CurrentUserId = model.User.Person_Id;
            model.Search.CurrentDeptId = model.User.Department == string.Empty ? 0 : int.Parse(model.User.Department);
            model.Search.MyLevel = model.User.ClearanceLevel;

            GetSearchInfo(model.Search);
            model.Search.CurrentSort = "PostingDate";
            model.Search.IsDesc = "&#9660;";

            return View(model);
        }
        
        public ActionResult Activation(string email, string id)
        {
            // Lets Activate the User
            if (String.IsNullOrEmpty(id))
            {
                // We do not have the userId. Redirect some where
                return View("Error");
            }
            else
            {
                int exists = 0;
                Guid userId = new Guid(id);

                try
                {
                    ServiceProxy proxy = new ServiceProxy();
                    proxy.Channel.ActivateUser(userId.ToString());
                    return RedirectToAction("Home", new { id = "Welcome" });
                }
                catch (Exception)
                {
                    return View("Error");
                }
            }
        }

        public ActionResult MyDocuments(string id, string msg)
        {
            if (!User.Identity.IsAuthenticated || Session["CurrentUser"] == null)
            {
                return RedirectToAction("Home");
            }
            
            BigModel model = GetModelDefaultInfo();
            model.MyDocs.Search = new Search();
            model.MyDocs.Search.CurrentUserId = model.User.Person_Id;
            model.MyDocs.Search.MyLevel = model.User.ClearanceLevel;
            GetSearchInfo(model.MyDocs.Search);
            AddUserData(model);

            Person user = Session["CurrentUser"] as Person;

            if (!string.IsNullOrEmpty(msg))
            {
                if (msg == "Deleted")
                    model.PopupMsg = "File has been deleted";

                if (msg == "FileNotFound")
                    model.PopupMsg = "Sorry, we can't seem to find your file. Could you upload another please?<br /> Sorry for the inconvenionce.";
            }

            return View(model);
        }

        public ActionResult Reports(string id, string msg)
        {
            if (!User.Identity.IsAuthenticated || Session["CurrentUser"] == null)
            {
                return RedirectToAction("Home");
            }

            BigModel model = GetModelDefaultInfo();
            model.IrisReports = new Reports();
            model.IrisReports.ReportsList = new List<Report>();
            model.IrisReports.CurrentSort = "Date";
            model.IrisReports.IsDesc = "&#9660;";
            model.IrisReports.NoReportsMessage = string.Empty;

            //Am I admin
            Person person = Session["CurrentUser"] as Person;
            AddAdminStuff(model);

            var jsonList = new List<object>();

            jsonList.Add(new { text = "Dashboard", id = 0 });
            jsonList.Add(new { text = "Passport Verificaton History", id = 1 });
            jsonList.Add(new { text = "Bank Login History", id = 2 });
            jsonList.Add(new { text = "Message History (GSM)", id = 3 });
            jsonList.Add(new { text = "Message History (Emails)", id = 4 });
            jsonList.Add(new { text = "Exception Logs", id = 5 });
            jsonList.Add(new { text = "Oracle Cursor Logs", id = 6 });

            model.IrisReports.ReportsArray = new JavaScriptSerializer().Serialize(jsonList);

            // Passport reports
            jsonList = new List<object>();

            jsonList.Add(new { text = "All Branches", id = 0 });
            jsonList.Add(new { text = "Local Branches", id = 1 });
            jsonList.Add(new { text = "Foreign Branches", id = 2 });

            model.IrisReports.PassportReportsArray = new JavaScriptSerializer().Serialize(jsonList);

            // General tab
            jsonList = new List<object>();

            jsonList.Add(new { text = "Staff Details", id = 20 });

            model.IrisReports.IrisUsers = new JavaScriptSerializer().Serialize(jsonList);

            if (!string.IsNullOrEmpty(msg))
            {
                if (msg == "Deleted")
                    model.PopupMsg = "File has been deleted";

                if (msg == "FileNotFound")
                    model.PopupMsg = "Sorry, we can't seem to find your file. Could you upload another please?<br /> Sorry for the inconvenionce.";
            }

            return View(model);
        }

        public ActionResult Calender(string id, string msg)
        {
            Person person = Session["CurrentUser"] as Person;

            if (Session["CurrentUser"] == null)
            {
                FormsAuthentication.SignOut();
                return RedirectToAction("Home");
            }

            BigModel model = GetModelDefaultInfo();
            model.Calender = GetCalenderInfo();

            return View(model);
        }

        public ActionResult MyAccount()
        {
            if (!User.Identity.IsAuthenticated || Session["CurrentUser"] == null)
            {
                return RedirectToAction("Home");
            }

            Person person = Session["CurrentUser"] as Person;
            BigModel model = GetModelDefaultInfo();
            model.Search = new Search();
            GetSearchInfo(model.Search);

            if (!string.IsNullOrEmpty(model.User.ProfilePicture))
            {
                // Add profile picture if it exists
                string loc = Path.Combine(Server.MapPath("~/Repository/Profilepics"), model.User.ProfilePicture);
                if (System.IO.File.Exists(loc))
                {
                    model.User.ProfilePicture = Convert.ToBase64String(System.IO.File.ReadAllBytes(loc));
                }
            }

            return View(model);
        }

        #endregion

        #region Json Actions

        #region My Documents

        public ActionResult UpdateFolderTree(string folder, string location)
        {
            Person person = Session["CurrentUser"] as Person;

            try
            {
                if (!string.IsNullOrEmpty(folder) && !string.IsNullOrEmpty(location))
                {
                    try
                    {
                        string loc = Server.MapPath(Path.Combine("~/Repository", location, folder));
                        if (!Directory.Exists(loc))
                        {
                            // Create folder in location
                            Directory.CreateDirectory(loc);
                        }
                        else
                        {
                            return Json(new { success = 3 }, JsonRequestBehavior.AllowGet);
                        }
                    }
                    catch (Exception ex)
                    {
                        return Json(new { success = 2 }, JsonRequestBehavior.AllowGet);
                    }
                }

                string tree1 = GetFolderStructure(Server.MapPath(Path.Combine("~/Repository/Staff", Helper.ToFirstLetterUpper(person.FirstName) + " " + Helper.ToFirstLetterUpper(person.Surname), "Public")), 1);
                string tree2 = GetFolderStructure(Server.MapPath(Path.Combine("~/Repository/Staff", Helper.ToFirstLetterUpper(person.FirstName) + " " + Helper.ToFirstLetterUpper(person.Surname), "Public")), 2);
                return Json(new { success = 1, tree1 = tree1, tree2 = tree2 }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { success = 0 }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult UpdateCreateFolderTree(string folder, string location)
        {
            Person person = Session["CurrentUser"] as Person;

            try
            {
                if (!string.IsNullOrEmpty(folder) && !string.IsNullOrEmpty(location))
                {
                    try
                    {
                        string loc = Server.MapPath(Path.Combine("~/Repository", location.Replace("__xx", ""), folder));
                        if (!Directory.Exists(loc))
                        {
                            // Create folder in location
                            Directory.CreateDirectory(loc);
                        }
                        else
                        {
                            return Json(new { success = 3 }, JsonRequestBehavior.AllowGet);
                        }
                    }
                    catch (Exception ex)
                    {
                        return Json(new { success = 2 }, JsonRequestBehavior.AllowGet);
                    }
                }

                string tree1 = GetFolderStructure(Server.MapPath(Path.Combine("~/Repository/Staff", Helper.ToFirstLetterUpper(person.FirstName) + " " + Helper.ToFirstLetterUpper(person.Surname), "Public")), 1);
                string tree2 = GetFolderStructure(Server.MapPath(Path.Combine("~/Repository/Staff", Helper.ToFirstLetterUpper(person.FirstName) + " " + Helper.ToFirstLetterUpper(person.Surname), "Public")), 2);
                return Json(new { success = 1, tree1 = tree1, tree2 = tree2 }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { success = 0 }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult IrisUpdateFolderTree(string folder, string location)
        {
            try
            {
                if (!string.IsNullOrEmpty(folder) && !string.IsNullOrEmpty(location))
                {
                    try
                    {
                        string loc = Server.MapPath(Path.Combine("~/Repository/Iris", location, folder));
                        if (!Directory.Exists(loc))
                        {
                            // Create folder in location just in case though we should never have to
                            Directory.CreateDirectory(loc);
                        }
                        else
                        {
                            return Json(new { success = 3 }, JsonRequestBehavior.AllowGet);
                        }
                    }
                    catch (Exception ex)
                    {
                        return Json(new { success = 2 }, JsonRequestBehavior.AllowGet);
                    }
                }

                string tree = GetFolderStructure(Server.MapPath(Path.Combine("~/Repository/Iris")), 1);

                return Json(new { success = 1, tree = tree }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { success = 0 }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult CreateFile(string title, string doc, string folder, string keywords, string level, string allowusers)
        {
            Person person = Session["CurrentUser"] as Person;

            if (person == null)
            {
                return Json(new { success = 3 }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                try
                {
                    System.IO.File.WriteAllText(Server.MapPath(Path.Combine("~/Repository", folder.Replace("__xx", ""), title, ".txt")), doc);

                    // Save document details to database
                    Document document = new Document()
                        {
                            DatePosted = DateTime.Now,
                            AllowIdString = allowusers,
                            DocDate = DateTime.Now,
                            DocName = title,
                            DocType = "txt",
                            Level = level,
                            Location = folder.Replace("|", @"\"),
                            Keywords = keywords,
                            User = person
                        };

                    int result = new ServiceProxy().Channel.SaveDocument(document);

                    // refresh data
                    MyDocuments docs = new MyDocuments();
                    docs.PublicRootDirector = GetFolderStructure(Server.MapPath(Path.Combine("~/Repository/Staff", Helper.ToFirstLetterUpper(person.FirstName) + " " + Helper.ToFirstLetterUpper(person.Surname), "Public")), 1);
                    return Json(new { status = 1, page = JsonPartialView(this, "_CreateDocument", docs) }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    return Json(new { success = 0 }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        #endregion

        #region My Account

        public ActionResult UploadProfilePicture()
        {
            Person person = Session["CurrentUser"] as Person;
            
            if (person == null)
            {
                return Json(new { success = 3 }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                // Read custom attributes
                string fileName = Request["HTTP_X_FILE_NAME"];
                string fileSize = Request["HTTP_X_FILE_SIZE"];
                string fileType = Request["X-File-Type"];

                try
                {
                    // Read input stream from request
                    byte[] buffer = new byte[Request.InputStream.Length];
                    int offset = 0;
                    int cnt = 0;
                    while ((cnt = Request.InputStream.Read(buffer, offset, 10)) > 0)
                    {
                        offset += cnt;
                    }

                    // Save file
                    string loc = person.FirstNameUpper + "_" + person.SurnameUpper + ".jpg";// +fileType.Replace("image/", "");

                    var saveToFileLoc = string.Format("{0}{1}\\{2}",
                                       Server.MapPath(Path.Combine("~/Repository")),
                                       "ProfilePics", loc);

                    using (FileStream fs = new FileStream(saveToFileLoc, FileMode.Create))
                    {
                        fs.Write(buffer, 0, buffer.Length);
                        fs.Flush();
                    }

                    // Save to database
                    new ServiceProxy().Channel.SetProfilePicture(loc, person.Person_Id);

                    // Update session
                    person.ProfilePicture = loc;
                    Session["CurrentUser"] = person;

                    return Json(new { success = 1, img = "data:" + fileType + ";base64," + Convert.ToBase64String(buffer) }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    return Json(new { success = 0 }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        public ActionResult ChangeUserPassword(string password)
        {
            Person person = Session["CurrentUser"] as Person;

            if (person == null)
            {
                return Json(new { success = 3 }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                try
                {
                    new ServiceProxy().Channel.ChangeStaffPassword(password, person.Person_Id);
                    return Json(new { success = 1 }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    return Json(new { success = 0 }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        public ActionResult UpdateUserProfile(string firstname, string surname, string dep, string pos, string phone, string gender, string oldpw, string newpw)
        {
            Person person = Session["CurrentUser"] as Person;

            if (person == null)
            {
                return Json(new { success = 3 }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                try
                {
                    Person pers = new Person();
                    pers.EmailAddress = person.EmailAddress;
                    pers.Person_Id = person.Person_Id;
                    pers.FirstName = firstname;
                    pers.Surname = surname;
                    pers.PhoneNumber = phone;
                    pers.Department = dep;
                    pers.Position = pos;
                    pers.Password = oldpw;
                    pers.Gender = gender;
                    pers.NewPassword = newpw;

                    int result = new ServiceProxy().Channel.UpdateUserInfo(pers);

                    if (result == 1)
                        return Json(new { success = 1 }, JsonRequestBehavior.AllowGet);
                    else
                        return Json(new { success = 2 }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    return Json(new { success = 0 }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        #endregion

        #region Registration

        public ActionResult Register(string firstname, string surname, string email, string phone, string pw)
        {
            if (ModelState.IsValid)
            {
                email = email + "@irissmart.com";
                int result = new ServiceProxy().Channel.VerifyUsernameDoesNotExists(email);

                if (result == 0)
                {
                    Person enrollee = new Person();
                    enrollee.FirstName = firstname;
                    enrollee.Surname = surname;
                    enrollee.EmailAddress = email;
                    enrollee.Password = pw;
                    enrollee.PhoneNumber = phone;

                    try
                    {
                        ServiceProxy proxy = new ServiceProxy();
                        proxy.Channel.RegisterUser(enrollee);
                        return Json(new { status = 0 }, JsonRequestBehavior.AllowGet);
                    }
                    catch (Exception ex)
                    {
                        new ServiceProxy().Channel.SaveError(Helper.CreateError(ex));

                        return View("Error");
                    }

                }
                else if (result == 1)
                {
                    return Json(new { status = 1 }, JsonRequestBehavior.AllowGet);
                }
                else
                    return Json(new { status = 2 }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { status = 3 }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult ResetPassword(string email)
        {
            try
            {
                int result = new ServiceProxy().Channel.ResetUserPassword(email);
                return Json(new { status = result }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { status = 5, message = ex.Message }, JsonRequestBehavior.AllowGet);
            }           
        }

        public ActionResult SaveNewPassword(string password, string email)
        {
            try
            {
                int result = new ServiceProxy().Channel.UpdateForgottenPassword(email, password);
                return Json(new { status = result }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        #endregion

        #region Document Search

        public ActionResult DownloadFile(string docId)
        {
            if (User.Identity.IsAuthenticated && Session["CurrentUser"] != null)
            {
                Person person = Session["CurrentUser"] as Person;

                if (!string.IsNullOrEmpty(docId))
                {
                    ServiceProxy proxy = new ServiceProxy();

                    try
                    {
                        Document doc = proxy.Channel.GetDocument(int.Parse(docId), person.Person_Id, (int)AuditEnum.Download);

                        string filepath = "/" + doc.Location + "/" + doc.DocName + "." + doc.DocType;
                        FileInfo file = new FileInfo(Server.MapPath(Path.Combine("~/Repository", filepath)));
                        return File(file.FullName, doc.DocType, file.Name);
                    }
                    catch (Exception)
                    {
                        return View("Error");
                    }
                }

                return View("Error");
            }
            else
            {
                FormsAuthentication.SignOut();
                return RedirectToAction("Home");
            }
        }

        public ActionResult SearchForDocument(string k, string doctype, string docinnertype, string client, string orderby, 
            string sortby, string cat, string proj, string staffdoc, string updatefrom, string updateto, string docdatefrom, 
            string docdateto, string ext, string page, string pgsize, string isdesc, string prevp, string numj)
        {
            if (User.Identity.IsAuthenticated && Session["CurrentUser"] != null)
            {
                Person person = Session["CurrentUser"] as Person;

                BigModel model = GetModelDefaultInfo();
                model.Search = new Search();
                GetSearchInfo(model.Search);
                if (!string.IsNullOrEmpty(pgsize))
                    model.Search.PageSize = pgsize;
                model.Search.DocList = DoSearch(k, doctype, docinnertype, client, cat, proj, staffdoc, updatefrom, 
                    updateto, docdatefrom, docdateto, ext, person.Person_Id);
                SortMyModel(model.Search, sortby, prevp, numj, @orderby, page, isdesc);
                SetMyPaging(model.Search, page);

                model.Search.CurrentUserId = model.User.Person_Id;
                model.Search.CurrentDeptId = model.User.Department == string.Empty ? 0 : int.Parse(model.User.Department);
                model.Search.MyLevel = person.ClearanceLevel;

                return Json(new { status = 1, page = JsonPartialView(this, "_DocumentSearch", model.Search) }, JsonRequestBehavior.AllowGet);
            }

            return Json(new { status = 3 }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetViewDoc(string docid)
        {
            try
            {
                if (User.Identity.IsAuthenticated && Session["CurrentUser"] != null)
                {
                    var person = Session["CurrentUser"] as Person;
                    if (person != null)
                    {
                        Document doc = new ServiceProxy().Channel.GetDocument(int.Parse(docid), person.Person_Id, (int)AuditEnum.View);
                        Search search = new Search();

                        // Ensure someone isn't trying to pull a fast one - Validate clearance
                        if (int.Parse(doc.Level) <= person.ClearanceLevel)
                        {
                            search.ViewDoc = "/" + doc.Location + "/" + doc.DocName + "." + doc.DocType;
                            search.ViewType = doc.DocType;

                            int testing = 0;

                            // Word documents
                            if (doc.DocType == "doc" || doc.DocType == "docx" || doc.DocType == "xlx" || doc.DocType == "xls" || doc.DocType == "ppt" || doc.DocType == "pptx")
                            {
                                // if pdf version doesnt exist, create it
                                if (!System.IO.File.Exists("/Repository/" + doc.Location + "/" + doc.DocName + ".pdf"))
                                {
                                    // Create the pdf file
                                    testing = CreatePdfFromWordDocument("/Repository/" + doc.Location);
                                }

                                search.ViewDoc = "/Repository/" + doc.Location + "/" + doc.DocName + ".pdf";
                                return Json(new { status = 5, testing, page = JsonPartialView(this, "_ViewPDF", search) }, JsonRequestBehavior.AllowGet);
                            }

                            // Pictures
                            if (doc.DocType.ToLower() == "jpg" || doc.DocType.ToLower() == "jpeg" || doc.DocType.ToLower() == "png" || doc.DocType.ToLower() == "gif" || doc.DocType.ToLower() == "bmp")
                            {
                                Image img = Image.FromFile(Server.MapPath(Path.Combine("~/Repository", doc.Location, doc.DocName, doc.DocType.ToLower())));
                                if (img.Width > 700 || img.Height > 500)
                                    search.DocTempImage = Helper.BitmapToByteArray((Bitmap)ScaleImage(img, 700, 500));
                                else
                                    search.DocTempImage = Helper.BitmapToByteArray((Bitmap)img);
                                return Json(new { status = 1, page = JsonPartialView(this, "_ViewPDF", search) }, JsonRequestBehavior.AllowGet);
                            }

                            // PDFs
                            if (doc.DocType == "pdf")
                            {
                                search.ViewDoc = "/Repository/" + doc.Location + "/" + doc.DocName + "." + doc.DocType;
                                return Json(new { status = 5, page = JsonPartialView(this, "_ViewPDF", search) }, JsonRequestBehavior.AllowGet);
                            }

                            // Excel
                            if (doc.DocType == "xls" || doc.DocType == "xlsx" || doc.DocType == "csv")
                            {
                                search.ViewDoc = "/Repository/" + doc.Location + "/" + doc.DocName + "." + doc.DocType;
                                return Json(new { status = 6, page = JsonPartialView(this, "_ViewPDF", search) }, JsonRequestBehavior.AllowGet);
                            }

                            // Some type you havn't accounted for. You should register this somewhere...
                            return Json(new { status = 0 }, JsonRequestBehavior.AllowGet);
                        }
                        else
                        {
                            return Json(new { status = 2 }, JsonRequestBehavior.AllowGet);
                        }
                    }
                }
                else
                {
                    return Json(new { status = 3 }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json(new { status = 100, message = ex.Message }, JsonRequestBehavior.AllowGet);
            }

            return Json(new { status = 0 }, JsonRequestBehavior.AllowGet);
        }
               
        public ActionResult AssignAccess(string peeps, string docid, string depts)
        {
            try
            {
                if (!string.IsNullOrEmpty(docid))
                {
                    try
                    {
                        // Set user access
                        new ServiceProxy().Channel.GiveAccessToUsers(peeps, docid);

                        // Set dept access
                        new ServiceProxy().Channel.GiveAccessToDepts(depts, docid);

                        return Json(new { success = 1 }, JsonRequestBehavior.AllowGet);
                    }
                    catch (Exception)
                    {
                        return Json(new { success = 2 }, JsonRequestBehavior.AllowGet);
                    }
                }

                return Json(new { success = 3 }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(new { success = 0 }, JsonRequestBehavior.AllowGet);
            }
        } 

        #endregion

        #region Reports

        public ActionResult GetViewReport(string searchtype, string from, string to, string page, string sortby, string prevp, string numj, string orderby, string isdesc, string pgsize)
        {
            try
            {
                Reports model = new Reports();
                int returncount = 0;

                if (!string.IsNullOrEmpty(pgsize))
                    model.PageSize = pgsize;
                model.ReportsList = new List<Report>();

                if (!User.Identity.IsAuthenticated || Session["CurrentUser"] == null)
                {
                    return Json(new { status = 3 }, JsonRequestBehavior.AllowGet);
                }

                DateTime fromx = DateTime.Now;
                DateTime tox = DateTime.Now;

                if (!string.IsNullOrEmpty(from))
                    fromx = DateTime.Parse(from);

                if (!string.IsNullOrEmpty(to))
                    tox = DateTime.Parse(to);

                // NIBSS reports are 0 - 10
                if (searchtype == "0")
                {
                    List<Report> r1 = new ServiceProxy().Channel.GetPassportVerificationHistory(fromx, tox, true);
                    List<Report> r2 = new ServiceProxy().Channel.GetBankLoginHistory(fromx, tox);
                    List<Report> r3 = new ServiceProxy().Channel.GetBankMessageHistory(fromx, tox);
                    List<Report> r4 = new ServiceProxy().Channel.GetBankExceptionsHistory(fromx, tox);
                    List<Report> r5 = new ServiceProxy().Channel.GetBankCursorStatus();

                    var report = r5.OrderByDescending(i => i.Cursor.Value).FirstOrDefault();
                    if (report != null)
                    {
                        List<Report> general = new List<Report>
                        {
                            new Report
                            {
                                GeneralReportName = "Passport Verification History",
                                GeneralReportCount = r1.Count,
                                GeneralReportId = 1
                            },
                            new Report()
                            {
                                GeneralReportName = "Bank Login History",
                                GeneralReportCount = r2.Count,
                                GeneralReportId = 2
                            },
                            new Report()
                            {
                                GeneralReportName = "Bank Message History (GSM)",
                                GeneralReportCount = r3.Where(x => x.Message.MessageType == 1).ToList().Count,
                                GeneralReportId = 3
                            },
                            new Report()
                            {
                                GeneralReportName = "Bank Message History (Emails)",
                                GeneralReportCount = r3.Where(x => x.Message.MessageType == 2).ToList().Count,
                                GeneralReportId = 4
                            },
                            new Report()
                            {
                                GeneralReportName = "Bank Exceptions History",
                                GeneralReportCount = r4.Count,
                                GeneralReportId = 5
                            },
                            new Report()
                            {
                                GeneralReportName = "Highest Oracle Cursor Value",
                                GeneralReportCount =
                                    report.Cursor.Value,
                                GeneralReportId = 6
                            }
                        };

                        model.ReportsList = general;
                    }
                }

                if (searchtype == "1")
                {
                    model.ReportsList = new ServiceProxy().Channel.GetPassportVerificationHistory(fromx, tox, false);
                    returncount = model.ReportsList.Count;
                }

                if (searchtype == "2")
                {
                    model.ReportsList = new ServiceProxy().Channel.GetBankLoginHistory(fromx, tox);
                    returncount = model.ReportsList.Count;
                }

                if (searchtype == "3")
                {
                    model.ReportsList = new ServiceProxy().Channel.GetBankMessageHistory(fromx, tox);
                    model.ReportsList = model.ReportsList.Where(x => x.Message.MessageType == 1).ToList();
                    returncount = model.ReportsList.Count;
                }

                if (searchtype == "4")
                {
                    model.ReportsList = new ServiceProxy().Channel.GetBankMessageHistory(fromx, tox);
                    model.ReportsList = model.ReportsList.Where(x => x.Message.MessageType == 2).ToList();
                    returncount = model.ReportsList.Count;
                }

                if (searchtype == "5")
                {
                    model.ReportsList = new ServiceProxy().Channel.GetBankExceptionsHistory(fromx, tox);
                    returncount = model.ReportsList.Count;
                }

                if (searchtype == "6")
                {
                    model.ReportsList = new ServiceProxy().Channel.GetBankCursorStatus();
                    returncount = model.ReportsList.Count;
                }

                // Admin reports are in the 10s
                if (searchtype == "10")
                {
                    List<Audit> auditlist = new ServiceProxy().Channel.GetAuditTrail(fromx, tox);

                    foreach (Audit audit in auditlist)
                    {
                        Report r = new Report(){ AuditItem = audit };
                        model.ReportsList.Add(r);
                    }

                    returncount = model.ReportsList.Count;

                    if (Session["StaffReport"] == null)
                    {
                        Session["StaffReport"] = model.ReportsList;
                    }
                }

                // General reports are in the 20s
                if (searchtype == "20")
                {
                    List<Person> irisusers = new ServiceProxy().Channel.GetUserList();

                    foreach (Person user in irisusers)
                    {
                        Report r = new Report() { IrisUser = user };
                        model.ReportsList.Add(r);
                    }

                    returncount = model.ReportsList.Count;

                    // Set admin
                    Person person = Session["CurrentUser"] as Person;
                    if (person != null)
                    {
                        model.IsAdmin = person.IsAdmin;
                    }

                    Session["StaffReport"] = model.ReportsList;
                }

                if (model.ReportsList.Count == 0)
                    model.NoReportsMessage = "No entries found matching your criteria.";

                if (string.IsNullOrEmpty(page))
                    page = "1";



                if (!string.IsNullOrEmpty(sortby) && !string.IsNullOrEmpty(prevp) && !string.IsNullOrEmpty(numj))
                {
                    SortReports(model, sortby, prevp, numj, orderby, page, isdesc, searchtype);
                }

                SetMyReportsPaging(model, page);

                Session["CurrentReport"] = model.ReportsList;
                model.ReportType = searchtype;

                if (searchtype == "10")
                    return Json(new { status = 1, count = returncount, page = JsonPartialView(this, "_AdminMaintenanceSearch", model) }, JsonRequestBehavior.AllowGet);

                if (searchtype == "20")
                    return Json(new { status = 1, count = returncount, page = JsonPartialView(this, "_GeneralReportSearch", model) }, JsonRequestBehavior.AllowGet);

                return Json(new { status = 1, count = returncount, page = JsonPartialView(this, "_NibssMaintenanceSearch", model) }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { status = 4, message = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult GetPassportImage(string ppid)
        {
            try
            {
                Report report = new ServiceProxy().Channel.GetPassportDetails(ppid);

                if (report != null)
                {
                    return Json(new { status = 1, page = JsonPartialView(this, "_ViewPassport", report) }, JsonRequestBehavior.AllowGet);
                }

                return Json(new { status = 0 }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(new { status = 0 }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult GetStaffImage(string ppid)
        {
            try
            {
                if (User.Identity.IsAuthenticated && Session["CurrentUser"] != null)
                {
                    Person person = Session["CurrentUser"] as Person;
                    List<Report> reportlist = Session["StaffReport"] as List<Report>;

                    if (reportlist != null)
                    {
                        Report report = reportlist.Where(x => (x.IrisUser != null ? x.IrisUser.Person_Id == int.Parse(ppid) : x.AuditItem.User.Person_Id == int.Parse(ppid))).FirstOrDefault();

                        // For staff list
                        if (report.IrisUser != null)
                        {
                            report.IrisUser.EmailAddress = report.IrisUser.EmailAddress.ToLower();
                            if (report.IrisUser.ClearanceLevel == 1)
                                report.IrisUser.ClearanceLevelWord = "One";
                            if (report.IrisUser.ClearanceLevel == 2)
                                report.IrisUser.ClearanceLevelWord = "Two";
                            if (report.IrisUser.ClearanceLevel == 3)
                                report.IrisUser.ClearanceLevelWord = "Three";

                            if (!string.IsNullOrEmpty(report.IrisUser.ProfilePicture))
                            {
                                // Add profile picture if it exists
                                string loc = Server.MapPath(Path.Combine("~/Repository/Profilepics", report.IrisUser.ProfilePicture));
                                if (System.IO.File.Exists(loc))
                                {
                                    report.IrisUser.ProfilePicture = Convert.ToBase64String(System.IO.File.ReadAllBytes(loc));
                                }
                            }
                            if (report != null)
                            {
                                return Json(new { status = 1, page = JsonPartialView(this, "_ViewStaff", report.IrisUser) }, JsonRequestBehavior.AllowGet);
                            }

                        }
                        else
                        {
                            report.AuditItem.User.EmailAddress = report.AuditItem.User.EmailAddress.ToLower();
                            if (report.AuditItem.User.ClearanceLevel == 1)
                                report.AuditItem.User.ClearanceLevelWord = "One";
                            if (report.AuditItem.User.ClearanceLevel == 2)
                                report.AuditItem.User.ClearanceLevelWord = "Two";
                            if (report.AuditItem.User.ClearanceLevel == 3)
                                report.AuditItem.User.ClearanceLevelWord = "Three";

                            if (!string.IsNullOrEmpty(report.AuditItem.User.ProfilePicture))
                            {
                                // Add profile picture if it exists
                                string loc = Server.MapPath(Path.Combine("~/Repository/Profilepics", report.AuditItem.User.ProfilePicture));
                                if (System.IO.File.Exists(loc))
                                {
                                    report.AuditItem.User.ProfilePicture = Convert.ToBase64String(System.IO.File.ReadAllBytes(loc));
                                }
                            }

                            if (report != null)
                            {
                                return Json(new { status = 1, page = JsonPartialView(this, "_ViewStaff", report.AuditItem.User) }, JsonRequestBehavior.AllowGet);
                            }
                        }
                    }

                    return Json(new { status = 0 }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { status = 2 }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception)
            {
                return Json(new { status = 0 }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult ChangeStaffAccessLevel(string level, string pid)
        {
            try
            {
                if (User.Identity.IsAuthenticated && Session["CurrentUser"] != null)
                {
                    Person person = Session["CurrentUser"]  as Person;

                    if (person.ClearanceLevel < int.Parse(level))
                        return Json(new { status = 0 }, JsonRequestBehavior.AllowGet);
                    else
                    {
                        int result = new ServiceProxy().Channel.EditStaffAccessLevel(int.Parse(pid), int.Parse(level), person.ClearanceLevel, person.Person_Id);
                        return Json(new { status = 1 }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    return Json(new { status = 2 }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public ActionResult GenerateReport(string searchtype, string from, string to)
        {
            try
            {
                // Get data for the interval
                DateTime From = string.IsNullOrEmpty(from) ? DateTime.Today : DateTime.ParseExact(from, "dd/MM/yyyy", null);
                DateTime To = string.IsNullOrEmpty(to) ? DateTime.Today : DateTime.ParseExact(to, "dd/MM/yyyy", null);

                List<PassportReport> data = new ServiceProxy().Channel.GetPassportsIssued(From, To);
                
                // Calculate details
                BranchReport report = new BranchReport();
                report.Page = 1;
                report.ReportName = "Production Report";
                report.ReportSubHeader = "OVERALL PASSPORTS ISSUANCE BY TYPE";
                report.Interval = "( BETWEEN " + From.ToString("dd-MMM-yyyy") + " AND " + To.ToString("dd-MMM-yyyy") + " )";
                report.TimeStamp = DateTime.Now.ToString("ddd, dd MMM yyyy, hh:mm tt");
                report.SubInterval = "From " + From.ToString("dd-MMM-yyyy") + " to " + To.ToString("dd-MMM-yyyy");
                        
                report.OfficialPassports = new Dictionary<string, int>();
                report.OrdinaryPassports = new Dictionary<string, int>();
                report.DiplomaticPassports = new Dictionary<string, int>();

                if (searchtype == "0")
                {
                    report.HeaderName = "ALL BRANCHES (TOTAL BOOKLETS ISSUED)";
                }

                if (searchtype == "1")
                { 
                    report.HeaderName = "LOCAL BRANCHES (TOTAL BOOKLETS ISSUED)";
                    report.ReportSubHeader = "LOCAL PASSPORTS ISSUANCE BY TYPE";
                }

                if (searchtype == "2")
                {
                    report.HeaderName = "FOREIGN BRANCHES (TOTAL BOOKLETS ISSUED)";
                    report.ReportSubHeader = "FOREIGN PASSPORTS ISSUANCE BY TYPE";
                }

                foreach (PassportReport rep in data)
                {
                    if (searchtype == "1")
                    {
                        // Local branches
                        if (rep.IsForeign == 1)
                            continue;
                    }

                    if (searchtype == "2")
                    {
                        // Foreign branches
                        if (rep.IsForeign == 0)
                            continue;
                    }

                    // NOTE
                    // Ayo requested that any passport issued that is official/diplomatic must be done from central
                    // Remove all the &&'s to return it back to normal

                    if (rep.DocType == "P")
                    {
                        report.OrdinaryPassports.Add(rep.BranchName, rep.NumberIssued);
                    }

                    if (rep.DocType == "PF" && rep.BranchCode == 369)
                    {
                        report.OfficialPassports.Add(rep.BranchName, rep.NumberIssued);
                    }

                    if (rep.DocType == "PD" && rep.BranchCode == 369)
                    {
                        report.DiplomaticPassports.Add(rep.BranchName, rep.NumberIssued);
                    }
                }

                report.DiplomaticPassportsTotal = report.DiplomaticPassports.Sum(x => x.Value);
                report.OrdinaryPassportsTotal = report.OrdinaryPassports.Sum(x => x.Value);
                report.OfficialPassportsTotal = report.OfficialPassports.Sum(x => x.Value);                

                return Json(new { status = 1, page = JsonPartialView(this, "_BranchReport", report) }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { status = 2, errormsg = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }
        
        #endregion

        #region Calender

        public ActionResult CreateCalenderEvent(string eventid, string bgcolor, string desc, string end, string groupid, string start, string textColor, string title, string allday, string staffinv, string email, string isUpdate)
        {
            Person person = Session["CurrentUser"] as Person;

            if (person == null)
            {
                return Json(new { success = 3 }, JsonRequestBehavior.AllowGet);
            }

            Event ev = new Event
            {
                BackgroundColor = bgcolor,
                Description = desc,
                End = end,
                GroupId = int.Parse(groupid),
                PostedBy = person,
                TimePosted = DateTime.Now,
                Start = start,
                TextColor = textColor,
                Title = title,
                IsAllDay = string.IsNullOrEmpty(allday) ? 0 : allday == "false" ? 0 : 1,
                StaffIinvited = staffinv,
                SendEmail = string.IsNullOrEmpty(email) ? 0 : email == "false" ? 0 : 1,
            };

            try
            {
                if (isUpdate == "0")
                {
                    int eid = new ServiceProxy().Channel.CreateCalenderEvent(ev);
                    return Json(new { success = 1, eventid = eid, postedby = person.FullName + " (" + ev.TimePosted.ToString("dd/MM/yyyy hh:mm tt") + ")" }, JsonRequestBehavior.AllowGet);
                }

                ev.EventId = int.Parse(eventid);

                int result = new ServiceProxy().Channel.UpdateCalenderEvent(ev);

                if (result == 0) // not yours
                    return Json(new { success = 2, postedby = person.FullName + " (" + ev.TimePosted.ToString("dd/MM/yyyy hh:mm tt") + ")" }, JsonRequestBehavior.AllowGet);

                return Json(new { success = 1, postedby = person.FullName + " (" + ev.TimePosted.ToString("dd/MM/yyyy hh:mm tt") + ")" }, JsonRequestBehavior.AllowGet);
            }
            catch(Exception)
            {
                return Json(new { success = 0 }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult DeleteCalenderEvent(string eventid)
        {
            Person person = Session["CurrentUser"] as Person;

            if (person == null)
                return Json(new { success = 3 }, JsonRequestBehavior.AllowGet);

            try
            {
                int result = new ServiceProxy().Channel.DeleteCalenderEvent(int.Parse(eventid), person.Person_Id);

                if (result == 0) // not yours
                    return Json(new { success = 2 }, JsonRequestBehavior.AllowGet);

                return Json(new { success = 1 }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(new { success = 0 }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult EmailChanges(string eventid, string desc, string end, string start, string title, string staffinv, string isDel)
        {
            Person person = Session["CurrentUser"] as Person;

            if (person == null)
            {
                return Json(new { success = 3 }, JsonRequestBehavior.AllowGet);
            }

            Event ev = new Event();

            if (isDel == "0")
            {
                ev.EventId = int.Parse(eventid);
                ev.Description = desc;
                ev.End = end;
                ev.PostedBy = person;
                ev.Start = start;
                ev.Title = title;
                ev.StaffIinvited = staffinv;
                ev.IsCancelled = int.Parse(isDel);
            }
            else
            {
                ev.EventId = int.Parse(eventid);
                ev.IsCancelled = int.Parse(isDel);
            }

            try
            {
                new ServiceProxy().Channel.UpdateStaffEventChange(ev);
                return Json(new { success = 1 }, JsonRequestBehavior.AllowGet);
            }
            catch(Exception)
            {
                return Json(new { success = 0 }, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        #endregion

        #region Operations

        private int CreatePdfFromWordDocument(string location)
        {
            // Create a new Microsoft Word application object
            Microsoft.Office.Interop.Word.Application word = new Microsoft.Office.Interop.Word.Application();

            // C# doesn't have optional arguments so we'll need a dummy value
            object oMissing = System.Reflection.Missing.Value;

            // Get list of Word files in specified directory
            DirectoryInfo dirInfo = new DirectoryInfo(location);
            FileInfo[] wordFiles = dirInfo.GetFiles("*.doc");

            word.Visible = false;
            word.ScreenUpdating = false;

            try
            {
                foreach (FileInfo wordFile in wordFiles)
                {
                    // Cast as Object for word Open method
                    Object filename = (Object)wordFile.FullName;

                    Microsoft.Office.Interop.Word.Document doc;

                    // Use the dummy value as a placeholder for optional arguments
                    doc = word.Documents.Open(ref filename, ref oMissing,
                        ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing,
                        ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing,
                        ref oMissing, ref oMissing, ref oMissing, ref oMissing);

                    doc.Activate();

                    object outputFileName = wordFile.FullName.Replace(".docx", ".pdf").Replace(".doc", ".pdf");
                    object fileFormat = Microsoft.Office.Interop.Word.WdSaveFormat.wdFormatPDF;

                    try
                    {
                        // Save document into PDF Format
                        doc.SaveAs(ref outputFileName,
                            ref fileFormat, ref oMissing, ref oMissing,
                            ref oMissing, ref oMissing, ref oMissing, ref oMissing,
                            ref oMissing, ref oMissing, ref oMissing, ref oMissing,
                            ref oMissing, ref oMissing, ref oMissing, ref oMissing);

                        // Close the Word document, but leave the Word application open.
                        // doc has to be cast to type _Document so that it will find the
                        // correct Close method.                
                        object saveChanges = Microsoft.Office.Interop.Word.WdSaveOptions.wdDoNotSaveChanges;
                        ((Microsoft.Office.Interop.Word._Document)doc).Close(ref saveChanges, ref oMissing, ref oMissing);
                        doc = null;
                    }
                    catch (Exception)
                    {
                        return 4;
                    }
                }
            }
            catch(Exception ex)
            {
                return 1;
            }

            // word has to be cast to type _Application so that it will find
            // the correct Quit method.
            ((Microsoft.Office.Interop.Word._Application)word).Quit(ref oMissing, ref oMissing, ref oMissing);
            word = null;

            return 2;
        }

        public static Image ScaleImage(Image image, int maxWidth, int maxHeight)
        {
            var ratioX = (double)maxWidth / image.Width;
            var ratioY = (double)maxHeight / image.Height;
            var ratio = Math.Min(ratioX, ratioY);

            var newWidth = (int)(image.Width * ratio);
            var newHeight = (int)(image.Height * ratio);

            var newImage = new Bitmap(newWidth, newHeight);
            Graphics.FromImage(newImage).DrawImage(image, 0, 0, newWidth, newHeight);
            return newImage;
        }

        public string JsonPartialView(Controller controller, string viewName, object model)
        {
            controller.ViewData.Model = model;

            using (var sw = new StringWriter())
            {
                var viewResult = ViewEngines.Engines.FindPartialView(controller.ControllerContext, viewName);
                var viewContext = new ViewContext(controller.ControllerContext, viewResult.View, controller.ViewData, controller.TempData, sw);

                viewResult.View.Render(viewContext, sw);
                viewResult.ViewEngine.ReleaseView(controller.ControllerContext, viewResult.View);

                return sw.ToString();
            }
        }

        private List<Document> DoSearch(string k, string doctype, string docinnertype, string client, string cat, 
            string proj, string staffdoc, string updatefrom, string updateto, string docdatefrom, string docdateto, 
            string ext, int userid)
        {
            List<Document> docs = new ServiceProxy().Channel.SearchForDocuments(k, cat.Replace("++", ","), 
                proj.Replace("++", ","), staffdoc, updatefrom, updateto, docdatefrom, docdateto, ext, userid);
            
            // Extra sorting
            if (!string.IsNullOrEmpty(doctype))
            {
                // If a specific section is selected
                for (int i = docs.Count - 1; i >= 0; i--)
                {
                    bool hasit = false;
                    foreach (string s in doctype.Split(','))
                    {
                        if (docs[i].Location.Contains(s))
                            hasit = true;
                    }

                    if (!hasit)
                        docs.RemoveAt(i);
                }

                // if a specific client is selected
                if (!string.IsNullOrEmpty(client))
                {
                    for (int i = docs.Count - 1; i >= 0; i--)
                    {
                        bool hasit = false;
                        foreach (string s in client.Split(','))
                        {
                            if (docs[i].Location.Contains(s))
                                hasit = true;
                        }

                        if (!hasit)
                            docs.RemoveAt(i);
                    }

                    // If a specific folder in that client is selected
                    if (!string.IsNullOrEmpty(docinnertype))
                    {
                        for (int i = docs.Count - 1; i >= 0; i--)
                        {
                            bool hasit = false;
                            foreach (string s in docinnertype.Split(','))
                            {
                                if (docs[i].Location.Contains(s))
                                    hasit = true;
                            }

                            if (!hasit)
                                docs.RemoveAt(i);
                        }
                    }
                }
            }

            // Clean up names a little
            foreach (Document doc in docs)
            {
                string[] allowedtypes = { "pdf", "jpg", "bmp", "jpeg", "png" };
                // Check if document has a pdf version
                if (allowedtypes.Contains(doc.DocType.ToLower()) || 
                    System.IO.File.Exists("/Repository/" + doc.Location + "/" + doc.DocName + ".pdf"))
                {
                    doc.HasPDF = true;
                }

                doc.Location = doc.Location.Replace("Iris/Clients/", "");
                doc.Location = doc.Location.Replace("Staff/", "");
                doc.Location = doc.Location.Replace("/Public/", "/");
                doc.Location = doc.Location.Replace("Iris/", "");
                doc.Location = doc.Location.Replace("/Project/", "/");
                doc.Location = doc.Location.Replace("/", " - ");
            }

            return docs;
        }

        private void SetMyPaging(Search search, string page)
        {
            var perpage = new List<object>
            {
                new {value = "5", text = "5"},
                new {value = "10", text = "10"},
                new {value = "30", text = "30"},
                new {value = "50", text = "50"}
            };

            search.PageSizeList = new SelectList(perpage.AsEnumerable(), "value", "text", search.PageSize);

            if (!string.IsNullOrEmpty(page) && page != "0")
            {
                search.NowViewing = int.Parse(page);
            }
            else
                search.NowViewing = 1;

            if (string.IsNullOrEmpty(search.PageSize))
            {
                search.PageSize = "30";
            }

            int pagesize = int.Parse(search.PageSize);
            search.PageCount = Convert.ToInt32(Math.Ceiling(((double)search.DocList.Count / pagesize)));
            int start = (search.NowViewing - 1) * pagesize;
            if (start > search.DocList.Count)
            {
                start = 0;
                search.NowViewing = 1;
            }
            search.JobListCount = search.DocList.Count;
            if (search.PageCount != 1 && search.DocList.Count != 0)
                search.DocList = search.DocList.GetRange(start, search.DocList.Count - (start + pagesize - 1) > 0 ? pagesize : search.DocList.Count - start);
        }

        private void SetMyReportsPaging(Reports reports, string page)
        {
            var perpage = new List<object>
            {
                new {value = "5", text = "5"},
                new {value = "10", text = "10"},
                new {value = "30", text = "30"},
                new {value = "50", text = "50"}
            };

            reports.PageSizeList = new SelectList(perpage.AsEnumerable(), "value", "text", reports.PageSize);

            if (!string.IsNullOrEmpty(page) && page != "0")
            {
                reports.NowViewing = int.Parse(page);
            }
            else
                reports.NowViewing = 1;

            if (string.IsNullOrEmpty(reports.PageSize))
            {
                reports.PageSize = "30";
            }

            int pagesize = int.Parse(reports.PageSize);
            reports.PageCount = Convert.ToInt32(Math.Ceiling(((double)reports.ReportsList.Count / pagesize)));
            int start = (reports.NowViewing - 1) * pagesize;
            if (start > reports.ReportsList.Count)
            {
                start = 0;
                reports.NowViewing = 1;
            }

            if (reports.PageCount != 1 && reports.ReportsList.Count != 0)
                reports.ReportsList = reports.ReportsList.GetRange(start, reports.ReportsList.Count - 
                    (start + pagesize - 1) > 0 ? pagesize : reports.ReportsList.Count - start);
        }

        private BigModel GetModelDefaultInfo()
        {
            if (Session["CurrentUser"] == null)
            {
                FormsAuthentication.SignOut();
            }

            BigModel model = new BigModel
            {
                MyDocs = new MyDocuments(),
                LoginModel = new LoginModel(),
                RegisterModel = new RegisterModel(),
                Search = new Search {PageSize = "10"}
            };
            
            if (model.Search.NowViewing == 0)
                model.Search.NowViewing = 1;

            if (User.Identity.IsAuthenticated && Session["CurrentUser"] != null)
            {
                Person user = Session["CurrentUser"] as Person;

                if (user != null)
                {
                    model.LoginModel.FirstName = Helper.ToFirstLetterUpper(user.FirstName);
                    model.LoginModel.Surname = Helper.ToFirstLetterUpper(user.Surname);
                    model.User = user;
                }
            }

            return model;
        }

        private Calender GetCalenderInfo()
        {
            ServiceProxy proxy = new ServiceProxy();
            DataSet dset = proxy.Channel.GetLists();
            Person person = Session["CurrentUser"] as Person;

            Calender model = new Calender();
            model = new Calender()
            {
                DefaultDate = DateTime.Now.ToString("yyyy-MM-dd")
            };

            // Get saved calender events
            List<Event> events = proxy.Channel.GetCalenderEvents();
            List<Event> removeme = new List<Event>();

            foreach (Event ev in events)
            {
                // Remove personal events and people who are not involved
                removeme = events.Where(x => (x.PostedBy.Person_Id != person.Person_Id && x.GroupId == 2)
                                             || (!string.IsNullOrEmpty(x.StaffIinvited) && !x.StaffIinvited.Split(',').Contains(person.Person_Id.ToString()) && x.GroupId == 3)
                ).ToList();
            }

            foreach (Event rm in removeme)
                events.Remove(rm);

            List<Event> myevents = events.Where(x => x.PostedBy.Person_Id == person.Person_Id).ToList();

            foreach (Event evn in myevents)
            {
                model.MyEvents += evn.EventId + ",";
            }

            var jsonList = new List<object>();

            var evs = from e in events
                select new
                {
                    id = e.EventId,
                    title = e.Title,
                    start = e.Start,
                    end = e.End,
                    backgroundColor = e.BackgroundColor,
                    textColor = e.TextColor,
                    postedby = e.PostedBy.FullName + " (" + e.TimePosted.ToString("dd/MM/yyyy hh:mm tt") + ")",
                    desc = e.Description,
                    groupid = e.GroupId,
                    allDay = e.IsAllDay == 1,
                    realstime = e.Start,
                    staffinv = e.StaffIinvited,
                    email = e.SendEmail == 1,
                };

            model.EventsArray = new JavaScriptSerializer().Serialize(evs);

            // Get default lists and the rest
            jsonList = new List<object>();

            foreach (DataRow row in dset.Tables["Staff"].Rows)
            {
                jsonList.Add(new
                {
                    text = Helper.ToFirstLetterUpper(row["firstname"].ToString()) + 
                           " " + Helper.ToFirstLetterUpper(row["surname"].ToString()),
                    id = row["staff_id"].ToString(),
                });
            }

            model.StaffArray = new JavaScriptSerializer().Serialize(jsonList);

            jsonList = new List<object>();

            foreach (DataRow row in dset.Tables["Colors"].Rows)
            {
                jsonList.Add(new
                {
                    text = row["color_name"].ToString(),
                    id = row["color_hex"].ToString(),
                });
            }

            model.ColorsArray = new JavaScriptSerializer().Serialize(jsonList);

            jsonList = new List<object>();

            foreach (DataRow row in dset.Tables["Groups"].Rows)
            {
                jsonList.Add(new
                {
                    text = row["group_name"].ToString(),
                    id = row["group_id"].ToString(),
                });
            }

            model.GroupsArray = new JavaScriptSerializer().Serialize(jsonList);

            jsonList = new List<object>();

            for (int i = 0; i < 25; i++)
            {
                jsonList.Add(new
                {
                    text = i.ToString("D2"),
                    id = i.ToString()
                });
            }

            model.HourArray = new JavaScriptSerializer().Serialize(jsonList);

            jsonList = new List<object>();

            for (int i = 0; i < 61; i++)
            {
                jsonList.Add(new
                {
                    text = i.ToString("D2"),
                    id = i.ToString()
                });
            }

            model.MinuteArray = new JavaScriptSerializer().Serialize(jsonList);

            return model;
        }

        private void GetSearchInfo(Search model)
        {
            model.DocList = new List<Document>();
            model.NoJobsMessage = "No documents found matching the selected criteria.";
                
            ServiceProxy proxy = new ServiceProxy();
            DataSet dset = proxy.Channel.GetLists();

            // Privilege levels
            var jsonList = new List<object>();

            foreach (DataRow row in dset.Tables["Levels"].Rows)
            {
                jsonList.Add(new
                {
                    text = row["level_name"].ToString(),
                    id = row["level_id"].ToString(),
                });
            }

            model.LevelsArray = new JavaScriptSerializer().Serialize(jsonList);

            // Add staff option as well
            jsonList = new List<object>();

            jsonList.Add(new
            {
                text = "Iris Documents",
                id = "Iris"
            });

            jsonList.Add(new
            {
                text = "Staff Documents",
                id = "Staff"
            });

            model.TierOneArray = new JavaScriptSerializer().Serialize(jsonList);

            // Staff list
            jsonList = new List<object>();

            foreach (DataRow row in dset.Tables["Staff"].Rows)
            {
                jsonList.Add(new
                {
                    text = Helper.ToFirstLetterUpper(row["firstname"].ToString()) + " " + Helper.ToFirstLetterUpper(row["surname"].ToString()),
                    id = row["firstname"] + " " + row["surname"]
                });
            }

            model.StaffArray = new JavaScriptSerializer().Serialize(jsonList);

            // Staff Using id
            jsonList = new List<object>();
            List<object> jsonList2 = new List<object>();

            foreach (DataRow row in dset.Tables["Users"].Rows)
            {
                jsonList2.Add(new
                {
                    text = Helper.ToFirstLetterUpper(row["firstname"].ToString()) + " " + Helper.ToFirstLetterUpper(row["surname"].ToString()),
                    id = int.Parse(row["user_id"].ToString())
                });

                if (int.Parse(row["clearance_level"].ToString()) < model.MyLevel)
                {
                    jsonList.Add(new
                    {
                        text = Helper.ToFirstLetterUpper(row["firstname"].ToString()) + " " + Helper.ToFirstLetterUpper(row["surname"].ToString()),
                        id = int.Parse(row["user_id"].ToString())
                    });
                }
            }

            model.StaffIdArray = new JavaScriptSerializer().Serialize(jsonList);
            model.StaffIdAllArray = new JavaScriptSerializer().Serialize(jsonList2);

            // Gender
            jsonList = new List<object>();

            jsonList.Add(new
            {
                text = "Male",
                id = "M"
            });

            jsonList.Add(new
            {
                text = "Female",
                id = "F"
            });

            model.GenderArray = new JavaScriptSerializer().Serialize(jsonList);

            // Document type
            jsonList = new List<object>();

            foreach (string folder in Directory.GetDirectories(Server.MapPath("~/Repository/Iris")))
            {
                jsonList.Add(new
                {
                    text = Path.GetFileNameWithoutExtension(folder),
                    id = "Iris/" + Path.GetFileNameWithoutExtension(folder)
                });
            }

            model.IrisSubcats = new JavaScriptSerializer().Serialize(jsonList);

            // Our clients
            jsonList = new List<object>();

            foreach (string folder in Directory.GetDirectories(Server.MapPath("~/Repository/Iris/Clients")))
            {
                jsonList.Add(new
                {
                    text = Path.GetFileNameWithoutExtension(folder),
                    id = "Iris/Clients/" + Path.GetFileNameWithoutExtension(folder)
                });
            }

            model.IrisClientArray = new JavaScriptSerializer().Serialize(jsonList);

            // Document inner types
            jsonList = new List<object>();

            foreach (string folder in Directory.GetDirectories(Server.MapPath("~/Repository/Iris/Clients/INEC/Project")))
            {
                jsonList.Add(new
                {
                    text = Path.GetFileNameWithoutExtension(folder),
                    id = "Iris/Clients/INEC/Project/" + Path.GetFileNameWithoutExtension(folder)
                });
            }

            model.IrisClientSubArray = new JavaScriptSerializer().Serialize(jsonList);

            // Departments
            jsonList = new List<object>();

            foreach (DataRow row in dset.Tables["Departments"].Rows)
            {
                jsonList.Add(new
                {
                    text = row["department"].ToString(),
                    id = row["department_id"].ToString(),
                });
            }

            model.DepartmentArray = new JavaScriptSerializer().Serialize(jsonList);

            // Positions
            jsonList = new List<object>();

            foreach (DataRow row in dset.Tables["Positions"].Rows)
            {
                jsonList.Add(new
                {
                    text = row["position"].ToString(),
                    id = row["position_id"].ToString(),
                });
            }

            model.PositionArray = new JavaScriptSerializer().Serialize(jsonList);

            // File Types
            jsonList = new List<object>();

            foreach (DataRow row in dset.Tables["DocTypes"].Rows)
            {
                jsonList.Add(new
                {
                    text = row["doc_type_ext"].ToString(),
                    id = row["doc_type_ext"].ToString(),
                });
            }

            model.FileTypeArray = new JavaScriptSerializer().Serialize(jsonList);
        }

        private void AddUserData(BigModel model)
        {
            Person person = Session["CurrentUser"] as Person;
            model.MyDocs.PublicRootDirector = Server.MapPath(Path.Combine("~/Repository/Staff", Helper.ToFirstLetterUpper(person.FirstName) + " " + Helper.ToFirstLetterUpper(person.Surname), "Public"));
            model.MyDocs.IrisRootDirector = Server.MapPath(Path.Combine("~/Repository/Iris"));
            
            model.MyDocs.FolderStructure = GetFolderStructure(model.MyDocs.PublicRootDirector, 1);
            model.MyDocs.CreateFolderStructure = GetFolderStructure(model.MyDocs.PublicRootDirector, 2);
            model.MyDocs.IrisFolderStructure = GetFolderStructure(model.MyDocs.IrisRootDirector, 1);
        }

        private void AddAdminStuff(BigModel model)
        {
            Person person = Session["CurrentUser"] as Person;
            model.IsAdmin = person.IsAdmin;

            var jsonList = new List<object>();

            jsonList.Add(new { text = "Audit Trail", id = 10 });
            model.AdminReports = new Reports();
            model.AdminReports.ReportsList = new List<Report>();
            model.AdminReports.CurrentSort = "Date";
            model.AdminReports.IsDesc = "&#9660;";
            model.AdminReports.NoReportsMessage = string.Empty;
            model.AdminReports.ReportsArray = new JavaScriptSerializer().Serialize(jsonList);
        }

        private string GetFolderStructure(string root, int addPublic)
        {
            string html = "<ul>";
            html = GetAllDirectories(html, root, addPublic);
            html += "</ul>";

            return html;
        }

        private string GetAllDirectories(string html, string folder, int addPublic)
        {
            string output = string.Empty;
            output = folder.Substring(folder.LastIndexOf("\\") + 1);

            if (addPublic == 1)
                html += "<li id='" + folder.Substring(folder.IndexOf("\\Repository") + 12) +"'>" + output;
            else
                html += "<li id='" + folder.Substring(folder.IndexOf("\\Repository") + 12) + "__xx'>" + output;

            // if it doesnt exist, create the folder
            if (!Directory.Exists(folder))
                Directory.CreateDirectory(folder);

            foreach (string dir in Directory.GetDirectories(folder))
            {
                html += "<ul>";
                html = GetAllDirectories(html, dir, addPublic);
                html += "</ul>";
            }
            html += "</li>";

            return html;
        }

        private void SortMyModel(Search search, string sortby, string prevp, string numj, string orderby, string page, string isdesc)
        {
            search.IsDesc = "&#9660;";
            if (!string.IsNullOrEmpty(sortby))
            {
                string currsort = string.Empty;
                if (TempData["currsort"] != null)
                    currsort = TempData["currsort"].ToString();
                else
                    currsort = sortby;

                switch (orderby)
                {
                    case "PostingDate":
                        {
                            if (currsort == "PostingDate")
                            {
                                if (prevp == page)
                                {
                                    if (!string.IsNullOrEmpty(isdesc) && isdesc == "6")
                                    {
                                        if (numj == "0")
                                            search.DocList = search.DocList.OrderByDescending(m => m.DatePosted).ToList();
                                        else
                                            search.DocList = search.DocList.OrderBy(m => m.DatePosted).ToList();
                                    }
                                    else
                                    {
                                        if (numj == "0")
                                            search.DocList = search.DocList.OrderBy(m => m.DatePosted).ToList();
                                        else
                                            search.DocList = search.DocList.OrderByDescending(m => m.DatePosted).ToList();
                                    }
                                }
                                else
                                {
                                    if (!string.IsNullOrEmpty(isdesc) && isdesc == "5")
                                        search.DocList = search.DocList.OrderBy(m => m.DatePosted).ToList();
                                    else
                                        search.DocList = search.DocList.OrderByDescending(m => m.DatePosted).ToList();
                                }
                            }
                            else
                                search.DocList = search.DocList.OrderBy(m => m.DatePosted).ToList();
                            break;
                        }
                    case "DocumentDate":
                        {
                            if (currsort == "DocumentDate")
                            {
                                if (prevp == page)
                                {
                                    if (!string.IsNullOrEmpty(isdesc) && isdesc == "6")
                                    {
                                        if (numj == "0")
                                            search.DocList = search.DocList.OrderByDescending(m => m.DocDate).ToList();
                                        else
                                            search.DocList = search.DocList.OrderBy(m => m.DocDate).ToList();
                                    }
                                    else
                                    {
                                        if (numj == "0")
                                            search.DocList = search.DocList.OrderBy(m => m.DocDate).ToList();
                                        else
                                            search.DocList = search.DocList.OrderByDescending(m => m.DocDate).ToList();
                                    }
                                }
                                else
                                {
                                    if (!string.IsNullOrEmpty(isdesc) && isdesc == "5")
                                        search.DocList = search.DocList.OrderByDescending(m => m.DocDate).ToList();
                                    else
                                        search.DocList = search.DocList.OrderBy(m => m.DocDate).ToList();
                                }
                            }
                            else
                                search.DocList = search.DocList.OrderBy(m => m.DocDate).ToList();
                            break;
                        }
                    case "DocTitle":
                        {
                            if (currsort == "DocTitle")
                            {
                                if (prevp == page)
                                {
                                    if (!string.IsNullOrEmpty(isdesc) && isdesc == "6")
                                    {
                                        if (numj == "0")
                                            search.DocList = search.DocList.OrderByDescending(m => m.DocName).ToList();
                                        else
                                            search.DocList = search.DocList.OrderBy(m => m.DocName).ToList();
                                    }
                                    else
                                    {
                                        if (numj == "0")
                                            search.DocList = search.DocList.OrderBy(m => m.DocName).ToList();
                                        else
                                            search.DocList = search.DocList.OrderByDescending(m => m.DocName).ToList();
                                    }
                                }
                                else
                                {
                                    if (!string.IsNullOrEmpty(isdesc) && isdesc == "5")
                                        search.DocList = search.DocList.OrderByDescending(m => m.DocName).ToList();
                                    else
                                        search.DocList = search.DocList.OrderBy(m => m.DocName).ToList();
                                }
                            }
                            else
                                search.DocList = search.DocList.OrderBy(m => m.DocName).ToList();

                            break;
                        }
                    case "Location":
                        {
                            if (currsort == "Location")
                            {
                                if (prevp == page)
                                {
                                    if (!string.IsNullOrEmpty(isdesc) && isdesc == "6")
                                    {
                                        if (numj == "0")
                                            search.DocList = search.DocList.OrderByDescending(m => m.Location).ToList();
                                        else
                                            search.DocList = search.DocList.OrderBy(m => m.Location).ToList();
                                    }
                                    else
                                    {
                                        if (numj == "0")
                                            search.DocList = search.DocList.OrderBy(m => m.Location).ToList();
                                        else
                                            search.DocList = search.DocList.OrderByDescending(m => m.Location).ToList();
                                    }
                                }
                                else
                                {
                                    if (!string.IsNullOrEmpty(isdesc) && isdesc == "5")
                                        search.DocList = search.DocList.OrderByDescending(m => m.Location).ToList();
                                    else
                                        search.DocList = search.DocList.OrderBy(m => m.Location).ToList();
                                }
                            }
                            else
                                search.DocList = search.DocList.OrderBy(m => m.Location).ToList();
                            break;
                        }
                    case "DocType":
                        {
                            if (currsort == "DocType")
                            {
                                if (prevp == page)
                                {
                                    if (!string.IsNullOrEmpty(isdesc) && isdesc == "6")
                                    {
                                        if (numj == "0")
                                            search.DocList = search.DocList.OrderByDescending(m => m.DocType).ToList();
                                        else
                                            search.DocList = search.DocList.OrderBy(m => m.DocType).ToList();
                                    }
                                    else
                                    {
                                        if (numj == "0")
                                            search.DocList = search.DocList.OrderBy(m => m.DocType).ToList();
                                        else
                                            search.DocList = search.DocList.OrderByDescending(m => m.DocType).ToList();
                                    }
                                }
                                else
                                {
                                    if (!string.IsNullOrEmpty(isdesc) && isdesc == "5")
                                        search.DocList = search.DocList.OrderByDescending(m => m.DocType).ToList();
                                    else
                                        search.DocList = search.DocList.OrderBy(m => m.DocType).ToList();
                                }
                            }
                            else
                                search.DocList = search.DocList.OrderBy(m => m.DocType).ToList();
                            break;
                        }
                    case "Level":
                        {
                            if (currsort == "Level")
                            {
                                if (prevp == page)
                                {
                                    if (!string.IsNullOrEmpty(isdesc) && isdesc == "6")
                                    {
                                        if (numj == "0")
                                            search.DocList = search.DocList.OrderByDescending(m => m.Level).ToList();
                                        else
                                            search.DocList = search.DocList.OrderBy(m => m.Level).ToList();
                                    }
                                    else
                                    {
                                        if (numj == "0")
                                            search.DocList = search.DocList.OrderBy(m => m.Level).ToList();
                                        else
                                            search.DocList = search.DocList.OrderByDescending(m => m.Level).ToList();
                                    }
                                }
                                else
                                {
                                    if (!string.IsNullOrEmpty(isdesc) && isdesc == "5")
                                        search.DocList = search.DocList.OrderByDescending(m => m.Level).ToList();
                                    else
                                        search.DocList = search.DocList.OrderBy(m => m.Level).ToList();
                                }
                            }
                            else
                                search.DocList = search.DocList.OrderBy(m => m.Level).ToList();
                            break;
                        }
                }

                search.CurrentSort = sortby;

            }
            else
                search.CurrentSort = "PostingDate";
        }

        private void SortReports(Reports report, string sortby, string prevp, string numj, string orderby, string page, string isdesc, string searchtype)
        {
            report.IsDesc = "&#9660;";
            if (!string.IsNullOrEmpty(sortby))
            {
                string currsort = string.Empty;
                if (TempData["currsort"] != null)
                    currsort = TempData["currsort"].ToString();
                else
                    currsort = sortby;

                #region Admin Tab

                if (searchtype == "10")
                {
                    switch (orderby)
                    {
                        case "Name":
                            {
                                report.ReportsList = DoSort(report.ReportsList, "AuditItem.User.FirstName", currsort, "Name", prevp, page, isdesc, numj);
                                break;
                            }
                        case "Date":
                            {
                                report.ReportsList = DoSort(report.ReportsList, "AuditItem.TimeStamp", currsort, "Date", prevp, page, isdesc, numj);
                                break;
                            }
                        case "Event":
                            {
                                report.ReportsList = DoSort(report.ReportsList, "AuditItem.EventTypeName", currsort, "Event", prevp, page, isdesc, numj);

                                break;
                            }
                        case "Doc":
                            {
                                report.ReportsList = DoSort(report.ReportsList, "AuditItem.Doc.DocName", currsort, "Doc", prevp, page, isdesc, numj);
                                break;
                            }
                    }
                }

                #endregion

                #region General Tab

                if (searchtype == "20")
                {
                    switch (orderby)
                    {
                        case "Name":
                            {
                                report.ReportsList = DoSort(report.ReportsList, "IrisUser.FirstName", currsort, "Name", prevp, page, isdesc, numj);
                                break;
                            }
                        case "RegistrationDate":
                            {
                                report.ReportsList = DoSort(report.ReportsList, "IrisUser.DateOfRegistration", currsort, "RegistrationDate", prevp, page, isdesc, numj);
                                break;
                            }
                        case "Department":
                            {
                                report.ReportsList = DoSort(report.ReportsList, "IrisUser.Department", currsort, "Department", prevp, page, isdesc, numj);

                                break;
                            }
                        case "ClearanceLevel":
                            {
                                report.ReportsList = DoSort(report.ReportsList, "IrisUser.ClearanceLevel", currsort, "ClearanceLevel", prevp, page, isdesc, numj);
                                break;
                            }
                    }
                }

                #endregion

                #region Nibss Passport Verification

                if (searchtype == "1")
                {
                    switch (orderby)
                    {
                        case "Name":
                            {
                                report.ReportsList = DoSort(report.ReportsList, "FirstName", currsort, "Name", prevp, page, isdesc, numj);
                                break;
                            }
                        case "RefNo":
                            {
                                report.ReportsList = DoSort(report.ReportsList, "DocRefNum", currsort, "RefNo", prevp, page, isdesc, numj);
                                break;
                            }
                        case "Bank":
                            {
                                report.ReportsList = DoSort(report.ReportsList, "Bank.BankName", currsort, "Bank", prevp, page, isdesc, numj);

                                break;
                            }
                        case "Branch":
                            {
                                report.ReportsList = DoSort(report.ReportsList, "Bank.BranchName", currsort, "Branch", prevp, page, isdesc, numj);
                                break;
                            }
                        case "Operator":
                            {
                                report.ReportsList = DoSort(report.ReportsList, "Operator.FirstName", currsort, "Operator", prevp, page, isdesc, numj);
                                break;
                            }
                        case "Date":
                            {
                                report.ReportsList = DoSort(report.ReportsList, "ViewDate", currsort, "Date", prevp, page, isdesc, numj);
                                break;
                            }
                        }
                    }

                    #endregion

                #region Nibss Operator Logs

                if (searchtype == "2")
                {
                    switch (orderby)
                    {
                        case "OprName":
                            {
                                report.ReportsList = DoSort(report.ReportsList, "Operator.FirstName", currsort, "OprName", prevp, page, isdesc, numj);
                                break;
                            }
                        case "WebAccId":
                            {
                                report.ReportsList = DoSort(report.ReportsList, "Operator.OperatorId", currsort, "WebAccId", prevp, page, isdesc, numj);
                                break;
                            }
                        case "OprBank":
                            {
                                report.ReportsList = DoSort(report.ReportsList, "Bank.BankName", currsort, "OprBank", prevp, page, isdesc, numj);
                                break;
                            }
                        case "OprBranch":
                            {
                                report.ReportsList = DoSort(report.ReportsList, "Bank.BranchName", currsort, "OprBranch", prevp, page, isdesc, numj);
                                break;
                            }

                        case "LoginDate":
                            {
                                report.ReportsList = DoSort(report.ReportsList, "Operator.LoginDate", currsort, "LoginDate", prevp, page, isdesc, numj);
                                break;
                            }
                    }
                }

                #endregion

                #region Nibss Message Logs

                if (searchtype == "3" || searchtype == "4")
                {
                    switch (orderby)
                    {
                        case "GSM":
                            {
                                report.ReportsList = DoSort(report.ReportsList, "Message.GSMNumber", currsort, "GSM", prevp, page, isdesc, numj);
                                break;
                            }
                        case "MsgTxt":
                            {
                                report.ReportsList = DoSort(report.ReportsList, "Message.MessageText", currsort, "MsgTxt", prevp, page, isdesc, numj);
                                break;
                            }
                        case "MsgSentDate":
                            {
                                report.ReportsList = DoSort(report.ReportsList, "Message.SentDate", currsort, "MsgSentDate", prevp, page, isdesc, numj);
                                break;
                            }
                        case "MsgErrormsg":
                            {
                                report.ReportsList = DoSort(report.ReportsList, "Message.ErrorMessage", currsort, "MsgErrormsg", prevp, page, isdesc, numj);
                                break;
                            }

                        case "Email":
                            {
                                report.ReportsList = DoSort(report.ReportsList, "Message.EmailAddress", currsort, "Email", prevp, page, isdesc, numj);
                                break;
                            }

                        case "MsgTxtEmail":
                            {
                                report.ReportsList = DoSort(report.ReportsList, "Message.EmailAddress", currsort, "MsgTxtEmail", prevp, page, isdesc, numj);
                                break;
                            }

                        case "MsgSentDateEmail":
                            {
                                report.ReportsList = DoSort(report.ReportsList, "Message.SentDate", currsort, "MsgSentDateEmail", prevp, page, isdesc, numj);
                                break;
                            }

                        case "MsgErrormsgEmail":
                            {
                                report.ReportsList = DoSort(report.ReportsList, "Message.ErrorMessage", currsort, "MsgErrormsgEmail", prevp, page, isdesc, numj);
                                break;
                            }
                    }
                }

                #endregion

                #region Nibss Exception Logs

                if (searchtype == "5")
                {
                    switch (orderby)
                    {
                        case "ExDate":
                            {
                                report.ReportsList = DoSort(report.ReportsList, "Error.ExceptionDate", currsort, "ExDate", prevp, page, isdesc, numj);
                                break;
                            }
                        case "ErrMsg":
                            {
                                report.ReportsList = DoSort(report.ReportsList, "Error.Message", currsort, "ErrMsg", prevp, page, isdesc, numj);
                                break;
                            }
                        case "ErrMachineIp":
                            {
                                report.ReportsList = DoSort(report.ReportsList, "Error.MachineIP", currsort, "ErrMachineIp", prevp, page, isdesc, numj);
                                break;
                            }
                        case "ErrBrowser":
                            {
                                report.ReportsList = DoSort(report.ReportsList, "Error.Browser", currsort, "ErrBrowser", prevp, page, isdesc, numj);
                                break;
                            }
                    }
                }

                #endregion

                #region Nibss Cursor Logs

                if (searchtype == "6")
                {
                    switch (orderby)
                    {
                        case "CursorVal":
                            {
                                report.ReportsList = DoSort(report.ReportsList, "Cursor.Value", currsort, "CursorVal", prevp, page, isdesc, numj);
                                break;
                            }
                        case "CursorName":
                            {
                                report.ReportsList = DoSort(report.ReportsList, "Cursor.Name", currsort, "CursorName", prevp, page, isdesc, numj);
                                break;
                            }
                        case "CursorSid":
                            {
                                report.ReportsList = DoSort(report.ReportsList, "Cursor.SID", currsort, "CursorSid", prevp, page, isdesc, numj);
                                break;
                            }
                    }
                }

                #endregion

                report.CurrentSort = sortby;
            }
            else
                report.CurrentSort = "Date";
        }

        private List<Report> DoSort(List<Report> reportlist, string propertyName, string currsort, string sortingby, string prevp, string page, string isdesc, string numj)
        {
            // random exceptions
            if (propertyName == "Cursor.Value")
                isdesc = isdesc == "6" ? "5" : "6";


            if (currsort == sortingby)
            {
                if (prevp == page)
                {
                    if (!string.IsNullOrEmpty(isdesc) && isdesc == "6")
                    {
                        if (numj == "0")
                            return reportlist.OrderBy(propertyName + " DESC").ToList();
                        else
                            return reportlist.OrderBy(propertyName).ToList();
                    }
                    else
                    {
                        if (numj == "0")
                            return reportlist.OrderBy(propertyName).ToList();
                        else
                            return reportlist.OrderBy(propertyName + " DESC").ToList();
                    }
                }
                else
                {
                    if (!string.IsNullOrEmpty(isdesc) && isdesc == "5")
                        return reportlist.OrderBy(propertyName + " DESC").ToList();
                    else
                        return reportlist.OrderBy(propertyName).ToList();
                }
            }
            else
                return reportlist.OrderBy(propertyName).ToList();
        }

        #endregion
    }
}
