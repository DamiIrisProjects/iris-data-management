﻿using System.Web.Mvc;
using System.Web.Routing;

namespace IrisDataManagement
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            routes.MapRoute(null, "connector", new { controller = "Files", action = "Index" });
            routes.MapRoute(null, "Thumbnails/{tmb}", new { controller = "Files", action = "Thumbs", tmb = UrlParameter.Optional });

            routes.MapRoute(
               "ControlPanelWelcome", // Route name
               "ControlPanel/{action}/{id}", // URL with parameters
               new { controller = "ControlPanel", action = "Welcome", id = UrlParameter.Optional } // Parameter defaults
           );

            routes.MapRoute(
               "Uploader", // Route name
               "Upload/{action}/{id}", // URL with parameters
               new { controller = "Upload", action = "Upload", id = UrlParameter.Optional } // Parameter defaults
           );

            routes.MapRoute(
              "Error", // Route name
              "Error/{action}/{id}", // URL with parameters
              new { controller = "Error", action = "Index", id = UrlParameter.Optional } // Parameter defaults
          );

            routes.MapRoute(
               "Account", // Route name
               "Account/{action}/{id}", // URL with parameters
               new { controller = "Account", action = "Login", id = UrlParameter.Optional } // Parameter defaults
           );

            routes.MapRoute(
               "Dashboards", // Route name
               "Dashboards/{action}/{id}", // URL with parameters
               new { controller = "Dashboards", action = "Home", id = UrlParameter.Optional } // Parameter defaults
           );

            routes.MapRoute(
               "DashboardsId", // Route name
               "Dashboards/{id}", // URL with parameters
               new { controller = "Dashboards", action = "Home", id = UrlParameter.Optional } // Parameter defaults
           );

            routes.MapRoute(
                "LoginScreen", // Route name
                "Account/{action}/{id}", // URL with parameters
                new { controller = "Account", action = "Login", id = UrlParameter.Optional } // Parameter defaults
            );

            routes.MapRoute(
                "Default2", // Route name
                "{action}/{id}/{ext}/{ext2}", // URL with parameters
                new { controller = "Home", action = "Home", id = UrlParameter.Optional, ext = UrlParameter.Optional, ext2 = UrlParameter.Optional }, new[] { "IrisDataManagement.Controllers" } // Parameter defaults
            );

            routes.MapRoute(
                "Default", // Route name
                "{controller}/{action}", // URL without parameters
                new { controller = "Home", action = "Home" }  // Parameter defaults
            );
        }
    }
}