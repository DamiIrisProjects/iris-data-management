﻿using System.Web.Optimization;
using System;

namespace IrisDataManagement
{
    public class BundleConfig
    {
        public static void AddDefaultIgnorePatterns(IgnoreList ignoreList)
        {
            if (ignoreList == null)
                throw new ArgumentNullException(nameof(ignoreList));
            ignoreList.Ignore("*.intellisense.js");
            ignoreList.Ignore("*-vsdoc.js");
            ignoreList.Ignore("*.debug.js", OptimizationMode.WhenEnabled);
            //ignoreList.Ignore("*.min.js", OptimizationMode.WhenDisabled);
            //ignoreList.Ignore("*.min.css", OptimizationMode.WhenDisabled);
        }

        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.IgnoreList.Clear();
            AddDefaultIgnorePatterns(bundles.IgnoreList);

            bundles.Add(new StyleBundle("~/Content/general").Include("~/Content/general.css"));

            bundles.Add(new ScriptBundle("~/bundles/master").Include(
                    "~/Scripts/xhoverintent.js",
                    "~/Scripts/Master.js"
            ));

            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                    "~/Scripts/jquery-{version}.js"
            ));

            bundles.Add(new StyleBundle("~/Content/login").Include(
                "~/Content/logging.css"
            ));

            bundles.Add(new ScriptBundle("~/bundles/login").Include(
                    "~/Scripts/jquery.validate.js",
                    "~/scripts/jquery.lightbox_me.js",
                    "~/Scripts/jquery.validate.unobtrusive.js",
                    "~/Scripts/loginjs.js"
           ));

            bundles.Add(new StyleBundle("~/Content/searchControlCss").Include(
                     "~/Content/SearchControl.css",
                     "~/Content/elfinder/css/smoothness-1.8.23/jquery-ui-1.8.23.custom.css",
                     "~/Content/select2.css"
           ));

            bundles.Add(new ScriptBundle("~/bundles/searchControlJs").Include(
                    "~/Scripts/jquery.select2.min.js",
                    "~/Scripts/jquery.lightbox_me.js",
                    "~/Scripts/jquery-ui-1.10.4.custom.min.js",
                    "~/Scripts/Search.js"
            ));

            bundles.Add(new StyleBundle("~/Content/mydocuments").Include(
                  "~/Content/MyDocs.css",
                  "~/Content/FileUploader.css",
                  "~/Content/select2.css",
                  "~/Content/elfinder/css/smoothness-1.8.23/jquery-ui-1.8.23.custom.css",
                  "~/Content/elfinder/css/elfinder.min.css",
                  "~/scripts/dist/themes/default/style.min.css"
            ));

            bundles.Add(new ScriptBundle("~/bundles/mydocuments").Include(
                    "~/Scripts/jquery-{version}.js",
                    "~/Scripts/jquery-ui.custom.min.js",
                    "~/Scripts/jquery.select2.min.js",
                    "~/ckeditor/ckeditor.js",
                    "~/scripts/jquery.lightbox_me.js",
                    "~/scripts/mydocs.js"
            ));

            bundles.Add(new StyleBundle("~/Content/myaccount").Include(
                "~/Content/MyDocs.css",
                "~/Content/select2.css",
                "~/Content/elfinder/css/smoothness-1.8.23/jquery-ui-1.8.23.custom.css"
          ));

            bundles.Add(new ScriptBundle("~/bundles/myaccount").Include(
                    "~/Scripts/jquery-{version}.js",
                     "~/Scripts/jquery.select2.min.js",
                    "~/scripts/jquery.lightbox_me.js",
                    "~/scripts/myaccount.js"
            ));

            bundles.Add(new StyleBundle("~/Content/reports").Include(
                 "~/Content/MyDocs.css",
                 "~/Content/reports.css",
                 "~/Content/select2.css",
                 "~/Content/elfinder/css/smoothness-1.8.23/jquery-ui-1.8.23.custom.css"
           ));

            bundles.Add(new ScriptBundle("~/bundles/reports").Include(
                    "~/Scripts/jquery-{version}.js",
                     "~/Scripts/jquery.select2.min.js",
                    "~/Scripts/jquery-ui-1.10.4.custom.min.js",
                    "~/scripts/jquery.lightbox_me.js",
                    "~/scripts/reports.js"
            ));

            bundles.Add(new StyleBundle("~/Content/DocSearchCss").Include(
                     "~/Content/DocSearch.css"
            ));

            bundles.Add(new ScriptBundle("~/bundles/DocSearchJs").Include(
                 "~/Scripts/imagesloaded.js"   
            ));

            bundles.Add(new StyleBundle("~/Content/CalenderCss").Include(
                     "~/Content/fullcalendar.css",
                     "~/Content/calender.css",
                      "~/Content/select2.css"
            ));

            bundles.Add(new ScriptBundle("~/bundles/CalenderJs").Include(
                "~/Scripts/moment.min.js",
                "~/scripts/jquery.lightbox_me.js",
                "~/Scripts/jquery-ui.custom.min.js",
                "~/Scripts/jquery.select2.min.js",
                "~/Scripts/fullcalendar.js",
                "~/Scripts/xCalender.js"
            ));

            bundles.Add(new StyleBundle("~/Content/DocSearchControlCss").Include(
                     "~/Content/DocSearchControl.css"
            ));

            bundles.Add(new ScriptBundle("~/bundles/mydocspartials").Include(
                    "~/scripts/dist/jstree.min.js",
                    "~/Scripts/jquery.select2.min.js",
                    "~/Scripts/mydocsPartials.js"
          ));

            bundles.Add(new StyleBundle("~/Content/BranchReportCss").Include(
                    "~/Content/branchreport.css"
           ));

            bundles.Add(new StyleBundle("~/Content/multiview").Include(
                "~/Content/logging.css",
                "~/Content/jquery-ui.css",
                "~/Content/sDashboard.css",
                "~/Content/jquery.gritter.css",
                "~/Content/datatables.css"
            ));

            bundles.Add(new ScriptBundle("~/bundles/multiview").Include(
                    "~/Scripts/libs/jquery/jquery-ui.js",
                    "~/Scripts/libs/touchpunch/jquery.ui.touch-punch.js",
                    "~/Scripts/libs/gitter/jquery.gritter.js",
                    "~/Scripts/libs/datatables/jquery.dataTables.js",
                    "~/Scripts/libs/flotr2/flotr2.js",
                    "~/Scripts/jquery-sDashboard.js",
                    "~/Scripts/multiview.js"
           ));
        }
    }
}