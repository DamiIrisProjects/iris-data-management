﻿namespace IrisDataManagement.Helpers
{
    public static class IHelper
    {
        public static string ToFirstLetterUpper(string input)
        {
            if (!string.IsNullOrEmpty(input))
            {
                string[] words = input.ToLower().Split(' ');
                string result = string.Empty;

                foreach (string word in words)
                {
                    if (word != string.Empty && word != " ")
                    {
                        char[] a = word.ToCharArray();
                        a[0] = char.ToUpper(a[0]);

                        if (result == string.Empty)
                            result = result + new string(a);
                        else
                            result = result + " " + new string(a);
                    }
                }

                return result;
            }

            return string.Empty;
        }

        public static string ToFirstLetterUpperOnly(string word)
        {
            if (word != string.Empty && word != " ")
            {
                char[] a = word.ToLower().ToCharArray();
                a[0] = char.ToUpper(a[0]);

                return new string(a);
            }

            return string.Empty;
        }
    }
}