﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using IrisCommon.Entities;
using System.Data;

namespace IrisContract
{
    [ServiceContract]
    public interface DataOperations
    {
        #region Registration and logins
        
        [OperationContract]
        int RegisterUser(Person enrollee);

        [OperationContract]
        int ActivateUser(string guid);

        [OperationContract]
        Person LoginUser(string username, string password);

        [OperationContract]
        int UpdateUserInfo(Person person);

        [OperationContract]
        string SendPasswordByMail(string email);

        [OperationContract]
        int VerifyPasswordChange(string email, string guid);

        [OperationContract]
        int UpdateForgottenPassword(string email, string password);

        [OperationContract]
        int VerifyUsernameDoesNotExists(string email);

        [OperationContract]
        int ResetUserPassword(string email);

        [OperationContract]
        int ChangeStaffPassword(string email, int userid);

        [OperationContract]
        void SetProfilePicture(string loc, int userid);

        #endregion

        #region Misc

        [OperationContract]
        void SaveError(Error error);

        [OperationContract]
        bool TestConnection();

        [OperationContract]
        DataSet GetLists();

        #endregion

        #region Documents

        [OperationContract]
        List<Document> GetUserDocuments(int user_id);

        [OperationContract]
        int SaveDocument(Document doc);

        [OperationContract]
        List<Document> SearchForDocuments(string k, string cat, string proj, string staffdoc, string updatefrom, string updateto, string docdatefrom, string docdateto, string ext, int userid);

        [OperationContract]
        Document GetDocument(int doc_id, int user_id, int audit_type);

        [OperationContract]
        void DeleteFile(string location);

        [OperationContract]
        void RenameFile(string location, string newname);

        [OperationContract]
        void GiveAccessToUsers(string userids, string docid);

        [OperationContract]
        void GiveAccessToDepts(string depts, string docid);

        #endregion

        #region Reports/Maintenance

        [OperationContract]
        List<Report> GetPassportVerificationHistory(DateTime start, DateTime stop, bool WithFace);

        [OperationContract]
        Report GetPassportDetails(string docno);

        [OperationContract]
        List<Report> GetBankLoginHistory(DateTime From, DateTime To);

        [OperationContract]
        List<Report> GetBankMessageHistory(DateTime From, DateTime To);

        [OperationContract]
        List<Report> GetBankExceptionsHistory(DateTime From, DateTime To);

        [OperationContract]
        List<Report> GetBankCursorStatus();

        [OperationContract]
        List<Person> GetUserList();

        [OperationContract]
        int EditStaffAccessLevel(int person_id, int level, int currlevel, int mystaff_id);

        [OperationContract]
        List<PassportReport> GetPassportsIssued(DateTime start, DateTime end);

        #endregion

        #region Calender

        [OperationContract]
        List<Event> GetCalenderEvents();

        [OperationContract]
        int CreateCalenderEvent(Event ev);

        [OperationContract]
        int UpdateCalenderEvent(Event ev);

        [OperationContract]
        int DeleteCalenderEvent(int eventid, int userid);

        [OperationContract]
        int UpdateStaffEventChange(Event ev);

        #endregion

        #region Audit

        [OperationContract]
        List<Audit> GetAuditTrail(DateTime From, DateTime To);

        #endregion

        #region Dashboards

        [OperationContract]
        Dictionary<string, int[]> GetBankUsersRegistered(DateTime start, DateTime end);

        [OperationContract]
        SliderData GetSliderData();

        [OperationContract]
        SliderData GetSliderDataToday();

        [OperationContract]
        Dictionary<string, int[]> GetPassportVerificationHistoryDic(DateTime start, DateTime end);

        [OperationContract]
        SliderData GetPassportDefaultData();

        [OperationContract]
        SliderData GetPassportData(DateTime start, DateTime end);

        [OperationContract]
        SocketWorksSummary GetSocketWorksDefaultData();

        [OperationContract]
        SocketWorksSummary GetSocketWorksTodaysData();

        #endregion   
    
        #region Branch Viewer

        [OperationContract]
        List<Branch> GetAllBranches();

        #endregion

        #region iDoc Application

        [OperationContract]
        List<string> GetDirectories(string path);
        
        [OperationContract]
        string CreateFolder(string path);

        [OperationContract]
        int SaveFile(byte[] file, string path);

        #endregion
    }
}
