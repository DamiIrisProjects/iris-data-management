﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.IO;
using System.Text.RegularExpressions;
using System.Windows.Controls;
using System.Drawing;
using System.Windows.Media.Imaging;
using System.Windows.Interop;
using System.Windows;
using System.Drawing.Drawing2D;
using System.Windows.Media;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using IrisCommon.Entities;

namespace IrisCommon.Helpers
{
    static public class Helper
    {
        static Random _random = new Random();
        static System.Windows.Media.Brush defaultComboBrush;

        public static byte[] BitmapToByteArray(Bitmap someBitmap)
        {
            using (Bitmap bitmap = new Bitmap(someBitmap))
            {
                byte[] byteArray = (byte[])TypeDescriptor.GetConverter(someBitmap).ConvertTo(someBitmap, typeof(byte[]));
                return byteArray;
            }
        }

        public static byte[] BitmapSourceToArray(BitmapSource bitmapSource)
        {
            BitmapEncoder encoder = new JpegBitmapEncoder();
            using (MemoryStream stream = new MemoryStream())
            {
                encoder.Frames.Add(BitmapFrame.Create(bitmapSource));
                encoder.Save(stream);
                return stream.ToArray();
            }
        }

        public static Bitmap BytesToBitmap(byte[] byteArray)
        {
            using (MemoryStream ms = new MemoryStream(byteArray))
            {
                Bitmap img = (Bitmap)System.Drawing.Image.FromStream(ms);
                return img;
            }
        }

        public static BitmapSource BitmapToSource(Bitmap someBitmap)
        {
            using (Bitmap bitmap = new Bitmap(someBitmap))
            {
                return Imaging.CreateBitmapSourceFromHBitmap(
                     bitmap.GetHbitmap(),
                     IntPtr.Zero,
                     Int32Rect.Empty,
                     BitmapSizeOptions.FromEmptyOptions());
            }
        }

        public static byte[] BitmapImageToByteArray(BitmapImage imageC)
        {
            MemoryStream memStream = new MemoryStream();
            JpegBitmapEncoder encoder = new JpegBitmapEncoder();
            encoder.Frames.Add(BitmapFrame.Create(imageC));
            encoder.Save(memStream);
            return memStream.GetBuffer();
        }

        public static Byte[] BufferFromBitmapImage(BitmapImage imageSource)
        {
            Stream stream = imageSource.StreamSource;
            Byte[] buffer = null;
            if (stream != null && stream.Length > 0)
            {
                using (BinaryReader br = new BinaryReader(stream))
                {
                    buffer = br.ReadBytes((Int32)stream.Length);
                }
            }

            return buffer;
        }

        public static bool NoTextAllowed(string text)
        {
            Regex regex = new Regex("^[0-9]+$"); //regex that matches allowed text
            return regex.IsMatch(text);
        }

        public static bool CheckTextBox(TextBox Textbox)
        {
            if (string.IsNullOrEmpty(Textbox.Text))
            {
                Textbox.Background = System.Windows.Media.Brushes.Pink;
                return false;
            }
            else
                Textbox.Background = System.Windows.Media.Brushes.White;

            return true;
        }

        public static bool CheckDatePicker(DatePicker datePicker)
        {
            if (datePicker.SelectedDate == DateTime.Now || datePicker.SelectedDate == null)
            {
                datePicker.BorderBrush = System.Windows.Media.Brushes.Pink;
                return false;
            }
            else
                datePicker.BorderBrush = System.Windows.Media.Brushes.Gray;

            return true;
        }

        public static bool CheckTickBox(CheckBox checkBox)
        {
            if (checkBox.IsChecked != true)
            {
                checkBox.BorderBrush = System.Windows.Media.Brushes.Red;
                checkBox.Foreground = System.Windows.Media.Brushes.Red;
                return false;
            }
            else
            {
                checkBox.BorderBrush = System.Windows.Media.Brushes.Gray;
                checkBox.Foreground = System.Windows.Media.Brushes.Black;
            }

            return true;
        }

        public static bool CheckRadioBox(List<RadioButton> list)
        {
            bool isTicked = false;
            foreach (RadioButton btn in list)
            {
                if (btn.IsChecked == true)
                    isTicked = true;
            }
            
            foreach (RadioButton btn in list)
            {
                if (isTicked == false)
                {
                    btn.BorderBrush = System.Windows.Media.Brushes.Red;
                    btn.Foreground = System.Windows.Media.Brushes.Red;
                }
                else
                {
                    btn.BorderBrush = System.Windows.Media.Brushes.Gray;
                    btn.Foreground = System.Windows.Media.Brushes.Black;
                }
            }

            return isTicked;
        }

        public static bool CheckComboBox(ComboBox combobox)
        {
            if (combobox.SelectedIndex == -1)
            {
                combobox.BorderBrush = System.Windows.Media.Brushes.Red;
                return false;
            }
            else
            {
                combobox.BorderBrush = System.Windows.Media.Brushes.Gray;
                return true;
            }
        }

        public static Bitmap ResizeImage(System.Drawing.Image srcImage, int width, int height)
        {
            Bitmap newImage = new Bitmap(width, height);
            using (Graphics gr = Graphics.FromImage(newImage))
            {
                gr.SmoothingMode = SmoothingMode.HighQuality;
                gr.InterpolationMode = InterpolationMode.HighQualityBicubic;
                gr.PixelOffsetMode = PixelOffsetMode.HighQuality;
                gr.DrawImage(srcImage, new System.Drawing.Rectangle(0, 0, width, height));
            }

            return newImage;
        }

        public static bool Onlyletters(string input)
        {
            char[] letters = "abcdefghijklmnopqrstuvwxyz".ToCharArray();

            foreach (char c in input.ToLower())
            {
                if (!letters.Contains(c))
                    return false;
            }

            return true;
        }

        public static bool OnlyNumbers(string input)
        {
            char[] numbers = "1234567890".ToCharArray();

            foreach (char c in input.ToLower())
            {
                if (!numbers.Contains(c))
                    return false;
            }

            return true;
        }

        public static string ToFirstLetterUpper(string input)
        {
            if (!string.IsNullOrEmpty(input))
            {
                string[] words = input.ToLower().Split(' ');
                string result = string.Empty;

                foreach (string word in words)
                {
                    if (word != string.Empty && word != " ")
                    {
                        char[] a = word.ToCharArray();
                        a[0] = char.ToUpper(a[0]);

                        if (result == string.Empty)
                            result = result + new string(a);
                        else
                            result = result + " " + new string(a);
                    }
                }

                return result;
            }

            return string.Empty;
        }

        public static IEnumerable<T> FindVisualChildren<T>(DependencyObject depObj) where T : DependencyObject
        {
            if (depObj != null)
            {
                for (int i = 0; i < VisualTreeHelper.GetChildrenCount(depObj); i++)
                {
                    DependencyObject child = VisualTreeHelper.GetChild(depObj, i);
                    if (child != null && child is T)
                    {
                        yield return (T)child;
                    }

                    foreach (T childOfChild in FindVisualChildren<T>(child))
                    {
                        yield return childOfChild;
                    }
                }
            }
        }

        public static T Clone<T>(this T source)
        {
            if (!typeof(T).IsSerializable)
            {
                throw new ArgumentException("The type must be serializable.", "source");
            }

            // Don't serialize a null object, simply return the default for that object
            if (Object.ReferenceEquals(source, null))
            {
                return default(T);
            }

            IFormatter formatter = new BinaryFormatter();
            Stream stream = new MemoryStream();
            using (stream)
            {
                formatter.Serialize(stream, source);
                stream.Seek(0, SeekOrigin.Begin);
                return (T)formatter.Deserialize(stream);
            }
        }

        public static void HighlightTextbox(bool highlight, TextBox textbox)
        {
            if (highlight)
                textbox.Background = System.Windows.Media.Brushes.Pink;
            else
                textbox.Background = System.Windows.Media.Brushes.White;
        }

        public static void HighlightCombobox(bool highlight, ComboBox combobox)
        {
            if (highlight)
            {
                if (defaultComboBrush == null)
                    defaultComboBrush = combobox.Background;
                combobox.Background = System.Windows.Media.Brushes.Pink;
            }
            else
            {
                if (defaultComboBrush == null)
                    defaultComboBrush = combobox.Background;
                combobox.Background = defaultComboBrush;
            }
        }

        public static string CreateJobId()
        {
            // Create randomly generated 7 character ID - first 2 Letters and the rest numbers
            StringBuilder s = new StringBuilder();
            s.Append(GetLetter());
            s.Append(GetLetter());
            for (int i = 2; i < 7; i++) { s.Append(GetNumber()); }

            return s.ToString().ToUpper();
        }

        public static char GetLetter()
        {
            // This method returns a random lowercase letter.
            // ... Between 'a' and 'z' inclusive.
            int num = _random.Next(0, 26); // Zero to 25
            char let = (char)('a' + num);
            return let;
        }

        public static int GetNumber()
        {
            // This method returns a random number.
            return _random.Next(0, 9); // Zero to 9
        }

        static public bool ValidateEmailAddress(string address, bool allowBlank)
        {
            //string patternLenient = @"\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*";

            string patternStrict = @"^(([^<>()[\]\\.,;:\s@\""]+"
               + @"(\.[^<>()[\]\\.,;:\s@\""]+)*)|(\"".+\""))@"
               + @"((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}"
               + @"\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+"
               + @"[a-zA-Z]{2,}))$";

            if (String.IsNullOrEmpty(address))
            {
                if (!allowBlank)
                    return false;
                else
                    return true;
            }

            if (!Regex.IsMatch(address, patternStrict))
                return false;

            return true;
        }

        public static bool ConfirmReqCode(string requestcode, string compareTo)
        {
            string s = requestcode[1].ToString() + requestcode[3] + requestcode[5] + requestcode[10] + requestcode[14] + requestcode[18];
            return s.Equals(compareTo) ? true : false;
        }

        public static string CreateReqCode(string requestcode)
        {
            return requestcode[1].ToString() + requestcode[3] + requestcode[5] + requestcode[10] + requestcode[14] + requestcode[18];
        }

        public static Error CreateError(Exception ex)
        {
            Error error = new Error()
            {
                Message = ex.Message,
                StackTrace = ex.StackTrace,
                TargetSite = ex.TargetSite.Name,
                Timestamp = DateTime.Now
            };

            if (ex.InnerException != null)
            {
                error.InnerExMsg = ex.InnerException.Message;
                error.InnerExSrc = ex.InnerException.Source;
                error.InnerExStkTrace = ex.InnerException.StackTrace;
                error.InnerExTargetSite = ex.InnerException.TargetSite.Name;
            }

            return error;
        }

        public static Error CreateAppError(Exception ex)
        {
            Error error = new Error()
            {
                Message = ex.Message,
                StackTrace = ex.StackTrace,
                TargetSite = ex.TargetSite.Name,
                Timestamp = DateTime.Now,
                AppError = 1
            };

            if (ex.InnerException != null)
            {
                error.InnerExMsg = ex.InnerException.Message;
                error.InnerExSrc = ex.InnerException.Source;
                error.InnerExStkTrace = ex.InnerException.StackTrace;
                error.InnerExTargetSite = ex.InnerException.TargetSite.Name;
            }

            return error;
        }

        public static string GetMimeType(string fileName)
        {
            string mimeType = "application/unknown";
            string ext = Path.GetExtension(fileName).ToLower();
            Microsoft.Win32.RegistryKey regKey = Microsoft.Win32.Registry.ClassesRoot.OpenSubKey(ext); // henter info fra windows registry
            if (regKey != null && regKey.GetValue("Content Type") != null)
            {
                mimeType = regKey.GetValue("Content Type").ToString();
            }
            else if (ext == ".png") // a couple of extra info, due to missing information on the server
            {
                mimeType = "image/png";
            }
            else if (ext == ".flv")
            {
                mimeType = "video/x-flv";
            }


            return mimeType;
        }
    }

    public static class ListExtenstions
    {
        public static void AddMany<T>(this List<T> list, params T[] elements)
        {
            for (int i = 0; i < elements.Length; i++)
            {
                list.Add(elements[i]);
            }
        }
    }
}
