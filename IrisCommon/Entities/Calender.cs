﻿namespace IrisCommon.Entities
{
    public class Calender
    {
        public string DefaultDate { get; set; }
        public string Start { get; set; }
        public string Stop { get; set; }
        public int GroupHour { get; set; }
        public int GroupMinute { get; set; }
        public int CurrentEventId { get; set; }
        public string EventTitle { get; set; }
        public string MyEvents { get; set; }
        public string SelectedStaff { get; set; }

        public string EventsArray { get; set; }
        public string StaffArray { get; set; }
        public string GroupsArray { get; set; }
        public string HourArray { get; set; }
        public string MinuteArray { get; set; }
        public string Group { get; set; }
        public string ColorsArray { get; set; }
        public string Color { get; set; }
        public string Background { get; set; }
    }
}
