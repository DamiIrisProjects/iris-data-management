﻿using System.Runtime.Serialization;

namespace IrisCommon.Entities
{
    [DataContract]
    public class OracleCursor
    {
        [DataMember]
        public int Value { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public int SID { get; set; }
    }
}
