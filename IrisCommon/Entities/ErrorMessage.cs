﻿using System;
using System.Runtime.Serialization;

namespace IrisCommon.Entities
{
    [DataContract]
    public class ErrorMessage
    {
        [DataMember]
        public DateTime ExceptionDate { get; set; }

        [DataMember]
        public string Message { get; set; }

        [DataMember]
        public string StackTrace { get; set; }

        [DataMember]
        public string MachineIP { get; set; }

        [DataMember]
        public string Browser { get; set; }
    }
}
