﻿using System.Runtime.Serialization;

namespace IrisCommon.Entities
{
    [DataContract]
    public class Bank
    {
        [DataMember]
        public int BankId { get; set; }

        [DataMember]
        public string BankName { get; set; }

        [DataMember]
        public int BranchId { get; set; }

        [DataMember]
        public string BranchName { get; set; }
    }
}
