﻿using System;
using System.Runtime.Serialization;

namespace IrisCommon.Entities
{
    public class AfisReport
    {
        [DataMember]
        public int BranchCode { get; set; }

        [DataMember]
        public string BranchName { get; set; }

        [DataMember]
        public DateTime EntryDay { get; set; }

        [DataMember]
        public int Pending { get; set; }

        [DataMember]
        public int Approved { get; set; }

        [DataMember]
        public int Dropped { get; set; }

        [DataMember]
        public int Rejected { get; set; }

        [DataMember]
        public int Enrols { get; set; }
    }
}
