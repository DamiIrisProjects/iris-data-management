﻿using System;

namespace IrisCommon.Entities
{
    public class Error
    {
        public string Message { get; set; }
        public string StackTrace { get; set; }
        public string TargetSite { get; set; }
        public string InnerExMsg { get; set; }
        public string InnerExSrc { get; set; }
        public string InnerExStkTrace { get; set; }
        public string InnerExTargetSite { get; set; }
        public string User { get; set; }
        public DateTime Timestamp { get; set; }
        public int AppError { get; set; }
    }
}