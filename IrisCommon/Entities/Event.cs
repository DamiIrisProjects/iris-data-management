﻿using System;
using System.Runtime.Serialization;

namespace IrisCommon.Entities
{
    [DataContract]
    public class Event
    {
        [DataMember]
        public int EventId { get; set; }

        [DataMember]
        public string Title { get; set; }

        [DataMember]
        public string Start { get; set; }

        [DataMember]
        public string End { get; set; }

        [DataMember]
        public string BackgroundColor { get; set; }

        [DataMember]
        public string TextColor { get; set; }
        
        [DataMember]
        public string Description { get; set; }

        [DataMember]
        public int GroupId { get; set; }

        [DataMember]
        public int IsCancelled { get; set; }

        [DataMember]
        public int IsAllDay { get; set; }

        [DataMember]
        public string StaffIinvited { get; set; }

        [DataMember]
        public int SendEmail { get; set; }

        [DataMember]
        public DateTime TimePosted { get; set; }

        [DataMember]
        public Person PostedBy { get; set; }
    }
}
