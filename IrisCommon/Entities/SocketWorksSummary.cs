﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace IrisCommon.Entities
{
    [DataContract]
    public class SocketWorksSummary
    {
        [DataMember]
        public int TotalUploaded { get; set; }

        [DataMember]
        public int UploadedToday { get; set; }

        [DataMember]
        public int UploadedThisWeek { get; set; }

        [DataMember]
        public int UploadedLastWeek { get; set; }

        [DataMember]
        public int UploadedThisMonth { get; set; }

        [DataMember]
        public int Redeemed { get; set; }
        
        [DataMember]
        public Dictionary<int, Branch> BranchDic { get; set; }

        [DataMember]
        public List<SocketWorksEntity> LastHourData { get; set; }
    }
}
