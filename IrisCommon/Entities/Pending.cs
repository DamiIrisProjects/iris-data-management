﻿using System;
using System.Runtime.Serialization;

namespace IrisCommon.Entities
{
    [DataContract]
    public class Pending
    {
        [DataMember]
        public DateTime Date { get; set; }

        [DataMember]
        public string QueryNo { get; set; }

        [DataMember]
        public string FormNo { get; set; }

        [DataMember]
        public string Priority { get; set; }

        [DataMember]
        public string Flag { get; set; }

        [DataMember]
        public string Appr { get; set; }
    }
}
