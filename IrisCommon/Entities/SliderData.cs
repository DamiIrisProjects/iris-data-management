﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace IrisCommon.Entities
{
    [DataContract]
    public class SliderData
    {
        #region NIBBS

        [DataMember]
        public List<Report> Piereports { get; set; }

        [DataMember]
        public List<Report> Verificationreports { get; set; }

        [DataMember]
        public List<Report> OracleCursor { get; set; }

        [DataMember]
        public Dictionary<string, int[]> Bankusersreports { get; set; }

        [DataMember]
        public Dictionary<string, int[]> VerifiedPassports { get; set; } 

        #endregion

        #region MyRegion

        [DataMember]
        public List<AfisReport> TotalAfisData { get; set; }

        [DataMember]
        public List<PassportReport> TotalPassportData { get; set; }

        [DataMember]
        public List<Pending> PendingReports { get; set; }


        #endregion
    }
}
