﻿using System;
using System.Runtime.Serialization;
using IrisCommon.Helpers;

namespace IrisCommon.Entities
{
    [DataContract]
    public class Operator
    {
        [DataMember]
        public int OperatorId { get; set; }

        [DataMember]
        public string FirstName { get; set; }

        [DataMember]
        public string Surname { get; set; }

        [DataMember]
        public DateTime LoginDate { get; set; }

        [DataMember]
        public string Comments { get; set; }

        public string FullName
        {
            get
            {
                return Helper.ToFirstLetterUpper(FirstName) + " " + Helper.ToFirstLetterUpper(Surname);
            }
        }
    }
}
