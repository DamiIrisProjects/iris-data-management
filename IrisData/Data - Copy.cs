﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Mail;
using System.Configuration;
using System.Data;
using MySql.Data.MySqlClient;
using System.Data.Common;
using System.ComponentModel;
using System.Security.Cryptography;
using System.IO;
using System.Globalization;
using IrisCommon.Entities;
using System.Net;
using IrisCommon.Helpers;

namespace IrisData
{
    public class Data
    {
        #region Consts

        const string smallparagraph = "<br />";
        const string paragraph = "<br /><br />";
        const string timezone = "W. Central Africa Standard Time";
        TimeZoneInfo nigTimeZone = TimeZoneInfo.FindSystemTimeZoneById(timezone);

        #endregion

        #region Registration and logins

        public int VerifyUsernameDoesNotExists(string email)
        {
            MysqlDataProv MysqlDataProv = new MysqlDataProv();
            DataSet dset = new DataSet();

            string query = "select email, is_active, registration_guid from iris.iris_user where email = @email";

            IDataParameter[] param =
                    {   
                        new MySqlParameter("email",  email.ToLower()),
                    };

            MysqlDataProv.RunProcedure(query, param, dset);
            if (dset.Tables[0].Rows.Count != 0)
            {
                // User already exists. Now if the user hasn't been activated already, send them an activation email
                if (dset.Tables[0].Rows[0]["is_active"].ToString() == "0")
                {
                    SendUserActivationMail(email, dset.Tables[0].Rows[0]["registration_guid"].ToString(), 1);
                    return 1;
                }
                else
                    return 2;

            }
            else
                return 0;
        }

        public int RegisterUser(Person enrollee)
        {
            MysqlDataProv MysqlDataProv = new MysqlDataProv();
            DataSet dset = new DataSet();

            // Hash password
            SaltedHash sh = SaltedHash.Create(enrollee.Password);

            string salt = sh.Salt;
            string hash = sh.Hash;
            string regGuid = Guid.NewGuid().ToString();

            string query = @"insert into iris.iris_user (firstname, phone_number, surname, email, PWD_HASH, PWD_SALT, date_created, registration_guid) values (@firstname, @phone_number, @surname, @email, @PWD_HASH, @PWD_SALT, @date_created, @registration_guid)";
            IDataParameter[] param =
                {   
                    new MySqlParameter("firstname",  enrollee.FirstName.ToUpper()),
                    new MySqlParameter("surname",  enrollee.Surname.ToUpper()),
                    new MySqlParameter("phone_number",  enrollee.PhoneNumber),
                    new MySqlParameter("email",  enrollee.EmailAddress.ToUpper()),
                    new MySqlParameter("PWD_HASH",  hash),
                    new MySqlParameter("PWD_SALT",  salt),
                    new MySqlParameter("date_created",  TimeZoneInfo.ConvertTimeFromUtc(DateTime.Now.ToUniversalTime(), nigTimeZone)),
                    new MySqlParameter("registration_guid",  regGuid)
                };

            int result = MysqlDataProv.ExecuteQuery(query, param);

            if (result != 0)
            {
                SendUserActivationMail(enrollee.EmailAddress, regGuid, 1);
            }

            return result;
        }

        public int ActivateUser(string guid)
        {
            MysqlDataProv MysqlDataProv = new MysqlDataProv();
            string query = "update iris.iris_user set is_active = 1 where registration_guid = @registration_guid";
            IDataParameter[] param =
                {   
                    new MySqlParameter("registration_guid",  guid)
                };

            return MysqlDataProv.ExecuteQuery(query, param);
        }

        public int ResetUserPassword(string email)
        {
            int res = VerifyUsernameDoesNotExists(email);

            // If the user exists
            if (res == 2)
            {
                SendPasswordByMail(email);
                return 0;
            }
            else
                return 2;
        }

        public int VerifyPassword(string email, string password)
        {
            // Get hash and salt from database
            MysqlDataProv MysqlDataProv = new MysqlDataProv();
            string query = @"select email, pwd_salt, pwd_hash from iris.iris_user where email = @email";

            MySqlParameter[] param = { new MySqlParameter("email", email) };
            DataSet dset = new DataSet();

            MysqlDataProv.RunProcedure(query, param, dset);

            if (dset.Tables[0].Rows.Count != 0)
            {
                DataRow row = dset.Tables[0].Rows[0];
                string salt = row["pwd_salt"].ToString();
                string hash = row["pwd_hash"].ToString();

                // Verify
                SaltedHash sh = SaltedHash.Create(salt, hash);
                bool value = sh.Verify(password);

                if (value) return 1;

                return 2;
            }
            else
                return 0;
        }

        public Person LoginUser(string email, string password)
        {
            // Get hash and salt from database
            MysqlDataProv MysqlDataProv = new MysqlDataProv();
            string query = @"select user_id, firstname, surname, email, gender, date_created, is_admin, phone_number, user_id, pwd_salt, pwd_hash, is_active, last_edit, clearance_level, profile_pic, u.position_id, u.department_id, position, department
                            from iris.iris_user u left join list_staff_positions lp on u.position_id = lp.position_id left join list_departments d on u.department_id = d.department_id where email = @email";

            MySqlParameter[] param = { new MySqlParameter("email", email) };
            DataSet dset = new DataSet();

            MysqlDataProv.RunProcedure(query, param, dset);

            if (dset.Tables[0].Rows.Count != 0)
            {
                DataRow row = dset.Tables[0].Rows[0];
                string salt = row["pwd_salt"].ToString();
                string hash = row["pwd_hash"].ToString();

                // Verify
                SaltedHash sh = SaltedHash.Create(salt, hash);
                bool value = sh.Verify(password);

                // Add audit trail
                Audit audit = new Audit()
                {
                    TimeStamp = DateTime.Now,
                    UserId = int.Parse(row["user_id"].ToString())
                };

                if (value)
                {
                    //set details
                    Person user = new Person();
                    user.Person_Id = int.Parse(row["user_id"].ToString());
                    user.FirstName = row["firstname"].ToString();
                    user.Gender = row["gender"].ToString();
                    user.PhoneNumber = row["phone_number"].ToString();
                    user.Surname = row["surname"].ToString();
                    user.ProfilePicture = row["profile_pic"].ToString();
                    user.Position = row["position_id"].ToString();
                    user.Department = row["department_id"].ToString();
                    user.PositionName = row["position"].ToString();
                    user.DepartmentName = row["department"].ToString();
                    user.EmailAddress = row["email"].ToString();
                    if (!string.IsNullOrEmpty(row["date_created"].ToString()))
                        user.DateOfRegistration = DateTime.Parse(row["date_created"].ToString());
                    if (!string.IsNullOrEmpty(row["last_edit"].ToString()))
                        user.LastActivityDate = DateTime.Parse(row["last_edit"].ToString());
                    user.IsActive = row["is_active"].ToString() == "1" ? true : false;
                    user.IsAdmin = row["is_admin"].ToString() == "1" ? true : false;
                    user.Salt = row["pwd_salt"].ToString();
                    user.Hash = row["pwd_hash"].ToString();
                    if (!string.IsNullOrEmpty(row["clearance_level"].ToString()))
                        user.ClearanceLevel = int.Parse(row["clearance_level"].ToString());

                    //Audit
                    audit.EventType = (int)AuditEnum.LoginSuccess;
                    AddAudit(audit);

                    return user;
                }
                else
                {
                    //Audit
                    audit.EventType = (int)AuditEnum.LoginFailed;
                    AddAudit(audit);
                    return null;
                }
            }
            else
            {
                //Audit
                Audit audit = new Audit()
                {
                    EventType = (int)AuditEnum.LoginFailed,
                    Description = email,
                    TimeStamp = DateTime.Now
                };

                AddAudit(audit);
                return null;
            }
        }

        public int ChangeStaffPassword(string password, int userid)
        {
            MysqlDataProv MysqlDataProv = new MysqlDataProv();

            string query = "update iris.iris_user set PWD_HASH = @PWD_HASH, PWD_SALT = @PWD_SALT, last_edit = @last_edit where user_id = @user_id";

            // Hash password

            SaltedHash sh = SaltedHash.Create(password);
            string salt = sh.Salt;
            string hash = sh.Hash;

            IDataParameter[] param =
            {   
                new MySqlParameter("user_id",  userid),
                new MySqlParameter("PWD_HASH",  hash),
                new MySqlParameter("PWD_SALT",  salt),
                new MySqlParameter("last_edit",  TimeZoneInfo.ConvertTimeFromUtc(DateTime.Now.ToUniversalTime(), nigTimeZone))
            };

            return MysqlDataProv.ExecuteQuery(query, param);
        }

        public int UpdateUserInfo(Person enrollee)
        {
            MysqlDataProv MysqlDataProv = new MysqlDataProv();

            if (VerifyPassword(enrollee.EmailAddress, enrollee.Password) == 1)
            {    
                if (enrollee.NewPassword != string.Empty)
                {
                    // Add audit
                    AddAudit(new Audit { EventType = (int)AuditEnum.UpdatedProfile, UserId = enrollee.Person_Id, TimeStamp = DateTime.Now, Description = "Changed Password" });

                    string query = "update iris.iris_user set firstname = @firstname, surname = @surname, position_id = @position_id, department_id = @department_id, phone_number = @phone_number, email = @email, gender = @gender, PWD_HASH = @PWD_HASH, PWD_SALT = @PWD_SALT, last_edit = @last_edit where user_id = @user_id";

                    // Hash password

                    SaltedHash sh = SaltedHash.Create(enrollee.NewPassword);
                    string salt = sh.Salt;
                    string hash = sh.Hash;

                    IDataParameter[] param =
                    {   
                        new MySqlParameter("user_id",  enrollee.Person_Id),
                        new MySqlParameter("firstname",  enrollee.FirstName.ToUpper()),
                        new MySqlParameter("surname",  enrollee.Surname.ToUpper()),
                        new MySqlParameter("email",  enrollee.EmailAddress),
                        new MySqlParameter("gender",  enrollee.Gender),
                        new MySqlParameter("department_id",  enrollee.Department),
                        new MySqlParameter("position_id",  enrollee.Position),
                        new MySqlParameter("PWD_HASH",  hash),
                        new MySqlParameter("PWD_SALT",  salt),
                        new MySqlParameter("phone_number",  enrollee.PhoneNumber),
                        new MySqlParameter("last_edit",  TimeZoneInfo.ConvertTimeFromUtc(DateTime.Now.ToUniversalTime(), nigTimeZone))
                    };

                    return MysqlDataProv.ExecuteQuery(query, param);
                }
                else
                {
                    // Add audit
                    AddAudit(new Audit { EventType = (int)AuditEnum.UpdatedProfile, UserId = enrollee.Person_Id, TimeStamp = DateTime.Now });

                    string query = "update iris.iris_user set firstname = @firstname, surname = @surname, position_id = @position_id, department_id = @department_id, phone_number = @phone_number, email = @email, gender = @gender, last_edit = @last_edit where user_id = @user_id";

                    IDataParameter[] param =
                    {   
                        new MySqlParameter("user_id",  enrollee.Person_Id),
                        new MySqlParameter("firstname",  enrollee.FirstName.ToUpper()),
                        new MySqlParameter("surname",  enrollee.Surname.ToUpper()),
                        new MySqlParameter("email",  enrollee.EmailAddress),
                        new MySqlParameter("department_id",  enrollee.Department),
                        new MySqlParameter("position_id",  enrollee.Position),
                        new MySqlParameter("gender",  enrollee.Gender),
                        new MySqlParameter("phone_number",  enrollee.PhoneNumber),
                        new MySqlParameter("last_edit",  TimeZoneInfo.ConvertTimeFromUtc(DateTime.Now.ToUniversalTime(), nigTimeZone))
                    };

                    return MysqlDataProv.ExecuteQuery(query, param);
                }
            }
            else
            {
                // Add audit
                AddAudit(new Audit { EventType = (int)AuditEnum.LoginFailed, UserId = enrollee.Person_Id, TimeStamp = DateTime.Now, Description = "Wrong password while updating profile" });

                return 0;
            }
        }

        private Person GetUserFromId(int user_id)
        {
            MysqlDataProv MysqlDataProv = new MysqlDataProv();
            DataSet dset = new DataSet();
            Person user = null;

            string query = @"select user_id, firstname, surname, email, gender, date_created, phone_number, user_id, pwd_salt, pwd_hash, is_active, last_edit, clearance_level, profile_pic, u.position_id, u.department_id, position, department
                            from iris.iris_user u left join list_staff_positions lp on u.position_id = lp.position_id 
                            left join list_departments d on u.department_id = d.department_id where user_id = @user_id"; 
            IDataParameter[] param =
                {   
                    new MySqlParameter("user_id",  user_id)
                };

            MysqlDataProv.RunProcedure(query, param, dset);

            if (dset.Tables[0].Rows.Count != 0)
            {
                DataRow row = dset.Tables[0].Rows[0];
                user = new Person();

                user.Person_Id = int.Parse(row["user_id"].ToString());
                user.FirstName = row["firstname"].ToString();
                user.Gender = row["gender"].ToString();
                user.PhoneNumber = row["phone_number"].ToString();
                user.Surname = row["surname"].ToString();
                user.ProfilePicture = row["profile_pic"].ToString();
                user.Position = row["position_id"].ToString();
                user.Department = row["department_id"].ToString();
                user.PositionName = row["position"].ToString();
                user.DepartmentName = row["department"].ToString();
                user.EmailAddress = row["email"].ToString();
                if (!string.IsNullOrEmpty(row["date_created"].ToString()))
                    user.DateOfRegistration = DateTime.Parse(row["date_created"].ToString());
                if (!string.IsNullOrEmpty(row["last_edit"].ToString()))
                    user.LastActivityDate = DateTime.Parse(row["last_edit"].ToString());
                user.IsActive = row["is_active"].ToString() == "1" ? true : false;
                user.Salt = row["pwd_salt"].ToString();
                user.Hash = row["pwd_hash"].ToString();
                if (!string.IsNullOrEmpty(row["clearance_level"].ToString()))
                    user.ClearanceLevel = int.Parse(row["clearance_level"].ToString());
            }

            return user;
        }

        private Person GetStaffFromId(int staff_id)
        {
            MysqlDataProv MysqlDataProv = new MysqlDataProv();
            DataSet dset = new DataSet();
            Person person = null;

            string query = "select staff_id, firstname, surname, email from iris.list_iris_staff where staff_id = @staff_id and is_active = 1";
            IDataParameter[] param =
                {   
                    new MySqlParameter("staff_id",  staff_id)
                };

            MysqlDataProv.RunProcedure(query, param, dset);

            if (dset.Tables[0].Rows.Count != 0)
            {
                DataRow row = dset.Tables[0].Rows[0];
                person = new Person();

                person.EmailAddress = row["email"].ToString();
                person.FirstName = row["firstname"].ToString();
                person.Person_Id = int.Parse(row["staff_id"].ToString());
                person.Surname = row["surname"].ToString();
            }

            return person;
        }

        public int VerifyPasswordChange(string email, string guid)
        {
            string query = "select is_changing_pw, new_key, last_pwchange_expiry from iris.iris_user where email = @email";

            MysqlDataProv MysqlDataProv = new MysqlDataProv();
            DataSet dset = new DataSet();

            IDataParameter[] param =
                        {   
                            new MySqlParameter("email",  email)
                        };

            MysqlDataProv.RunProcedure(query, param, dset);

            // Verify if its correct
            if (dset.Tables[0].Rows.Count != 0)
            {
                int pwstatus = int.Parse(dset.Tables[0].Rows[0]["is_changing_pw"].ToString());
                Guid userkey = Guid.Parse(dset.Tables[0].Rows[0]["new_key"].ToString());
                DateTime expiry = DateTime.Parse((dset.Tables[0].Rows[0]["last_pwchange_expiry"].ToString()));

                // Check password status is set to changing
                if (pwstatus == 1)
                {
                    // Check Guid correct
                    int result = userkey.CompareTo(Guid.Parse(guid));

                    if (result != 0)
                        return 1;
                    else
                    {
                        // Check Expiration
                        if (TimeZoneInfo.ConvertTimeFromUtc(DateTime.Now.ToUniversalTime(), nigTimeZone).CompareTo(expiry) == 1)
                            return 2;
                        else
                            return 0;
                    }
                }
                else
                    return 1;
            }
            else
                return 1;
        }

        public int UpdateForgottenPassword(string email, string password)
        {
            MysqlDataProv MysqlDataProv = new MysqlDataProv();
            DataSet dset = new DataSet();

            // Hash password
            SaltedHash sh = SaltedHash.Create(password);

            string salt = sh.Salt;
            string hash = sh.Hash;

            // Update Password
            string query = "update iris.iris_user set PWD_HASH = @PWD_HASH, pwd_salt = @pwd_salt where email = @email";

            IDataParameter[] param =
                    {   
                        new MySqlParameter("PWD_HASH", hash),
                        new MySqlParameter("email",  email.ToLower()),
                        new MySqlParameter("pwd_salt",  salt)
                    };

            MysqlDataProv.ExecuteQuery(query, param);

            // Update member is_changingpw
            query = "update iris.iris_user set is_changing_pw = 0 where email = @email";

            IDataParameter[] param2 =
                    {   
                        new MySqlParameter("email",  email)
                    };

            MysqlDataProv.ExecuteQuery(query, param2);

            return 0;
        }

        public void SetProfilePicture(string loc, int userid)
        {
            MysqlDataProv MysqlDataProv = new MysqlDataProv();
            string query = "update iris.iris_user set profile_pic = @profile_pic where user_id = @user_id";

            IDataParameter[] param =
                    {   
                        new MySqlParameter("user_id",  userid),
                        new MySqlParameter("profile_pic",  loc),
                        new MySqlParameter("last_edit",  TimeZoneInfo.ConvertTimeFromUtc(DateTime.Now.ToUniversalTime(), nigTimeZone))
                    };

            MysqlDataProv.ExecuteQuery(query, param);
        }

        public string SendPasswordByMail(string email)
        {
            MysqlDataProv MysqlDataProv = new MysqlDataProv();
            DataSet dset = new DataSet();
            Guid new_key = Guid.NewGuid();

            //Set guid and pw status
            string query = "update iris.iris_user set is_changing_pw = 1, new_key = @new_key, last_pwchange_expiry = @last_pwchange_expiry where email = @email";

            IDataParameter[] param2 =
                    {   
                        new MySqlParameter("new_key",  new_key.ToString()),
                        new MySqlParameter("email",  email),
                        new MySqlParameter("last_pwchange_expiry",  TimeZoneInfo.ConvertTimeFromUtc(DateTime.Now.ToUniversalTime(), nigTimeZone).AddHours(1))
                    };
            MysqlDataProv.ExecuteQuery(query, param2);

            SendPasswordMail(email, new_key.ToString());
            return string.Empty;
            
        }

        private void SendPasswordMail(string email, string guid)
        {
            MailMessage mail = new MailMessage();
            string iris = "NoReply@irissmart.com.ng";
            string friendlyName = "Iris Smart Technologies";
            string server = string.Empty;
            if (ConfigurationManager.AppSettings["serverurl"] == null)
                server = "http://10.10.10.50:451";
            else
                server = ConfigurationManager.AppSettings["serverurl"].ToString();

            string buttonstyle = "background-color: #336699; font-family: arial; text-align:center; width:180px; height:30px; color: White; margin: 15px 0px 20px 0px; border: none; cursor: pointer; line-height: 30px; font-size: 15px; border-radius: 2px 2px 2px 2px; -moz-border-radius: 2px 2px 2px 2px; -webkit-border-radius: 2px 2px 2px 2px; -khtml-border-radius:  2px 2px 2px 2px;";
            string button = "<a style='text-decoration: none' href=\"" + server + "/Home?guid=" + guid + "&usr=" + email + "\"><div Style='" + buttonstyle + "'>Reset My Password</div></a>";

            StringBuilder sb = new StringBuilder();
            sb.Append("<div style='color:#333'>Hello, </div><br/>");
            sb.Append("<div style='color:#333'>Following your request on the Iris website, here is a link from which you can change your password:</div><br/>");
            sb.Append(button);
            sb.Append("<div style='color:#333; margin: 0px 0px 15px 0px'><b>Note:</b> You must be on the Iris network for this to work</div><br/>");
            sb.Append("<div style='color:#333; margin: 0px 0px 10px 0px'>Sincerly,</div><br/>");
            sb.Append("<div style='color:#333'>Iris Software Team</div>");

            mail.From = new MailAddress(iris, friendlyName);
            mail.To.Add(email);

            //set the content
            mail.Subject = "Password Recovery";
            mail.Body = sb.ToString();
            mail.IsBodyHtml = true;

            SendMail(mail);
        }

        public void SendUserActivationMail(string email, string guid, int type)
        {
            MailMessage mail = new MailMessage();
            string server = ConfigurationManager.AppSettings["serviceurl"].ToString();
            string iris = ConfigurationManager.AppSettings["CompanyEmail"].ToString();
            string friendlyName = ConfigurationManager.AppSettings["FriendlyName"].ToString();
            string button = string.Empty;
            string buttonstyle = "background-color: #336699; font-family: arial; text-align:center; width:150px; height:30px; color: White; margin: 15px 0px 20px 0px; border: none; cursor: pointer; line-height: 30px; font-size: 16px; border-radius: 2px 2px 2px 2px; -moz-border-radius: 2px 2px 2px 2px; -webkit-border-radius: 2px 2px 2px 2px; -khtml-border-radius:  2px 2px 2px 2px;";
            button = "<a href='" + server + "/Activation?Email=" + email + "&ID=" + guid + "'><div Style='" + buttonstyle + "'>Activate</div></a>";

            StringBuilder sb = new StringBuilder();
            sb.Append("<div style='color:#333'>Hello, </div><br/>");
            sb.Append("<div style='color:#333'>To complete your registration, please click on the button below:</div><br/>");
            sb.Append(button);
            sb.Append("<div style='color:#333'>Sincerly,</div><br/>");
            sb.Append("<div style='color:#333'>Iris Software Team</div>");

            mail.From = new MailAddress(iris, friendlyName);
            mail.To.Add(email);

            //set the content
            mail.Subject = "Iris Registration Completion";
            mail.Body = sb.ToString();
            mail.IsBodyHtml = true;

            SendMail(mail);

            //SendMeEmail(email, 2);
        }
                
        public void SendFeedback(string message, string email)
        {
            MailMessage mail = new MailMessage();
            string rjl = "NoReply@ajobiswaiting.com";
            string friendlyName = "AJobIsWaiting.com";

            mail.From = new MailAddress(rjl, friendlyName);
            mail.To.Add("ucheogbu@gmail.com");
            mail.To.Add("dami.b.lawal@gmail.com");

            //set the content
            mail.Subject = "Feedback at AJobIsWaiting.com";
            mail.IsBodyHtml = true;
            mail.Body = message + "<br /><br />Reply To: " + email + "<br /><br />";

            MysqlDataProv MysqlDataProv = new MysqlDataProv();
            string query = "insert into irisuser_feedback (feedback, user_email) values (@feedback, @user_email)";
            IDataParameter[] param =
            {   
                new MySqlParameter("feedback",  message),
                new MySqlParameter("user_email",  email)
            };

            MysqlDataProv.ExecuteQuery(query, param);
            SendMail(mail);
        }

        public void SendMeEmail(string Username, int type)
        {
            MailMessage mail = new MailMessage();
            string wangsEmail = ConfigurationManager.AppSettings["CompanyEmail"].ToString();
            string friendlyName = ConfigurationManager.AppSettings["FriendlyName"].ToString();
            StringBuilder sb = new StringBuilder();

            if (type == 1)
            {
                sb.Append("Someone just registered on AJW to post a single job" + Environment.NewLine + Environment.NewLine);
                sb.Append("Email address: " + Username + Environment.NewLine + Environment.NewLine);
                mail.Subject = "Single Job Post";
            }

            if (type == 2)
            {
                sb.Append("A User just reistered and was sent an activation mail" + Environment.NewLine + Environment.NewLine);
                sb.Append("Email address: " + Username + Environment.NewLine + Environment.NewLine);
                mail.Subject = "User Registration (Activation mail)";
            }

            if (type == 3)
            {
                sb.Append("A User requrested for their password to be changed" + Environment.NewLine + Environment.NewLine);
                sb.Append("Email address: " + Username + Environment.NewLine + Environment.NewLine);
                mail.Subject = "User Password recovery";
            }

            if (type == 4)
            {
                sb.Append("A joobseeker just applied for a job" + Environment.NewLine + Environment.NewLine);
                sb.Append("Email address: " + Username + Environment.NewLine + Environment.NewLine);
                mail.Subject = "User Application";
            }

            if (type == 5)
            {
                sb.Append("Application sent via 'Email My CV'" + Environment.NewLine + Environment.NewLine);
                sb.Append("Email address: " + Username + Environment.NewLine + Environment.NewLine);
                mail.Subject = "Email My CV application sent";
            }

            if (type == 6)
            {
                sb.Append("Messages were sent all a certain number of applicants" + Environment.NewLine + Environment.NewLine);
                sb.Append("Email address: " + Username + Environment.NewLine + Environment.NewLine);
                mail.Subject = "Employer emails applicants";
            }

            if (type == 7)
            {
                sb.Append("List of all job applicants requested" + Environment.NewLine + Environment.NewLine);
                sb.Append("Email address: " + Username + Environment.NewLine + Environment.NewLine);
                mail.Subject = "Job applicants List requested";
            }

            mail.From = new MailAddress(wangsEmail, friendlyName);
            mail.To.Add("dami.b.lawal@gmail.com");
            mail.Body = sb.ToString();

            SendMail(mail);
        }
        
        #endregion

        #region Misc

        public bool TestConnection()
        {
            return true;
        }

        public void SaveError(Error error)
        {
            MysqlDataProv MysqlDataProv = new MysqlDataProv();
            string query = "insert into iris.errorlogs (message, stacktrace, innerex_msg, innerex_source, innerex_trace, time_logged, is_app_error) values (@message, @stacktrace, @innerex_msg, @innerex_source, @innerex_trace, @time_logged, @is_app_error)";
            IDataParameter[] param =
                    {   
                        new MySqlParameter("message",  error.Message),
                        new MySqlParameter("stacktrace",  error.StackTrace),
                        new MySqlParameter("innerex_msg",  error.InnerExMsg),
                        new MySqlParameter("innerex_source",  error.InnerExSrc),
                        new MySqlParameter("innerex_trace",  error.InnerExStkTrace),
                        new MySqlParameter("time_logged",  TimeZoneInfo.ConvertTimeFromUtc(DateTime.Now.ToUniversalTime(), nigTimeZone)),
                        new MySqlParameter("is_app_error",  error.AppError),
                    };

            MysqlDataProv.ExecuteQuery(query, param);
        }

        private int GetPreviousSequenceValue(DbCommand cmd)
        {
            MysqlDataProv MysqlDataProv = new MysqlDataProv();
            return MysqlDataProv.GetIdNumber(cmd);
        }

        public DataSet GetLists()
        {
            MysqlDataProv MysqlDataProv = new MysqlDataProv();
            DataSet set = new DataSet();

            string query = @"select tier_id, tier_value, question_value from iris.list_tier_one l left join iris.list_tier_questions q on l.question_id = q.question_id order by tier_value";
            DataSet dset = new DataSet();

            dset = new DataSet();
            query = @"select firstname, surname, staff_id from iris.list_iris_staff order by firstname";
            MysqlDataProv.RunProcedure(query, null, dset);
            dset.Tables[0].TableName = "Staff";
            set.Tables.Add(dset.Tables[0].Copy());

            dset = new DataSet();
            query = @"select position, position_id from iris.list_staff_positions";
            MysqlDataProv.RunProcedure(query, null, dset);
            dset.Tables[0].TableName = "Positions";
            set.Tables.Add(dset.Tables[0].Copy());

            dset = new DataSet();
            query = @"select department, department_id from iris.list_departments";
            MysqlDataProv.RunProcedure(query, null, dset);
            dset.Tables[0].TableName = "Departments";
            set.Tables.Add(dset.Tables[0].Copy());

            dset = new DataSet();
            query = @"select firstname, surname, user_id, email, clearance_level from iris.iris_user order by firstname";
            MysqlDataProv.RunProcedure(query, null, dset);
            dset.Tables[0].TableName = "Users";
            set.Tables.Add(dset.Tables[0].Copy());

            dset = new DataSet();
            query = @"select color_id, color_name, color_hex from iris.list_colors order by color_name";
            MysqlDataProv.RunProcedure(query, null, dset);
            dset.Tables[0].TableName = "Colors";
            set.Tables.Add(dset.Tables[0].Copy());

            dset = new DataSet();
            query = @"select group_id, group_name from iris.list_groups order by group_id";
            MysqlDataProv.RunProcedure(query, null, dset);
            dset.Tables[0].TableName = "Groups";
            set.Tables.Add(dset.Tables[0].Copy());

            dset = new DataSet();
            query = @"select doc_type_id, doc_type_name, doc_type_ext from iris.list_doc_types order by doc_type_ext";
            MysqlDataProv.RunProcedure(query, null, dset);
            dset.Tables[0].TableName = "DocTypes";
            set.Tables.Add(dset.Tables[0].Copy());

            dset = new DataSet();
            query = @"select level_id, level from iris.list_priv_level order by level";
            MysqlDataProv.RunProcedure(query, null, dset);
            dset.Tables[0].TableName = "Levels";
            set.Tables.Add(dset.Tables[0].Copy());
            
            return set;
        }

        private void SendMail(MailMessage msg)
        {
            try
            {
                SmtpClient smtp = new SmtpClient();
                smtp.SendCompleted += new
                SendCompletedEventHandler(SendCompletedCallback);
                smtp.SendAsync(msg, msg);
            }
            catch (Exception ex)
            {
                Error error = Helper.CreateError(ex);
                SaveError(error);                
            }
        }

        private static void SendCompletedCallback(object sender, AsyncCompletedEventArgs e)
        {
            // Get the unique identifier for this asynchronous operation.
            MailMessage msg = (MailMessage)e.UserState;

            if (e.Cancelled)
            {
                
            }
            if (e.Error != null)
            {
                
            }
            else
            {
                
            }
        }

        #endregion

        #region Documents

        public List<Document> GetUserDocuments(int user_id)
        {
            List<Document> docs = new List<Document>();

            return docs;
        } 

        public int SaveDocument(Document doc)
        {
            MysqlDataProv MysqlDataProv = new MysqlDataProv();
            MysqlDataProv.CreateTransactionCommand();

            string query = "insert into iris.documents (doc_name, location, owner_id, time_posted, doc_date, access_level, keywords, doc_ext, phys_location, allow_dept_id, allow_specific_id) values (@doc_name, @location, @owner_id, @time_posted, @doc_date, @access_level, @keywords, @doc_ext, @phys_location, @allow_dept_id, @allow_specific_id)";

            MysqlDataProv.command.Parameters.Add(new MySqlParameter("doc_name", doc.DocName));
            MysqlDataProv.command.Parameters.Add(new MySqlParameter("location", doc.Location));
            MysqlDataProv.command.Parameters.Add(new MySqlParameter("keywords", doc.Keywords));
            MysqlDataProv.command.Parameters.Add(new MySqlParameter("phys_location", doc.PhysicalLocation));
            MysqlDataProv.command.Parameters.Add(new MySqlParameter("allow_dept_id", doc.AllowDeptIdString));
            MysqlDataProv.command.Parameters.Add(new MySqlParameter("allow_specific_id", doc.AllowIdString));
            MysqlDataProv.command.Parameters.Add(new MySqlParameter("doc_ext", doc.DocType));
            MysqlDataProv.command.Parameters.Add(new MySqlParameter("owner_id", doc.User.Person_Id));
            MysqlDataProv.command.Parameters.Add(new MySqlParameter("time_posted", DateTime.Now));
            MysqlDataProv.command.Parameters.Add(new MySqlParameter("doc_date", doc.DocDate));
            MysqlDataProv.command.Parameters.Add(new MySqlParameter("access_level", doc.Level));

            MysqlDataProv.command.CommandText = query;
            MysqlDataProv.command.ExecuteNonQuery();

            int docid = GetPreviousSequenceValue(MysqlDataProv.command);
            MysqlDataProv.command.Transaction.Commit();

            // Add Audit trail
            AddAudit(new Audit { Description = doc.DocName + "." + doc.DocType, DocId = docid, TimeStamp = DateTime.Now, UserId = doc.User.Person_Id, EventType = (int)AuditEnum.Upload });

            return 1;
        }

        public List<Document> SearchForDocuments(string k, string cat, string proj, string staffdoc, string updatefrom, string updateto, string docdatefrom, string docdateto, string doctype, int userid)
        {
            MysqlDataProv MysqlDataProv = new MysqlDataProv();
            DataSet dset = new DataSet();
            List<Document> docList = new List<Document>();

            string query = "select doc_id, doc_name, location, phys_location, keywords, owner_id, time_posted, doc_date, access_level, is_active, doc_ext, allow_specific_id, allow_dept_id from iris.documents ";

            string extra = string.Empty;

            if (!string.IsNullOrEmpty(k))
            {
                extra = "where (doc_name like '%" + k + "%' or location like '%" + k + "%' or keywords like '%" + k + "%' or doc_ext like '%" + k + "%')";
            }

            if (!string.IsNullOrEmpty(cat))
            {
                if (!string.IsNullOrEmpty(proj))
                {
                    string[] docs = proj.Split(',');
                    bool isfirst = true;
                    bool wasempty = false;
                    foreach (string project in docs)
                    {
                        if (extra == string.Empty)
                        {
                            extra = "where (location like '%" + project + "%'";
                            wasempty = true;
                        }
                        else
                        {
                            if (isfirst && !wasempty)
                            {
                                isfirst = false;
                                extra += " and (location like '" + project + "%'";
                            }
                            else
                                extra += " or location like '" + project + "%'";
                        }
                    }

                    extra += ")";
                }
                else if (!string.IsNullOrEmpty(staffdoc))
                {
                    string[] docs = staffdoc.Split(',');
                    bool isfirst = true;
                    bool wasempty = false;
                    foreach (string staff in docs)
                    {
                        if (extra == string.Empty)
                        {
                            extra = "where (location like '%Staff/" + staff + "%'";
                            wasempty = true;
                        }
                        else
                        {
                            if (isfirst && !wasempty)
                            {
                                isfirst = false;
                                extra += " and (location like '%Staff/" + staff + "%'";
                            }
                            else
                                extra += " or location like '%Staff/" + staff + "%'";
                        }
                    }

                    extra += ")";
                }
                else
                {
                    string[] docs = cat.Split(',');
                    bool isfirst = true;
                    bool wasempty = false;
                    foreach (string c in docs)
                    {
                        if (extra == string.Empty)
                        {
                            extra = "where (location like '%" + c + "%'";
                            wasempty = true;
                        }
                        else
                        {
                            if (isfirst && !wasempty)
                            {
                                isfirst = false;
                                extra += " and (location like '" + c + "%'";
                            }
                            else
                                extra += " or location like '" + c + "%'";
                        }
                    }

                    extra += ")";
                }
            }

            if (!string.IsNullOrEmpty(updatefrom))
            {
                if (extra == string.Empty)
                    extra = "where time_posted > STR_TO_DATE('" + DateTime.Parse(updatefrom).AddDays(-1).ToShortDateString() + "', '%d/%m/%Y')";
                else
                    extra += " and time_posted > STR_TO_DATE('" + DateTime.Parse(updatefrom).AddDays(-1).ToShortDateString() + "', '%d/%m/%Y')";
            }

            if (!string.IsNullOrEmpty(updateto))
            {
                if (extra == string.Empty)
                    extra = "where time_posted < STR_TO_DATE('" + DateTime.Parse(updateto).AddDays(1).ToShortDateString() + "', '%d/%m/%Y')";
                else
                    extra += " and time_posted < STR_TO_DATE('" + DateTime.Parse(updateto).AddDays(1).ToShortDateString() + "', '%d/%m/%Y')";
            }

            if (!string.IsNullOrEmpty(docdatefrom))
            {
                if (extra == string.Empty)
                    extra = "where doc_date > STR_TO_DATE('" + DateTime.Parse(docdatefrom).AddDays(-1).ToShortDateString() + "', '%d/%m/%Y')";
                else
                    extra += " and doc_date > STR_TO_DATE('" + DateTime.Parse(docdatefrom).AddDays(-1).ToShortDateString() + "', '%d/%m/%Y')";
            }

            if (!string.IsNullOrEmpty(docdateto))
            {
                if (extra == string.Empty)
                    extra = "where doc_date < STR_TO_DATE('" + DateTime.Parse(docdateto).AddDays(1).ToShortDateString() + "', '%d/%m/%Y')";
                else
                    extra += " and doc_date < STR_TO_DATE('" + DateTime.Parse(docdateto).AddDays(1).ToShortDateString() + "', '%d/%m/%Y')";
            }

            if (!string.IsNullOrEmpty(doctype))
            {
                string[] docs = doctype.Split(',');
                bool isfirst = true;
                bool wasempty = false;
                foreach (string doc in docs)
                {
                    if (extra == string.Empty)
                    {
                        extra = "where (doc_ext = '" + doc + "'";
                        wasempty = true;
                    }
                    else
                    {
                        if (isfirst && !wasempty)
                        {
                            isfirst = false;
                            extra += " and (doc_ext = '" + doc + "'";
                        }
                        else
                            extra += " or doc_ext = '" + doc + "'";
                    }
                }

                extra += ")";
            }

            if (string.IsNullOrEmpty(extra))
            {
                extra += "where is_active = 1";
            }
            else
            {
                extra += " and is_active = 1";
            }

            MysqlDataProv.RunProcedure(query + extra, null, dset);

            foreach (DataRow row in dset.Tables[0].Rows)
            {
                Person user = GetUserFromId(int.Parse(row["owner_id"].ToString()));

                Document doc = new Document()
                {
                    DocId = int.Parse(row["doc_id"].ToString()),
                    DatePosted = DateTime.Parse(row["time_posted"].ToString()),
                    DocDate = !string.IsNullOrEmpty(row["doc_date"].ToString()) ? DateTime.Parse(row["doc_date"].ToString()) : (DateTime?)null,
                    DocName = row["doc_name"].ToString(),
                    PhysicalLocation = row["phys_location"].ToString(),
                    DocType = row["doc_ext"].ToString(),
                    Keywords = row["keywords"].ToString(),
                    Level = row["access_level"].ToString(),
                    Location = row["location"].ToString(),
                    AllowId = string.IsNullOrEmpty(row["allow_specific_id"].ToString()) ? new List<int>() : row["allow_specific_id"].ToString().Split(',').Select(int.Parse).ToList(),
                    AllowDeptId = string.IsNullOrEmpty(row["allow_dept_id"].ToString()) ? new List<int>() : row["allow_dept_id"].ToString().Split(',').Select(int.Parse).ToList()
                };

                doc.User = user;
                docList.Add(doc);
            }

            // Add audit trail
            Audit audit = new Audit()
            {
                EventType = (int)AuditEnum.Search,
                TimeStamp = DateTime.Now,
                UserId = userid,
                Description = k
            };

            AddAudit(audit);

            return docList;
        }

        public Document GetDocument(int doc_id, int userid, int audit_type)
        {
            MysqlDataProv MysqlDataProv = new MysqlDataProv();
            DataSet dset = new DataSet();
            Document result = null;

            string query = "select doc_id, doc_name, location, phys_location, keywords, owner_id, time_posted, doc_date, access_level, is_active, doc_ext from iris.documents where doc_id = @doc_id";

            IDataParameter[] param =
                {   
                    new MySqlParameter("doc_id",  doc_id)
                };

            MysqlDataProv.RunProcedure(query, param, dset);            

            if (dset.Tables[0].Rows.Count != 0)
            {
                DataRow row = dset.Tables[0].Rows[0];

                Document doc = new Document()
                {
                    DocId = int.Parse(row["doc_id"].ToString()),
                    DatePosted = DateTime.Parse(row["time_posted"].ToString()),
                    DocDate = !string.IsNullOrEmpty(row["doc_date"].ToString()) ? DateTime.Parse(row["doc_date"].ToString()) : (DateTime?)null,
                    DocName = row["doc_name"].ToString(),
                    DocType = row["doc_ext"].ToString(),
                    Keywords = row["keywords"].ToString(),
                    Level = row["access_level"].ToString(),
                    Location = row["location"].ToString(),
                    PhysicalLocation = row["phys_location"].ToString()
                };

                result = doc;
            }

            if (audit_type != 0)
            {
                // Add audit trail

                Audit audit = new Audit()
                {
                    EventType = audit_type,
                    TimeStamp = DateTime.Now,
                    DocId = result.DocId,
                    UserId = userid
                };

                AddAudit(audit);
            }

            return result;
        }

        public void GiveAccessToUsers(string userids, string docid)
        {
            MysqlDataProv MysqlDataProv = new MysqlDataProv();

            string query = "update iris.documents set allow_specific_id = @allow_specific_id where doc_id = @doc_id";

            IDataParameter[] paramx =
                {   
                    new MySqlParameter("allow_specific_id",  userids),
                    new MySqlParameter("doc_id",  docid)
                };

            MysqlDataProv.ExecuteQuery(query, paramx);
        }

        public void GiveAccessToDepts(string deptId, string docid)
        {
            MysqlDataProv MysqlDataProv = new MysqlDataProv();

            string query = "update iris.documents set allow_dept_id = @allow_dept_id where doc_id = @doc_id";

            IDataParameter[] paramx =
                {   
                    new MySqlParameter("allow_dept_id",  deptId),
                    new MySqlParameter("doc_id",  docid)
                };

            MysqlDataProv.ExecuteQuery(query, paramx);
        }

        public void DeleteFile(string location)
        {
            MysqlDataProv MysqlDataProv = new MysqlDataProv();

            string query = "update iris.documents set is_active = 0 where location = @location and doc_name = @doc_name and doc_ext = @doc_ext";

            string loc = Path.GetDirectoryName(location).Replace("\\", "/");
            string docname = Path.GetFileNameWithoutExtension(location).Replace("\\", "/");
            string docext = Path.GetExtension(location).Replace(".", "");

            IDataParameter[] paramx =
                {   
                    new MySqlParameter("location",  loc),
                    new MySqlParameter("doc_name",  docname),
                    new MySqlParameter("doc_ext",  docext)
                };

            MysqlDataProv.ExecuteQuery(query, paramx);

            // Register audit trail
            AddAudit(new Audit() { EventType = (int)AuditEnum.Delete, Description = Path.GetFileName(location) });
        }

        public void RenameFile(string location, string newname)
        {
            MysqlDataProv MysqlDataProv = new MysqlDataProv();

            string query = "update iris.documents set doc_name = @newdocname where location = @location and doc_name = @doc_name and doc_ext = @doc_ext";

            string loc = Path.GetDirectoryName(location).Replace("\\", "/");
            string docname = Path.GetFileNameWithoutExtension(location).Replace("\\", "/");
            string docext = Path.GetExtension(location).Replace(".", "");

            IDataParameter[] paramx =
                {   
                    new MySqlParameter("newdocname",  Path.GetFileNameWithoutExtension(newname)),
                    new MySqlParameter("location",  loc),
                    new MySqlParameter("doc_name",  docname),
                    new MySqlParameter("doc_ext",  docext)
                };

            MysqlDataProv.ExecuteQuery(query, paramx);
        }

        #endregion

        #region Reports/Maintenance

        #region NIBSS

        public List<Report> GetPassportVerificationHistory(DateTime start, DateTime end)
        {
            DataSet dset = new DataSet();
            string query = @"select  e.entity_id, e.surname as opr_surname, e.firstname as opr_firstname, ph.create_date, ph.input_entity_id, ph.docno,ph.input_surname, ph.firstname, ph.othername1, 
                            ph.sex, ph.birthdate, ph.faceimage, ph.stagecode, b.id, b.bank_name, bb.bank_branch_name, bb.bank_branch_id , ph.verification_reason_text
                            from nibss.passport_hist ph 
                            inner join nibss.entity e
                            on e.entity_id = ph.input_entity_id
                            inner join nibss.web_accnt wa
                            on e.entity_id = wa.entity_id
                            inner join nibss.entity_bank eb 
                            on e.entity_id = eb.entity_id 
                            inner join nibss.bank_branch bb 
                            on eb.bank_branch_id = bb.bank_branch_id
                            inner join nibss.bank b
                            on bb.bank_id = b.id
                            where ph.create_date >= to_date('" + start.ToString("dd-MMM-yyyy") + @"', 'DD-Mon-YYYY')
                            and ph.create_date <= to_date('" + end.AddDays(1).ToString("dd-MMM-yyyy") + @"', 'DD-Mon-YYYY')
                            order by b.bank_name,bb.bank_branch_name";

            OracleDataProv OracleDataProv = new OracleDataProv();
            OracleDataProv.RunProcedure(query, null, dset);

            List<Report> reports = new List<Report>();

            foreach (DataRow row in dset.Tables[0].Rows)
            {
                Report report = new Report()
                {
                    PassportHolderId = int.Parse(row["input_entity_id"].ToString()),
                    Bank = new Bank()
                    {
                        BankId = int.Parse(row["id"].ToString()),
                        BranchId = int.Parse(row["bank_branch_id"].ToString()),
                        BankName = ShortenBankName(row["bank_name"].ToString()),
                        BranchName = row["bank_branch_name"].ToString(),
                    },
                    DOB = DateTime.Parse(row["birthdate"].ToString()),
                    ViewDate = DateTime.Parse(row["create_date"].ToString()),
                    FirstName = row["firstname"].ToString(),
                    Surname = row["input_surname"].ToString(),
                    OtherName = row["othername1"].ToString(),
                    Sex = row["sex"].ToString(),
                    DocRefNum = row["docno"].ToString(),
                    FaceImage = row["faceimage"] as byte[],
                    Stage_Code = row["stagecode"].ToString(),
                    VerificationReason = row["verification_reason_text"].ToString(),
                    Operator = new Operator()
                    {
                        FirstName = row["opr_firstname"].ToString(),
                        Surname = row["opr_surname"].ToString(),
                        OperatorId = int.Parse(row["entity_id"].ToString())
                    }
                };

                reports.Add(report);
            }

            return reports;
        }

        public List<Report> GetBankLoginHistory(DateTime start, DateTime end)
        {
            DataSet dset = new DataSet();
            string query = @"select lh.web_accnt_id, e.entity_id, e.surname, e.firstname,  b.id, b.bank_name, bb.bank_branch_name, bb.bank_branch_id, lh.login_date, lh.comments
                            from nibss.login_history lh 
                            full outer join nibss.web_accnt wa
                            on wa.web_accnt_id = lh.web_accnt_id
                            full outer join nibss.entity e
                            on e.entity_id = wa.entity_id
                            full outer join nibss.entity_bank eb 
                            on e.entity_id = eb.entity_id 
                            full outer join nibss.bank_branch bb 
                            on eb.bank_branch_id = bb.bank_branch_id
                            full outer join nibss.bank b
                            on bb.bank_id = b.id
                            where lh.login_date >= to_date('" + start.ToString("dd-MMM-yyyy") + @"', 'DD-Mon-YYYY')
                            and lh.login_date <= to_date('" + end.AddDays(1).ToString("dd-MMM-yyyy") + @"', 'DD-Mon-YYYY')
                            order by lh.login_date";

            OracleDataProv OracleDataProv = new OracleDataProv();
            OracleDataProv.RunProcedure(query, null, dset);

            List<Report> reports = new List<Report>();

            foreach (DataRow row in dset.Tables[0].Rows)
            {
                if (!string.IsNullOrEmpty(row["web_accnt_id"].ToString()))
                {
                    Report report = new Report()
                    {
                        EntityId = int.Parse(row["entity_id"].ToString()),                        
                        Operator = new Operator()
                        {
                            FirstName = row["firstname"].ToString(),
                            Surname = row["surname"].ToString(),
                            OperatorId = int.Parse(row["web_accnt_id"].ToString()),
                            LoginDate = DateTime.Parse(row["login_date"].ToString()),
                            Comments = row["comments"].ToString(),
                        }
                    };

                    report.Bank = new Bank() { BankName = string.Empty, BranchName = string.Empty };

                    if (!string.IsNullOrEmpty(row["id"].ToString()))
                    {
                        report.Bank.BankId = int.Parse(row["id"].ToString());
                        report.Bank.BranchId = int.Parse(row["bank_branch_id"].ToString());
                        report.Bank.BankName = ShortenBankName(row["bank_name"].ToString());
                        report.Bank.BranchName = row["bank_branch_name"].ToString();
                    }

                    reports.Add(report);
                }
            }

            return reports;
        }

        public List<Report> GetBankMessageHistory(DateTime start, DateTime end)
        {
            DataSet dset = new DataSet();
            string query = @"select msg_type_id, message_text, gsm_number, email_address, sent_date, error_msg, createdate
                            from nibss.msg 
                            where sent_date >= to_date('" + start.ToString("dd-MMM-yyyy") + @"', 'DD-Mon-YYYY')
                            and sent_date <= to_date('" + end.AddDays(1).ToString("dd-MMM-yyyy") + @"', 'DD-Mon-YYYY')
                            order by createdate";
                            
            OracleDataProv OracleDataProv = new OracleDataProv();
            OracleDataProv.RunProcedure(query, null, dset);

            List<Report> reports = new List<Report>();

            foreach (DataRow row in dset.Tables[0].Rows)
            {
                Report report = new Report()
                {
                    Message = new Message()
                    {
                        CreateDate = DateTime.Parse(row["createdate"].ToString()),
                        EmailAddress = row["email_address"].ToString(),
                        ErrorMessage = row["error_msg"].ToString(),
                        GSMNumber = row["gsm_number"].ToString(),
                        MessageText = row["message_text"].ToString(),
                        MessageType = int.Parse(row["msg_type_id"].ToString()),
                        SentDate = DateTime.Parse(row["sent_date"].ToString()),
                    }
                };

                reports.Add(report);
            }

            return reports;
        }

        public List<Report> GetBankExceptionsHistory(DateTime start, DateTime end)
        {
            DataSet dset = new DataSet();
            string query = @"select exceptiondate, message, browser, stacktrace, machinenameorip from nibss.exceptionlog 
                            where exceptiondate >= to_date('" + start.ToString("dd-MMM-yyyy") + @"', 'DD-Mon-YYYY')
                            and exceptiondate <= to_date('" + end.AddDays(1).ToString("dd-MMM-yyyy") + @"', 'DD-Mon-YYYY')
                            order by exceptiondate";

            OracleDataProv OracleDataProv = new OracleDataProv();
            OracleDataProv.RunProcedure(query, null, dset);

            List<Report> reports = new List<Report>();

            foreach (DataRow row in dset.Tables[0].Rows)
            {
                Report report = new Report()
                {
                    Error = new ErrorMessage()
                    {
                        ExceptionDate = DateTime.Parse(row["exceptiondate"].ToString()),
                        Message = row["message"].ToString(),
                        Browser = row["browser"].ToString(),
                        MachineIP = row["machinenameorip"].ToString(),
                        StackTrace = row["stacktrace"].ToString()
                    }
                };

                reports.Add(report);
            }

            return reports;
        }

        public List<Report> GetBankCursorStatus()
        {
            DataSet dset = new DataSet();
            string query = @"select value, name, sid from ( select ss.value, sn.name, ss.sid
                             from v$sesstat ss, v$statname sn
                             where ss.statistic# = sn.statistic#
                             and sn.name like '%opened cursors current%'
                             order by value desc) where rownum < 11 order by value";

            OracleDataProv OracleDataProv = new OracleDataProv();
            OracleDataProv.RunProcedure(query, null, dset);

            List<Report> reports = new List<Report>();

            foreach (DataRow row in dset.Tables[0].Rows)
            {
                Report report = new Report()
                {
                    Cursor = new OracleCursor()
                    {
                        Name = row["name"].ToString(),
                        SID = int.Parse(row["sid"].ToString()),
                        Value = int.Parse(row["value"].ToString()),
                    }
                };

                reports.Add(report);
            }

            return reports;
        }

        private string ShortenBankName(string name)
        {
            switch (name)
            {
                case "FIRST CITY MONUMENT BANK PLC":
                    {
                        return "FCMB";
                    }
                case "SKYE BANK PLC":
                    {
                        return "Skye";
                    }
                case "WEMA BANK PLC":
                    {
                        return "WEMA";
                    }
                case "GUARANTY TRUST BANK PLC":
                    {
                        return "GTB";
                    }
                case "STANBIC-IBTC BANK PLC":
                    {
                        return "Stanbic";
                    }
                case "KEYSTONE BANK LTD.":
                    {
                        return "Keystone";
                    }
                case "FIRST BANK OF NIGERIA PLC":
                    {
                        return "FBN";
                    }
                case "UNITED BANK FOR AFRICA PLC":
                    {
                        return "UBA";
                    }
                case "STERLING BANK PLC":
                    {
                        return "Sterling";
                    }
                case "STANDARD CHARTERED BANK NIGERIA LTD":
                    {
                        return "SCBN";
                    }
                case "DIAMOND BANK PLC":
                    {
                        return "Diamond";
                    }
                case "FIDELITY BANK PLC":
                    {
                        return "Fidelity";
                    }
                case "CITIBANK NIGERIA LTD":
                    {
                        return "Citi";
                    }
                case "ENTERPRISE BANK LTD.":
                    {
                        return "Ent";
                    }
                case "ZENITH BANK PLC":
                    {
                        return "Zenith";
                    }
                case "UNION BANK OF NIGERIA PLC":
                    {
                        return "Union";
                    }
                case "MAINSTREET BANK LTD.":
                    {
                        return "Mainstr";
                    }
                case "UNITY BANK PLC":
                    {
                        return "Unity";
                    }
                case "JAIZ BANK PLC":
                    {
                        return "Jaiz";
                    }
                case "HERITAGE BANK":
                    {
                        return "Heriatage";
                    }
                case "ECOBANK NIGERIA PLC":
                    {
                        return "Eco";
                    }
            }

            return name;
        }

        #endregion

        #region General

        public List<Person> GetUserList()
        {
            MysqlDataProv MysqlDataProv = new MysqlDataProv();
            DataSet dset = new DataSet();
            List<Person> users = new List<Person>();

            string query = @"select user_id, firstname, surname, email, gender, date_created, phone_number, user_id, pwd_salt, pwd_hash, is_active, last_edit, clearance_level, profile_pic, u.position_id, u.department_id, position, department
                            from iris.iris_user u left join list_staff_positions lp on u.position_id = lp.position_id 
                            left join list_departments d on u.department_id = d.department_id order by firstname";

            MysqlDataProv.RunProcedure(query, null, dset);

            if (dset.Tables[0].Rows.Count != 0)
            {
                foreach (DataRow row in dset.Tables[0].Rows)
                {
                    Person user = new Person();

                    user.Person_Id = int.Parse(row["user_id"].ToString());
                    user.FirstName = row["firstname"].ToString();
                    user.Gender = row["gender"].ToString();
                    user.PhoneNumber = row["phone_number"].ToString();
                    user.Surname = row["surname"].ToString();
                    user.ProfilePicture = row["profile_pic"].ToString();
                    user.Position = row["position_id"].ToString();
                    user.Department = row["department_id"].ToString();
                    user.PositionName = row["position"].ToString();
                    user.DepartmentName = row["department"].ToString();
                    user.EmailAddress = row["email"].ToString();
                    if (!string.IsNullOrEmpty(row["date_created"].ToString()))
                        user.DateOfRegistration = DateTime.Parse(row["date_created"].ToString());
                    if (!string.IsNullOrEmpty(row["last_edit"].ToString()))
                        user.LastActivityDate = DateTime.Parse(row["last_edit"].ToString());
                    user.IsActive = row["is_active"].ToString() == "1" ? true : false;
                    user.Salt = row["pwd_salt"].ToString();
                    user.Hash = row["pwd_hash"].ToString();
                    if (!string.IsNullOrEmpty(row["clearance_level"].ToString()))
                        user.ClearanceLevel = int.Parse(row["clearance_level"].ToString());

                    users.Add(user);
                }
            }

            return users;
        }

        public int EditStaffAccessLevel(int person_id, int level, int currlevel, int mystaff_id)
        {
            MysqlDataProv MysqlDataProv = new MysqlDataProv();
            DataSet dset = new DataSet();

            // Save audit trail
            Person p = GetUserFromId(person_id);
            AddAudit(new Audit { EventType = (int)AuditEnum.AccessLevelChange, TimeStamp = DateTime.Now, UserId = mystaff_id, Description = p.FullName + ": Level " + p.ClearanceLevel + " to " + level });

            string query = @"update iris.iris_user set clearance_level = @clearance_level where user_id = @user_id";

            IDataParameter[] param =
            {   
                new MySqlParameter("user_id",  person_id),
                new MySqlParameter("clearance_level",  level)
            };

            MysqlDataProv.ExecuteQuery(query, param);
            return 1;
        }

        #endregion

        #endregion

        #region Calender

        public List<Event> GetCalenderEvents()
        {
            MysqlDataProv MysqlDataProv = new MysqlDataProv();
            DataSet dset = new DataSet();
            List<Event> eventslist = new List<Event>();

            string query = @"select event_id, title, start, user_id, end, bg_color, bg.color_name as bg_color_name, bg.color_hex as bg_color_hex, tx.color_name as tx_color_name, 
                             tx.color_hex as tx_color_hex, user_id, description, e.group_id, group_name, time_posted , is_allday, staff_invited, send_email    
                             from iris.calender_events e 
							 left join list_groups g on g.group_id = e.group_id 
							 left join list_colors bg on bg.color_hex = e.bg_color
                             left join list_colors tx on tx.color_hex = e.text_color where e.is_active = 1";

            MysqlDataProv.RunProcedure(query, null, dset);

            foreach (DataRow row in dset.Tables[0].Rows)
            {
                Event ev = new Event();
                Person user = GetUserFromId(int.Parse(row["user_id"].ToString()));

                if (int.Parse(row["group_id"].ToString()) == user.GroupId   // in my group
                    || int.Parse(row["group_id"].ToString()) == 1           // for everyone 
                    || user.GroupId == 0                                    // i am admin
                    || user.Person_Id == int.Parse(row["user_id"].ToString())     // i posted it
                    || (int.Parse(row["group_id"].ToString()) == 6 && user.Person_Id == int.Parse(row["user_id"].ToString())) // me only
                    )
                {
                    ev.BackgroundColor = row["bg_color_hex"].ToString();
                    ev.Description = row["description"].ToString();
                    ev.End = row["end"].ToString();
                    ev.EventId = int.Parse(row["event_id"].ToString());
                    ev.GroupId = int.Parse(row["group_id"].ToString());
                    ev.PostedBy = user;
                    ev.IsAllDay = int.Parse(row["is_allday"].ToString());
                    ev.Start = row["start"].ToString();
                    ev.TextColor = row["tx_color_hex"].ToString();
                    ev.TimePosted = DateTime.Parse(row["time_posted"].ToString());
                    ev.Title = row["title"].ToString();
                    ev.SendEmail = int.Parse(row["send_email"].ToString());
                    ev.StaffIinvited = row["staff_invited"].ToString();

                    eventslist.Add(ev);
                }
            }

            return eventslist;
        }

        public int CreateCalenderEvent(Event ev)
        {
            MysqlDataProv MysqlDataProv = new MysqlDataProv();
            MysqlDataProv.CreateTransactionCommand();

            DataSet dset = new DataSet();

            string query = @"insert into iris.calender_events (title, start, end, bg_color, text_color, user_id, description, group_id, time_posted, is_allday, staff_invited, send_email) values
                             (@title, @start, @end, @bg_color, @text_color, @user_id, @description, @group_id, @time_posted, @is_allday, @staff_invited, @send_email)";

            MysqlDataProv.command.Parameters.Add(new MySqlParameter("title",  ev.Title));
            MysqlDataProv.command.Parameters.Add(new MySqlParameter("start",  ev.Start));
            MysqlDataProv.command.Parameters.Add(new MySqlParameter("end",  ev.End));
            MysqlDataProv.command.Parameters.Add(new MySqlParameter("bg_color",  ev.BackgroundColor));
            MysqlDataProv.command.Parameters.Add(new MySqlParameter("text_color",  ev.TextColor));
            MysqlDataProv.command.Parameters.Add(new MySqlParameter("user_id",  ev.PostedBy.Person_Id));
            MysqlDataProv.command.Parameters.Add(new MySqlParameter("description",  ev.Description));
            MysqlDataProv.command.Parameters.Add(new MySqlParameter("group_id",  ev.GroupId));
            MysqlDataProv.command.Parameters.Add(new MySqlParameter("time_posted",  ev.TimePosted));
            MysqlDataProv.command.Parameters.Add(new MySqlParameter("is_allday", ev.IsAllDay));
            MysqlDataProv.command.Parameters.Add(new MySqlParameter("staff_invited", ev.StaffIinvited));
            MysqlDataProv.command.Parameters.Add(new MySqlParameter("send_email", ev.SendEmail));

            MysqlDataProv.command.CommandText = query;
            MysqlDataProv.command.ExecuteNonQuery();

            int eventid = GetPreviousSequenceValue(MysqlDataProv.command);
            MysqlDataProv.command.Transaction.Commit();

            // Add audit
            AddAudit(new Audit { EventType = (int)AuditEnum.CreateEvent, UserId = ev.PostedBy.Person_Id, TimeStamp = DateTime.Now, Description = eventid.ToString() });

            // Send the email to errbody
            if (ev.SendEmail == 1)
                SendCalenderEventEmail(ev);

            return eventid;
        }

        public int UpdateCalenderEvent(Event ev)
        {
            MysqlDataProv MysqlDataProv = new MysqlDataProv();
            DataSet dset = new DataSet();

            // Check if this even is actually the yours
            string query = "select user_id from iris.calender_events where event_id = @event_id";
            DataSet temp = new DataSet();
            IDataParameter[] paramx =
                {   
                    new MySqlParameter("event_id",  ev.EventId)
                };

            MysqlDataProv.RunProcedure(query, paramx, temp);

            if (temp.Tables[0].Rows[0]["user_id"].ToString() == ev.PostedBy.Person_Id.ToString())
            {
                // Add audit
                AddAudit(new Audit { EventType = (int)AuditEnum.UpdateEvent, UserId = ev.PostedBy.Person_Id, TimeStamp = DateTime.Now, Description = ev.EventId.ToString() });

                query = @"update iris.calender_events set title = @title, start = @start, end = @end, bg_color = @bg_color, text_color = @text_color, user_id = @user_id, is_allday = @is_allday,
                             description = @description, group_id = @group_id, time_posted = @time_posted, staff_invited = @staff_invited, send_email = @send_email where event_id = @event_id";

                IDataParameter[] param =
                {   
                    new MySqlParameter("event_id",  ev.EventId),
                    new MySqlParameter("title",  ev.Title),
                    new MySqlParameter("start",  ev.Start),
                    new MySqlParameter("end",  ev.End),
                    new MySqlParameter("bg_color",  ev.BackgroundColor),
                    new MySqlParameter("text_color",  ev.TextColor),
                    new MySqlParameter("user_id",  ev.PostedBy.Person_Id),
                    new MySqlParameter("description",  ev.Description),
                    new MySqlParameter("group_id",  ev.GroupId),
                    new MySqlParameter("time_posted",  ev.TimePosted),
                    new MySqlParameter("is_allday",  ev.IsAllDay),
                    new MySqlParameter("staff_invited",  ev.StaffIinvited),
                    new MySqlParameter("send_email",  ev.SendEmail)
                };

                MysqlDataProv.ExecuteQuery(query, param);
                return 1;
            }
            else
                return 0;
        }

        public int DeleteCalenderEvent(int eventid, int userid)
        {
            MysqlDataProv MysqlDataProv = new MysqlDataProv();
            DataSet dset = new DataSet();

            // Check if this even is actually the yours
            string query = "select user_id from iris.calender_events where event_id = @event_id";
            DataSet temp = new DataSet();
            IDataParameter[] paramx =
                {   
                    new MySqlParameter("event_id",  eventid)
                };

            MysqlDataProv.RunProcedure(query, paramx, temp);

            if (temp.Tables[0].Rows[0]["user_id"].ToString() == userid.ToString())
            {
                // Add audit
                AddAudit(new Audit { EventType = (int)AuditEnum.DeleteEvent, UserId = userid, TimeStamp = DateTime.Now, Description = eventid.ToString() });

                query = @"update iris.calender_events set is_active = 0 where event_id = @event_id";

                IDataParameter[] param =
                {   
                    new MySqlParameter("event_id",  eventid)
                };

                MysqlDataProv.ExecuteQuery(query, param);
                return 1;
            }
            else
                return 0;
        }

        public int UpdateStaffEventChange(Event ev)
        {
            MailMessage mail = new MailMessage();
            string irisEmail = ConfigurationManager.AppSettings["CompanyEmail"].ToString();
            string friendlyName = ConfigurationManager.AppSettings["FriendlyName"].ToString();

            if (ev.IsCancelled == 1)
            {
                ev = GetEventFromId(ev.EventId);
                ev.IsCancelled = 1;
            }

            List<Person> sendto = new List<Person>();
            foreach (string pid in ev.StaffIinvited.Split(','))
            {
                sendto.Add(GetStaffFromId(int.Parse(pid)));
            }

            string people = string.Empty;
            foreach (Person p in sendto)
            {
                if (people == string.Empty)
                    people += p.FirstName + " " + p.Surname;
                else
                    people += ", " + p.FirstName + " " + p.Surname;
            }

            foreach (Person staffmember in sendto)
            {
                StringBuilder sb = new StringBuilder();

                if (ev.IsCancelled == 0)
                {
                    sb.Append("<div style='margin-bottom: 15px'>Hello " + staffmember.FirstName + ",</div>");
                    sb.Append("<div style='margin-bottom: 15px'>There has been a change to an event you were invited to. Here are the new details:</div>");
                    sb.Append("<div>");
                    sb.Append("<div style='width:90px; text-align:right; margin-right: 10px;float: left;'>Event Title:</div>");
                    sb.Append("<div style='float: left;'>" + ev.Title + "</div>");
                    sb.Append("<div style='clear: both;'></div>");
                    sb.Append("</div><div style='margin-top:5px'>");
                    sb.Append("<div style='width:90px; text-align:right; margin-right: 10px;float: left;'>From:</div>");
                    sb.Append("<div style='float: left;'>" + DateTime.Parse(ev.Start).ToString("hh:mm tt") + "</div>");
                    sb.Append("<div style='clear: both;'></div>");
                    sb.Append("</div><div style='margin-top:5px'>");
                    sb.Append("<div style='width:90px; text-align:right; margin-right: 10px;float: left;'>To:</div>");
                    sb.Append("<div style='float: left;'>" + DateTime.Parse(ev.End).ToString("hh:mm tt") + "</div>");
                    sb.Append("<div style='clear: both;'></div>");
                    sb.Append("</div><div style='margin-top:5px'>");
                    sb.Append("<div style='width:90px; text-align:right; margin-right: 10px;float: left;'>Staff Involved:</div>");
                    sb.Append("<div style='float: left;'>" + people + "</div>");
                    sb.Append("<div style='clear: both;'></div>");
                    sb.Append("</div><div style='margin-top:5px'>");
                    if (!string.IsNullOrEmpty(ev.Description))
                    {
                        sb.Append("<div style='width:90px; text-align:right; margin-right: 10px;float: left;'>Details:</div>");
                        sb.Append("<div style='float: left;'>" + ev.Description + "</div>");
                        sb.Append("<div style='clear: both;'></div>");
                        sb.Append("</div><div>");
                    }
                    sb.Append("</div><div style='margin-top:15px'>");
                    sb.Append("<div>Please contact " + ev.PostedBy.FullName + " if you have any questions regarding these changes.</div>");
                    sb.Append("</div><div style='margin-top:20px'>");
                    sb.Append("<div>Regards,</div>");
                    sb.Append("</div><div style='margin-top:25px'>");
                    sb.Append("Iris Smart Technologies Ltd");
                    sb.Append("</div>");
                }
                else
                {
                    sb.Append("<div style='margin-bottom: 15px'>Hello " + staffmember.FirstName + ",</div>");
                    sb.Append("<div style='margin-bottom: 15px'>This is to inform you that the event \"" + ev.Title + "\" scheduled to take place on " +
                        DateTime.Parse(ev.Start).ToString("dd/MMM/yyyy") + " at " + DateTime.Parse(ev.Start).ToString("hh:mmtt").ToLower() + " has been cancelled.</div>");
                    sb.Append("</div><div style='margin-top:15px'>");
                    sb.Append("<div>Please contact " + ev.PostedBy.FullName + " if you have any questions.</div>");
                    sb.Append("</div><div style='margin-top:20px'>");
                    sb.Append("<div>Regards,</div>");
                    sb.Append("</div><div style='margin-top:25px'>");
                    sb.Append("Iris Smart Technologies Ltd");
                    sb.Append("</div>");
                }

                mail.From = new MailAddress(irisEmail, friendlyName);
                //mail.To.Add(staffmember.EmailAddress);
                mail.To.Add("dami.b.lawal@gmail.com");
                mail.IsBodyHtml = true;

                //set the content
                mail.Subject = "Event Rescheduled";
                mail.Body = sb.ToString();

                SendMail(mail);

            }

            return 1;
        }

        private Event GetEventFromId(int eventid)
        {
            MysqlDataProv MysqlDataProv = new MysqlDataProv();
            DataSet dset = new DataSet();
            List<Event> eventslist = new List<Event>();

            string query = @"select title, start, user_id, end, bg_color, bg.color_name as bg_color_name, bg.color_hex as bg_color_hex, tx.color_name as tx_color_name, 
                             tx.color_hex as tx_color_hex, user_id, description, e.group_id, group_name, time_posted , is_allday, staff_invited, send_email    
                             from iris.calender_events e 
							 left join list_groups g on g.group_id = e.group_id 
							 left join list_colors bg on bg.color_hex = e.bg_color
                             left join list_colors tx on tx.color_hex = e.text_color where e.event_id = @event_id";

            IDataParameter[] param =
                {   
                    new MySqlParameter("event_id",  eventid)
                };

            MysqlDataProv.RunProcedure(query, param, dset);

            if (dset.Tables[0].Rows.Count != 0)
            {
                DataRow row = dset.Tables[0].Rows[0];
                Event ev = new Event();
                Person user = GetUserFromId(int.Parse(row["user_id"].ToString()));
                ev.BackgroundColor = row["bg_color_hex"].ToString();
                ev.Description = row["description"].ToString();
                ev.End = row["end"].ToString();
                ev.EventId = eventid;
                ev.GroupId = int.Parse(row["group_id"].ToString());
                ev.PostedBy = user;
                ev.IsAllDay = int.Parse(row["is_allday"].ToString());
                ev.Start = row["start"].ToString();
                ev.TextColor = row["tx_color_hex"].ToString();
                ev.TimePosted = DateTime.Parse(row["time_posted"].ToString());
                ev.Title = row["title"].ToString();
                ev.SendEmail = int.Parse(row["send_email"].ToString());
                ev.StaffIinvited = row["staff_invited"].ToString();

                return ev;
            }
            else
                return null;
        }

        public void SendCalenderEventEmail(Event ev)
        {
            MailMessage mail = new MailMessage();
            string irisEmail = ConfigurationManager.AppSettings["CompanyEmail"].ToString();
            string friendlyName = ConfigurationManager.AppSettings["FriendlyName"].ToString();

            List<Person> sendto = new List<Person>();
            foreach (string pid in ev.StaffIinvited.Split(','))
            {
                sendto.Add(GetStaffFromId(int.Parse(pid)));
            }

            string people = string.Empty;
            foreach (Person p in sendto)
            {
                if (people == string.Empty)
                    people += p.FirstName + " " + p.Surname;
                else
                    people += ", " + p.FirstName + " " + p.Surname;
            }

            foreach (Person staffmember in sendto)
            {
                StringBuilder sb = new StringBuilder();
                sb.Append("<div style='margin-bottom: 15px'>Hello " + staffmember.FirstName + ",</div>");
                sb.Append("<div style='margin-bottom: 15px'>You have been invited to an Iris meeting:</div>");
                sb.Append("<div>");
                sb.Append("<div style='width:90px; text-align:right; margin-right: 10px;float: left;'>Event Title:</div>");
                sb.Append("<div style='float: left;'>" + ev.Title + "</div>");
                sb.Append("<div style='clear: both;'></div>");
                sb.Append("</div><div style='margin-top:5px'>");
                sb.Append("<div style='width:90px; text-align:right; margin-right: 10px;float: left;'>From:</div>");
                sb.Append("<div style='float: left;'>" + DateTime.Parse(ev.Start).ToString("hh:mm tt") + "</div>");
                sb.Append("<div style='clear: both;'></div>");
                sb.Append("</div><div style='margin-top:5px'>");
                sb.Append("<div style='width:90px; text-align:right; margin-right: 10px;float: left;'>To:</div>");
                sb.Append("<div style='float: left;'>" + DateTime.Parse(ev.End).ToString("hh:mm tt") + "</div>");
                sb.Append("<div style='clear: both;'></div>");
                sb.Append("</div><div style='margin-top:5px'>");
                sb.Append("<div style='width:90px; text-align:right; margin-right: 10px;float: left;'>Staff Involved:</div>");
                sb.Append("<div style='float: left;'>" + people + "</div>");
                sb.Append("<div style='clear: both;'></div>");
                sb.Append("</div><div style='margin-top:5px'>");
                if (!string.IsNullOrEmpty(ev.Description))
                {
                    sb.Append("<div style='width:90px; text-align:right; margin-right: 10px;float: left;'>Details:</div>");
                    sb.Append("<div style='float: left;'>" + ev.Description + "</div>");
                    sb.Append("<div style='clear: both;'></div>");
                    sb.Append("</div><div>");
                }
                sb.Append("</div><div style='margin-top:15px'>");
                sb.Append("<div>Please contact " + ev.PostedBy.FullName + " if you have any questions regarding this meeting.</div>");
                sb.Append("</div><div style='margin-top:20px'>");
                sb.Append("<div>Regards,</div>");
                sb.Append("</div><div style='margin-top:25px'>");
                sb.Append("Iris Smart Technologies Ltd");
                sb.Append("</div>");

                mail.From = new MailAddress(irisEmail, friendlyName);
                //mail.To.Add(staffmember.EmailAddress);
                mail.To.Add("dami.b.lawal@gmail.com");
                mail.IsBodyHtml = true;

                //set the content
                mail.Subject = "Event Scheduled";
                mail.Body = sb.ToString();

                SendMail(mail);
            }
        }

        #endregion

        #region Audit

        public List<Audit> GetAuditTrail(DateTime start, DateTime end)
        {
            DataSet dset = new DataSet();
            string query = @"SELECT audit_id, event_type_name, event_type, user_id, time_stamp, doc_id, description FROM iris.audit a 
                            left join list_event_types l on l.event_type_id = a.event_type
                            where time_stamp >= STR_TO_DATE('" + start.ToString("dd-MMM-yyyy") + @"', '%d-%M-%Y')
                            and time_stamp <= STR_TO_DATE('" + end.AddDays(1).ToString("dd-MMM-yyyy") + @"', '%d-%M-%Y')";

            MysqlDataProv MysqlDataProv = new MysqlDataProv();
            MysqlDataProv.RunProcedure(query, null, dset);

            List<Audit> audits = new List<Audit>();

            foreach (DataRow row in dset.Tables[0].Rows)
            {
                Audit audit = new Audit()
                {
                    Doc = string.IsNullOrEmpty(row["doc_id"].ToString()) ? null : GetDocument(int.Parse(row["doc_id"].ToString()), 0, 0),
                    EventType = int.Parse(row["event_type"].ToString()),
                    EventTypeName = row["event_type_name"].ToString(),
                    Description = row["description"].ToString(),
                    TimeStamp = DateTime.Parse(row["time_stamp"].ToString()),
                    User = string.IsNullOrEmpty(row["user_id"].ToString()) ? null : GetUserFromId(int.Parse(row["user_id"].ToString()))
                };

                // if its an event, set the description to the title of the event
                if (audit.EventType == 11 || audit.EventType == 12 || audit.EventType == 13)
                {
                    if (!string.IsNullOrEmpty(audit.Description) && audit.Description.All(Char.IsDigit))
                    {
                        Event ev = GetEventFromId(int.Parse(audit.Description));
                        audit.Description = ev.Title;
                    }
                }

                // if its an document, set the description to the title of the document
                if (audit.EventType == 4 || audit.EventType == 5 || audit.EventType == 6)
                {
                    if (audit.Doc != null)
                    {
                        audit.Description = audit.Doc.DocName + "." + audit.Doc.DocType;
                    }
                }

                // spaces for austerity sake
                audit.EventTypeName = string.Concat(audit.EventTypeName.Select(x => Char.IsUpper(x) ? " " + x : x.ToString())).TrimStart(' ');
                audits.Add(audit);
            }

            return audits;
        }

        public void AddAudit(Audit audit)
        {
            MysqlDataProv MysqlDataProv = new MysqlDataProv();
            string query = "insert into iris.audit (event_type, user_id, time_stamp, doc_id, description) values (@event_type, @user_id, @time_stamp, @doc_id, @description)";
            IDataParameter[] param =
                    {   
                        new MySqlParameter("event_type",  audit.EventType),
                        new MySqlParameter("user_id",  audit.UserId == 0 ? null : audit.UserId),
                        new MySqlParameter("time_stamp",  DateTime.Now),
                        new MySqlParameter("description",  audit.Description),
                        new MySqlParameter("doc_id",  audit.DocId == 0 ? null : audit.DocId)
                    };

            MysqlDataProv.ExecuteQuery(query, param);
        }

        #endregion

        #region Dashboards

        public SliderData GetPassportData()
        {
            SliderData result = new SliderData();

            // Total Afis info
            List<AfisReport> Afisdata = new List<AfisReport>();

            DataSet dset = new DataSet();
            string query = @"select branchcode, branchname, entryday, 
            sum(pending) as pending, sum(approved) as approved, sum(dropped) as dropped, sum(rejected) as rejected
            from afisquery_stats
            group by branchcode, branchname, entryday
            having entryday > add_months( sysdate, -12 )";

            OracleDataProv OracleDataProv = new OracleDataProv("OracleConnectionStringPassport");
            OracleDataProv.RunProcedure(query, null, dset);

            foreach (DataRow row in dset.Tables[0].Rows)
            {
                AfisReport report = new AfisReport()
                {
                    Approved = int.Parse(row["approved"].ToString()),
                    Dropped = int.Parse(row["dropped"].ToString()),
                    Pending = int.Parse(row["pending"].ToString()),
                    Rejected = int.Parse(row["rejected"].ToString()),
                    BranchCode = int.Parse(row["branchcode"].ToString()),
                    BranchName = row["branchname"].ToString(),
                    EntryDay = DateTime.Parse(row["entryday"].ToString())
                };

                report.Enrols = report.Approved + report.Rejected + report.Dropped;

                Afisdata.Add(report);
            }

            result.TotalAfisData = Afisdata;

            // Total Enrollments info
            List<PassportReport> passportData = new List<PassportReport>();

            dset = new DataSet();
            query = @"select branchcode, branchname, issueday, issued from enrolprofile_stats where issueday > add_months( sysdate, -12 )";

            OracleDataProv = new OracleDataProv("OracleConnectionStringPassport");
            OracleDataProv.RunProcedure(query, null, dset);

            foreach (DataRow row in dset.Tables[0].Rows)
            {
                PassportReport report = new PassportReport()
                {

                    NumberIssued = int.Parse(row["issued"].ToString()),
                    BranchCode = int.Parse(row["branchcode"].ToString()),
                    BranchName = row["branchname"].ToString(),
                    IssueDay = DateTime.Parse(row["issueday"].ToString())
                };

                passportData.Add(report);
            }

            result.TotalPassportData = passportData;

            return result;
        }

        public SliderData GetSliderData()
        {
            SliderData result = new SliderData()
            {
                Piereports = GetPassportVerificationHistory(DateTime.Now.AddYears(-10), DateTime.Now),
                Verificationreports = GetPassportVerificationHistory(new DateTime(2013, 1, 1), DateTime.Now),
                Bankusersreports = GetBankUsersRegistered(DateTime.Now.AddMonths(-12), DateTime.Now.AddMonths(-0)),
                OracleCursor = GetBankCursorStatus()
            };

            return result;
        }

        public Dictionary<string, int[]> GetBankUsersRegistered(DateTime start, DateTime end)
        {
            DataSet dset = new DataSet();
            string query = @"select b.bank_name, create_date from nibss.bank b
                            left join nibss.entity_bank eb 
                            on eb.bank_id = b.id
                            left join nibss.web_accnt wa
                            on wa.entity_id = eb.entity_id
                            where create_date >= to_date('" + start.ToString("dd-MMM-yyyy") + @"', 'DD-Mon-YYYY')
                            and create_date <= to_date('" + end.AddDays(1).ToString("dd-MMM-yyyy") + @"', 'DD-Mon-YYYY') order by create_date";

            OracleDataProv OracleDataProv = new OracleDataProv();
            OracleDataProv.RunProcedure(query, null, dset);

            Dictionary<string, int[]> users = new Dictionary<string, int[]>();
            Dictionary<string, int> banktotal = new Dictionary<string, int>();

            foreach (DataRow row in dset.Tables[0].Rows)
            {
                string bankname = ShortenBankName(row["bank_name"].ToString());
                DateTime creationdate = DateTime.Parse(row["create_date"].ToString());

                // add total
                if (!banktotal.ContainsKey(bankname))
                    banktotal.Add(bankname, 0);

                banktotal[bankname]++;

                if (!users.ContainsKey(bankname))
                    users.Add(bankname, new int[12]);

                int mod = MathMod(creationdate.Month - DateTime.Now.Month, 12);
                int temp = 0;

                if (mod == 0)
                    mod = 12;

                (users[bankname] as int[])[mod - 1] = banktotal[bankname];

                // update all upwards from it
                int monthsleft = Math.Abs(((creationdate.Year - DateTime.Now.Year) * 12) + creationdate.Month - DateTime.Now.Month);

                for (int i = 0; i < monthsleft; i++)
                {
                    (users[bankname] as int[])[MathMod(creationdate.Month - DateTime.Now.Month + i, 12)] = banktotal[bankname];
                }

                if (temp != mod && bankname == "Diamond")
                {
                    var x = 1;
                }
            }

            return users;
        }

        static int MathMod(int a, int b)
        {
            return ((a % b) + b) % b;
        }


        #endregion

    }
}
