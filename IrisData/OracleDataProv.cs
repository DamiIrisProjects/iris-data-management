﻿using System;
using System.Configuration;
using System.Data;
using System.Data.OracleClient;
using System.Data.Common;

namespace IrisData
{
    public class OracleDataProv
    {
        private string connectionString = ConfigurationManager.ConnectionStrings["OracleConnectionString"].ToString();
        DbConnection connection;
        public DbCommand command;

        public OracleDataProv()
        {

        }

        public OracleDataProv(string conn)
        {
            connectionString = ConfigurationManager.ConnectionStrings[conn].ToString();
        }

        public void CreateConnection()
        {
            if (connection == null)
            {
                try
                {
                    connection = new OracleConnection(connectionString);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }

        public void CreateTransactionCommand()
        {
            if (command != null && command.Transaction != null && command.Transaction.Connection != null && command.Transaction.Connection.State == ConnectionState.Open)
                command.Transaction.Connection.Close();

            if (connection == null)
                CreateConnection();

            if (connection.State == ConnectionState.Closed)
                connection.Open();

            DbTransaction transaction = connection.BeginTransaction();

            command = connection.CreateCommand();
            command.Transaction = transaction;
        }

        public int ExecuteQuery(IDataParameter[] parameters)
        {
            int iRowsAffected = -1;
            DbCommand cmd = command;

            try
            {
                if (cmd.Connection.State == ConnectionState.Closed)
                    cmd.Connection.Open();

                if (parameters != null && parameters.Length > 0)
                {
                    cmd.Parameters.Clear();

                    for (int i = 0; i < parameters.Length; i++)
                    {
                        if (parameters[i] != null)
                            cmd.Parameters.Add(parameters[i]);
                    }
                }

                iRowsAffected = cmd.ExecuteNonQuery();
            }
            catch
            { throw; }

            return iRowsAffected;
        }

        public int ExecuteScalar(string strQuery)
        {
            int iRowsAffected = -1;
            DbCommand cmd = command;
            command.CommandText = strQuery;

            try
            {
                if (cmd.Connection.State == ConnectionState.Closed)
                    cmd.Connection.Open();

                iRowsAffected = (Int32)cmd.ExecuteScalar();
            }
            catch
            { throw; }

            return iRowsAffected;
        }

        public int ExecuteQuery(string strQuery, IDataParameter[] parameters)
        {
            int iRowsAffected = -1;

            try
            {
                if (connection == null)
                {
                    CreateConnection();
                }

                if (connection.State != ConnectionState.Open)
                    connection.Open();

                using (DbCommand cmd = connection.CreateCommand())
                {
                    cmd.CommandText = strQuery;
                    if (parameters != null && parameters.Length > 0)
                    {
                        for (int i = 0; i < parameters.Length; i++)
                        {
                            if (parameters[i].Value == null)
                                parameters[i].Value = DBNull.Value;

                            cmd.Parameters.Add(parameters[i]);
                        }
                    }

                    cmd.Connection = connection;
                    iRowsAffected = cmd.ExecuteNonQuery();
                }
            }
            catch
            { throw; }
            finally
            {
                connection.Close();
            }
            return iRowsAffected;
        }

        public void RunProcedure(string strQuery, IDataParameter[] parameters, DataSet dataSet)
        {
            try
            {
                // Make sure there is a connection before running this
                if (connection == null)
                {
                    CreateConnection();
                }

                if (connection.State != ConnectionState.Open)
                    connection.Open();

                using (DbCommand cmd = connection.CreateCommand())
                {
                    cmd.CommandText = strQuery;
                    if (parameters != null && parameters.Length > 0)
                    {
                        for (int i = 0; i < parameters.Length; i++)
                        {
                            cmd.Parameters.Add(parameters[i]);
                        }
                    }

                    DbDataAdapter OracleDA = new OracleDataAdapter();
                    OracleDA.SelectCommand = cmd;
                    OracleDA.Fill(dataSet);
                    connection.Close();
                }
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
            }
        }

        public int GetIdNumber(DbCommand cmd)
        {
            try
            {
                if (cmd.Connection.State == ConnectionState.Closed)
                    cmd.Connection.Open();

                cmd.CommandText = "select last_insert_id();";
                return Convert.ToInt32(cmd.ExecuteScalar());
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void CloseQuery()
        {
            connection.Close();
        }
    }
}
