﻿using System;
using System.Configuration;
using System.Data;
using MySql.Data.MySqlClient;
using System.Data.Common;

namespace IrisData
{
    public class MysqlDataProv
    {
        private string connectionString = ConfigurationManager.ConnectionStrings["MySqlConnectionString"].ToString();
        DbConnection connection;
        public DbCommand command;

        public void CreateConnection()
        {
            if (connection == null)
            {
                try
                {
                    connection = new MySqlConnection(connectionString);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }

        public void CreateTransactionCommand()
        {
            if (command != null && command.Transaction != null && command.Transaction.Connection != null && command.Transaction.Connection.State == ConnectionState.Open)
                command.Transaction.Connection.Close();

            if (connection == null)
                CreateConnection();

            if (connection.State == ConnectionState.Closed)
                connection.Open();

            DbTransaction transaction = connection.BeginTransaction();

            command = connection.CreateCommand();
            command.Transaction = transaction;
        }

        public int ExecuteQuery(IDataParameter[] parameters)
        {
            int iRowsAffected = -1;
            DbCommand cmd = command;

            try
            {
                if (cmd.Connection.State == ConnectionState.Closed)
                    cmd.Connection.Open();

                if (parameters != null && parameters.Length > 0)
                {
                    cmd.Parameters.Clear();

                    for (int i = 0; i < parameters.Length; i++)
                    {
                        if (parameters[i] != null)
                            cmd.Parameters.Add(parameters[i]);
                    }
                }

                iRowsAffected = cmd.ExecuteNonQuery();
            }
            catch
            { throw; }
            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    cmd.Dispose();
                    connection.Close();
                }
            }

            return iRowsAffected;
        }

        public int ExecuteQuery(string strQuery, IDataParameter[] parameters)
        {
            int iRowsAffected = -1;

            try
            {
                if (connection == null)
                {
                    CreateConnection();
                }

                if (connection.State != ConnectionState.Open)
                    connection.Open();

                using (DbCommand cmd = connection.CreateCommand())
                {
                    cmd.CommandText = strQuery;
                    if (parameters != null && parameters.Length > 0)
                    {
                        for (int i = 0; i < parameters.Length; i++)
                        {
                            cmd.Parameters.Add(parameters[i]);
                        }
                    }

                    cmd.Connection = connection;
                    iRowsAffected = cmd.ExecuteNonQuery();
                }
            }
            catch
            { throw; }
            finally
            {
                connection.Close();
            }
            return iRowsAffected;
        }

        public void RunProcedure(string strQuery, IDataParameter[] parameters, DataSet dataSet)
        {
            try
            {
                // Make sure there is a connection before running this
                if (connection == null)
                {
                    CreateConnection();
                }

                if (connection.State != ConnectionState.Open)
                connection.Open();

                using (DbCommand cmd = connection.CreateCommand())
                {
                    cmd.CommandText = strQuery;
                    if (parameters != null && parameters.Length > 0)
                    {
                        for (int i = 0; i < parameters.Length; i++)
                        {
                            cmd.Parameters.Add(parameters[i]);
                        }
                    }
                        
                    DbDataAdapter MySqlDA = new MySqlDataAdapter();
                    MySqlDA.SelectCommand = cmd;
                    MySqlDA.Fill(dataSet);
                    connection.Close();
                }
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
            }
        }

        public int GetIdNumber(DbCommand cmd)
        {
            DataSet dset = new DataSet();
            try
            {
                if (cmd.Connection.State == ConnectionState.Closed)
                    cmd.Connection.Open();

                cmd.CommandText = "select last_insert_id();";
                return Convert.ToInt32(cmd.ExecuteScalar());
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void CloseQuery()
        {
            connection.Close();
        }
    }
}
