﻿using System;
using System.Collections.Generic;
using System.Linq;
using IrisContract;
using IrisData;
using IrisCommon.Entities;
using System.Data;
using System.IO;
using System.Configuration;

namespace IrisService
{
    public class myService : DataOperations
    {
        #region Registrations and Logins

        public Person LoginUser(string username, string password)
        {
            Data data = new Data();
            return data.LoginUser(username, password);
        }

        public int UpdateUserInfo(Person person)
        {
            Data data = new Data();
            return data.UpdateUserInfo(person);
        }

        public string SendPasswordByMail(string email)
        {
            Data data = new Data();
            return data.SendPasswordByMail(email);
        }

        public int VerifyPasswordChange(string email, string guid)
        {
            Data data = new Data();
            return data.VerifyPasswordChange(email, guid);
        }

        public int UpdateForgottenPassword(string email, string password)
        {
            Data data = new Data();
            return data.UpdateForgottenPassword(email, password);
        }

        public int VerifyUsernameDoesNotExists(string email)
        {
            Data data = new Data();
            return data.VerifyUsernameDoesNotExists(email);
        }

        public int RegisterUser(Person enrollee)
        {
            Data data = new Data();
            return data.RegisterUser(enrollee);
        }

        public int ActivateUser(string guid)
        {
            Data data = new Data();
            return data.ActivateUser(guid);
        }

        public int ResetUserPassword(string email)
        {
            Data data = new Data();
            return data.ResetUserPassword(email);
        }

        public int ChangeStaffPassword(string email, int userid)
        {
            Data data = new Data();
            return data.ChangeStaffPassword(email, userid);
        }

        public void SetProfilePicture(string loc, int userid)
        {
            Data data = new Data();
            data.SetProfilePicture(loc, userid);
        }

        #endregion

        #region Misc

        public bool TestConnection()
        {
            Data data = new Data();
            return data.TestConnection();
        }

        public void SaveError(Error error)
        {
            Data data = new Data();
            data.SaveError(error);
        }

        public DataSet GetLists()
        {
            Data data = new Data();
            return data.GetLists();
        }

        #endregion

        #region Documents

        public List<Document> GetUserDocuments(int user_id)
        {
            Data data = new Data();
            return data.GetUserDocuments(user_id);
        }

        public int SaveDocument(Document doc)
        {
            Data data = new Data();
            return data.SaveDocument(doc);
        }

        public List<Document> SearchForDocuments(string k, string cat, string proj, string staffdoc, string updatefrom, string updateto, string docdatefrom, string docdateto, string ext, int userid)
        {
            Data data = new Data();
            return data.SearchForDocuments(k, cat, proj, staffdoc, updatefrom, updateto, docdatefrom, docdateto, ext, userid);
        }

        public Document GetDocument(int doc_id, int userid, int audit_type)
        {
            Data data = new Data();
            return data.GetDocument(doc_id, userid, audit_type);
        }

        public void GiveAccessToUsers(string userids, string docid)
        {
            Data data = new Data();
            data.GiveAccessToUsers(userids, docid);
        }

        public void GiveAccessToDepts(string depts, string docid)
        {
            Data data = new Data();
            data.GiveAccessToDepts(depts, docid);
        }

        public void DeleteFile(string location)
        {
            Data data = new Data();
            data.DeleteFile(location);
        }

        public void RenameFile(string location, string newname)
        {
            Data data = new Data();
            data.RenameFile(location, newname);
        }

        #endregion

        #region Reports/Maintenance

        public List<Report> GetPassportVerificationHistory(DateTime start, DateTime stop, bool WithFace)
        {
            Data data = new Data();
            return data.GetPassportVerificationHistory(start, stop, WithFace);
        }

        public Report GetPassportDetails(string docno)
        {
            Data data = new Data();
            return data.GetPassportDetails(docno);
        }

        public List<Report> GetBankLoginHistory(DateTime From, DateTime To)
        {
            Data data = new Data();
            return data.GetBankLoginHistory(From, To);
        }

        public List<Report> GetBankMessageHistory(DateTime From, DateTime To)
        {
            Data data = new Data();
            return data.GetBankMessageHistory(From, To);
        }

        public List<Report> GetBankExceptionsHistory(DateTime From, DateTime To)
        {
            Data data = new Data();
            return data.GetBankExceptionsHistory(From, To);
        }

        public List<Report> GetBankCursorStatus()
        {
            Data data = new Data();
            return data.GetBankCursorStatus();
        }

        public List<Person> GetUserList()
        {
            Data data = new Data();
            return data.GetUserList();
        }

        public int EditStaffAccessLevel(int person_id, int level, int currlevel, int mystaff_id)
        {
            Data data = new Data();
            return data.EditStaffAccessLevel(person_id, level, currlevel, mystaff_id);
        }

        public List<PassportReport> GetPassportsIssued(DateTime start, DateTime end)
        {
            Data data = new Data();
            return data.GetPassportsIssued(start, end);
        }

        #endregion

        #region Calender

        public List<Event> GetCalenderEvents()
        {
            Data data = new Data();
            return data.GetCalenderEvents();
        }

        public int CreateCalenderEvent(Event ev)
        {
            Data data = new Data();
            return data.CreateCalenderEvent(ev);
        }

        public int UpdateCalenderEvent(Event ev)
        {
            Data data = new Data();
            return data.UpdateCalenderEvent(ev);
        }

        public int DeleteCalenderEvent(int eventid, int userid)
        {
            Data data = new Data();
            return data.DeleteCalenderEvent(eventid, userid);
        }

        public int UpdateStaffEventChange(Event ev)
        {
            Data data = new Data();
            return data.UpdateStaffEventChange(ev);
        }

        #endregion

        #region Audit

        public List<Audit> GetAuditTrail(DateTime From, DateTime To)
        {
            Data data = new Data();
            return data.GetAuditTrail(From, To);
        } 

        #endregion

        #region Dashboards

        public Dictionary<string, int[]> GetBankUsersRegistered(DateTime start, DateTime end)
        {
            Data data = new Data();
            return data.GetBankUsersRegistered(start, end);
        }

        public SliderData GetSliderData()
        {
            Data data = new Data();
            return data.GetSliderData();
        }

        public SliderData GetSliderDataToday()
        {
            Data data = new Data();
            return data.GetSliderDataToday();
        }

        public Dictionary<string, int[]> GetPassportVerificationHistoryDic(DateTime start, DateTime end)
        {
            Data data = new Data();
            return data.GetPassportVerificationHistoryDic(start, end);
        }

        public SliderData GetPassportDefaultData()
        {
            Data data = new Data();
            return data.GetPassportDefaultData();
        }

        public SliderData GetPassportData(DateTime start, DateTime end)
        {
            Data data = new Data();
            return data.GetPassportData(start, end);
        } 

        // Socketworks
        public SocketWorksSummary GetSocketWorksDefaultData()
        {
            Data data = new Data();
            return data.GetSocketWorksDefaultData();
        }

        public SocketWorksSummary GetSocketWorksTodaysData()
        {
            Data data = new Data();
            return data.GetSocketWorksTodaysData();
        } 

        #endregion

        #region Branch Viewer

        public List<Branch> GetAllBranches()
        {
            Data data = new Data();
            return data.GetAllBranches();
        }

        #endregion

        #region iDoc Application

        public List<string> GetDirectories(string path)
        {
            return Directory.GetDirectories(path).ToList();
        }

        public string CreateFolder(string path)
        {
            try
            {
                string repository = ConfigurationManager.AppSettings["repositoryFolder"];
                if (Directory.Exists(Path.Combine(repository, path)))
                    return "Folder already exists";
                else
                    Directory.CreateDirectory(Path.Combine(repository, path));

                return "Folder created succesfully";
            }
            catch(Exception ex)
            {
                return ex.Message;
            }
        }

        public int SaveFile(byte[] file, string path)
        {
            try
            {
                string repository = ConfigurationManager.AppSettings["repositoryFolder"];
                File.WriteAllBytes(Path.Combine(repository, path), file);
                return 1;
            }
            catch(Exception ex)
            {
                return 0;
            }
        }



        #endregion
    }
}
