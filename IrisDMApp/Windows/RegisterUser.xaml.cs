﻿using System;
using System.Windows;
using IrisCommon.Entities;

namespace VMS.Windows
{
    /// <summary>
    /// Interaction logic for RegisterUser.xaml
    /// </summary>
    public partial class RegisterUser : Window
    {
        public RegisterUser()
        {
            InitializeComponent();
        }

        #region Operations

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            if (!OperatorDetails.ValidateInfo())
            {
                TabControl.SelectedIndex = 0;
                return;
            }

            if (!LoginDetails.ValidateUsernamePW())
            {
                TabControl.SelectedIndex = 1;
                return;
            }

            // Register User
            Person op = OperatorDetails.GetUserDetails();
            op.Username = LoginDetails.txtUsername.Text;
            op.Password = LoginDetails.txtPassword.Password;

            try
            {
                string result = string.Empty; // new Data().CreateOperator(op);

                if (result == string.Empty)
                {
                    MessageBox.Show("Operator created and pending approval", "Success", MessageBoxButton.OK, MessageBoxImage.Information);
                    Close();
                }
                
                else if (result == "1")
                {
                    MessageBox.Show("Username already in use.", "Validation Failed", MessageBoxButton.OK, MessageBoxImage.Information);
                }

                else
                {
                    MessageBox.Show("A record your fingerprint already exists as Username : '" + result + "'", "Validation Failed", MessageBoxButton.OK, MessageBoxImage.Information);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Could not save operator", "Error Saving Operator", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

        #endregion
    }
}
