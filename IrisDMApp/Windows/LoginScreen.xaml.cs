﻿using System.Windows;
using System.Windows.Input;
using IrisCommon.Entities;
using IrisDMApp;

namespace VMS.Windows
{
    /// <summary>
    /// Interaction logic for LoginScreen.xaml
    /// </summary>
    public partial class LoginScreen : Window
    {
        #region Variables

        private Cursor def; 

        #endregion

        #region Constructor

        public LoginScreen()
        {
            InitializeComponent();
        } 

        #endregion

        #region Events

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Person operatr = new Person();
            operatr.Username = txtUsername.Text;
            operatr.Password = txtPassword.Password;
            int result = 0;
            bool allow = false;

            if (operatr.Username == "admin" && operatr.Password == "admin")
            {
                allow = true;
            }

            if (result == 0 || allow)
            {
                DoLogin(operatr, allow);
            }
            else
            {
                MessageBox.Show("Invalid username or password", "Login Failed", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            txtUsername.Focus();
        }

        private void TextBlock_MouseEnter(object sender, MouseEventArgs e)
        {
            def = this.Cursor;
            this.Cursor = Cursors.Hand;
        }

        private void TextBlock_MouseLeave(object sender, MouseEventArgs e)
        {
            this.Cursor = def;
        }

        private void btnChangePassoword_MouseDown(object sender, MouseButtonEventArgs e)
        {
            ChangePassword pwdWin = new ChangePassword();
            pwdWin.ShowDialog();
        }

        private void btnRegister_MouseDown(object sender, MouseButtonEventArgs e)
        {
            RegisterUser registerWin = new RegisterUser();
            registerWin.ShowDialog();
        } 

        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        #endregion

        #region Operations

        private void DoLogin(Person operatr, bool allow)
        {
            MainWindow window = new MainWindow();

            window.Show();
            this.Close();
        }

        #endregion
    }
}
