﻿using System.Windows;

namespace IrisDMApp.Windows
{
    /// <summary>
    /// Interaction logic for EnterFolderWindow.xaml
    /// </summary>
    public partial class EnterFolderWindow : Window
    {
        public EnterFolderWindow()
        {
            InitializeComponent();
        }

        public string FolderName { get; set; }

        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void btnCreate_Click(object sender, RoutedEventArgs e)
        {
            FolderName = txtFolderName.Text;
            Close();
        }
    }
}
