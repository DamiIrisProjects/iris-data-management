﻿using System.Collections.Generic;
using System.Windows;
using System.Windows.Media.Imaging;

namespace VMS.Windows
{
    /// <summary>
    /// Interaction logic for PopupImage.xaml
    /// </summary>
    public partial class PopupImage : Window
    {
        public List<string> ImageList { get; set; }
        BitmapSource currentImage;
        int index = 0;

        public PopupImage()
        {
            InitializeComponent();
        }

        public PopupImage(List<string> sourceimages)
        {
            InitializeComponent();

            ImageList = sourceimages;
            if (ImageList != null && ImageList.Count != 0)
            {
                CurrentImage = GetImage(index);

                if (ImageList.Count > 1)
                {
                    ToggleButton(2, true);
                }
            }
        }

        private BitmapSource GetImage(int index)
        {
            var bitmap = new BitmapImage();
            bitmap.BeginInit();
            bitmap.UriSource = new System.Uri(ImageList[index]);
            bitmap.CacheOption = BitmapCacheOption.OnLoad;
            bitmap.EndInit();
            return bitmap;
        }

        private void ToggleButton(int btn, bool val)
        {
            if (btn == 1)
            {
                btnPrevious.IsEnabled = val;
                btnPrevious.Opacity = val ? 1 : 0.7;
            }
            else
            {
                btnNext.IsEnabled = val;
                btnNext.Opacity = val ? 1 : 0.7;
            }
        }

        private BitmapSource CurrentImage
        {
            get { return currentImage; }
            set
            {
                currentImage = value;
                docImage.Source = currentImage;
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void btnPrevious_Click(object sender, RoutedEventArgs e)
        {
            index--;
            CurrentImage = GetImage(index);
            ToggleButton(2, true);

            if (index == 0)
                ToggleButton(1, false);
        }

        private void btnNext_Click(object sender, RoutedEventArgs e)
        {
            index++;
            CurrentImage = GetImage(index);
            if (ImageList.Count - 1 == index)
                ToggleButton(2, false);
                
            ToggleButton(1, true);
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            CurrentImage = null;
            ImageList = null;
        }
    }
}
