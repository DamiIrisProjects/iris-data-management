﻿using System;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Threading;

namespace VMS.Windows
{
    /// <summary>
    /// Interaction logic for popupInfo.xaml
    /// </summary>
    public partial class PopupInfo : Window
    {
        public PopupInfo(string msg, int color)
        {
            InitializeComponent();

            Opacity = 0;
            txtInfo.Text = msg;

            if (color == 1)
                txtInfo.Foreground = Brushes.Green;
            if (color == 2)
                txtInfo.Foreground = Brushes.Maroon;
            if (color == 3)
                txtInfo.Foreground = Brushes.Red;


            DispatcherTimer dt = new DispatcherTimer();
            if (color == 2)
                dt.Interval = new TimeSpan(0, 0, 0, 2, 1); //set the interval 2 second
            else
                dt.Interval = new TimeSpan(0, 0, 0, 1, 5); //set the interval 2 second
            dt.Start(); //start the timer
            dt.Tick += new EventHandler(dt_Tick); //set the timer tick
        }

        private void dt_Tick(object sender, EventArgs e) //timer tick
        {
            var ObjAnimation = new DoubleAnimation(0, (Duration)TimeSpan.FromSeconds(0.3));
            ObjAnimation.Completed += new EventHandler(Animation_FadeOutCompleted);
            this.BeginAnimation(UIElement.OpacityProperty, ObjAnimation);
        }

        private void Animation_FadeOutCompleted(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            var ObjAnimation = new DoubleAnimation(1, (Duration)TimeSpan.FromSeconds(0.3));
            this.BeginAnimation(UIElement.OpacityProperty, ObjAnimation);
        }
    }
}
