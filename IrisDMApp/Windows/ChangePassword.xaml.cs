﻿using System;
using System.Windows;

namespace VMS.Windows
{
    /// <summary>
    /// Interaction logic for ChangePassword.xaml
    /// </summary>
    public partial class ChangePassword : Window
    {
        public ChangePassword()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                bool isValid = ChangePasswordDetails.ValidateUsernamePW();

                if (isValid)
                {
                    string message = string.Empty; // new Data().ChangePassword(ChangePasswordDetails.txtUsername.Text, ChangePasswordDetails.txtPassword.Password, ChangePasswordDetails.txtConfirmPassword.Password);

                    if (message == string.Empty)
                    {
                        MessageBox.Show("Password successfully changed", "Status", MessageBoxButton.OK, MessageBoxImage.Information);
                        Close();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error changing password", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }
    }
}
