﻿using System;
using System.IO;
using System.Text;

namespace IrisDMApp
{
    public static class LogError
    {
        public static DateTime Today { get; set; }
        private static TextWriter tw;

        public static void LogTextError(Exception ex)
        {
            string filename = DateTime.Today.ToString("yyyy-MM-dd") + "_ErrLog.txt";
            string errormessage = CreateExceptionString(ex);
            string demarcation = "===========";
            string path = "C:/IrisAppLogs";
            string fullpath = Path.Combine(path, filename);

            // Make sure path exists
            if (!Directory.Exists(path))
                Directory.CreateDirectory(path);

            // Save as textfile
            using (tw = new StreamWriter(fullpath, true))
            {
                tw.WriteLine("");
                tw.WriteLine(DateTime.Now.ToString("hh:mm:ss tt"));
                tw.WriteLine(demarcation);
                tw.WriteLine(errormessage);
                tw.Close();
            }
        }

        private static string CreateExceptionString(Exception e)
        {
            StringBuilder sb = new StringBuilder();
            CreateExceptionString(sb, e, string.Empty);

            return sb.ToString();
        }

        private static void CreateExceptionString(StringBuilder sb, Exception e, string indent)
        {
            if (indent == null)
            {
                indent = string.Empty;
            }
            else if (indent.Length > 0)
            {
                sb.AppendFormat("{0}Inner ", indent);
            }

            sb.AppendFormat("Exception Found:\n{0}Type: {1}", indent, e.GetType().FullName);
            sb.AppendFormat("\n{0}Message: {1}", indent, e.Message);
            sb.AppendFormat("\n{0}Source: {1}", indent, e.Source);
            sb.AppendFormat("\n{0}Stacktrace: {1}", indent, e.StackTrace);

            if (e.InnerException != null)
            {
                sb.Append("\n");
                CreateExceptionString(sb, e.InnerException, indent + "  ");
            }
        }
    }
}
