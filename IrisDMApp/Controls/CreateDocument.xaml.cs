﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Imaging;
using IrisCommon.Entities;
using VMS.Helpers;
using System.IO;
using IrisCommon.Helpers;
using VMS.Windows;
using System.Windows.Markup;
using System.Xml;
using System.Diagnostics;
using System.Windows.Threading;
using System.Reflection;

namespace VMS.Controls
{
    /// <summary>
    /// Interaction logic for CreateDocument.xaml
    /// </summary>
    public partial class CreateDocument : UserControl
    {
        #region Constructor

        public CreateDocument()
        {
            InitializeComponent();
        } 

        #endregion

        #region Variables

        public List<Document> documents = new List<Document>();
        public byte[] currentDoc;
        public bool IsChecked { get; set; }
        public event ScannerCompletedEventHandler ScanComplete;
        public delegate void ScannerCompletedEventHandler(ScannerControl control, EventArgs e);
        public event ClearClickedEventHandler ClearClicked;
        public delegate void ClearClickedEventHandler(ScannerControl control, EventArgs e);

        #endregion

        #region Events

        private void btnScanSave_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (PiggyBank.ScannerWindow == null)
                    PiggyBank.ScannerWindow = new WPFTwain.MainWindow();
                
                PiggyBank.ScannerWindow.ShowDialog();

                if (PiggyBank.ScannerWindow.ScannedFile != null)
                {
                    Image DocImage = new Image();
                    DocImage.Source = PiggyBank.ScannerWindow.ScannedFile;
                    
                    JpegBitmapEncoder encoder = new JpegBitmapEncoder();

                    using (MemoryStream stream = new MemoryStream())
                    {
                        BitmapSource bs = DocImage.Source as BitmapSource;
                        BitmapFrame bf = BitmapFrame.Create(bs);
                        encoder.Frames.Add(bf);
                        encoder.Save(stream);
                        currentDoc = stream.ToArray();
                    }
                    
                    btnViewScan.IsEnabled = true;
                    btnViewScan.Opacity = 1;
                    btnClear.IsEnabled = true;
                    btnClear.Opacity = 1;

                    OnScanComplete(new EventArgs());
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

        private void btnViewScan_Click(object sender, RoutedEventArgs e)
        {
            PopupImage document = new PopupImage();
            document.docImage.Source = Helper.BitmapToSource(Helper.BytesToBitmap(currentDoc));
            document.ShowDialog();
        }

        public void OnScanComplete(EventArgs e)
        {
            ScannerCompletedEventHandler scannerEvent = ScanComplete;

            if (ScanComplete != null)
            {
                ScanComplete(null, e);
            }
        }

        public void OnClearClicked(EventArgs e)
        {
            ClearClickedEventHandler scannerEvent = ClearClicked;

            if (ClearClicked != null)
            {
                ClearClicked(null, e);
            }
        }

        #endregion

        #region Operations

        public void Clear()
        {
            btnViewScan.Opacity = 0.4;
            btnViewScan.IsEnabled = false;
            btnClear.Opacity = 0.4;
            btnClear.IsEnabled = false;
        }

        private void btnClear_Click(object sender, RoutedEventArgs e)
        {
            ClearScannerStuff();
        }

        public void ClearScannerStuff()
        {
            documents.Clear();
            btnClear.IsEnabled = false;
            btnClear.Opacity = 0.4;
            btnViewScan.IsEnabled = false;
            btnViewScan.Opacity = 0.4;
        }

        #endregion

        #region TreeView Control

        void TreeView_Loaded(object sender, RoutedEventArgs e)
        {
            /// Create main expanded node of TreeView
            treeView.Items.Add(TreeView_CreateComputerItem());
            /// Update open directories every 5 second
            DispatcherTimer timer = new DispatcherTimer(TimeSpan.FromSeconds(5),
                DispatcherPriority.Background, TreeView_Update, Dispatcher);
        }

        void TreeView_Update(object sender, EventArgs e)
        {
            Stopwatch s = new Stopwatch();
            s.Start();
            /// Update drives and folders in Computer
            /// create copy for detect what item was expanded
            TreeView oldTreeView = CloneUsingXaml(treeView) as TreeView;
            /// populate items from scratch
            treeView.Items.Clear();
            /// add computer expanded node with all drives
            treeView.Items.Add(TreeView_CreateComputerItem());
            TreeViewItem newComputerItem = treeView.Items[0] as TreeViewItem;
            TreeViewItem oldComputerItem = oldTreeView.Items[0] as TreeViewItem;
            /// Save old state of item
            newComputerItem.IsExpanded = oldComputerItem.IsExpanded;
            newComputerItem.IsSelected = oldComputerItem.IsSelected;
            /// check all drives for creating it's root folders
            foreach (TreeViewItem newDrive in (treeView.Items[0] as TreeViewItem).Items)
                if (newDrive.Items.Contains(null))
                    /// Find relative old item for newDrive
                    foreach (TreeViewItem oldDrive in oldComputerItem.Items)
                        if (oldDrive.Tag as string == newDrive.Tag as string)
                        {
                            newDrive.IsSelected = oldDrive.IsSelected;
                            if (oldDrive.IsExpanded)
                            {
                                newDrive.Items.Clear();
                                TreeView_AddDirectoryItems(oldDrive, newDrive);
                            }
                            break;
                        }
            s.Stop();
        }

        void TreeView_AddDirectoryItems(TreeViewItem oldItem, TreeViewItem newItem)
        {
            newItem.IsExpanded = oldItem.IsExpanded;
            newItem.IsSelected = oldItem.IsSelected;
            /// add folders in this drive
            string[] directories = Directory.GetDirectories(newItem.Tag as string);
            /// for each folder create TreeViewItem
            foreach (string directory in directories)
            {
                TreeViewItem treeViewItem = new TreeViewItem();
                treeViewItem.Header = new DirectoryInfo(directory).Name;
                treeViewItem.Tag = directory;
                try
                {
                    if (Directory.GetDirectories(directory).Length > 0)
                        /// find respective old folder
                        foreach (TreeViewItem oldDir in oldItem.Items)
                            if (oldDir.Tag as string == directory)
                            {
                                if (oldDir.IsExpanded)
                                {
                                    TreeView_AddDirectoryItems(oldDir, treeViewItem);
                                }
                                else
                                {
                                    treeViewItem.Items.Add(null);
                                }
                                break;
                            }
                }
                catch { }
                treeViewItem.Expanded += TreeViewItem_Expanded;
                treeViewItem.PreviewMouseDown += TreeViewItem_Selected;
                newItem.Items.Add(treeViewItem);
            }
        }

        TreeViewItem TreeView_CreateComputerItem()
        {
            TreeViewItem computer = new TreeViewItem { Header = "Iris", IsExpanded = true };
            computer.Expanded += TreeViewItem_Expanded;
            computer.PreviewMouseDown += TreeViewItem_Selected;

            string debug = System.IO.Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location + System.IO.Path.DirectorySeparatorChar);
            string output = debug.Substring(0, debug.IndexOf("\\IrisDMApp\\") + 1) + "IrisDataManagement\\Repository\\Iris";

            try
            {
                foreach (string folder in Directory.GetDirectories(output))
                {
                    TreeViewItem driveItem = new TreeViewItem();
                    driveItem.Header = System.IO.Path.GetFileName(folder);
                    driveItem.Tag = System.IO.Path.GetFullPath(folder);
                    if (Directory.GetDirectories(folder).Length > 0)
                        driveItem.Items.Add(null);
                    driveItem.Expanded += TreeViewItem_Expanded;
                    driveItem.PreviewMouseDown += TreeViewItem_Selected;
                    computer.Items.Add(driveItem);
                }
            }
            catch (Exception)
            {

            }

            return computer;
        }

        void TreeViewItem_Selected(object sender, RoutedEventArgs e)
        {
            TreeViewItem item = (TreeViewItem)sender;
            txtSelectedFolder.Text = item.Header.ToString();
            btnNewFolder.IsEnabled = true;
            btnNewFolder.Opacity = 1;
        }

        void TreeViewItem_Expanded(object sender, RoutedEventArgs e)
        {
            TreeViewItem rootItem = (TreeViewItem)sender;

            if (rootItem.Items.Count == 1 && rootItem.Items[0] == null)
            {
                rootItem.Items.Clear();

                string[] dirs;
                try
                {
                    dirs = Directory.GetDirectories((string)rootItem.Tag);
                }
                catch
                {
                    return;
                }

                foreach (var dir in dirs)
                {
                    TreeViewItem subItem = new TreeViewItem();
                    subItem.Header = new DirectoryInfo(dir).Name;
                    subItem.Tag = dir;
                    try
                    {
                        if (Directory.GetDirectories(dir).Length > 0)
                            subItem.Items.Add(null);
                    }
                    catch { }
                    subItem.Expanded += TreeViewItem_Expanded;
                    subItem.PreviewMouseDown += TreeViewItem_Selected;
                    rootItem.Items.Add(subItem);
                }
            }
        }

        object CloneUsingXaml(object obj)
        {
            string xaml = XamlWriter.Save(obj);
            return XamlReader.Load(new XmlTextReader(new StringReader(xaml)));
        }

        #endregion

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            if (VerifyData())
            {
                Document doc = new Document();
                doc.DatePosted = DateTime.Now;
                doc.Doc = currentDoc;
                doc.DocDate = DateTime.Parse(txtDateOfDoc.Text);
                doc.DocName = txtDocName.Text;
                doc.DocType = "jpg";
                doc.PhysicalLocation = txtPhysLocation.Text;
            }
        }

        private bool VerifyData()
        {
            bool result = true;

            if (Helper.CheckDatePicker(txtDateOfDoc))
                result = false;

            if (Helper.CheckTextBox(txtDocName))
                result = false;

            if (txtSelectedFolder.Text == string.Empty)
            {
                MessageBox.Show("Please select a folder to upload to", "Document Folder not selected", MessageBoxButton.OK, MessageBoxImage.Information);
                return false;
            }

            if (currentDoc == null)
            {
                MessageBox.Show("You have not uploaded any files yet", "No uploads found", MessageBoxButton.OK, MessageBoxImage.Information);
                return false;
            }

            return result;
        }

        private void btnNewFolder_Click(object sender, RoutedEventArgs e)
        {
        }

        
    }
}
