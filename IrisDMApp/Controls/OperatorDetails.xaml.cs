﻿using System.Collections.Generic;
using System.Windows.Controls;
using System.Windows.Input;
using IrisCommon.Entities;
using IrisCommon.Helpers;

namespace VMS.Controls
{
    /// <summary>
    /// Interaction logic for PersonDetailsInfo.xaml
    /// </summary>
    public partial class OperatorDetails : UserControl
    {
        public OperatorDetails()
        {
            InitializeComponent();
        }

        private void txtPhonenum_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = !Helper.NoTextAllowed(e.Text);
        }

        public bool ValidateInfo()
        {
            bool isValid = true;

            isValid = Helper.CheckTextBox(txtFname);
            isValid = Helper.CheckTextBox(txtLname) && isValid;
            isValid = Helper.CheckTextBox(txtPhonenum) && isValid;
            isValid = Helper.CheckDatePicker(datePicker) && isValid;
            isValid = Helper.CheckRadioBox(new List<RadioButton>(){ rdMiss, rdMr, rdMrs}) && isValid;

            return isValid;
        }

        public Person GetUserDetails()
        {
            Person person = new Person();
            person.FirstName = txtFname.Text;
            person.Surname = txtLname.Text;
            person.EmailAddress = txtEmail.Text;
            person.PhoneNumber = txtPhonenum.Text;

            return person;
        }


    }
}
