﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using VMS.Windows;
using VMS.Helpers;
using IrisCommon.Entities;
using TwainDotNet;
using TwainDotNet.Wpf;
using System.Drawing;

namespace VMS.Controls
{
    /// <summary>
    /// Interaction logic for ScannerControl.xaml
    /// </summary>
    public partial class ScannerControl : UserControl
    {
        #region Variables

        public List<Document> documents = new List<Document>();
        public Document currentDoc = new Document();

        #endregion

        #region Constructor

        public ScannerControl()
        {
            InitializeComponent();
        }

        #endregion

        #region Events

        private void btnScanSave_Click(object sender, RoutedEventArgs e)
        {
            Scan();
        }

        private void btnViewScan_Click(object sender, RoutedEventArgs e)
        {
            PopupImage document = new PopupImage();
            List<string> sourceimages = new List<string>();
           
            foreach (string img in images)
            {
                sourceimages.Add(img);
            }

            document.ImageList = sourceimages;
            document.ShowDialog();
        }

        #endregion

        #region Operations

        public void Clear()
        {
            btnViewScan.Opacity = 0.4;
            btnViewScan.IsEnabled = false;
            btnClear.Opacity = 0.4;
            btnClear.IsEnabled = false;
        }

        private void btnClear_Click(object sender, RoutedEventArgs e)
        {
            ClearScannerStuff();
        }

        public void ClearScannerStuff()
        {
            documents.Clear();
            btnClear.IsEnabled = false;
            btnClear.Opacity = 0.4;
            btnViewScan.IsEnabled = false;
            btnViewScan.Opacity = 0.4;
        }

        #endregion

        #region Twain

        private Twain _twain;
        private ScanSettings _settings;

        private Bitmap resultImage;
        private List<string> images;

        private void btnChangeScanner_MouseDown(object sender, MouseButtonEventArgs e)
        {
            _twain.SelectSource();
        }

        private void Scan()
        {
            IsEnabled = false;

            _settings = new ScanSettings
            {
                ShowTwainUI = false,
                ShowProgressIndicatorUI = true
            };

            try
            {
                _twain.StartScanning(_settings);
            }
            catch (TwainException ex)
            {
                MessageBox.Show(ex.Message);
            }

            IsEnabled = true;
        }

        #endregion

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            _twain = new Twain(new WpfWindowMessageHook(PiggyBank.MainWindow));
            _twain.TransferImage += delegate(Object s, TransferImageEventArgs args)
            {
                if (args.Image != null)
                {
                    if (images == null)
                        images = new List<string>();
                    resultImage = args.Image;
                }
            };

            _twain.ScanningComplete += delegate
            {
                IsEnabled = true;
            };
        }
    }
}
