﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Configuration;
using IrisCommon.Entities;
using VMS.Helpers;
using System.IO;
using IrisCommon.Helpers;
using VMS.Windows;
using System.Windows.Markup;
using System.Xml;
using System.Diagnostics;
using System.Windows.Threading;
using System.Drawing.Imaging;
using IrisDMApp.Helpers;
using System.Drawing;
using TwainDotNet;
using TwainDotNet.Wpf;
using Irisproxy;
using IrisDMApp.Windows;
using IrisDMApp;

namespace VMS.Controls
{
    /// <summary>
    /// Interaction logic for CreatePDFDocument.xaml
    /// </summary>
    public partial class CreatePDFDocument : UserControl
    {
        #region Constructor

        public CreatePDFDocument()
        {
            InitializeComponent();
        } 

        #endregion

        #region Variables

        List<Bitmap> images;
        public byte[] currentDoc;
        private Bitmap resultImage;
        private string selectedTag;
        private string newfolder;

        static string app_dir = ConfigurationManager.AppSettings["Server"].ToString();
        static string tempfolder = System.IO.Path.Combine(System.AppDomain.CurrentDomain.BaseDirectory, "TempScans");

        #endregion

        #region Operations

        private void btnClear_Click(object sender, RoutedEventArgs e)
        {
            ClearScannerStuff();
        }

        public void ClearScannerStuff()
        {
            if (images != null)
                images.Clear();
            btnClear.IsEnabled = false;
            btnClear.Opacity = 0.4;
            btnViewScan.IsEnabled = false;
            btnViewScan.Opacity = 0.4;
        }

        private bool VerifyData()
        {
            bool result = true;

            if (!Helper.CheckDatePicker(txtDateOfDoc))
                result = false;

            if (!Helper.CheckTextBox(txtDocName))
                result = false;

            if (txtSelectedFolder.Text == string.Empty)
            {
                MessageBox.Show("Please select a folder to upload to", "Document Folder not selected", MessageBoxButton.OK, MessageBoxImage.Information);
                return false;
            }

            if (images == null || images.Count == 0)
            {
                MessageBox.Show("You have not uploaded any files yet", "No uploads found", MessageBoxButton.OK, MessageBoxImage.Information);
                return false;
            }

            return result;
        }

        private void Clear()
        {
            //cmbLevels.SelectedIndex = -1;
            txtDateOfDoc.Text = string.Empty;
            txtDocName.Text = string.Empty;
            txtKeywords.Text = string.Empty;
            txtSelectedFolder.Text = string.Empty;
        }

        #endregion

        #region Events

        private void btnScanSave_Click(object sender, RoutedEventArgs e)
        {
            Scan();
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            if (System.ComponentModel.DesignerProperties.GetIsInDesignMode(this))
            {
                return;
            }

            _twain = new Twain(new WpfWindowMessageHook(PiggyBank.MainWindow));
            _twain.TransferImage += delegate(Object s, TransferImageEventArgs args)
            {
                if (args.Image != null)
                {
                    if (images == null)
                        images = new List<Bitmap>();
                    resultImage = args.Image;
                    images.Add(args.Image);

                }
            };

            _twain.ScanningComplete += delegate
            {
                IsEnabled = true;
            };

            cmbLevels.SelectedIndex = 2;
        }

        private void btnViewScan_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                List<string> sourceimages = new List<string>();

                DirectoryInfo dir = new DirectoryInfo(tempfolder);
                if (dir.GetFiles().Length != 0)
                {
                    foreach (FileInfo img in dir.GetFiles())
                    {
                        //var bitmap = new BitmapImage();
                        //bitmap.BeginInit();
                        //bitmap.UriSource = new System.Uri(img.FullName);
                        //bitmap.CacheOption = BitmapCacheOption.OnLoad;
                        //bitmap.EndInit();
                        //sourceimages.Add(bitmap);
                        sourceimages.Add(img.FullName);
                    }
                }

                PopupImage document = new PopupImage(sourceimages);
                document.ShowDialog();
            }
            catch (Exception ex)
            {
                LogError.LogTextError(ex);
                MessageBox.Show(ex.Message, "Error showing documents", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

        private void btnNewFolder_Click(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrEmpty(selectedTag))
            {
                PiggyBank.ShowPopupInfo("Please select the folder you want to create it in", 1);
            }
            else
            {
                EnterFolderWindow win = new EnterFolderWindow();
                win.ShowDialog();

                if (!string.IsNullOrEmpty(win.FolderName))
                {
                    if (!Directory.Exists(app_dir + selectedTag + "\\" + win.FolderName))
                    {
                        Directory.CreateDirectory(app_dir + selectedTag + "\\" + win.FolderName);
                        PiggyBank.ShowPopupInfo("Folder created successfully", 1);
                    }
                    else
                        PiggyBank.ShowPopupInfo("Folder already exists", 3);
                }
            }
        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (VerifyData())
                {
                    string docname = txtDocName.Text;
                    string folder = txtSelectedFolder.Text;
                    string doctype = string.Empty;
                    int savemsg = 0;
                    bool invalidimg = false;

                    if (images.Count != 1)
                    {
                        // Save PDF
                        PdfSettings PdfSettings = new PdfSettings(PdfOrientation.Portrait, PdfSize.Letter);
                        PdfConverter converter = new PdfConverter(PdfSettings);

                        converter.SaveFrom(images, app_dir + selectedTag + "\\" + docname + ".pdf");
                        doctype = "pdf";
                        savemsg = 1;
                    }
                    else
                    {
                        if (images.Count != 0)
                        {
                            //var imgguid = images[0].RawFormat.Guid;
                            //foreach (ImageCodecInfo codec in ImageCodecInfo.GetImageDecoders())
                            //{
                            //    if (codec.FormatID == imgguid)
                            //    {
                            //        doctype = codec.MimeType;

                            //        EncoderParameters encoderParams = null;
                            //        int quality = 100; // highest quality
                            //        EncoderParameter qualityParam = new EncoderParameter( 
                            //            System.Drawing.Imaging.Encoder.Quality, quality );
                            //        encoderParams = new EncoderParameters( 1 );
                            //        encoderParams.Param[0] = qualityParam;
                            //        images[0].Save(app_dir + selectedTag + "\\" + docname + "." + doctype, codec, encoderParams);
                            //    }
                            //}

                            if (ImageFormat.Jpeg.Equals(images[0].RawFormat))
                            {
                                // JPEG
                                images[0].Save(app_dir + selectedTag + "\\" + docname + ".jpg", ImageFormat.Jpeg);
                                doctype = "jpg";
                            }
                            else if (ImageFormat.Png.Equals(images[0].RawFormat))
                            {
                                // PNG
                                images[0].Save(app_dir + selectedTag + "\\" + docname + ".png", ImageFormat.Png);
                                doctype = "png";
                            }
                            else if (ImageFormat.Jpeg.Equals(images[0].RawFormat))
                            {
                                // JPEG
                                images[0].Save(app_dir + selectedTag + "\\" + docname + ".jpeg", ImageFormat.Gif);
                                doctype = "jpeg";
                            }
                            else
                            {
                                // Save PDF
                                PdfSettings PdfSettings = new PdfSettings(PdfOrientation.Portrait, PdfSize.Letter);
                                PdfConverter converter = new PdfConverter(PdfSettings);

                                converter.SaveFrom(images, app_dir + selectedTag + "\\" + docname + ".pdf");
                                doctype = "pdf";
                                savemsg = 1;
                            }

                            savemsg = 2;
                        }
                    }

                    if (invalidimg)
                    {
                        PiggyBank.ShowPopupInfo("Invalid image type", 3);
                    }
                    else
                    {
                        // Save document details to database
                        Document doc = new Document();
                        doc.DatePosted = DateTime.Now;
                        doc.DocDate = DateTime.Parse(txtDateOfDoc.Text);
                        doc.DocName = docname;
                        doc.DocType = doctype;
                        doc.Keywords = txtKeywords.Text;
                        doc.PhysicalLocation = txtPhysLocation.Text;
                        doc.Level = cmbLevels.Text;
                        doc.Location = selectedTag.Replace("\\", "/");
                        doc.User = new Person() { Person_Id = 8 };

                        try
                        {
                            int result = new ServiceProxy().Channel.SaveDocument(doc);
                            ClearScannerStuff();
                            Clear();

                            if (savemsg == 1)
                                PiggyBank.ShowPopupInfo("Files successfully saved", 1);

                            if (savemsg == 2)
                                PiggyBank.ShowPopupInfo("File successfully saved", 1);

                            Clear();
                        }
                        catch (Exception)
                        {
                            PiggyBank.ShowPopupInfo("Error saving document", 2);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

        #endregion

        #region TreeView Control

        void TreeView_Loaded(object sender, RoutedEventArgs e)
        {
            if (System.ComponentModel.DesignerProperties.GetIsInDesignMode(this))
            {
                return;
            }

            /// Create main expanded node of TreeView
            treeView.Items.Add(TreeView_CreateComputerItem());
            /// Update open directories every 5 second
            DispatcherTimer timer = new DispatcherTimer(TimeSpan.FromSeconds(5),
                DispatcherPriority.Background, TreeView_Update, Dispatcher);
        }

        void TreeView_Update(object sender, EventArgs e)
        {
            Stopwatch s = new Stopwatch();
            s.Start();
            /// Update drives and folders in Computer
            /// create copy for detect what item was expanded
            TreeView oldTreeView = CloneUsingXaml(treeView) as TreeView;
            /// populate items from scratch
            treeView.Items.Clear();
            /// add computer expanded node with all drives
            treeView.Items.Add(TreeView_CreateComputerItem());
            TreeViewItem newComputerItem = treeView.Items[0] as TreeViewItem;
            TreeViewItem oldComputerItem = oldTreeView.Items[0] as TreeViewItem;
            /// Save old state of item
            newComputerItem.IsExpanded = oldComputerItem.IsExpanded;
            newComputerItem.IsSelected = oldComputerItem.IsSelected;
            /// check all drives for creating it's root folders
            foreach (TreeViewItem newDrive in (treeView.Items[0] as TreeViewItem).Items)
                if (newDrive.Items.Contains(null))
                    /// Find relative old item for newDrive
                    foreach (TreeViewItem oldDrive in oldComputerItem.Items)
                        if (oldDrive.Tag as string == newDrive.Tag as string)
                        {
                            newDrive.IsSelected = oldDrive.IsSelected;
                            if (oldDrive.IsExpanded)
                            {
                                newDrive.Items.Clear();
                                TreeView_AddDirectoryItems(oldDrive, newDrive);
                            }
                            break;
                        }
            s.Stop();
        }

        void TreeView_AddDirectoryItems(TreeViewItem oldItem, TreeViewItem newItem)
        {
            newItem.IsExpanded = oldItem.IsExpanded;
            newItem.IsSelected = oldItem.IsSelected;
            /// add folders in this drive
            string[] directories = Directory.GetDirectories(newItem.Tag as string);
            /// for each folder create TreeViewItem
            foreach (string directory in directories)
            {
                TreeViewItem treeViewItem = new TreeViewItem();
                treeViewItem.Header = new DirectoryInfo(directory).Name;
                treeViewItem.Tag = directory;
                try
                {
                    if (Directory.GetDirectories(directory).Length > 0)
                        /// find respective old folder
                        foreach (TreeViewItem oldDir in oldItem.Items)
                            if (oldDir.Tag as string == directory)
                            {
                                if (oldDir.IsExpanded)
                                {
                                    TreeView_AddDirectoryItems(oldDir, treeViewItem);
                                }
                                else
                                {
                                    treeViewItem.Items.Add(null);
                                }
                                break;
                            }
                }
                catch { }
                treeViewItem.Expanded += TreeViewItem_Expanded;
                treeViewItem.PreviewMouseDown += TreeViewItem_Selected;
                newItem.Items.Add(treeViewItem);
            }
        }

        TreeViewItem TreeView_CreateComputerItem()
        {
            TreeViewItem computer = new TreeViewItem { Header = "Iris", IsExpanded = true };
            computer.Expanded += TreeViewItem_Expanded;
            computer.PreviewMouseDown += TreeViewItem_Selected;
                        
            try
            {
                foreach (string folder in Directory.GetDirectories(app_dir + "Iris"))
                {
                    TreeViewItem driveItem = new TreeViewItem();
                    driveItem.Header = System.IO.Path.GetFileName(folder);
                    driveItem.Tag = System.IO.Path.GetFullPath(folder);
                    if (Directory.GetDirectories(folder).Length > 0)
                        driveItem.Items.Add(null);
                    driveItem.Expanded += TreeViewItem_Expanded;
                    driveItem.PreviewMouseDown += TreeViewItem_Selected;
                    computer.Items.Add(driveItem);
                }
            }
            catch (Exception)
            {

            }

            return computer;
        }

        void TreeViewItem_Selected(object sender, RoutedEventArgs e)
        {
            TreeViewItem item = (TreeViewItem)sender;
            txtSelectedFolder.Text = item.Header.ToString();
            if (item.Tag != null)
                selectedTag = item.Tag.ToString().Substring(item.Tag.ToString().IndexOf("Iris\\"));
            btnNewFolder.IsEnabled = true;
            btnNewFolder.Opacity = 1;
        }

        void TreeViewItem_Expanded(object sender, RoutedEventArgs e)
        {
            TreeViewItem rootItem = (TreeViewItem)sender;

            if (rootItem.Items.Count == 1 && rootItem.Items[0] == null)
            {
                rootItem.Items.Clear();

                string[] dirs;
                try
                {
                    dirs = Directory.GetDirectories((string)rootItem.Tag);
                }
                catch
                {
                    return;
                }

                foreach (var dir in dirs)
                {
                    TreeViewItem subItem = new TreeViewItem();
                    subItem.Header = new DirectoryInfo(dir).Name;
                    subItem.Tag = dir;
                    try
                    {
                        if (Directory.GetDirectories(dir).Length > 0)
                            subItem.Items.Add(null);
                    }
                    catch { }
                    subItem.Expanded += TreeViewItem_Expanded;
                    subItem.PreviewMouseDown += TreeViewItem_Selected;
                    rootItem.Items.Add(subItem);
                }
            }
        }

        object CloneUsingXaml(object obj)
        {
            string xaml = XamlWriter.Save(obj);
            return XamlReader.Load(new XmlTextReader(new StringReader(xaml)));
        }

        #endregion

        #region Twain

        private Twain _twain;
        private ScanSettings _settings;

        private void OnSelectSourceButtonClick(object sender, RoutedEventArgs e)
        {
            _twain.SelectSource();
        }

        private void Scan()
        {
            IsEnabled = false;

            _settings = new ScanSettings
            {
                ShowTwainUI = (bool)btnShowGui.IsChecked,
                ShowProgressIndicatorUI = true
            };

            try
            {
                _twain.StartScanning(_settings);
                btnViewScan.IsEnabled = true;
                btnViewScan.Opacity = 1;
                btnClear.IsEnabled = true;
                btnClear.Opacity = 1;
            }
            catch (TwainException ex)
            {
                MessageBox.Show(ex.Message);
            }

            IsEnabled = true;
        }

        #endregion


        
    }
}
