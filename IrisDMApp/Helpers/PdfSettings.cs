﻿namespace IrisDMApp.Helpers
{
    public enum PdfOrientation
    {
        Portrait,
        Landscape
    }

    public enum PdfSize
    {
        Letter,
        Legal
    }

    public class PdfSettings
    {
        #region Fields

        /// <summary>
        /// width for pdf export in inches
        /// </summary>
        private double _width = 8.5;

        /// <summary>
        /// height for pdf export in inches
        /// </summary>
        private double _height = 11;

        /// <summary>
        /// Left margin.
        /// by default we set it to 0.5 inches
        /// </summary>
        private double _leftMargin = 0.5;

        /// <summary>
        /// Right margin.
        /// by default we set it to 0.5 inches
        /// </summary>
        private double _rightMargin = 0.5;

        /// <summary>
        /// Top margin.
        /// by default we set it to 0.5 inches
        /// </summary>
        private double _topMargin = 0.5;

        /// <summary>
        /// Bottom margin.
        /// by default we set it to 0.5 inches
        /// </summary>
        private double _bottomMargin = 0.5;

        /// <summary>
        /// Size of PDF output pages. 
        /// </summary>
        private PdfSize _size;

        /// <summary>
        /// Orientation of PDF output pages. 
        /// </summary>
        private PdfOrientation _orientation;

        #endregion //Fields

        #region Constructors

        /// <summary>
        /// Constructor which creates custom settings. 
        /// Margins can be set separate in Properties. 
        /// Default margins are 0.5 in. 
        /// </summary>
        /// <param name="width">Width in inches.</param>
        /// <param name="height">Height in inches.</param>
        public PdfSettings(double width, double height)
        {
            Width = width;
            Height = height;
        }

        /// <summary>
        /// Constructor accepts standard settings. 
        /// </summary>
        /// <param name="orientation">Page orientation.</param>
        /// <param name="size">Page size. </param>
        public PdfSettings(PdfOrientation orientation, PdfSize size)
        {
            Orientation = orientation;
            Size = size;
        }

        /// <summary>
        /// Default Constructor
        /// </summary>
        public PdfSettings()
        {
        }

        #endregion //Constructors

        #region Properties


        /// <summary>
        /// Bottom margin.
        /// by default it is set to 0.5 inches.
        /// If bottom and top margins are greater then height, then they are set to default.
        /// </summary>
        public double BottomMargin
        {
            get
            {
                return _bottomMargin;
            }
            set
            {
                if (value + _topMargin > Height)
                {
                    _bottomMargin = 0.5;
                    _topMargin = 0.5;
                }
                else
                    _bottomMargin = value;
            }
        }

        /// <summary>
        /// Formatted label for Margin value;
        /// </summary>
        public string BottomMarginLabel
        {
            get { return BottomMargin.ToString("00.00") + "in"; }
        }

        /// <summary>
        /// height for pdf export in inches
        /// </summary>
        public double Height
        {
            get
            {
                return _height;
            }
            set
            {
                _height = value;
            }
        }

        /// <summary>
        /// Left margin.
        /// by default it is set to 0.5 inches.
        /// If Right and Left margins are greater then width, then they are set to default.
        /// </summary>
        public double LeftMargin
        {
            get
            {
                return _leftMargin;
            }
            set
            {
                if (value + _rightMargin > Width)
                {
                    _leftMargin = 0.5;
                    _rightMargin = 0.5;
                }
                else
                    _leftMargin = value;
            }
        }

        /// <summary>
        /// Formatted label for Margin value;
        /// </summary>
        public string LeftMarginLabel
        {
            get { return LeftMargin.ToString("00.00") + "in"; }
        }

        /// <summary>
        /// Orientation of PDF output pages. 
        /// </summary>
        public PdfOrientation Orientation
        {
            get
            {
                return _orientation;
            }
            set
            {
                _orientation = value;

                if (_orientation == PdfOrientation.Landscape)
                {
                    if (Size == PdfSize.Legal)
                    {
                        Height = 8.5;
                        Width = 14;
                    }
                    else if (Size == PdfSize.Letter)
                    {
                        Height = 8.5;
                        Width = 11;
                    }
                }
                else
                {
                    if (Size == PdfSize.Legal)
                    {
                        Height = 14;
                        Width = 8.5;
                    }
                    else if (Size == PdfSize.Letter)
                    {
                        Height = 11;
                        Width = 8.5;
                    }
                }
            }
        }

        /// <summary>
        /// Right margin.
        /// by default it is set to 0.5 inches.
        /// If Right and Left margins are greater then width, then they are set to default.
        /// </summary>
        public double RightMargin
        {
            get
            {
                return _rightMargin;
            }
            set
            {
                if (value + _leftMargin > Width)
                {
                    _leftMargin = 0.5;
                    _rightMargin = 0.5;
                }
                else
                    _rightMargin = value;
            }
        }

        /// <summary>
        /// Formatted label for Margin value;
        /// </summary>
        public string RightMarginLabel
        {
            get { return RightMargin.ToString("00.00") + "in"; }
        }

        /// <summary>
        /// Size of PDF output pages. 
        /// </summary>
        public PdfSize Size
        {
            get
            {
                return _size;
            }
            set
            {
                _size = value;

                if (_orientation == PdfOrientation.Landscape)
                {
                    if (Size == PdfSize.Legal)
                    {
                        Height = 8.5;
                        Width = 14;
                    }
                    else if (Size == PdfSize.Letter)
                    {
                        Height = 8.5;
                        Width = 11;
                    }
                }
                else
                {
                    if (Size == PdfSize.Legal)
                    {
                        Height = 14;
                        Width = 8.5;
                    }
                    else if (Size == PdfSize.Letter)
                    {
                        Height = 11;
                        Width = 8.5;
                    }
                }
            }
        }

        /// <summary>
        /// Top margin.
        /// by default it is set to 0.5 inches.
        /// If bottom and top margins are greater then height, then they are set to default.
        /// </summary>
        public double TopMargin
        {
            get
            {
                return _topMargin;
            }
            set
            {
                if (value + _bottomMargin > Height)
                {
                    _bottomMargin = 0.5;
                    _topMargin = 0.5;
                }
                else
                    _topMargin = value;
            }
        }

        /// <summary>
        /// Formatted label for Margin value;
        /// </summary>
        public string TopMarginLabel
        {
            get { return TopMargin.ToString("00.00") + "in"; }
        }

        /// <summary>
        /// width for pdf export in inches
        /// </summary>
        public double Width
        {
            get
            {
                return _width;
            }
            set
            {
                _width = value;
            }
        }

        /// <summary>
        /// width for pdf export in inches
        /// </summary>
        public string WidthLabel
        {
            get
            {
                return Width.ToString("00.00") + "in";
            }
        }

        /// <summary>
        /// height for pdf export in inches
        /// </summary>
        public string HeightLabel
        {
            get
            {
                return Height.ToString("00.00") + "in";
            }
        }

        /// <summary>
        /// Returns formatted height of the pdf body = (height - top and bottom margins).
        /// </summary>
        public string BodyHeightLabel
        {
            get { return (Height - _topMargin - _bottomMargin).ToString("00.00") + "in"; }
        }

        /// <summary>
        /// Returns height of the pdf body = (height - top and bottom margins).
        /// </summary>
        public double BodyHeight
        {
            get { return (Height - _topMargin - _bottomMargin); }
        }

        /// <summary>
        /// Returns formatted width of the pdf body = (width - left and right margins).
        /// </summary>
        public string BodyWidthLabel
        {
            get { return (Width - _leftMargin - _rightMargin).ToString("00.00") + "in"; }
        }

        /// <summary>
        /// Returns width of the pdf body = (width - left and right margins).
        /// </summary>
        public double BodyWidth
        {
            get { return (Width - _leftMargin - _rightMargin); }
        }


        #endregion //Properties
    }
}