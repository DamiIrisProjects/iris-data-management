﻿using System;
using System.Runtime.Serialization;
using System.Security.Permissions;

namespace IrisDMApp.Helpers
{
    public enum ScanningSize
    {
        Letter,
        Legal
    }

    public enum Color
    {
        GrayScale = 0,
        Color = 1,
        BlackWhite = 4
    }

    public class ScannerSettings : ISerializable
    {
        #region Fields
        /// <summary>
        /// Width of the scanned image.
        /// Default is 8500. 
        /// </summary>
        private double _width = 8.5;

        /// <summary>
        /// Height of the scanned image.
        /// Default is 11000.
        /// </summary>
        private double _height = 11;

        /// <summary>
        /// Optical Resolution.
        /// </summary>
        private int _resolution = 150;

        /// <summary>
        /// Color setting, default is 0 - grayscale. 
        /// </summary>
        private int _color = 1; // grayscale

        /// <summary>
        /// Horizontal Cropping. 
        /// Default is 0.5 in
        /// </summary>
        private double _horizontalCrop = 0.5; // if cropping required it can be set in here for horizontal.

        /// <summary>
        /// Vertical Cropping
        /// Default is 0.5 in
        /// </summary>
        private double _verticalCrop = 0.5; // if cropping required it can be set in here for vertical.

        /// <summary>
        /// Standard scanning size
        /// </summary>
        private ScanningSize _size = ScanningSize.Letter;

        #endregion //Fields

        #region Constructors

        /// <summary>
        /// Default Constructor
        /// </summary>
        public ScannerSettings()
        {

        }

        /// <summary>
        /// Creates settings object for WIA scanner. 
        /// </summary>
        /// <param name="size">Standard paper size.</param>
        /// <param name="resolution">scanning resolution (e.g. for 300x300 pass 300).</param>
        /// <param name="color">Color setting, default is gray scale.</param>
        public ScannerSettings(ScanningSize size, int resolution, Color color)
        {
            Size = size;
            _color = (int)color;
            _resolution = resolution;
        }

        /// <summary>
        /// Creates settings object for WIA scanner and resolution of 150x150 pixels. 
        /// </summary>
        /// <param name="size">Standard paper size.</param>
        public ScannerSettings(ScanningSize size)
        {
            Size = size;
        }

        /// <summary>
        /// Creates customized settings for WIA scanner
        /// </summary>
        /// <param name="width">Scanner's sheet feed width.</param>
        /// <param name="height">Scanner's sheet feed height.</param>
        /// <param name="resolution">Optical resolution.</param>
        public ScannerSettings(double width, double height, int resolution, double horizontalCrop, double verticalCrop)
        {
            Width = width;
            Height = height;
            _resolution = resolution;
            _horizontalCrop = horizontalCrop;
            _verticalCrop = verticalCrop;
        }


        #endregion //Constructors

        #region Properties

        /// <summary>
        /// Color setting, default is 0 - grayscale. 
        /// </summary>
        public int Color
        {
            get
            {
                return _color;
            }
            set { _color = value; }
        }

        /// <summary>
        /// Height of the scanned image.
        /// Default is 11 in.
        /// </summary>
        public double Height
        {
            get
            {
                return _height;
            }
            set
            {
                _height = value;
            }
        }

        /// <summary>
        /// Horizontal Cropping. 
        /// Default is 0.5 in.
        /// </summary>
        public double HorizontalCrop
        {
            get
            {
                return _horizontalCrop;
            }
            set
            {
                _horizontalCrop = value;
            }
        }

        /// <summary>
        /// Optical Resolution. Default is 120 pixels per inch.
        /// </summary>
        public int Resolution
        {
            get
            {
                return _resolution;
            }
            set
            {
                _resolution = value;
            }
        }

        /// <summary>
        /// Standard scanning size
        /// </summary>
        public ScanningSize Size
        {
            get
            {
                return _size;
            }
            set
            {
                _size = value;
                Width = GetWidth(_size);
                Height = GetHeight(_size);
            }
        }

        /// <summary>
        /// Vertical Cropping
        /// Default is 0.5 in.
        /// </summary>
        public double VerticalCrop
        {
            get
            {
                return _verticalCrop;
            }
            set
            {
                _verticalCrop = value;
            }
        }

        /// <summary>
        /// Width of the scanned image.
        /// Default is 8.5 in. 
        /// </summary>
        public double Width
        {
            get
            {
                return _width;
            }
            set
            {
                _width = value;
            }
        }
        #endregion //Properties

        #region Methods
        public static double GetWidth(ScanningSize size)
        {
            if (size == ScanningSize.Legal)
            {
                return 8.5;
            }
            else if (size == ScanningSize.Letter)
            {
                return 8.5;
            }
            else return 8.5;
        }

        public static double GetHeight(ScanningSize size)
        {
            if (size == ScanningSize.Legal)
            {
                return 14;
            }
            else if (size == ScanningSize.Letter)
            {
                return 11;
            }
            else return 11;
        }
        #endregion //Methods

        #region ISerializable Members

        /// <summary>
        /// Constructor for serializer.
        /// </summary>
        /// <param name="info">Serialization data.</param>
        /// <param name="context">Serialization streaming context.</param>
        public ScannerSettings(SerializationInfo info, StreamingContext context)
        {

            Color = info.GetInt32("Color");
            Height = info.GetDouble("Height");
            HorizontalCrop = info.GetDouble("HorizontalCrop");
            Resolution = info.GetInt32("Resolution");
            Nullable<ScanningSize> size = info.GetValue("Size", typeof(Nullable<ScanningSize>)) as Nullable<ScanningSize>;
            Size = ((size != null) ? size.Value : ScanningSize.Letter);
            VerticalCrop = info.GetDouble("VerticalCrop");
            Width = info.GetDouble("Width");
        }

        /// <summary>
        /// Implementation for ISerializable. 
        /// </summary>
        /// <param name="info">Serialization data.</param>
        /// <param name="context">Serialization streaming context.</param>
        [SecurityPermission(SecurityAction.LinkDemand, Flags = SecurityPermissionFlag.SerializationFormatter)]
        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("Color", Color);
            info.AddValue("Height", Height);
            info.AddValue("HorizontalCrop", HorizontalCrop);
            info.AddValue("Resolution", Resolution);
            Nullable<ScanningSize> size = Size;
            info.AddValue("Size", size);
            info.AddValue("VerticalCrop", VerticalCrop);
            info.AddValue("Width", Width);

        }

        #endregion
    }
}