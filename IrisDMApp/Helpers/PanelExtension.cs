﻿using System.Windows.Controls;
using System.Windows;
using System.Windows.Media;

namespace VMS.Helpers
{
    public class PanelExtension : StackPanel
    {
        public bool IsReadOnly
        {
            get { return (bool)GetValue(IsReadOnlyProperty); }
            set { SetValue(IsReadOnlyProperty, value); }
        }
        public static readonly DependencyProperty IsReadOnlyProperty =
            DependencyProperty.Register("IsReadOnly", typeof(bool), typeof(PanelExtension),
            new PropertyMetadata(new PropertyChangedCallback(OnIsReadOnlyChanged)));

        private static void OnIsReadOnlyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            ((PanelExtension)d).OnIsReadOnlyChanged(e);
        }

        protected virtual void OnIsReadOnlyChanged(DependencyPropertyChangedEventArgs e)
        {
            SetIsEnabledOfChildren(this);
        }

        public PanelExtension()
        {
            this.Loaded += new RoutedEventHandler(PanelExtension_Loaded);
        }

        void PanelExtension_Loaded(object sender, RoutedEventArgs e)
        {
            this.SetIsEnabledOfChildren(this);
        }

        private void SetIsEnabledOfChildren(Visual control)
        {
            int ChildNumber = VisualTreeHelper.GetChildrenCount(control);

            for (int i = 0; i <= ChildNumber - 1; i++)
            {
                Visual v = (Visual)VisualTreeHelper.GetChild(control, i);

                if (v is TextBox)
                {
                    (v as TextBox).IsReadOnly = IsReadOnly;
                    (v as TextBox).Opacity = 1;
                }

                if (v is RadioButton)
                {
                    (v as RadioButton).IsEnabled = !IsReadOnly;
                    (v as RadioButton).Opacity = 1;
                }

                if (v is DatePicker)
                {
                    (v as DatePicker).IsEnabled = !IsReadOnly;
                    (v as DatePicker).Opacity = 1;
                }

                if (v is ComboBox)
                {
                    (v as ComboBox).IsEnabled = !IsReadOnly;
                    (v as ComboBox).Opacity = 1;
                }

                if (v is DataGrid)
                {
                    (v as DataGrid).IsReadOnly = IsReadOnly;
                }

                if (VisualTreeHelper.GetChildrenCount(v) > 0)
                {
                    SetIsEnabledOfChildren(v);
                }
            }
        }
    }
}
