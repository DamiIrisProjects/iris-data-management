﻿using System.Collections.Generic;
using IrisCommon.Entities;
using VMS.Windows;

namespace VMS.Helpers
{
    public static class PiggyBank
    {
        public static Person CurrentOperator { get; set; }

        public static List<Person> OperatorsList { get; set; }

        public static WPFTwain.MainWindow ScannerWindow
        {
            get;
            set;
        }

        public static IrisDMApp.MainWindow MainWindow { get; set; }

        public static void ShowPopupInfo(string msg, int color)
        {
            PopupInfo info = new PopupInfo(msg, color);
            info.Show();
        }
    }
}
