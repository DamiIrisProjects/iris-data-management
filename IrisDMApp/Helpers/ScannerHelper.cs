﻿using System.IO;

namespace IrisDMApp.Helpers
{
    public class ScannerHelper
    {
        public ScannerSettings ScannerSettings { get; set; }
        public PdfSettings PdfSettings { get; set; }

        /// <summary>
        /// Constructor with default settings. 
        /// </summary>
        public ScannerHelper()
        {
            ScannerSettings = new ScannerSettings(ScanningSize.Letter, 250, Color.Color);
            PdfSettings = new PdfSettings(PdfOrientation.Portrait, PdfSize.Letter);
        }

        /// <summary>
        /// Customizable constructor
        /// </summary>
        /// <param name="scannerSettings">Settings for the scanner.</param>
        /// <param name="pdfSettings">Settings for PDF page(s).</param>
        public ScannerHelper(ScannerSettings scannerSettings, PdfSettings pdfSettings)
        {
            ScannerSettings = scannerSettings;
            PdfSettings = pdfSettings;
        }

        public void ScanToPdf(string newPdfFileName)
        {
            ImageScanner scanner = new ImageScanner(ScannerSettings);
            PdfConverter converter = new PdfConverter(PdfSettings);

            converter.SaveFrom(scanner.Scan(), newPdfFileName);
        }

        /// <summary>
        /// This method scans to Pdf Stream. 
        /// </summary>
        /// <returns>Return open stream with PDF binary data in it. The stream needs to be flushed and closed after use.</returns>
        public Stream ScanToPdf()
        {
            ImageScanner scanner = new ImageScanner(ScannerSettings);
            PdfConverter converter = new PdfConverter(PdfSettings);

            return converter.ConvertFrom(scanner.Scan());
        }
    }
}
