﻿using System;
using System.Collections.Generic;
using System.Linq;
using WIA;
using System.Drawing;
using System.IO;
using System.Security.Permissions;
using System.Security.Principal;
using IrisDMApp.Properties;

[assembly: System.Security.Permissions.FileIOPermission(SecurityAction.RequestMinimum, Unrestricted = true)]
namespace IrisDMApp.Helpers
{
    public class ImageScanner
    {
        #region Fields
        private Device _device = null;
        private ScannerSettings _scannerSettings;
        #endregion //Fields

        #region Constructors

        /// <summary>
        /// 
        /// </summary>
        /// <exception cref="ImageScannerException">This exception can be thrown,
        /// if any errors are present while initializing scanner.</exception>
        public ImageScanner(ScannerSettings scannerSettings)
        {
            AppDomain.CurrentDomain.SetPrincipalPolicy(PrincipalPolicy.WindowsPrincipal);

            if (scannerSettings == null)
                throw new ImageScannerException("Scanner Settings are not specified!");

            _scannerSettings = scannerSettings;

            Settings settings = new Settings();
            try
            {
                // try automatically select scanner.
                if (!String.IsNullOrEmpty(settings.DeviceId))
                {
                    _device = GetDevice(settings.DeviceId);
                }

                // if didn't succeed try manually select scanner.
                if (_device == null)
                {
                    _device = GetDevice(settings);
                }

            }
            catch (Exception ex)
            {
                throw new ImageScannerException("Scanner was not selected or is not available!\n" + ex.Message);
            }

            // if device is still null, then throw an error. 
            if (_device == null)
                throw new ImageScannerException("Scanner was not selected or is not available!");

        }

        #endregion //Constructors

        #region Public Methods

        public void SetDevice()
        {
            Settings settings = new Settings();
            _device = GetDevice(settings);
        }

        /// <summary>
        /// This will scan images, but everything must be ready before doing this. 
        /// If device (scanner) is not ready an ImageScannerException will be thrown. 
        /// </summary>
        /// <returns>Collection of scanned images.</returns>
        public IEnumerable<Bitmap> Scan()
        {
            List<Bitmap> images = new List<Bitmap>();

            if (_device == null)
                throw new ImageScannerException("Scanner is not available! Please select a scanner and try again.");

            WIA.Item item = null;
            try
            {
                foreach (DeviceCommand command in _device.Commands)
                {
                    item = _device.ExecuteCommand(command.CommandID);
                }
            }
            catch (Exception ex)
            {
                // skip this
            }

            try
            {
                // if item is still not initialized, we'll try a different approach
                if (item == null)
                {
                    foreach (Item i in _device.Items)
                    {
                        foreach (DeviceCommand command in i.Commands)
                        {
                            item = _device.ExecuteCommand(command.CommandID);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                // skip this
            }

            try
            {
                // if item is still null, we'll pick the first available
                foreach (WIA.Item i in _device.Items)
                {
                    item = i;
                    break;
                }
            }
            catch (Exception ex)
            {
                //skip this
            }

            if (item == null)
                throw new ImageScannerException("Scanner is not ready!\nPlease turn scanner on, feed paper into the scanner and try again.");
            try
            {
                // setting properties (dimensions and resolution of the scanning.)
                setItem(item, "4106", item.Formats[1]);
                setItem(item, "6146", _scannerSettings.Color); // color setting (default is gray scale)
                setItem(item, "6147", _scannerSettings.Resolution); //horizontal resolution
                setItem(item, "6148", _scannerSettings.Resolution); // vertical resolution
                setItem(item, "6149", _scannerSettings.HorizontalCrop); // horizontal starting position
                setItem(item, "6150", _scannerSettings.VerticalCrop); // vertical starting position
                setItem(item, "6151", (int)((double)_scannerSettings.Resolution * (_scannerSettings.Width - _scannerSettings.HorizontalCrop)));  // width
                setItem(item, "6152", (int)((double)_scannerSettings.Resolution * (_scannerSettings.Height - _scannerSettings.VerticalCrop))); // height
            }
            catch (Exception ex)
            {
                throw new ImageScannerException("Was not able to set scanning parameters.\n" + ex.Message);
            }

            // if we reached this point, then scanner is probably initialized. 
            bool isTransferring = true;
            foreach (string format in item.Formats)
            {
                while (isTransferring)
                {
                    try
                    {
                        WIA.ImageFile file = (item.Transfer(format)) as WIA.ImageFile;
                        if (file != null)
                        {
                            Stream stream = new MemoryStream();
                            stream.Write(file.FileData.get_BinaryData() as Byte[], 0, (file.FileData.get_BinaryData() as Byte[]).Length);

                            // resetting stream position to beginning after data was written into it. 
                            stream.Position = 0;
                            Bitmap bitmap = new Bitmap(stream);
                            images.Add(bitmap);
                        }
                        else
                            isTransferring = false; // something happend and we didn't get image
                    }
                    catch (Exception ex)
                    {
                        // most likely done transferring
                        // I was not able to find a way to pole scanner for paper feed status. 
                        isTransferring = false;

                        // scanner's paper feeder was not loaded with paper.
                        if (images.Count() == 0)
                            throw new ImageScannerException("Scanner is not loaded with paper or not ready.");

                    }
                }
            }

            return images;
        }

        #endregion //Public Methods

        #region Private Methods

        private void setItem(IItem item, object property, object value)
        {
            WIA.Property aProperty = item.Properties.get_Item(ref property);
            aProperty.set_Value(ref value);
        }

        private Device GetDevice(Settings settings)
        {
            Device device = null;
            CommonDialogClass dialog = new CommonDialogClass();
            if (String.IsNullOrEmpty(settings.DeviceId))
            {
                device = dialog.ShowSelectDevice(WiaDeviceType.ScannerDeviceType, true, false);
                if (device != null)
                {
                    settings.DeviceId = device.DeviceID;
                    settings.Save();
                }
            }
            return device;
        }

        private Device GetDevice(string deviceId)
        {
            WIA.DeviceManager manager = new DeviceManager();
            Device device = null;
            foreach (DeviceInfo info in manager.DeviceInfos)
            {
                if (info.DeviceID == deviceId)
                {
                    device = info.Connect();
                    break;
                }
            }
            return device;
        }
        #endregion //Private Methods
    }
}