﻿using System;
using System.Runtime.Serialization;
using System.Security.Permissions;

namespace IrisDMApp.Helpers
{
    public class PdfConverterException : Exception, ISerializable
    {
        #region Properties

        public string UserFriendlyMessage { get; private set; }

        #endregion //Properties

        #region Constructors

        /// <summary>
        /// All messages are set to String.Empty.
        /// </summary>
        public PdfConverterException()
            : base(String.Empty)
        {
            UserFriendlyMessage = String.Empty;
        }

        /// <summary>
        /// UserFriendlyMessage is set to message.
        /// </summary>
        /// <param name="message">Error message.</param>
        public PdfConverterException(string message)
            : base(message)
        {
            UserFriendlyMessage = message;
        }

        /// <summary>
        /// UserFriendlyMessage is set to message. 
        /// </summary>
        /// <param name="message">Error Message</param>
        /// <param name="innerException">Inner Exception if any, null otherwise. </param>
        public PdfConverterException(string message, Exception innerException)
            : base(message, innerException)
        {
            UserFriendlyMessage = message;
        }

        /// <summary>
        /// Custom exception for PdfConverter.
        /// </summary>
        /// <param name="message">Detailed error message.</param>
        /// <param name="userFriendlyMessage">User friendly error message.</param>
        public PdfConverterException(string message, string userFriendlyMessage)
            : base(message)
        {
            UserFriendlyMessage = userFriendlyMessage;
        }

        /// <summary>
        /// Custom exception for PdfConverter.
        /// </summary>
        /// <param name="message">Detailed error message.</param>
        /// <param name="innerException">Inner Exception if any, null otherwise. 
        /// (If inner exception is null use different overloaded constructor)</param>
        /// <param name="userFriendlyMessage">User friendly error message.</param>
        public PdfConverterException(string message, Exception innerException, string userFriendlyMessage)
            : base(message, innerException)
        {
            UserFriendlyMessage = userFriendlyMessage;
        }

        /// <summary>
        /// Custom exception for PdfConverter.
        /// </summary>
        /// <param name="info">Serialization data.</param>
        /// <param name="context">Serialization streaming context.</param>
        public PdfConverterException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
            UserFriendlyMessage = info.GetString("UserFriendlyMessage");
        }

        #endregion //Constructors

        #region Methods

        /// <summary>
        /// Implementation for ISerializable. 
        /// </summary>
        /// <param name="info">Serialization data.</param>
        /// <param name="context">Serialization streaming context.</param>
        [SecurityPermission(SecurityAction.LinkDemand, Flags = SecurityPermissionFlag.SerializationFormatter)]
        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            base.GetObjectData(info, context);
            info.AddValue("UserFriendlyMessage", UserFriendlyMessage);
        }

        #endregion //Methods

    }
}