﻿using System;
using System.Windows.Data;
using System.Globalization;

namespace VMS.Helpers
{
    public class DateTimeFormatConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if ((DateTime)value == DateTime.MinValue)
                return string.Empty;
            else
            {
                if (!string.IsNullOrEmpty((string)parameter))
                {
                    if ((string)parameter == "2")
                        return ((DateTime)value).ToString("hh:mm tt");
                    if ((string)parameter == "1")
                        return ((DateTime)value).ToString("dd-MMM-yyyy");
                }


                return ((DateTime)value).ToString("dd-MMM-yyyy hh:mm tt");
            }
        }


        public object ConvertBack(object value, System.Type targetType, object parameter, CultureInfo culture)
        {
            throw new System.NotImplementedException();
        }
    }
}
