﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using IrisCommon.Entities;
using VMS.Helpers;
using System.IO;
using IrisCommon.Helpers;
using VMS.Windows;
using System.Windows.Markup;
using System.Xml;
using System.Diagnostics;
using System.Windows.Threading;
using System.Drawing.Imaging;
using IrisDMApp.Helpers;
using System.Drawing;
using TwainDotNet;
using TwainDotNet.Wpf;
using Irisproxy;
using IrisDMApp.Windows;

namespace IrisDMApp
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        #region Variables

        public byte[] currentDoc;
        private Bitmap resultImage;
        private string selectedTag;
        private string newfolder;
        static string tempfolder = System.IO.Path.Combine(System.AppDomain.CurrentDomain.BaseDirectory, "TempScans");

        #endregion

        #region Constructor

        public MainWindow()
        {
            try
            {
                InitializeComponent();
                PiggyBank.MainWindow = this;

                // Clear temp folder
                if (Directory.Exists(tempfolder))
                    foreach (System.IO.FileInfo file in new DirectoryInfo(tempfolder).GetFiles()) file.Delete();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error starting App", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        } 

        #endregion

        #region Operations

        private void btnClear_Click(object sender, RoutedEventArgs e)
        {
            ClearScannerStuff();
        }

        public void ClearScannerStuff()
        {                  
            // Clear temp folder
            if (Directory.Exists(tempfolder))
            {
                try
                {
                    //////////// need to do this for some reason

                    List<string> sourceimages = new List<string>();

                    DirectoryInfo dir = new DirectoryInfo(tempfolder);
                    if (dir.GetFiles().Length != 0)
                    {
                        foreach (FileInfo img in dir.GetFiles())
                        {
                            sourceimages.Add(img.FullName);
                        }
                    }

                    PopupImage document = new PopupImage(sourceimages);
                    document.Visibility = System.Windows.Visibility.Hidden;
                    document.Show();
                    document.Close();

                    //////////////////////

                    //foreach (System.IO.FileInfo file in new DirectoryInfo(tempfolder).GetFiles()) file.Delete();

                    btnClear.IsEnabled = false;
                    btnClear.Opacity = 0.4;
                    btnViewScan.IsEnabled = false;
                    btnViewScan.Opacity = 0.4;
                }
                catch(Exception ex)
                {
                    LogError.LogTextError(ex);
                    MessageBox.Show(ex.Message, "Error clearing documents", MessageBoxButton.OK, MessageBoxImage.Information);
                }
            }
        }

        private bool VerifyData()
        {
            bool result = true;

            if (!Helper.CheckDatePicker(txtDateOfDoc))
            {
                MessageBox.Show("Please enter a date for the document", "No date entered", MessageBoxButton.OK, MessageBoxImage.Information);
                return false;
            }

            if (!Helper.CheckTextBox(txtDocName))
            {
                MessageBox.Show("Please give your document a name", "No name entered", MessageBoxButton.OK, MessageBoxImage.Information);
                return false;
            }

            if (txtSelectedFolder.Text == string.Empty)
            {
                MessageBox.Show("Please select a folder to upload to", "Document Folder not selected", MessageBoxButton.OK, MessageBoxImage.Information);
                return false;
            }

            if (!Directory.Exists(tempfolder) || new DirectoryInfo(tempfolder).GetFiles().Length == 0)
            {
                MessageBox.Show("You have not uploaded any files yet", "No uploads found", MessageBoxButton.OK, MessageBoxImage.Information);
                return false;
            }

            return result;
        }

        private void Clear()
        {
            //cmbLevels.SelectedIndex = -1;
            txtDateOfDoc.Text = string.Empty;
            txtDocName.Text = string.Empty;
            txtKeywords.Text = string.Empty;
            txtSelectedFolder.Text = string.Empty;
        }

        #endregion

        #region Events

        private void btnScanSave_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Scan();
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message, "Scanner Error", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            
        }

        private void btnViewScan_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                List<string> sourceimages = new List<string>();

                DirectoryInfo dir = new DirectoryInfo(tempfolder);
                if (dir.GetFiles().Length != 0)
                {
                    foreach (FileInfo img in dir.GetFiles())
                    {
                        sourceimages.Add(img.FullName);

                        //IntPtr hbitmap = new Bitmap(img.FullName).GetHbitmap();
                        //sourceimages.Add(Imaging.CreateBitmapSourceFromHBitmap(
                        //            hbitmap,
                        //            IntPtr.Zero,
                        //            Int32Rect.Empty,
                        //            BitmapSizeOptions.FromEmptyOptions()));
                        //Gdi32Native.DeleteObject(hbitmap);
                    }
                }

                PopupImage document = new PopupImage(sourceimages);
                document.ShowDialog();
            }
            catch (Exception ex)
            {
                LogError.LogTextError(ex);
                MessageBox.Show(ex.Message, "Error showing documents", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

        private void btnNewFolder_Click(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrEmpty(selectedTag))
            {
                PiggyBank.ShowPopupInfo("Please select the folder you want to create it in", 1);
            }
            else
            {
                EnterFolderWindow win = new EnterFolderWindow();
                win.ShowDialog();

                ServiceProxy proxy = new ServiceProxy();
                if (!string.IsNullOrEmpty(win.FolderName))
                {
                    string path = System.IO.Path.Combine(selectedTag, win.FolderName);
                    string response = proxy.Channel.CreateFolder(path);
                    PiggyBank.ShowPopupInfo(response, 1);
                }
            }
        }
               
        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (VerifyData())
                {
                    string docname = txtDocName.Text;
                    string folder = txtSelectedFolder.Text;
                    string doctype = string.Empty;
                    int savemsg = 0;
                    bool invalidimg = false;

                    ServiceProxy proxy = new ServiceProxy();
                    DirectoryInfo dir = new DirectoryInfo(tempfolder);
                    if (dir.GetFiles().Length != 1)
                    {
                        // Save PDF
                        PdfSettings PdfSettings = new PdfSettings(PdfOrientation.Portrait, PdfSize.Letter);
                        PdfConverter converter = new PdfConverter(PdfSettings);

                        List<Bitmap> images = new List<Bitmap>();

                        // Create List of images
                        foreach (FileInfo img in dir.GetFiles())
                        {
                            images.Add((Bitmap)Bitmap.FromFile(img.FullName));
                        }

                        converter.SaveFrom(images, System.IO.Path.Combine(tempfolder, docname + ".pdf"));

                        // transfer to server
                        byte[] file = File.ReadAllBytes(System.IO.Path.Combine(tempfolder, docname + ".pdf"));
                        int result = proxy.Channel.SaveFile(file, System.IO.Path.Combine(selectedTag, docname + ".pdf"));
                        if (result != 1)
                            MessageBox.Show("Failed to save PDF to server", "Error saving file", MessageBoxButton.OK, MessageBoxImage.Error);

                        doctype = "pdf";
                        savemsg = 1;
                    }
                    else
                    {
                        using (Bitmap bitmap = (Bitmap)Bitmap.FromFile(dir.GetFiles()[0].FullName))
                        {
                            if (ImageFormat.Jpeg.Equals(bitmap.RawFormat))
                            {
                                // JPEG
                                if (proxy.Channel.SaveFile(File.ReadAllBytes(dir.GetFiles()[0].FullName), System.IO.Path.Combine(selectedTag, docname + ".jpg")) != 1)
                                    MessageBox.Show("Failed to save PDF to server", "Error saving file", MessageBoxButton.OK, MessageBoxImage.Error);

                                doctype = "jpg";
                            }
                            else if (ImageFormat.Png.Equals(bitmap.RawFormat))
                            {
                                // PNG
                                if (proxy.Channel.SaveFile(File.ReadAllBytes(dir.GetFiles()[0].FullName), System.IO.Path.Combine(selectedTag, docname + ".png")) != 1)
                                    MessageBox.Show("Failed to save PDF to server", "Error saving file", MessageBoxButton.OK, MessageBoxImage.Error);
                                doctype = "png";
                            }
                            else if (ImageFormat.Jpeg.Equals(bitmap.RawFormat))
                            {
                                // GIF
                                if (proxy.Channel.SaveFile(File.ReadAllBytes(dir.GetFiles()[0].FullName), System.IO.Path.Combine(selectedTag, docname + ".gif")) != 1)
                                    MessageBox.Show("Failed to save PDF to server", "Error saving file", MessageBoxButton.OK, MessageBoxImage.Error);
                                doctype = "gif";
                            }
                            else
                            {
                                // Save PDF
                                PdfSettings PdfSettings = new PdfSettings(PdfOrientation.Portrait, PdfSize.Letter);
                                PdfConverter converter = new PdfConverter(PdfSettings);

                                List<Bitmap> images = new List<Bitmap>();

                                // Create List of images
                                foreach (FileInfo img in dir.GetFiles())
                                {
                                    images.Add((Bitmap)Bitmap.FromFile(img.FullName));
                                }

                                converter.SaveFrom(images, System.IO.Path.Combine(tempfolder, selectedTag, docname + ".pdf"));

                                // transfer to server
                                byte[] file = File.ReadAllBytes(System.IO.Path.Combine(tempfolder, selectedTag, docname + ".pdf"));

                                if (proxy.Channel.SaveFile(file, System.IO.Path.Combine(selectedTag, docname + ".pdf")) != 1)
                                    MessageBox.Show("Failed to save PDF to server", "Error saving file", MessageBoxButton.OK, MessageBoxImage.Error);

                                doctype = "pdf";
                                savemsg = 1;
                            }

                            savemsg = 2;
                        }
                    }

                    if (invalidimg)
                    {
                        PiggyBank.ShowPopupInfo("Invalid image type", 3);
                    }
                    else
                    {
                        // Save document details to database
                        Document doc = new Document();
                        doc.DatePosted = DateTime.Now;
                        doc.DocDate = DateTime.Parse(txtDateOfDoc.Text);
                        doc.DocName = docname;
                        doc.DocType = doctype;
                        doc.Keywords = txtKeywords.Text;
                        doc.PhysicalLocation = txtPhysLocation.Text;
                        doc.Level = cmbLevels.Text;
                        doc.Location = selectedTag.Replace("\\", "/");

                        // Temporarily till I implement login functions - 9 = Hezekiah
                        doc.User = new Person() { Person_Id = 9 };

                        try
                        {
                            int result = new ServiceProxy().Channel.SaveDocument(doc);
                            ClearScannerStuff();
                            Clear();

                            if (savemsg == 1)
                                PiggyBank.ShowPopupInfo("Files successfully saved", 1);

                            if (savemsg == 2)
                                PiggyBank.ShowPopupInfo("File successfully saved", 1);

                            Clear();
                        }
                        catch (Exception ex)
                        {
                            LogError.LogTextError(ex);
                            PiggyBank.ShowPopupInfo("Error saving document", 2);
                        }
                    }
                }
                
            }
            catch (Exception ex)
            {
                LogError.LogTextError(ex);
                MessageBox.Show(ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

        private void btnHelp_MouseDown(object sender, MouseButtonEventArgs e)
        {

        }

        private void btnLogout_MouseDown(object sender, MouseButtonEventArgs e)
        {

        }

        private void btnSavePic_Click(object sender, RoutedEventArgs e)
        {
            //    try
            //    {
            //        string docname = txtDocName.Text;
            //        DirectoryInfo dir = new DirectoryInfo(tempfolder);

            //        if (dir.GetFiles().Length != 0)
            //        {
            //            // Save PDF
            //            PdfSettings PdfSettings = new PdfSettings(PdfOrientation.Portrait, PdfSize.Letter);
            //            PdfConverter converter = new PdfConverter(PdfSettings);

            //            converter.SaveFrom(images, "C:\\TempImages" + "\\" + docname + ".pdf");

            //            MessageBox.Show("Images successfully saved to C://TempImages", "Status", MessageBoxButton.OK, MessageBoxImage.Information);
            //            Clear();
            //        }
            //        else
            //        {
            //            MessageBox.Show("No image scanned yet", "No image found", MessageBoxButton.OK, MessageBoxImage.Information);
            //        }
            //    }
            //    catch (Exception ex)
            //    {
            //        LogError.LogTextError(ex);
            //        MessageBox.Show(ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Information);
            //    }
        }

        #endregion

        #region TreeView Control

        void TreeView_Loaded(object sender, RoutedEventArgs e)
        {
            if (System.ComponentModel.DesignerProperties.GetIsInDesignMode(this))
            {
                return;
            }

            /// Create main expanded node of TreeView
            treeView.Items.Add(TreeView_CreateComputerItem());
            /// Update open directories every 5 second
            DispatcherTimer timer = new DispatcherTimer(TimeSpan.FromSeconds(5),
                DispatcherPriority.Background, TreeView_Update, Dispatcher);
        }

        void TreeView_Update(object sender, EventArgs e)
        {
            Stopwatch s = new Stopwatch();
            s.Start();
            /// Update drives and folders in Computer
            /// create copy for detect what item was expanded
            TreeView oldTreeView = CloneUsingXaml(treeView) as TreeView;
            /// populate items from scratch
            treeView.Items.Clear();
            /// add computer expanded node with all drives
            treeView.Items.Add(TreeView_CreateComputerItem());
            TreeViewItem newComputerItem = treeView.Items[0] as TreeViewItem;
            TreeViewItem oldComputerItem = oldTreeView.Items[0] as TreeViewItem;
            /// Save old state of item
            newComputerItem.IsExpanded = oldComputerItem.IsExpanded;
            newComputerItem.IsSelected = oldComputerItem.IsSelected;
            /// check all drives for creating it's root folders
            foreach (TreeViewItem newDrive in (treeView.Items[0] as TreeViewItem).Items)
                if (newDrive.Items.Contains(null))
                    /// Find relative old item for newDrive
                    foreach (TreeViewItem oldDrive in oldComputerItem.Items)
                        if (oldDrive.Tag as string == newDrive.Tag as string)
                        {
                            newDrive.IsSelected = oldDrive.IsSelected;
                            if (oldDrive.IsExpanded)
                            {
                                newDrive.Items.Clear();
                                TreeView_AddDirectoryItems(oldDrive, newDrive);
                            }
                            break;
                        }
            s.Stop();
        }

        void TreeView_AddDirectoryItems(TreeViewItem oldItem, TreeViewItem newItem)
        {
            ServiceProxy proxy = new ServiceProxy();

            newItem.IsExpanded = oldItem.IsExpanded;
            newItem.IsSelected = oldItem.IsSelected;
            /// add folders in this drive
            string[] directories = proxy.Channel.GetDirectories(newItem.Tag as string).ToArray();
            /// for each folder create TreeViewItem
            foreach (string directory in directories)
            {
                TreeViewItem treeViewItem = new TreeViewItem();
                treeViewItem.Header = new DirectoryInfo(directory).Name;
                treeViewItem.Tag = directory;
                try
                {
                    if (proxy.Channel.GetDirectories(directory).Count > 0)
                        /// find respective old folder
                        foreach (TreeViewItem oldDir in oldItem.Items)
                            if (oldDir.Tag as string == directory)
                            {
                                if (oldDir.IsExpanded)
                                {
                                    TreeView_AddDirectoryItems(oldDir, treeViewItem);
                                }
                                else
                                {
                                    treeViewItem.Items.Add(null);
                                }
                                break;
                            }
                }
                catch { }
                treeViewItem.Expanded += TreeViewItem_Expanded;
                treeViewItem.PreviewMouseDown += TreeViewItem_Selected;
                newItem.Items.Add(treeViewItem);
            }
        }

        TreeViewItem TreeView_CreateComputerItem()
        {
            TreeViewItem computer = new TreeViewItem { Header = "Iris", IsExpanded = true };
            computer.Expanded += TreeViewItem_Expanded;
            computer.PreviewMouseDown += TreeViewItem_Selected;

            try
            {
                List<string> dir = new ServiceProxy().Channel.GetDirectories(@"C:\Repository\Iris");
                foreach (string folder in dir)
                {
                    TreeViewItem driveItem = new TreeViewItem();
                    driveItem.Header = System.IO.Path.GetFileName(folder);
                    driveItem.Tag = System.IO.Path.GetFullPath(folder);

                    List<string> dir2 = new ServiceProxy().Channel.GetDirectories(folder);
                    if (dir2.Count > 0)
                        driveItem.Items.Add(null);
                    driveItem.Expanded += TreeViewItem_Expanded;
                    driveItem.PreviewMouseDown += TreeViewItem_Selected;
                    computer.Items.Add(driveItem);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            return computer;
        }

        void TreeViewItem_Selected(object sender, RoutedEventArgs e)
        {
            TreeViewItem item = (TreeViewItem)sender;
            txtSelectedFolder.Text = item.Header.ToString();
            if (item.Tag != null)
                selectedTag = item.Tag.ToString().Substring(item.Tag.ToString().IndexOf("Iris\\"));
            btnNewFolder.IsEnabled = true;
            btnNewFolder.Opacity = 1;
        }

        void TreeViewItem_Expanded(object sender, RoutedEventArgs e)
        {
            TreeViewItem rootItem = (TreeViewItem)sender;

            if (rootItem.Items.Count == 1 && rootItem.Items[0] == null)
            {
                rootItem.Items.Clear();

                string[] dirs;
                try
                {
                    dirs = Directory.GetDirectories((string)rootItem.Tag);
                }
                catch
                {
                    return;
                }

                foreach (var dir in dirs)
                {
                    TreeViewItem subItem = new TreeViewItem();
                    subItem.Header = new DirectoryInfo(dir).Name;
                    subItem.Tag = dir;
                    try
                    {
                        if (Directory.GetDirectories(dir).Length > 0)
                            subItem.Items.Add(null);
                    }
                    catch { }
                    subItem.Expanded += TreeViewItem_Expanded;
                    subItem.PreviewMouseDown += TreeViewItem_Selected;
                    rootItem.Items.Add(subItem);
                }
            }
        }

        object CloneUsingXaml(object obj)
        {
            string xaml = XamlWriter.Save(obj);
            return XamlReader.Load(new XmlTextReader(new StringReader(xaml)));
        }

        #endregion

        #region Twain

        private Twain _twain;
        private ScanSettings _settings;

        private void OnSelectSourceButtonClick(object sender, RoutedEventArgs e)
        {
            try
            {
                if (_twain == null)
                {
                    _twain = new Twain(new WpfWindowMessageHook(PiggyBank.MainWindow));
                    _twain.TransferImage += delegate(Object s, TransferImageEventArgs args)
                    {
                        if (args.Image != null)
                        {
                            resultImage = args.Image;

                            // Memory handling. Instead of saving the image, create temp image and save reference
                            Directory.CreateDirectory(tempfolder);
                            bool stop = false;
                            int x = 1;

                            // loop till u find free filename
                            while (!stop)
                            {
                                string filename = System.IO.Path.Combine(tempfolder, "temp" + x + ".jpg");
                                if (File.Exists(filename))
                                    x++;
                                else
                                {
                                    foreach (ImageCodecInfo codec in ImageCodecInfo.GetImageDecoders())
                                    {
                                        if (codec.FormatID == args.Image.RawFormat.Guid)
                                        {
                                            EncoderParameters encoderParams = null;
                                            int quality = 60;
                                            EncoderParameter qualityParam = new EncoderParameter(
                                                System.Drawing.Imaging.Encoder.Quality, quality);
                                            encoderParams = new EncoderParameters(1);
                                            encoderParams.Param[0] = qualityParam;
                                            args.Image.Save(filename, codec, encoderParams);

                                            stop = true;
                                        }
                                    }
                                }
                            }
                        }
                    };

                    _twain.ScanningComplete += delegate
                    {
                        IsEnabled = true;
                    };

                    cmbLevels.SelectedIndex = 2;
                }

                _twain.SelectSource();
            }
            catch (Exception ex)
            {
                LogError.LogTextError(ex);
                MessageBox.Show(ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

        private void Scan()
        {
            IsEnabled = false;

            _settings = new ScanSettings
            {
                ShowTwainUI = (bool)btnShowGui.IsChecked,
                ShowProgressIndicatorUI = true
            };

            try
            {
                if (_twain == null)
                {
                    _twain = new Twain(new WpfWindowMessageHook(PiggyBank.MainWindow));
                    _twain.TransferImage += delegate(Object s, TransferImageEventArgs args)
                    {
                        if (args.Image != null)
                        {
                            resultImage = args.Image;

                            // Memory handling. Instead of saving the image, create temp image and save reference
                            Directory.CreateDirectory(tempfolder);
                            bool stop = false;
                            int x = 1;

                            // loop till u find free filename
                            while (!stop)
                            {
                                string filename = System.IO.Path.Combine(tempfolder, "temp" + x + ".jpg");
                                if (File.Exists(filename))
                                    x++;
                                else
                                {
                                    EncoderParameters encoderParams = null;
                                    int quality = 60;
                                    EncoderParameter qualityParam = new EncoderParameter(
                                        System.Drawing.Imaging.Encoder.Quality, quality);
                                    encoderParams = new EncoderParameters(1);
                                    encoderParams.Param[0] = qualityParam;
                                    args.Image.Save(filename, GetEncoderInfo("image/jpeg"), encoderParams);

                                    stop = true;
                                }
                            }
                        }
                    };

                    _twain.ScanningComplete += delegate
                    {
                        IsEnabled = true;
                    };

                    cmbLevels.SelectedIndex = 2;
                }

                _twain.StartScanning(_settings);
                btnViewScan.IsEnabled = true;
                btnViewScan.Opacity = 1;
                btnClear.IsEnabled = true;
                btnClear.Opacity = 1;
                btnChangeScanner.IsEnabled = true;
                btnChangeScanner.Opacity = 1;
            }
            catch (TwainException ex)
            {
                IsEnabled = true;
                LogError.LogTextError(ex);
                MessageBox.Show(ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Information);
            }

            IsEnabled = true;
        }

        private ImageCodecInfo GetEncoderInfo(String mimeType)
        {
            int j;
            ImageCodecInfo[] encoders;
            encoders = ImageCodecInfo.GetImageEncoders();
            for(j = 0; j < encoders.Length; ++j)
            {
                if(encoders[j].MimeType == mimeType)
                    return encoders[j];
            }
            return null;
        }

        #endregion
    }
}
