﻿using System;
using System.Configuration;
using System.ServiceModel;
using IrisContract;

namespace Irisproxy
{
    public class ServiceProxy
    {
        ChannelFactory<DataOperations> cf = null;

        public DataOperations Channel { get; set; }

        public string ServiceBaseAddress { get; set; }

        public ServiceProxy()
        {
            //Get from config
            ServiceBaseAddress = ConfigurationManager.AppSettings["ServiceUrl"].ToString() + "/myService.svc";

            var binding = new BasicHttpBinding();
            binding.MaxReceivedMessageSize = int.MaxValue;
            binding.TransferMode = TransferMode.Streamed;
            //binding.Security.Mode = SecurityMode.None;
            //binding.Security.Transport.ClientCredentialType = HttpClientCredentialType.None;
            binding.SendTimeout = TimeSpan.FromMinutes(15);
            binding.CloseTimeout = TimeSpan.FromMinutes(15);
            binding.OpenTimeout = TimeSpan.FromMinutes(15);
            binding.ReceiveTimeout = TimeSpan.FromMinutes(15);
            //binding.MaxReceivedMessageSize = int.MaxValue;


            //binding.ReaderQuotas.MaxArrayLength = 2147483647;

            //////
            //////    REMOVE THIS FOR LIVE. THIS IS JUST FOR THE TEMP DEV SSL CERTIFICATE
            //////
            //ServicePointManager.ServerCertificateValidationCallback = new RemoteCertificateValidationCallback(
            //    delegate
            //    {
            //        return true;
            //    });

            cf = new ChannelFactory<DataOperations>(binding, ServiceBaseAddress);

            //cf.Endpoint.Behaviors.Add(new wsHttpBehavior());
            Channel = cf.CreateChannel();
        }
    }
}
